import collections

#print obj
def array_to_dictionary(array):
    dict={}
    for element in array:
        for key in element:
            dict[key]=element[key]
    
    return dict
    
def getrepeatedStatus(listOfStatus):
    if 'Issue' in listOfStatus:
        status='Issue'
    elif 'Warning' in listOfStatus:
        status='Warning'
    else:
        status='Ok'
    return status
    
def prepRMData(obj,ParentChildDict):

	print '***************'
	print ParentChildDict
	print '***************'

	record = obj.copy()
	ReportingData={}
	QualityAggregate=[]
	ShelfLifeAggregate=[]
	QuantityAggregate=[]
	QualityBatch=[]
	QualityRM=[]
	QuantityRM=[]
	ShelfLifeRM=[]
	quality_status={}
	shelflife_status={}
	quantity_status={}
	del record['StatusIndicator']
	#del record['Status_Count']
	if record.has_key('IssueCount'):
		del record['IssueCount']

	if record.has_key('OKCount'):
		del record['OKCount']

	if record.has_key('WarningCount'):
		del record['WarningCount']

	if record.has_key('NACount'):
		del record['NACount']
	# del record['_id']

	#print record
	#print '\n =================='

	##varibale initialization
	qlty_issue_count = qlty_warning_count = qlty_ok_count = qlty_na_count = qntity_issue_count = qntity_warning_count = qntity_ok_count = qntity_na_count = shlife_issue_count = shlife_warning_count = shlife_ok_count = shlife_na_count = a_count = b_count = c_count = d_count = s_na_count = lesser_count = greater_count = equal_count = q_na_count =0
	qlty_status_issue_count = qlty_status_warning_count = qlty_status_ok_count = qlty_status_na_count = 0
	Quality_Status_Indicator = []
	Quantity_Status_Indicator = []
	ShelfLife_Status_Indicator = []

	for RM in record:
		if isinstance(record[RM], collections.MutableMapping):
			Quality_Status_Indicator.append(record[RM]['QualityAdherence']['StatusIndicator'])

			if record[RM]['QualityAdherence']['StatusIndicator']=='Issue':
				qlty_issue_count=qlty_issue_count+1
			elif record[RM]['QualityAdherence']['StatusIndicator']=='Warning':
				qlty_warning_count=qlty_warning_count+1
			elif record[RM]['QualityAdherence']['StatusIndicator']=='Ok':
				qlty_ok_count=qlty_ok_count+1
			elif record[RM]['QualityAdherence']['StatusIndicator']=='NA':
				qlty_na_count=qlty_na_count+1
			
			for param in record[RM]['QualityAdherence']:
				data=record[RM]['QualityAdherence'][param]
				if isinstance(data,dict):
					row={}
					row['RawMaterial']=RM
					row['Parameter']=param
					
					for parent,childlist in ParentChildDict.iteritems():
						if param in childlist:
							row['ParentName'] = parent
							break
						else:
							row['ParentName'] = 'NA'
						
						
					
					row['UpperLimit']=data['UpperLimit']
					row['LowerLimit']=data['LowerLimit']
					row['Actual_Average_Readings']=data['POLevelAverage']
					row['Percentage_Average_Deviation']=data['PO_Product_Deviation_Percentage']
					row['ConfigAvgReadings']=data['ProductLevelAverage']
					row['StatusIndicator']=data['StatusIndicator']
					row['BatchCount']=record[RM]['QuantityAdherence']['BatchCount']
					row['RMDisplay']=record[RM]['Material_Display']
					if data['StatusIndicator'].lower() == 'issue':
						qlty_status_issue_count=qlty_status_issue_count+1
					elif data['StatusIndicator'].lower() == 'warning':
						qlty_status_warning_count=qlty_status_warning_count+1
					elif data['StatusIndicator'].lower() == 'ok':
						qlty_status_ok_count=qlty_status_ok_count+1
					elif data['StatusIndicator'].lower() == 'na':
						qlty_status_na_count=qlty_status_na_count+1
					
					QualityRM.append(row)

			##ShelfLife Data

			if record[RM]['ShelfLifeAdherence']['RemainingShelfLife_Percentage']=='NA':
				s_na_count=s_na_count+1
			elif float(record[RM]['ShelfLifeAdherence']['RemainingShelfLife_Percentage'])>=75:
				a_count=a_count+1
			elif float(record[RM]['ShelfLifeAdherence']['RemainingShelfLife_Percentage'])>=50:
				b_count=b_count+1
			elif float(record[RM]['ShelfLifeAdherence']['RemainingShelfLife_Percentage'])>=25:
				c_count=c_count+1
			else:
				d_count=d_count+1
				
			ShelfLife_Status_Indicator.append(record[RM]['ShelfLifeAdherence']['StatusIndicator'])
				
			if record[RM]['ShelfLifeAdherence']['StatusIndicator']=='Issue':
				shlife_issue_count=shlife_issue_count+1
			elif record[RM]['ShelfLifeAdherence']['StatusIndicator']=='Warning':
				shlife_warning_count=shlife_warning_count+1
			elif record[RM]['ShelfLifeAdherence']['StatusIndicator']=='Ok':
				shlife_ok_count=shlife_ok_count+1
			else:
				shlife_na_count=shlife_na_count+1
				
			##ShelfLifeRM Data

			row={}
			row['RawMaterial']=RM
			for param in record[RM]['ShelfLifeAdherence']:
				row[param]=record[RM]['ShelfLifeAdherence'][param]
			#del row['Status_Count']
			#del row['IssueCount']
			#del row['OKCount']
			#del row['WarningCount']
			#del row['NACount']
			row['RMDisplay']=record[RM]['Material_Display']
			row['BatchCount']=record[RM]['QuantityAdherence']['BatchCount']
			ShelfLifeRM.append(row)
				
			##Quantity Data

			if record[RM]['QuantityAdherence']['RequiredQuantity']=='NA' or record[RM]['QuantityAdherence']['ConsumedQuantity']=='NA':
				q_na_count=q_na_count+1
			elif record[RM]['QuantityAdherence']['RequiredQuantity']>record[RM]['QuantityAdherence']['ConsumedQuantity']:
				lesser_count=lesser_count+1
			elif record[RM]['QuantityAdherence']['RequiredQuantity']==record[RM]['QuantityAdherence']['ConsumedQuantity']:
				equal_count=equal_count+1
			elif record[RM]['QuantityAdherence']['RequiredQuantity']<record[RM]['QuantityAdherence']['ConsumedQuantity']:
				greater_count=greater_count+1
				
			Quantity_Status_Indicator.append(record[RM]['QuantityAdherence']['StatusIndicator'])

			if record[RM]['QuantityAdherence']['StatusIndicator']=='Issue':
				qntity_issue_count=qntity_issue_count+1
			elif record[RM]['QuantityAdherence']['StatusIndicator']=='Warning':
				qntity_warning_count=qntity_warning_count+1
			elif record[RM]['QuantityAdherence']['StatusIndicator']=='Ok':
				qntity_ok_count=qntity_ok_count+1
			else:
				qntity_na_count=qntity_na_count+1

			##Quantity RM
			row={}
			row['RawMaterial']=RM
			for param in record[RM]['QuantityAdherence']:
				row[param]=record[RM]['QuantityAdherence'][param]
				
			row['RMDisplay']=record[RM]['Material_Display']
			QuantityRM.append(row)

			##Quality RM Batch Level

			del record[RM]['BatchInfo']['StatusIndicator']
			#del record[RM]['BatchInfo']['Status_Count']
			#del record[RM]['BatchInfo']['IssueCount']
			#del record[RM]['BatchInfo']['OKCount']
			#del record[RM]['BatchInfo']['WarningCount']
			#del record[RM]['BatchInfo']['NACount']
			for batch in record[RM]['BatchInfo']:
				if isinstance(record[RM]['BatchInfo'][batch],dict):
					data=record[RM]['BatchInfo'][batch]['SAPQCCharacterList']
					for param in data:
						if isinstance(data[param],dict):
							row={}
							row['RawMaterial']=RM
							row['Batch']=batch
							row['Parameter']=param
							row['UpperLimit']=data[param]['UpperLimit']
							row['LowerLimit']=data[param]['LowerLimit']
							row['MeanValue']=data[param]['MeanValue']
							QualityBatch.append(row)
						
	##Quality Data

	QualityAggregate.append({'name':'Issue','y':qlty_issue_count})
	QualityAggregate.append({'name':'Warning','y':qlty_warning_count})
	QualityAggregate.append({'name':'Ok','y':qlty_ok_count})
	QualityAggregate.append({'name':'NA','y':qlty_na_count})

	qlty_status=getrepeatedStatus(Quality_Status_Indicator)
	QualityAggregate.append({'status':qlty_status})

	ReportingData['QualityAggregate']=QualityAggregate
	ReportingData['QualityRM']=QualityRM

	##ShelfLife Data

	ShelfLifeAggregate.append({'name':'Q1','y':a_count})
	ShelfLifeAggregate.append({'name':'Q2','y':b_count})
	ShelfLifeAggregate.append({'name':'Q3','y':c_count})
	ShelfLifeAggregate.append({'name':'Q4','y':d_count})
	ShelfLifeAggregate.append({'name':'NA','y':s_na_count})

	ShelfLife_status=getrepeatedStatus(ShelfLife_Status_Indicator)
	ShelfLifeAggregate.append({'Status':ShelfLife_status})

	ReportingData['ShelfLifeAggregate']=ShelfLifeAggregate
	ReportingData['ShelfLifeRM']=ShelfLifeRM

	##Quantity Data

	QuantityAggregate.append({'name':'Equal','y':equal_count})
	QuantityAggregate.append({'name':'Greater','y':greater_count})
	QuantityAggregate.append({'name':'Lesser','y':lesser_count})
	QuantityAggregate.append({'name':'NA','y':q_na_count})

	Quantity_status=getrepeatedStatus(Quantity_Status_Indicator)
	QuantityAggregate.append({'status':Quantity_status})

	ReportingData['QuantityAggregate']=QuantityAggregate
	ReportingData['QuantityRM']=QuantityRM

	##Quality RM Batch Level

	ReportingData['QualityBatch']=QualityBatch
		
	##quality_status

	quality_status['issue_count'] = qlty_status_issue_count
	quality_status['warning_count'] = qlty_status_warning_count
	quality_status['ok_count'] = qlty_status_ok_count
	quality_status['na_count'] = qlty_status_na_count
	ReportingData['quality_status']=quality_status

	##shelflife_status

	shelflife_status['issue_count']=shlife_issue_count 
	shelflife_status['warning_count']=shlife_warning_count
	shelflife_status['ok_count']=shlife_ok_count
	shelflife_status['na_count']=shlife_na_count
	ReportingData['shelflife_status']=shelflife_status

	##quantity_status

	quantity_status['issue_count']=qntity_issue_count 
	quantity_status['warning_count']=qntity_warning_count
	quantity_status['ok_count']=qntity_ok_count
	quantity_status['na_count']=qntity_na_count
	ReportingData['quantity_status']=quantity_status

	return ReportingData