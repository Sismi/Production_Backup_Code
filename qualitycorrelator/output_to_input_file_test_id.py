# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 17:48:30 2016

@author: IN0055
"""

from pymongo import MongoClient
import re
import pandas as pd
import json,ast
import collections
from unidecode import unidecode
from Crypto.Cipher import AES
import math

def dis(train,test):
    squared=pow((train-test),2)
    squaredsum=squared.sum(axis=1)
    squaredsumroot=math.sqrt(squaredsum)
    return(squaredsumroot)

#This function read the environment table
#It takes no argument and return the environment  variables
def getpremodellingparam():    
    client = MongoClient('193.96.100.95',27017) #Connect to server
    client.admin.authenticate('henkelUser', 'QpQc#123', mechanism='SCRAM-SHA-1')
    db=client.correlatorHenkelProduction #set the database
    #Reading the table
    cursor=db.config_correlator_model_environment.find({})
    for row in cursor:
        premodelling_param=row
    client.close() #close the connection
    return premodelling_param

#Function to get the output parameters to show on UI when we select the product which output parameters will be shown
#This function takes parameter
#product: product for which is selected on the screen
def get_op_parameters(product):
    #This is the function that is called by the UI
    premodelling_param=getpremodellingparam()
    #call getpremodellingparam to get all environment variables
    connection_str = premodelling_param['MongoConnectionUrl']
    #set the connection string
    database = premodelling_param['Database'] 
    #set the database
    username = premodelling_param['UserName'] 
    #set the username
    password = premodelling_param['Password'] 
    #set the password
    benchmarkcollection = premodelling_param['BenchmarkCollection'] 
    benchmarkcollection = 'config_correlator_parameterMapping'
    config_collection = 'model_config'
    #Data collection will have all the history data 
    output_params={}
    client = MongoClient(connection_str,connectTimeoutMS=10000000) #Connect to server
    client.admin.authenticate(username, password, mechanism='SCRAM-SHA-1')#Admin authentication
    db=client[database] #set the database
    cursor = db[config_collection].find({'MaterialNumber':product,'ActiveFlag':1},{'_id':0,'Premodelling_Output_Parameters':1})
    for row in cursor:
        for param in row['Premodelling_Output_Parameters']:
            if(row['Premodelling_Output_Parameters'][param]['PreModelUsageFlag'] =='Y'):
                #print param
                output_params[param]=row['Premodelling_Output_Parameters'][param]['IDKey']
            
    #fetch details from other table
    #numericl parameters stores only scaled variables
    numerical_parameters_dict=dict() #Stores the final json            
    #Refer the benchmark table for all the client = MongoClient(connection_str,connectTimeoutMS=10000000) #Connect to server
    client.admin.authenticate(username, password, mechanism='SCRAM-SHA-1')#Admin authentication
    db=client[database] #set the database
    cursor = db[benchmarkcollection].find({'MaterialNumber':product,'ActiveFlag':1,'Config_Level':'OutputParameterMapping'},{'_id':0,'Groups':1})
    for row in cursor:
        for grp,grpvals in row.iteritems():
            for grpinfo, grpinfoval in grpvals.iteritems():
                if 'Output_Parameters|'+grpinfoval['MasterChild']+'|GroupActualValue' in output_params:
                    #create the list
                    #get upper limit lower limit and the name
                    temp={} #temporary json for upper limit and lower limit and benchmark
                    temp['Name']= output_params['Output_Parameters|'+grpinfoval['MasterChild']+'|GroupActualValue']
                    temp['LowerLimit']=grpinfoval['MasterData']['LowerLimit'] #assign lower limit
                    temp['UpperLimit']=grpinfoval['MasterData']['UpperLimit'] #assign upper limit
                    temp['Benchmark']=grpinfoval['MasterData']['Benchmark'] #assign benchmark
                    #The parameter name is encoded by default so we do unidecode
                    numerical_parameters_dict[grpinfoval['MasterChild']]=temp    
    #return the dictionary withh all the three values
    return numerical_parameters_dict


def get_op_parameters_impute(product):
    #This is the function that is called by the UI
    premodelling_param=getpremodellingparam()
    #call getpremodellingparam to get all environment variables
    connection_str = premodelling_param['MongoConnectionUrl']
    #set the connection string
    database = premodelling_param['Database'] 
    #set the database
    username = premodelling_param['UserName'] 
    #set the username
    password = premodelling_param['Password'] 
    #set the password
    benchmarkcollection = premodelling_param['BenchmarkCollection'] 
    benchmarkcollection = 'config_correlator_parameterMapping'
    config_collection = 'model_config'
    #Data collection will have all the history data 
    output_params={}
    client = MongoClient(connection_str,connectTimeoutMS=10000000) #Connect to server
    client.admin.authenticate(username, password, mechanism='SCRAM-SHA-1')#Admin authentication
    db=client[database] #set the database
    cursor = db[config_collection].find({'MaterialNumber':product,'ActiveFlag':1},{'_id':0,'Premodelling_Output_Parameters':1})
    for row in cursor:
        for param in row['Premodelling_Output_Parameters']:
            if(row['Premodelling_Output_Parameters'][param]['PreModelUsageFlag'] =='Y'):
                #print param
                output_params[param]=row['Premodelling_Output_Parameters'][param]['IDKey']
            
    #fetch details from other table
    #numericl parameters stores only scaled variables
    numerical_parameters_dict=dict() #Stores the final json            
    #Refer the benchmark table for all the client = MongoClient(connection_str,connectTimeoutMS=10000000) #Connect to server
    client.admin.authenticate(username, password, mechanism='SCRAM-SHA-1')#Admin authentication
    db=client[database] #set the database
    cursor = db[benchmarkcollection].find({'MaterialNumber':product,'ActiveFlag':1,'Config_Level':'OutputParameterMapping'},{'_id':0,'Groups':1})
    for row in cursor:
        for grp,grpvals in row.iteritems():
            for grpinfo, grpinfoval in grpvals.iteritems():
                if 'Output_Parameters|'+grpinfoval['MasterChild']+'|GroupActualValue' in output_params:
                    #create the list
                    #get upper limit lower limit and the name
                    temp={} #temporary json for upper limit and lower limit and benchmark
                    temp['Name']= grpinfoval['MasterChild']
                    temp['LowerLimit']=grpinfoval['MasterData']['LowerLimit'] #assign lower limit
                    temp['UpperLimit']=grpinfoval['MasterData']['UpperLimit'] #assign upper limit
                    temp['Benchmark']=grpinfoval['MasterData']['Benchmark'] #assign benchmark
                    #The parameter name is encoded by default so we do unidecode
                    numerical_parameters_dict[output_params['Output_Parameters|'+grpinfoval['MasterChild']+'|GroupActualValue']]=temp    
    #return the dictionary withh all the three values
    return numerical_parameters_dict






#get_op_parameters("2","1258914")

#This function read all the output parameter values and returns the data frame
#It takes inputs such as
#product: product for which is selected on the screen
#connection_str:connaction string (database connection)
#databse: database name
#username : username for database
#password :password for database
def getData(product,connection_str,database,username,password,datacollection,configcollection):
    client = MongoClient(connection_str,connectTimeoutMS=10000000) #Connect to server
    client.admin.authenticate(username, password, mechanism='SCRAM-SHA-1')
    db=client[database] #set the database
    #Temp dictionary to store evry rowWQ
    temp_dict=dict()  
    #initialize Final dataframe with all data
    data = pd.DataFrame()
    i=1
    cursor=db[datacollection].find({'MaterialNumber':product},{"_id":0})
    for row in cursor:
        #Select Output parameters actual value from model data
        for qcparams in row['Model_Data']:        
            if re.search('Output_Parameters',qcparams):
                temp_dict[unidecode(qcparams)]=row['Model_Data'][qcparams]
                #temp_dict['SiteID']=row['SiteID']
        temp_dict=ast.literal_eval(json.dumps(temp_dict))
        #print temp_dict
        #Append every row to data frame
        data=data.append(pd.DataFrame(temp_dict, index=[row['BatchNumber'],]))
        print i
        i=i+1
                    
    #call the function to get benchmark value for imputing NA
    #impute based on site ID
    #get all the unique sites
    #unique_site=list(data['SiteID'].unique())
    #final_data=pd.DataFrame()
    #for site in unique_site:
    parameters=get_op_parameters_impute(product)
    
    #Get the names to filter the data
    parameter_names=list()
    for p in parameters:
        parameter_names.append(p)
    #Data has only parameters that are shown on UI
    data=data[[ s  for s in parameter_names]]
    
    #Get benchmark from 
    #benchmark_data=get_benchmark(product)
    #empty dictionary to store benchmark
    benchmark_dict=dict()
    #Iterate throu the parameters and get benchmark
    for k,v in parameters.iteritems():
        #benchmark_dict['Output_Parameters|'+ k + '|ActualValue']=v['Benchmark']
        benchmark_dict[ k ]=v['Benchmark']
        benchmark_dict=ast.literal_eval(json.dumps(benchmark_dict))
    
    #Convert it to dataframe
    benchmark_data=pd.DataFrame()
    benchmark_data=benchmark_data.append(pd.DataFrame(benchmark_dict, index=[1,]))
    
    #fill the NA with benchmark
    data=data.fillna(benchmark_data.mean())   
    
        #rowbind data for every site
        #final_data=final_data.append(data_temp, ignore_index=True)
     
     #Final data has NAN if the parameter was present in only one plant
     #use the other plants value to impute the NA's
    return(data)


#This function takes in the batch number and provides the PO details to display on UI
#This function is called by "mergepo"
def get_po_details(batchnumber,connection_str,database,username,password,app,encoded_username_url):    
    client = MongoClient(connection_str,connectTimeoutMS=10000000) #Connect to server
    client.admin.authenticate(username, password, mechanism='SCRAM-SHA-1')
    db=client[database] #set the database    
    #Get the pulse reporting collection
    premodelling_param=getpremodellingparam()
    detailcollection = premodelling_param['PODetailCollection']
    #create the ordered dictionary to store all the details
    outtoinput_detail_dict = collections.OrderedDict()
    #Ordered Dicionary for output parameters
    Output_parameters=collections.OrderedDict()
    #Gebral infoo dictionary for storing general information on UI
    #This is ordered as we have a fixed orderto show on screen
    GeneralInfo=collections.OrderedDict()
    data = db[detailcollection].find({'BatchNumber':batchnumber}) 
    #Loop for the and get the necessary info to show on UI and create a dictionary
    for row in data:
        GeneralInfo['QualityScore']=float(row['AvgQualityScore'])
        GeneralInfo['QualitySegment']=row['QualitySegment']
        #GeneralInfo['ProcessOrder']=row['ProcessOrder']
        GeneralInfo['ProcessOrder']='<a class="linking_mes_correlator" title="MES Link" href="http://ap-hipas.henkelgroup.net/ipas/ipas_Preweigh/Manufacture/ViewManufacturePO.aspx?id='+row['SiteID']+'&processorder='+row['ProcessOrder']+'&archive=true&3app='+app+'&username='+encoded_username_url+'" target="blank" style="text-decoration:none;color:#222;padding:6px 5px;">ProcessOrder:<span class="pull-right" style="padding-right:20px;">'+row['ProcessOrder']+'</span><i class="fa fa-external-link" aria-hidden="true" style="position:absolute;left:23em;top:0.7em;"></i></a>'
        GeneralInfo['ProductionStartDateTime']=row['ProductionStartDateTime'].strftime('%d-%m-%Y')		
        GeneralInfo['MaterialNumber']=row['MaterialNumber']
        GeneralInfo['Material_Description']=row['Material_Display']        
        GeneralInfo['SiteID']=row['SiteID']
        GeneralInfo['SiteName']=row['SiteName']
        GeneralInfo['SBU']=row['SBU']
        GeneralInfo['Technology']=row['Technology']
        for opparam in row['OutputScore']:
        #print row['OutputScore'][opparam]['ActualValue']
            Output_parameters[opparam]=row['OutputScore'][opparam]['ActualValue']
            outtoinput_detail_dict["GeneralInfo"]=GeneralInfo
            outtoinput_detail_dict["Output_parameters"]=Output_parameters
        #print outtoinput_detail_dict
    return outtoinput_detail_dict

#This function takes list of batch numbers ans input and calls the get_po_details function for each PO and merges to one dictionary to display on UI
#This function is  called from "outputtoinput"
def merge_po(list_batchnumbers,connection_str,database,username,password,app,encoded_username_url):
    #Empty list to store all the data to display on UI
    complete_list=[]
    #call the get_po_details for every batch
    for batchnumber in list_batchnumbers:
        po_det=get_po_details(batchnumber,connection_str,database,username,password,app,encoded_username_url)
        #append to list
        complete_list.append(po_det)
    #return to "outouttoinput"
    return complete_list
	
def AESencrypt(password, plaintext, base64=True):		
    SALT_LENGTH = 32		
    DERIVATION_ROUNDS=1337		
    BLOCK_SIZE = 16		
    KEY_SIZE = 32		
    MODE = AES.MODE_CBC		
     		
    paddingLength = 16 - (len(plaintext) % 16)		
    paddedPlaintext = plaintext+chr(paddingLength)*paddingLength		
    derivedKey = password		
    derivedKey = hashlib.sha256(derivedKey).digest()		
    derivedKey = derivedKey[:KEY_SIZE]		
    iv = derivedKey[:BLOCK_SIZE]		
    cipherSpec = AES.new(derivedKey, MODE, iv)		
    ciphertext = cipherSpec.encrypt(paddedPlaintext)		
    if base64:		
        import base64		
        return base64.b64encode(ciphertext)		
    else:		
        return ciphertext.encode("hex")






#This function returns the 3 nearest batches for the output parameter provided
#This function is called from UI on the press of correlate button
#It takes a input json which has product and output parameter
#The structure of the json is {"MaterialNumber":"1258776","opparam":{"viscosity（HM,@140C)":2500,"softening point":80,"viscosity（HM）":2500}}
def outputtoinput(testjson):
    #testjson = {"MaterialNumber":"1149569","opparam":{"Output_Parameters|139":92,"Output_Parameters|146":5400,"Output_Parameters|43":2400,"Output_Parameters|148":1300}}
    #This is the function that is called by the UI
    premodelling_param=getpremodellingparam()
    #call getpremodellingparam to get all environment variables
    connection_str = premodelling_param['MongoConnectionUrl']
    #set the connection string
    database = premodelling_param['Database'] 
    #set the database
    username = premodelling_param['UserName'] 
    #set the username
    password = premodelling_param['Password'] 
    #set the password
    datacollection = premodelling_param['ModelDataSourceCollection'] 
    #Data collection will have all the history data 
    
    #This is a config collection to get benchmark upperlimit and lowerlimit to show on UI and to impute NAs
    configcollection = premodelling_param['ModelConfigCollection'] 
    #testjson is a json passed from UI we extract the values and run the algorithm
    product=testjson['MaterialNumber']
    #site=testjson['SiteID']
    #Get the product from the json
    opparam=testjson['opparam']
    opparam=collections.OrderedDict(opparam)
	
    #Get the output parameters from the json
    app=testjson['app']
    encoded_username_url=testjson['encoded_username_url']
    opparam_new=dict() #Dictionary to store the output parameters
    for k,v in opparam.iteritems():
        #Loop through the output param
        opparam_new[k]=v
    test_data = pd.DataFrame() #Dataframe for storing the output param for modelling
    #create the data frame
    test_data = test_data.append(pd.DataFrame(opparam, index=[1,]))
    #change the names of column append with strings to match reporting data
    ####test_data.columns=[('Output_Parameters|'+ unidecode(s.decode('utf-8')) + '|ActualValue') for s in list((test_data.columns.values))]
    #call the get data function to get history data for the product
    #Call getData at site level 
    data=getData(product,connection_str,database,username,password,datacollection,configcollection)
    # Normalize all of the numeric columns
    min_val=data.min()
    max_val=data.max()
    data_normalized = (data - min_val) / (max_val-min_val)
    # Fill in NA values in nba_normalized
    data_normalized.fillna(0, inplace=True)
    # Find the normalized vector for lebron james.
    test_normalized = (test_data - min_val) / (max_val-min_val)

    
    #If the new data is less than the history data then we make it 0 
    #and if its greater than history data then we make it 1
    test_normalized[test_normalized<0] = 0
    test_normalized[test_normalized>1] = 1
    
    
    # Find the distance between lebron james and everyone else.
    euclidean_distances = data_normalized.apply(lambda row: dis(row, test_normalized), axis=1)

    # Create a new dataframe with distances.
    distance_frame = pd.DataFrame(data={"dist": euclidean_distances, "idx": euclidean_distances.index})
    distance_frame.sort("dist", inplace=True)

    # Find the most similar player to lebron (the lowest distance to lebron is lebron, the second smallest is the most similar non-lebron player)
    nearest = distance_frame.iloc[0:3]
    most_similar_batch = list(nearest["idx"])
    output=merge_po(most_similar_batch,connection_str,database,username,password,app,encoded_username_url)
    return(output)




