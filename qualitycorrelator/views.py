from django.shortcuts import render
from django.db.backends.util import CursorWrapper
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from django.db import connection as default_connection
#from mongoengine import *
from datetime import datetime
from datetime import timedelta
import pymongo
from pymongo import MongoClient
import logging
import json
from bson import json_util
import math
from decimal import *
import ast
import sys
import csv
from utils import array_to_dictionary, prepRMData
#import csv
from django.http import StreamingHttpResponse
from operator import itemgetter
import requests
from unidecode import unidecode
from collections import OrderedDict
import dateutil.parser
from dateutil.parser import parse
import output_to_input_file_test_id
import AlterStatus
from AlterStatus import *
import hashlib, os, urllib
from Crypto.Cipher import AES
import re
import copy
from operator import itemgetter
#from subprocess import call


sys.path.insert(0,'/home/sarajesh/Correlator_Data_Load_Code_Optimised/Prediction')

import CalculateScore
import PrepPredictData

def connection_staggedpos():
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_StagedPO_QulaityScoreSeg_pandg	
	return table_cursor
	
def connection_cor_rca():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.report_correlator_rca
	return table_cursor

def connection_data_issue_rca():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.data_issue_correlator_rca
	return table_cursor

def connection_data_issue_staged_po():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.data_issue_correlator_staged_po
	return table_cursor
	
def connection_cor_staged_po():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.report_correlator_staged_po
	return table_cursor
	
def connection_rca_feedback():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.rca_feedback
	return table_cursor

def mongoConnection():
	client = MongoClient('193.96.100.95',27017)
	client.admin.authenticate('henkelUser', 'QpQc#123', mechanism='SCRAM-SHA-1')
	db=client.correlator_ProdDump
	return db

def mongoConnection_to_ver2():
	client = MongoClient('193.96.100.95',27017,connect=False)
	client.admin.authenticate('henkelUser', 'QpQc#123', mechanism='SCRAM-SHA-1')
	db=client.correlatorHenkelProduction
	return db
	
def connection_status_rules():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.config_correlator_statusIndicator
	#table_cursor=conn.correlator_ProdDump.config_correlator_defaults
	return table_cursor	

		
def connection_defaults():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.config_correlator_defaults
	return table_cursor		


def connection_parent_child_data():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlatorHenkelProducton.config_correlator_parameterMapping
	return table_cursor	


def connection_load_scheduler():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlatorHenkelProducton.config_load_scheduler
	return table_cursor	


def connection_audit_log():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlatorHenkelProducton.audit_log
	return table_cursor		

	

def connection_config_correlator():
	url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump_Ver2.config_correlator_rca
	return table_cursor	
	
	
@csrf_exempt
def index(request):			
	template = loader.get_template('qualitycorrelator/theme.html')
	context = RequestContext(request, {'appuser': request.session['user_alias'] })
	return HttpResponse(template.render(context), content_type="text/html")

def filter_dict(param_name,param_display,pipeline,param_dict,param_list):
	db = mongoConnection_to_ver2()
	filters_dict={}	
	dict={}
	list=[]
	result_cursor=db.report_correlator_rca.aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	if param_name=='Resource_Display':
		param_name_temp='Resources'
	else:
		param_name_temp=param_name
	for r in result_cursor:

		list.append(r[param_name].encode('utf-8'))
		dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list

	return filters_dict
	
def input_filter_dict(param_name,param_display,pipeline,param_dict,param_list,Analysis_Name):
	db = mongoConnection_to_ver2()
	filters_dict={}	
	dict={}
	list=[]
	if Analysis_Name == 'Finished Goods':
		'''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=db.report_correlator_rca
	elif Analysis_Name == 'Stagged PO':
		'''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=db.report_correlator_staged_po
	else:
		print 'default'
		
	result_cursor=table_cursor.aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	for r in result_cursor:
		list.append(r[param_name].encode('utf-8'))
		dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list
	return filters_dict

def rejected_filter_dict(param_name,param_display,pipeline,param_dict,param_list,Analysis_Name):
	db = mongoConnection_to_ver2()
	filters_dict={}	
	dict={}
	list=[]
	if Analysis_Name == 'Finished Goods':
		'''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=db.rejected_correlator_fng_processorders
	elif Analysis_Name == 'Stagged PO':
		'''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=db.rejected_correlator_staged_po
	else:
		print 'default'
		
	result_cursor=table_cursor.aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	for r in result_cursor:
		list.append(r[param_name].encode('utf-8'))
		dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list
	return filters_dict
	
def data_issue_filter_dict(param_name,param_display,pipeline,param_dict,param_list,Analysis_Name):
	db = mongoConnection_to_ver2()
	filters_dict={}	
	dict={}
	list=[]
	if Analysis_Name == 'Finished Goods':
		'''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=db.data_issue_correlator_rca
	elif Analysis_Name == 'Stagged PO':
		'''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/admin?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=db.data_issue_correlator_staged_po
	else:
		print 'default'
		
	result_cursor=table_cursor.aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	for r in result_cursor:
		list.append(r[param_name].encode('utf-8'))
		dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list
	return filters_dict
	
def config_filter_dict(param_name,param_display,pipeline,param_dict,param_list):
	db = mongoConnection_to_ver2()
	filters_dict={}	
	dict={}
	list=[]
	result_cursor=db.rule_correlator_aggregate_count.aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	if param_name=='Resource_Display':
		param_name_temp='Resources'
	else:
		param_name_temp=param_name
	for r in result_cursor:
		list.append(r[param_name].encode('utf-8'))
		dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list


	return filters_dict
	
def pages_display1(request):
	query2 = "select * from user_getassociatedapppages("+str(request.session['user_id'])+")"
	cur1 = default_connection.cursor()
	cur1.execute(query2)
	result1 = cur1.fetchall()
	administrator_list = []
	correlator_list = []
	for i in result1:
		d = OrderedDict()
		if i[4] == 'Correlator':
			correlator_list.append(i)
		if i[4] == 'Administrator':
			administrator_list.append(i)
	pagedatarray=OrderedDict()

	if(len(correlator_list)>0):
		pagedatarray['Correlator']=correlator_list
	if(len(administrator_list)>0):
		pagedatarray['Administrator']=administrator_list
	return pagedatarray
	
@csrf_exempt
def output_to_input(request):
	db=mongoConnection_to_ver2()
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		syncdate=''
		result_cursor=db.report_correlator_rca.aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
		filters_dict={}
		'''Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list')
		Plant_dict = Plant_data['plant_dict']
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list	
		
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list')
		sbu_dict = sbu_dict['sbu_dict']
		for key,value in sbu_dict.iteritems():
			sbu_list.append(key)
		filters_dict['sbu_list']=sbu_list	'''

		technology_list=[]
		technology_dict={}
		#pipeline['SiteID']={'$in':[Plant_list[0]]}
		#pipeline['SBU']={'$in':[sbu_list[0]]}
		pipeline={}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list')
		technology_dict = technology_dict['technology_dict']
		for key,value in technology_dict.iteritems():
			technology_list.append(key)
		filters_dict['technology_list']=technology_list
		
		product_list=[]
		product_dict={}
		#pipeline['SiteID']={'$in':[Plant_list[0]]}
		#pipeline['SBU']={'$in':[sbu_list[0]]}
		pipeline['Technology']={'$in':[technology_list[0]]}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
		product_dict = product_dict['product_dict']
		for key,value in product_dict.iteritems():
			product_list.append(key)
		filters_dict['product_list']=product_list
		
		pagedatarray =pages_display1(request)
		url_check='/qualitycorrelator/output_to_input'
		url_status=0
		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitycorrelator/output.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'],"pages_dict": pagedatarray,'page':"","product_dict":product_dict,"technology_dict":technology_dict,'product_list':filters_dict['product_list'],'technology_list':filters_dict['technology_list'], 'syncdate': syncdate })
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'],"pages_dict": pagedatarray,'page':"","plant_dict":Plant_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'], 'syncdate': syncdate })
			return HttpResponse(template.render(context), content_type="text/html")
	else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/output_to_input/'
		return HttpResponseRedirect("/user_mod/login/")
	
@csrf_exempt
def rejected_view(request):
	db = mongoConnection_to_ver2()
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filters_dict={}
		syncdate=''
		result_cursor_datedata=db.rejected_correlator_fng_processorders.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
			max_date = r['maxdate'].strftime('%Y-%m-%d')
			min_date = r['mindate']
			sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
			start_filter_data = r['syncdate']
			sync_date = r['syncdate'].strftime('%d %b %y')
			sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
		

		min_date = min_date.date()
		
		Plant_list=[]
		pipeline={}
		Plant_data = rejected_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list','Finished Goods')
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		filters_dict['Plant_list']=Plant_list

		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		product_dict=rejected_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Finished Goods')
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_list
		
		pagedatarray =pages_display1(request)  
		url_check='/qualitycorrelator/rejected_view'
		url_status=0
		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitycorrelator/rejected_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'plant_dict':Plant_dict,'product_dict':product_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':sync_date,'sync_date_3':sync_date_3,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'syncdate2': sync_date_2 })	
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate })	
			return HttpResponse(template.render(context), content_type="text/html")	


	else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/rejected_view/'
		return HttpResponseRedirect("/user_mod/login/")
	
@csrf_exempt
def data_issue_view(request):
	db = mongoConnection_to_ver2()
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filters_dict={}
		syncdate=''
		result_cursor_datedata=db.data_issue_correlator_rca.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
			max_date = r['maxdate'].strftime('%Y-%m-%d')
			min_date = r['mindate']
			sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
			start_filter_data = r['syncdate']
			sync_date = r['syncdate'].strftime('%d %b %y')
			sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
		

		min_date = min_date.date()
		
		Plant_list=[]
		pipeline={}
		Plant_data = data_issue_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list','Finished Goods')
		#Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		filters_dict['Plant_list']=Plant_list
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)

		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		product_dict=data_issue_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Finished Goods')
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_list
		
		pagedatarray =pages_display1(request)  
		url_check='/qualitycorrelator/rejected_view'
		url_status=0
		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitycorrelator/data_issue_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'plant_dict':Plant_dict,'product_dict':product_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':sync_date,'sync_date_3':sync_date_3,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'syncdate2': sync_date_2 })	
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate })	
			return HttpResponse(template.render(context), content_type="text/html")	


	else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/data_issue_view/'
		return HttpResponseRedirect("/user_mod/login/")
	
@csrf_exempt
def configuration_view(request):
	db=mongoConnection_to_ver2()
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filters_dict={}
		syncdate=''
		#result_cursor=mongoConnection().report_correlator_rca.aggregate([ {'$group':{ '_id':{'UpdatedDate':'$UpdatedDateTime'}} },{'$sort':{'_id.UpdatedDateTime':-1}},{ '$limit' : 1}])
		result_cursor_datedata=db.report_correlator_rca.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			#syncdate=r['_id']['UpdatedDate'].strftime('%d %b %y')
			syncdate=r['syncdate'].strftime('%d %b %y')

				
		product_list=[]
		product_dict={}
		pipeline={}
		product_dict=config_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		
		pagedatarray =pages_display1(request)  
		url_check='/qualitycorrelator/configuration_view'
		url_status=0
		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitycorrelator/config.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'product_list':filters_dict['product_list'],'product_dict':product_dict})
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date})
			return HttpResponse(template.render(context), content_type="text/html")	

	else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/configuration_view/'
		return HttpResponseRedirect("/user_mod/login/")	
	
	
@csrf_exempt
def input_to_output(request):
    if 'user_id'  in request.session:
		db = mongoConnection_to_ver2()
		user_id=request.GET.get('username')
		syncdate=''
		result_cursor_datedata=db.report_correlator_rca.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
			max_date = r['maxdate'].strftime('%Y-%m-%d')
			min_date = r['mindate']
			sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
			start_filter_data = r['syncdate']
			sync_date = r['syncdate'].strftime('%d %b %y')
			sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
		

		min_date = min_date.date()
			
		filters_dict={}
		technology_list=[]
		technology_dict={}
		pipeline={}
		technology_dict=input_filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list','Finished Goods')
		technology_dict = technology_dict['technology_dict']
		for key,value in technology_dict.iteritems():
			technology_list.append(key)
		filters_dict['technology_list']=technology_list
			
		product_list=[]
		product_dict={}
		pipeline['Technology']={'$in':[technology_list[0]]}
		product_dict=input_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Finished Goods')
		product_dict = product_dict['product_dict']
		for key,value in product_dict.iteritems():
			product_list.append(key)
		filters_dict['product_list']=product_list
			
		Plant_list=[]
		Plant_dict={}
		pipeline = {}
		pipeline['Technology']={'$in':[technology_list[0]]}
		pipeline['MaterialNumber']={'$in':[product_list[0]]}
		Plant_data = input_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list','Finished Goods')
		Plant_dict = Plant_data['plant_dict']
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list
		
		
		batchnumber_list=[]
		batchnumber_dict={}
		pipeline['Technology']={'$in':[technology_list[0]]}
		pipeline['MaterialNumber']={'$in':[product_list[0]]}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		end_filter_date = start_filter_data - timedelta(days=366)
		pipeline['ProductionStartDateTime']={'$lte':start_filter_data, '$gte': end_filter_date}
		batchnumber_dict = input_filter_dict('BatchNumber','BatchNumber',pipeline,'batchnumber_dict','batchnumber_list','Finished Goods')
		batchnumber_dict = batchnumber_dict['batchnumber_dict']
		
		'''for key,value in batchnumber_dict.iteritems():
			batchnumber_list.append(key)'''
		#filters_dict['batchnumber_list']=batchnumber_list
		
		batch_cursor=db.report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
		batch_list=[]
		for batch in batch_cursor:
			dict={}
			dict['BatchNumber']=batch['BatchNumber']
			dict['QualitySegment']=batch['QualitySegment']
			batch_list.append(dict)
		filters_dict['batch_list']=batch_list
		
		for key in batch_list:

			for k,v in key.iteritems():
				if(k == 'QualitySegment'):
					if(key['QualitySegment']=='A' or key['QualitySegment']=='B'): 
						key['QualitySegment']='#19b698'
					elif(key['QualitySegment']=='C' or key['QualitySegment']=='D'): 
						key['QualitySegment']='#ea6153'
					else:
						key['QualitySegment']='#D3D3D3'
					tempdict={}
					tempdict['BatchNumber']=key['BatchNumber']
					tempdict['QualitySegment']=key['QualitySegment']
			batchnumber_list.append(tempdict)

		filters_dict['batchnumber_list']=batchnumber_list
				
		pagedatarray =pages_display1(request)  
		url_check='/qualitycorrelator/input_to_output'
		url_status=0
		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitycorrelator/input.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'],"pages_dict": pagedatarray,'page':"","batch_list":filters_dict['batch_list'],"plant_dict":Plant_dict,"technology_dict":technology_dict,"plant_list":filters_dict['Plant_list'],"product_dict":product_dict,'product_list':filters_dict['product_list'],'technology_list':filters_dict['technology_list'],"batchnumber_list":filters_dict['batchnumber_list'],'syncdate':sync_date,'sync_date_3':sync_date_3,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'syncdate2': sync_date_2 })
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'],"pages_dict": pagedatarray,'page':"","plant_dict":plant_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'batchnumber_list':filters_dict['batchnumber_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'], 'syncdate':sync_date,'sync_date_3':sync_date_3,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'syncdate2': sync_date_2 })
			return HttpResponse(template.render(context), content_type="text/html")
    else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/input_to_output/'
		return HttpResponseRedirect("/user_mod/login/")


@csrf_exempt
def rootcauseanalysis(request):
    if 'user_id'  in request.session:
		db = mongoConnection_to_ver2()
		plant_list=[]
		plant_dict=OrderedDict()
		sbu_list=[]
		technology_list=[]
		batchnumber_list=[]
		product_list=[]
		filters_dict={}

		if request.method == 'GET' and 'siteid' in request.GET:
			# syncdate=''		
			# result_cursor=mongoConnection().report_fng_qualscore_qualitysegment.aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])		
			# for r in result_cursor:		
				# syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
			
			syncdate=''
			'''result_cursor_datedata=mongoConnection().report_correlator_rca.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
			for r in result_cursor_datedata:
				start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
				max_date = r['maxdate'].strftime('%Y-%m-%d')
				min_date = r['mindate']
				start_filter_data = r['syncdate']
				sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
				sync_date= r['syncdate'].strftime('%d %b %y')
				sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
			
			#x = d.replace(day=1)
			#start_date = x.date()
			min_date = min_date.date()'''
			result_cursor_datedata=db.report_correlator_rca.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
			print result_cursor_datedata
			
			for r in result_cursor_datedata:
				print("inside the checking function")
				start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
				max_date = r['maxdate'].strftime('%Y-%m-%d')
				min_date = r['mindate']
				sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
				start_filter_data = r['syncdate']
				sync_date = r['syncdate'].strftime('%d %b %y')
				sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
			
			min_date = min_date.date()
			
			'''if (start_date > min_date):
				start_date = min_date'''
			
			 

			reqsitename=request.GET.get('sitename')
			reqsiteid=request.GET.get('siteid')		
			filters_dict={}
			Plant_list=[]
			pipeline={}
			orderplant_dict=OrderedDict()
			orderplant_dict[reqsiteid]=reqsitename
			Plant_list.append(reqsiteid)
			Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list')
			Plant_dict = Plant_data['plant_dict']
			for key,value in Plant_dict.iteritems():
				if(key!=reqsiteid):
					orderplant_dict[key]=value
					Plant_list.append(key)
			Plant_dict=orderplant_dict
			filters_dict['Plant_list']=Plant_list

		
			technology_list=[]
			technology_dict={}
			pipeline['SiteID']={'$in':[Plant_list[0]]}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			
			reqtechnologyname=request.GET.get('technology')
			reqtechnologyid=request.GET.get('technology')
			ordertechnology_dict=OrderedDict()
			ordertechnology_dict[reqtechnologyid]=reqtechnologyname
			technology_list.append(reqtechnologyname)
			technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list')
			technology_dict = technology_dict['technology_dict']
			for key,value in technology_dict.iteritems():
				if(key!=reqtechnologyid):
					ordertechnology_dict[key]=value
					technology_list.append(value)
			technology_dict=ordertechnology_dict
			filters_dict['technology_list']=technology_list		
			
			if 'from_link' in request.GET:
				product_list=(request.GET.get('product').split(','))
				filters_dict['product_list']=product_list
				batchnumber_list=(request.GET.get('processorder').split(','))
				filters_dict['batchnumber_list']=batchnumber_list		

			else:	
				product_list=[]
				product_dict={}
				reqproductid=request.GET.get('product')
				pipeline['SiteID']={'$in':[Plant_list[0]]}
			#	pipeline['SBU']={'$in':[sbu_list[0]]}
				pipeline['Technology']={'$in':[technology_list[0]]}
				product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
				product_dict = product_dict['product_dict']
				orderproduct_dict=OrderedDict()		
				product_list.append(reqproductid)			
				for k, v in product_dict.iteritems():
					if k == reqproductid:
						orderproduct_dict[k]=v
						
				for key,value in product_dict.iteritems():
					if(key!=reqproductid):
						orderproduct_dict[key]=value
						product_list.append(key)
				product_dict=orderproduct_dict

				filters_dict['product_list']=product_list

				
				
				batchnumber_list=[]
				batchnumber_dict={}
				pipeline['SiteID']={'$in':[Plant_list[0]]}
			#	pipeline['SBU']={'$in':[sbu_list[0]]}
				pipeline['Technology']={'$in':[technology_list[0]]}
				pipeline['MaterialNumber']={'$in':[product_list[0]]}
				end_filter_date = start_filter_data - timedelta(days=366)
				pipeline['ProductionStartDateTime']={'$lte':start_filter_data, '$gte': end_filter_date}
				batchnumbers=filter_dict('BatchNumber','BatchNumber',pipeline,'batchnumber_dict','batchnumber_list')
				batchnumber_dict = batchnumbers['batchnumber_dict']
				reqbatchnumber=request.GET.get('processorder')
				batch_cursor=db.report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
				batch_list=[]
				for batch in batch_cursor:
					if(batch['BatchNumber'] != reqbatchnumber):
						dict={}
						dict['BatchNumber']=batch['BatchNumber']
						dict['QualitySegment']=batch['QualitySegment']
						batch_list.append(dict)
				
				filters_dict['batch_list']=batch_list
				
				
				reqbatchnumber=request.GET.get('processorder')
				result_cursor=db.report_correlator_rca.distinct('QualitySegment',{'BatchNumber':reqbatchnumber})
				
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=reqbatchnumber
					if(r=='A' or r=='B'): 
						tempdict['QualitySegment']='#19b698'
					elif(r=='C' or r=='D'):
						tempdict['QualitySegment']='#ea6153'
					else:
						tempdict['QualitySegment']='#D3D3D3'
					batchnumber_list.append(tempdict)
					
				orderbatchnumber_dict=OrderedDict()	
				orderbatchnumber_dict[reqbatchnumber]=reqbatchnumber
				
				for key in batch_list:

					for k,v in key.iteritems():

						if(v!=reqproductid and k == 'QualitySegment'):
							if(key['QualitySegment']=='A' or key['QualitySegment']=='B'): 
								key['QualitySegment']='#19b698'
							elif(key['QualitySegment']=='C' or key['QualitySegment']=='D'): 
								key['QualitySegment']='#ea6153'
							else:
								key['QualitySegment']='#D3D3D3'
							tempdict={}
							tempdict['BatchNumber']=key['BatchNumber']
							tempdict['QualitySegment']=key['QualitySegment']
					batchnumber_list.append(tempdict)

				filters_dict['batchnumber_list']=batchnumber_list

		else:
			user_id=request.GET.get('username')
			# syncdate=''
			# result_cursor=mongoConnection().report_fng_qualscore_qualitysegment.aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
			# for r in result_cursor:
				# syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

			syncdate=''
			result_cursor_datedata=db.report_correlator_rca.aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
			for r in result_cursor_datedata:

				start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
				max_date = r['maxdate'].strftime('%Y-%m-%d')
				min_date = r['mindate']
				sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
				start_filter_data = r['syncdate']
				sync_date = r['syncdate'].strftime('%d %b %y')
				sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
			

			min_date = min_date.date()

			filters_dict={}
			Plant_list=[]
			Plant_dict={}
			pipeline={}
			Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list')
			Plant_dict = Plant_data['plant_dict']
			for key,value in Plant_dict.iteritems():
				Plant_list.append(key)
			filters_dict['Plant_list']=Plant_list	
			
			technology_list=[]
			technology_dict={}
			pipeline['SiteID']={'$in':[Plant_list[0]]}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list')
			technology_dict = technology_dict['technology_dict']
			for key,value in technology_dict.iteritems():
				technology_list.append(key)
			filters_dict['technology_list']=technology_list
			
			product_list=[]
			product_dict={}
			pipeline['SiteID']={'$in':[Plant_list[0]]}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			pipeline['Technology']={'$in':[technology_list[0]]}
			product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')

			product_dict = product_dict['product_dict']
			for key,value in product_dict.iteritems():
				product_list.append(key)
			filters_dict['product_list']=product_list
			
			batchnumber_list=[]
			batchnumber_dict={}
			pipeline['SiteID']={'$in':[Plant_list[0]]}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			pipeline['Technology']={'$in':[technology_list[0]]}
			pipeline['MaterialNumber']={'$in':[product_list[0]]}
			end_filter_date = start_filter_data - timedelta(days=366)
			pipeline['ProductionStartDateTime']={'$lte':start_filter_data, '$gte': end_filter_date}
			batchnumber_dict=filter_dict('BatchNumber','BatchNumber',pipeline,'batchnumber_dict','batchnumber_list')
			batchnumber_dict = batchnumber_dict['batchnumber_dict']

			for key,value in batchnumber_dict.iteritems():
				batchnumber_list.append(key)
			filters_dict['batchnumber_list']=batchnumber_list
			
			batch_cursor=db.report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
			batch_list=[]
			for batch in batch_cursor:

				dict={}
				dict['BatchNumber']=batch['BatchNumber']
				dict['QualitySegment']=batch['QualitySegment']
				batch_list.append(dict)
			filters_dict['batch_list']=batch_list
			
		pagedatarray =pages_display1(request)
		url_check='/qualitycorrelator/rootcauseanalysis'
		url_status=0

		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			#template = loader.get_template('qualitycorrelator/rca_new.html')
			template = loader.get_template('qualitycorrelator/rca_new.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'], "pages_dict": pagedatarray,'page':"","product_dict":product_dict,"technology_dict":technology_dict,"plant_dict":Plant_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'batchnumber_list':filters_dict['batchnumber_list'],'batch_list':filters_dict['batch_list'],'technology_list':filters_dict['technology_list'], 'syncdate':sync_date,'sync_date_3':sync_date_3,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'syncdate2': sync_date_2,'appuserid':request.session['user_id'] })
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'], "pages_dict": pagedatarray,'page':"","plant_dict":Plant_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'batchnumber_list':filters_dict['batchnumber_list'],'technology_list':filters_dict['technology_list'], 'syncdate':sync_date,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'sync_date_3':sync_date_3,'syncdate2': sync_date_2,'appuserid':request.session['user_id'] })
			return HttpResponse(template.render(context), content_type="text/html")
    else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/rootcauseanalysis/'
		return HttpResponseRedirect("/user_mod/login/")
	
	
@csrf_exempt
def stagged_pos(request):
    if 'user_id'  in request.session:
		db = mongoConnection_to_ver2()
		user_id=request.GET.get('username')
		result_cursor_datedata=db.report_correlator_staged_po.aggregate([{'$match':{'ProductionStartDateTime':{'$ne':'NA'}}},{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			start_date =(r['maxdate'] - timedelta(days=365)).replace(day=1).date()
			max_date = r['maxdate'].strftime('%Y-%m-%d')
			min_date = r['mindate']
			sync_date_3 = r['syncdate'].strftime('%Y-%m-%d')
			start_filter_data = r['syncdate']
			sync_date = r['syncdate'].strftime('%d %b %y')
			sync_date_2 = r['syncdate'].strftime('%y-%m-%d')
		min_date = min_date.date()

		filters_dict={}
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = input_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list','Stagged PO')
		Plant_dict = Plant_data['plant_dict']
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list	

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		#pipeline['SBU']={'$in':[sbu_list[0]]}
		technology_dict=input_filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list','Stagged PO')
		technology_dict = technology_dict['technology_dict']
		for key,value in technology_dict.iteritems():
			technology_list.append(key)
		filters_dict['technology_list']=technology_list
		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		#pipeline['SBU']={'$in':[sbu_list[0]]}
		pipeline['Technology']={'$in':[technology_list[0]]}
		product_dict=input_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Stagged PO')

		product_dict = product_dict['product_dict']
		for key,value in product_dict.iteritems():
			product_list.append(key)
		filters_dict['product_list']=product_list
		
		batchnumber_list=[]
		batchnumber_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		#pipeline['SBU']={'$in':[sbu_list[0]]}
		pipeline['Technology']={'$in':[technology_list[0]]}
		pipeline['MaterialNumber']={'$in':[product_list[0]]}
		end_filter_date = start_filter_data - timedelta(days=366)
		pipeline['ProductionStartDateTime']={'$lte':start_filter_data, '$gte': end_filter_date}
		batchnumber_dict=input_filter_dict('BatchNumber','BatchNumber',pipeline,'batchnumber_dict','batchnumber_list','Stagged PO')
		batchnumber_dict = batchnumber_dict['batchnumber_dict']

		for key,value in batchnumber_dict.iteritems():
			batchnumber_list.append(key)
		filters_dict['batchnumber_list']=batchnumber_list

		batch_cursor=db.report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1})
		batch_list=[]
		for batch in batch_cursor:
			dict={}
			dict['BatchNumber']=batch['BatchNumber']
			dict['QualitySegment']=batch['PredictedQualitySegment']
			batch_list.append(dict)
		filters_dict['batch_list']=batch_list
				
		for key in batch_list:

			for k,v in key.iteritems():
				if(k == 'QualitySegment'):
					if(key['QualitySegment']=='A' or key['QualitySegment']=='B'): 
						key['QualitySegment']='#19b698'
					elif(key['QualitySegment']=='C' or key['QualitySegment']=='D'): 
						key['QualitySegment']='#ea6153'
					else:
						key['QualitySegment']='#D3D3D3'
					tempdict={}
					tempdict['BatchNumber']=key['BatchNumber']
					tempdict['QualitySegment']=key['QualitySegment']
			batchnumber_list.append(tempdict)

		filters_dict['batchnumber_list']=batchnumber_list
		
		pagedatarray =pages_display1(request)  
		url_check='/qualitycorrelator/stagged_pos'
		url_status=0
		for row in pagedatarray['Correlator']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			#template = loader.get_template('qualitycorrelator/rca_new.html')
			template = loader.get_template('qualitycorrelator/rca_new.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'], "pages_dict": pagedatarray,'page':"","product_dict":product_dict,"technology_dict":technology_dict,"plant_dict":Plant_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'batchnumber_list':filters_dict['batch_list'],'batch_list':filters_dict['batchnumber_list'],'technology_list':filters_dict['technology_list'], 'syncdate':sync_date,'sync_date_3':sync_date_3,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'syncdate2': sync_date_2 })
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitycorrelator/forbidden.html')
			context = RequestContext(request, {'appuser': request.session['user_alias'], "pages_dict": pagedatarray,'page':"","plant_dict":Plant_dict,"plant_list":filters_dict['plant_list'],'product_list':filters_dict['product_list'],'batchnumber_list':filters_dict['batchnumber_list'],'batch_list':filters_dict['batch_list'],'technology_list':filters_dict['technology_list'], 'syncdate':sync_date,'min_date':min_date, 'start_date':start_date, 'max_date':sync_date, 'sync_date_3':sync_date_3,'syncdate2': sync_date_2 })
			return HttpResponse(template.render(context), content_type="text/html")
    else:
		request.session['next'] = 'http://193.96.100.95/qualitycorrelator/stagged_pos/'
		return HttpResponseRedirect("/user_mod/login/")
	
	
	
	
def get_filter_pipeline(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_dict={}
	plant=''
	line=''
	resource=''
	product=''
	sbu=''
	technology=''
#---------------------getting data------------------------------------#	
	data_list=[]
	data_list=request.GET.getlist('plant_list[]')
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data.encode('utf-8'))
	else:
		plant=data_list[0].encode('utf-8')
		
		
	data_list=[]
	data_list=request.GET.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data.encode('utf-8'))
	else:
		sbu=data_list[0].encode('utf-8')


	data_list=[]
	data_list=request.GET.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data.encode('utf-8'))
	else:
		technology=data_list[0].encode('utf-8')		
		
		
	data_list=[]
	data_list=request.GET.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data.encode('utf-8'))
	else:
		product=data_list[0].encode('utf-8')	

		
		
	data_list=[]
	data_list=request.GET.getlist('line_list[]')
	if len(data_list)>1:
		for data in data_list:
			line_list.append(data.encode('utf-8'))
	else:
		line=data_list[0].encode('utf-8')
		
	data_list=[]
	data_list=request.GET.getlist('resource_list[]')
	if len(data_list)>1:
		for data in data_list:
			resource_list.append(data.encode('utf-8'))
	else:
		resource=data_list[0].encode('utf-8')
#----------------------making Pipeline-------------------------------
	if (len(plant_list)>=2 ):
		filter_dict['SiteID']={'$in':plant_list }  
	else:
		if(plant!='all'):	
			filter_dict['SiteID']=plant  
	
	if (len(sbu_list)>=2 ):
		filter_dict['SBU']={'$in':sbu_list }  
	else:
		if(sbu!='all'):	
			filter_dict['SBU']=sbu 

	if (len(technology_list)>=2 ):
		filter_dict['Technology']={'$in':technology_list }  
	else:
		if(technology!='all'):	
			filter_dict['Technology']=technology  

	if (len(product_list)>=2 ):
		filter_dict['MaterialNumber']={'$in':product_list }  
	else:
		if(product!='all'):	
			filter_dict['MaterialNumber']=product  			
			
	if (len(line_list)>=2 ):
		filter_dict['LineName']={'$in':line_list } 	
	else:
		if(line!='all'):
			filter_dict['LineName']=line
			
	if (len(resource_list)>=2 ):
		filter_dict['Resources']={'$in':resource_list }
	else:
		if(resource!='all'):
			filter_dict['Resources']=resource
	
	return filter_dict

	
	
	
	
def get_pipeline(request):
	plant_list=[]
	date_list=[]
	filter_list=[ ]
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	product=''
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.GET.getlist('plant_list[]') 
	
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]

	data_list=[]
	data_list=request.GET.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data)
	else:	
		product=data_list[0].encode('utf-8')	

		
		
	data_list=[]
	data_list=request.GET.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data)
	else:
		sbu=data_list[0]	
		
	data_list=[]
	data_list=request.GET.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data)
	else:
		technology=data_list[0]			
		
	if (len(technology_list)>=2 ):
		filter_list.append({'Technology':{'$in':technology_list } } )
	else:
		if(technology!='all'):
			filter_list.append( {'Technology':technology } )
			
	if (len(sbu_list)>=2 ):
		filter_list.append({'SBU':{'$in':sbu_list } } )
	else:
		if(sbu!='all'):
			filter_list.append( {'SBU':sbu } )

	if (len(plant_list)>=2 ):
		filter_list.append({'SiteID':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteID':plant } )

			
	if (len(product_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':product_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'MaterialNumber':product }	)

		
	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			logging.info('***********************pipeline**********************')
			logging.info(filter_pipeline)
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline
	
	
@csrf_exempt			
def get_filter_data(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	date_list=[]
	date=''
	pipeline=get_filter_pipeline(request)
	change_type=request.GET.get("change_type", "")
	change_type=change_type.encode('utf-8')
	page_view=request.GET.get('page_view')
	if(page_view=="rootcauseanalysis"):
		data_list=[]
		data_list=request.GET.getlist('date_list[]')
		if len(data_list)>1:
			for data in data_list:
				date_list.append(data)
		else:
			date=data_list[0]
	elif(page_view =="stagged_pos"):
		data_list=[]
		data_list=request.GET.getlist('date_list[]')
		if len(data_list)>1:
			for data in data_list:
				date_list.append(data)
		else:
			date=data_list[0]
	if (page_view=="stagged_pos"):
		if (change_type=='plant'):
			product_list=[]		
			sbu_list=[]
			sbu_dict={}
			#pipeline={}
			sbu_dict=input_filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list','Stagged PO')
			#sbu_list=sbu_dict['sbu_list']
			sbu_dict = sbu_dict['sbu_dict']
			filters_dict['sbu_list']=sbu_dict
			
			for key,value in sbu_dict.iteritems():
				sbu_list.append(key)
			
			technology_list=[]
			technology_dict={}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			technology_dict=input_filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list','Stagged PO')
			#technology_list=technology_dict['technology_list']
			technology_dict = technology_dict['technology_dict']	
			filters_dict['technology_list']=technology_dict
			
			for key,value in technology_dict.iteritems():
				technology_list.append(key)
			
			product_list=[]
			product_dict={}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			pipeline['Technology']={'$in':[technology_list[0]]}
			product_dict=input_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Stagged PO')
			#product_list=product_dict['product_list']
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)

			if(len(product_list)>0):
				pipeline['MaterialNumber']=product_list[0]
				if(page_view=="stagged_pos"):
					if (len(date_list)>=2 ):
						pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
					else:
						pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
				batchnumber_list=[]

				result_cursor=mongoConnection_to_ver2().report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1})
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=r['BatchNumber']
					tempdict['QualitySegment']=r['PredictedQualitySegment']
					batchnumber_list.append(tempdict)
				filters_dict['batchnumber_list']=batchnumber_list
			else:
				filters_dict['batchnumber_list']=[]
		elif (change_type=='sbu'):
				
			technology_list=[]
			technology_dict={}
			technology_dict=input_filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list','Stagged PO')
			#technology_list=technology_dict['technology_list']
			technology_dict = technology_dict['technology_dict']
			filters_dict['technology_list']=technology_dict
			
			for key,value in technology_dict.iteritems():
				technology_list.append(key)
			
			product_list=[]
			product_dict={}
			pipeline['Technology']={'$in':[technology_list[0]]}
			product_dict=input_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Stagged PO')
			#product_list=product_dict['product_list']
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)

			if(len(product_list)>0):
				pipeline['MaterialNumber']=product_list[0]
				if(page_view=="stagged_pos"):
					if (len(date_list)>=2 ):
						pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
					else:
						pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
				batchnumber_list=[]
				result_cursor=mongoConnection_to_ver2().report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1})
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=r['BatchNumber']
					tempdict['QualitySegment']=r['PredictedQualitySegment']
					batchnumber_list.append(tempdict)
				filters_dict['batchnumber_list']=batchnumber_list	
			else:
				filters_dict['batchnumber_list']=[]		
			
		elif (change_type=='technology'):

			product_list=[]
			product_dict={}
			product_dict=input_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list','Stagged PO')
			#product_list=product_dict['product_list']
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)
				
			if(len(product_list)>0):
				pipeline['MaterialNumber']=product_list[0]
				if(page_view=="stagged_pos"):
					if (len(date_list)>=2 ):
						pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
					else:
						pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
				batchnumber_list=[]
				result_cursor=mongoConnection_to_ver2().report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1})
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=r['BatchNumber']
					tempdict['QualitySegment']=r['PredictedQualitySegment']
					batchnumber_list.append(tempdict)
				filters_dict['batchnumber_list']=batchnumber_list	
			else:
				filters_dict['batchnumber_list']=[]		
		else:

			if(page_view=="stagged_pos"):
				if (len(date_list)>=2 ):
					pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
				else:
					
					pipeline['ProductionStartDateTime']=datetime.strptime(date, '%Y-%m-%d')
			batchnumber_list=[]
			result_cursor=mongoConnection_to_ver2().report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1})
			for r in result_cursor:
				tempdict={}
				tempdict['BatchNumber']=r['BatchNumber']
				tempdict['QualitySegment']=r['PredictedQualitySegment']
				batchnumber_list.append(tempdict)
			
			filters_dict['batchnumber_list']=batchnumber_list
	else:
		if (change_type=='plant'):
			product_list=[]		
			sbu_list=[]
			sbu_dict={}
			#pipeline={}
			sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list')
			#sbu_list=sbu_dict['sbu_list']
			sbu_dict = sbu_dict['sbu_dict']
			filters_dict['sbu_list']=sbu_dict
			
			for key,value in sbu_dict.iteritems():
				sbu_list.append(key)
			
			technology_list=[]
			technology_dict={}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list')
			#technology_list=technology_dict['technology_list']
			technology_dict = technology_dict['technology_dict']	
			filters_dict['technology_list']=technology_dict
			
			for key,value in technology_dict.iteritems():
				technology_list.append(key)
			
			product_list=[]
			product_dict={}
			#pipeline['SBU']={'$in':[sbu_list[0]]}
			pipeline['Technology']={'$in':[technology_list[0]]}
			product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
			#product_list=product_dict['product_list']
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)
			
			if(len(product_list)>0):
				pipeline['MaterialNumber']=product_list[0]
				if(page_view=="rootcauseanalysis"):
					if (len(date_list)>=2 ):
						pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
					else:
						pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
				batchnumber_list=[]

				result_cursor=mongoConnection_to_ver2().report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=r['BatchNumber']
					tempdict['QualitySegment']=r['QualitySegment']
					batchnumber_list.append(tempdict)
				filters_dict['batchnumber_list']=batchnumber_list
			else:
				filters_dict['batchnumber_list']=[]
			
			
		elif (change_type=='sbu'):
				
			technology_list=[]
			technology_dict={}
			technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list')
			#technology_list=technology_dict['technology_list']
			technology_dict = technology_dict['technology_dict']
			filters_dict['technology_list']=technology_dict
			
			for key,value in technology_dict.iteritems():
				technology_list.append(key)
			
			product_list=[]
			product_dict={}
			pipeline['Technology']={'$in':[technology_list[0]]}
			product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
			#product_list=product_dict['product_list']
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)
			
			if(len(product_list)>0):
				pipeline['MaterialNumber']=product_list[0]
				if(page_view=="rootcauseanalysis"):
					if (len(date_list)>=2 ):
						pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
					else:
						pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
				batchnumber_list=[]
				result_cursor=mongoConnection_to_ver2().report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=r['BatchNumber']
					tempdict['QualitySegment']=r['QualitySegment']
					batchnumber_list.append(tempdict)
				filters_dict['batchnumber_list']=batchnumber_list	
			else:
				filters_dict['batchnumber_list']=[]		
			
		elif (change_type=='technology'):

			product_list=[]
			product_dict={}
			product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
			#product_list=product_dict['product_list']
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)
			
			if(len(product_list)>0):
				pipeline['MaterialNumber']=product_list[0]
				if(page_view=="rootcauseanalysis"):
					if (len(date_list)>=2 ):
						pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
					else:
						pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
				batchnumber_list=[]
				result_cursor=mongoConnection_to_ver2().report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
				for r in result_cursor:
					tempdict={}
					tempdict['BatchNumber']=r['BatchNumber']
					tempdict['QualitySegment']=r['QualitySegment']
					batchnumber_list.append(tempdict)
				filters_dict['batchnumber_list']=batchnumber_list	
			else:
				filters_dict['batchnumber_list']=[]		
		else:

			if(page_view=="rootcauseanalysis"):
				if (len(date_list)>=2 ):
					pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
				else:
					
					pipeline['ProductionStartDateTime']=datetime.strptime(date, '%Y-%m-%d')
			batchnumber_list=[]
			result_cursor=mongoConnection_to_ver2().report_correlator_rca.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
			for r in result_cursor:
				tempdict={}
				tempdict['BatchNumber']=r['BatchNumber']
				tempdict['QualitySegment']=r['QualitySegment']
				batchnumber_list.append(tempdict)
			
			filters_dict['batchnumber_list']=batchnumber_list	
	
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")

@csrf_exempt			
def get_output_filter_data(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	date_list=[]
	date=''
	pipeline=get_filter_pipeline(request)
	change_type=request.GET.get("change_type", "")
	change_type=change_type.encode('utf-8')
	page_view=request.GET.get('page_view')
	if (change_type=='technology'):
		product_list=[]
		product_dict={}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list')
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_dict
		
		for key,value in product_dict.iteritems():
			product_list.append(key)
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")
	
def getInputFilterData(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	date_list=[]
	date=''
	pipeline=get_filter_pipeline(request)
	change_type=request.GET.get("change_type", "")
	change_type=change_type.encode('utf-8')
	page_view=request.GET.get('page_view')
	change_value=request.GET.get('change_value')
	if(change_value == 'Finished Goods'):
		db = mongoConnection_to_ver2().report_correlator_rca
	elif(change_value == 'Stagged PO'):
		db = mongoConnection_to_ver2().report_correlator_staged_po
	if(page_view=="input_to_output"):
		data_list=[]
		data_list=request.GET.getlist('date_list[]')
		if len(data_list)>1:
			for data in data_list:
				date_list.append(data)
		else:
			date=data_list[0]
	
	if(change_type=='analysis'):
		batchnumber_list=[]
		if (len(date_list)>=2 ):
			pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
		else:
			pipeline['ProductionStartDateTime']={'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')}
		result_cursor=db.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
		for r in result_cursor:
			tempdict={}
			tempdict['BatchNumber']=r['BatchNumber']
			tempdict['QualitySegment']=r['QualitySegment']
			batchnumber_list.append(tempdict)
		filters_dict['batchnumber_list']=batchnumber_list
	elif(change_type == 'technology'):
		product_list=[]
		product_dict={}
		product_dict=input_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',change_value)
		#product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_dict
		
		for key,value in product_dict.iteritems():
			product_list.append(key)
							
		plant_list=[]
		plant_dict={}
		pipeline['MaterialNumber'] = {'$in':[product_list[0]]}
		plant_dict=input_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',change_value)
		#plant_list=plant_dict['plant_list']
		plant_dict = plant_dict['plant_dict']
		filters_dict['plant_list']=plant_dict
		
		for key,value in plant_dict.iteritems():
			plant_list.append(key)
		
		batchnumber_list=[]
		pipeline['SiteID'] = plant_list[0]
		if (len(date_list)>=2 ):
			pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
		else:
			pipeline['ProductionStartDateTime']={'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')}
		result_cursor=db.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
		for r in result_cursor:
			tempdict={}
			tempdict['BatchNumber']=r['BatchNumber']
			tempdict['QualitySegment']=r['QualitySegment']
			batchnumber_list.append(tempdict)
		filters_dict['batchnumber_list']=batchnumber_list

	elif(change_type == 'product'):
		plant_list=[]
		plant_dict={}
		plant_dict=input_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',change_value)
		#plant_list=plant_dict['plant_list']
		plant_dict = plant_dict['plant_dict']
		filters_dict['plant_list']=plant_dict
		
		for key,value in plant_dict.iteritems():
			plant_list.append(key)
		
		batchnumber_list=[]
		pipeline['SiteID'] = plant_list[0]
		if (len(date_list)>=2 ):
			pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
		else:
			pipeline['ProductionStartDateTime']={'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')}
		result_cursor=db.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
		for r in result_cursor:
			tempdict={}
			tempdict['BatchNumber']=r['BatchNumber']
			tempdict['QualitySegment']=r['QualitySegment']
			batchnumber_list.append(tempdict)
		filters_dict['batchnumber_list']=batchnumber_list
	elif(change_type == 'plant'):
		if (len(date_list)>=2 ):
			pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
		else:
			pipeline['ProductionStartDateTime']={'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')}
		batchnumber_list=[]
		result_cursor=db.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
		for r in result_cursor:
			tempdict={}
			tempdict['BatchNumber']=r['BatchNumber']
			tempdict['QualitySegment']=r['QualitySegment']
			batchnumber_list.append(tempdict)
		filters_dict['batchnumber_list']=batchnumber_list
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")

@csrf_exempt	
def getRejectedFilterData(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	date_list=[]
	date=''
	pipeline=get_filter_pipeline(request)
	change_type=request.GET.get("change_type", "")
	change_type=change_type.encode('utf-8')
	page_view=request.GET.get('page_view')
	change_value=request.GET.get('change_value')
	if(change_value == 'Finished Goods'):
		db = mongoConnection_to_ver2().rejected_correlator_fng_processorders
	elif(change_value == 'Stagged PO'):
		db = mongoConnection_to_ver2().rejected_correlator_staged_po
	if(page_view=="rejected_view"):
		data_list=[]
		data_list=request.GET.getlist('date_list[]')
		if len(data_list)>1:
			for data in data_list:
				date_list.append(data)
		else:
			date=data_list[0]
		'''if (len(date_list)>=2 ):
			pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
		else:
			pipeline['ProductionStartDateTime']={'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')}'''
		if(change_type == 'plant'):
			product_list=[]
			product_dict={}
			product_dict=rejected_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',change_value)
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)
				
	elif(page_view=="data_issue_view"):
		data_list=[]
		data_list=request.GET.getlist('date_list[]')
		if len(data_list)>1:
			for data in data_list:
				date_list.append(data)
		else:
			date=data_list[0]
		if(change_type == 'plant'):
			product_list=[]
			product_dict={}
			product_dict=data_issue_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',change_value)
			product_dict = product_dict['product_dict']
			filters_dict['product_list']=product_dict
			
			for key,value in product_dict.iteritems():
				product_list.append(key)
				
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")

def get_output_parameters_staggedpos(request):
	BatchNumber=float(request.GET.get('BatchNumber'))
	output_parameters={}
	result_cursor=connection_staggedpos().find({'BatchNumber':BatchNumber},{'OutputScore':1,'AvgQualityScore':1,'QualitySegment':1,'_id':0})
	for r in result_cursor:
		result=r['OutputScore']
		AvgQualityScore=r['AvgQualityScore']
		QualitySegment=r['QualitySegment']
	for k,v in result.iteritems():
		for kk,vv in v.iteritems():
			if(kk=='ActualValue'):
				output_parameters[k]=vv

	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	qualityscore=0
	table_cursor=conn.henkel.src_StagedProcessOrders
	BatchNumber=request.GET.get('BatchNumber')
	data = table_cursor.find({'StagingPhaseInfo':{'$ne':[]},'BatchNumber':BatchNumber}).limit(1)
	data_list = []
	for row in data:
		data_list.append(row)
	gen_info = ["ProcessOrderNumber", "SiteID", "SiteName", "MaterialNumber", "SBU", "Technology"]    
	pro_info = ["IsPhaseDelayed", "ChangeOverDone"]
	rawm_info = ["QualityAverage"]
	ccp_info = ["ActualValue"]
	operator_info = ["PhaseStartUserID" , "PhaseStartUserName"]
	machine_info = [ "PhaseResourceName","PhaseResourceID"]

	for r in data_list:
		op_dict = {}
		op_dict['GeneralInfo'] = {}
		for k,v in r.iteritems():
			if k == 'StagingPhaseInfo':
					for kk, vv in v.iteritems():
						op_dict[kk] = {}
						op_dict[kk]['RawMaterialInfo'] = {}
						for kp, kv in vv.iteritems():
							if kp == 'RawMaterialInfo':
									 for kd, pd in kv.iteritems():
										 op_dict[kk]['RawMaterialInfo'][kd] = {}
										 for kdd, pdd in pd.iteritems():
											 if kdd in rawm_info:
													for eq_key,eq_val in pd[kdd].iteritems():
																op_dict[kk]['RawMaterialInfo'][kd][eq_key]=eq_val	

	result_data={'output_parameters':output_parameters,'AvgQualityScore':AvgQualityScore,'QualitySegment':QualitySegment,'op_dict':op_dict}	
	return HttpResponse(json.dumps(result_data), content_type="application/json")
	
def get_count_staggedpos(request):
	total=0
	date_list=[]
	staggedpos_count={}
	pipeline ={}
	pipeline['$match']={}
	pipeline['$match']['$and']=[]
	#pipeline=get_pipeline(request)
	#pipeline=get_filter_pipeline(request)
	plantlist = request.GET.getlist('plant_list[]')
	technologylist = request.GET.getlist('technology_list[]')
	productlist = request.GET.getlist('product_list[]')
	date_li = request.GET.getlist('date_list[]')
	for data in date_li:
		date_list.append(data)
	
	pipeline['$match']['$and'].append({'SiteID':{'$in':plantlist}})
	pipeline['$match']['$and'].append({'Technology':{'$in':technologylist}})
	pipeline['$match']['$and'].append({'MaterialNumber':{'$in':productlist}})
	pipeline['$match']['$and'].append({'ProductionStartDateTime':{'$lte':datetime.strptime(date_list[1], '%Y-%m-%d')}})
	pipeline['$match']['$and'].append({'ProductionStartDateTime':{'$gte':datetime.strptime(date_list[0], '%Y-%m-%d')}})
	
	result_cursor=mongoConnection_to_ver2().report_correlator_staged_po.aggregate([pipeline,{'$group':{'_id':{'PredictedQualitySegment':'$PredictedQualitySegment'},'Count':{'$sum':1} } },{'$project':{'_id':0,'QualitySegment':'$_id.PredictedQualitySegment','Count':1 }} ])
	for r in result_cursor:
		total=total+r['Count']
		staggedpos_count[r['QualitySegment']]=r['Count']
	staggedpos_count['Total']=total	
	return HttpResponse(json.dumps(staggedpos_count), content_type="application/json")


def get_batchnumbers_staggedpos_segment(request):
	# SiteID=request.GET.get('SiteID')
	# Technology=request.GET.get('Technology')
	# SBU=request.GET.get('SBU')
	# MaterialNumber=request.GET.get('MaterialNumber')
	pipeline=get_filter_pipeline(request)
	QualitySegment=request.GET.get('QualitySegment')
	BatchNumber=[]
	batchnumber_list = []
	data_list=[]
	date_list =[]
	data_list=request.GET.getlist('date_list[]')

	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]
		
	if (len(date_list)>=2 ):
		pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
	else:
		pipeline['ProductionStartDateTime']=  {'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')} 
		
	if(QualitySegment=='all'):

		#result_cursor1=connection_staggedpos().find({'SiteID':SiteID,'Technology':Technology,'SBU':SBU,'MaterialNumber':MaterialNumber},{'BatchNumber':1,'_id':0})
		result_cursor1=mongoConnection_to_ver2().report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1,'_id':0})
	else:
		#result_cursor1=connection_staggedpos().find({'SiteID':SiteID,'Technology':Technology,'SBU':SBU,'MaterialNumber':MaterialNumber,'QualitySegment':QualitySegment},{'BatchNumber':1,'_id':0})
		pipeline['PredictedQualitySegment']=QualitySegment
		result_cursor1=mongoConnection_to_ver2().report_correlator_staged_po.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1,'_id':0})
	for r in result_cursor1:
		#BatchNumber.append(r)
		tempdict={}
		tempdict['BatchNumber']=r['BatchNumber']
		tempdict['QualitySegment']=r['PredictedQualitySegment']
		batchnumber_list.append(tempdict)
	return HttpResponse(json.dumps(batchnumber_list), content_type="application/json")

		
	
def get_output_parameters(request):	
	parameter_list=[]
	p=''
	MaterialNumber=str(request.GET.get('product').encode('utf-8'))
	#SiteID=request.GET.get('siteid').encode('utf-8')
	parameters_json = output_to_input_file_test_id.get_op_parameters(MaterialNumber)
	return HttpResponse(json.dumps(parameters_json), content_type="application/json")

@csrf_exempt	
def get_output(request):
	#oi_dict=ast.literal_eval(request.GET.get('output_parameters'))
	data = unidecode(request.GET.get('chk',''))
	data1=ast.literal_eval(data)
	username = request.session['username']
	encrpytion_key = "71079162"
	encrpyted_username = AESencrypt(encrpytion_key, username)
	encoded_username_url = urllib.quote(encrpyted_username, safe='')
	app = '20263146'
	data1['app']=app
	data1['encoded_username_url']=encoded_username_url
	po_details = output_to_input_file_test_id.outputtoinput(data1)
	return HttpResponse(json.dumps(po_details), content_type="json")
	
def AESencrypt(password, plaintext, base64=True):		
    SALT_LENGTH = 32		
    DERIVATION_ROUNDS=1337		
    BLOCK_SIZE = 16		
    KEY_SIZE = 32		
    MODE = AES.MODE_CBC		
     		
    paddingLength = 16 - (len(plaintext) % 16)		
    paddedPlaintext = plaintext+chr(paddingLength)*paddingLength		
    derivedKey = password		
    derivedKey = hashlib.sha256(derivedKey).digest()		
    derivedKey = derivedKey[:KEY_SIZE]		
    iv = derivedKey[:BLOCK_SIZE]		
    cipherSpec = AES.new(derivedKey, MODE, iv)		
    ciphertext = cipherSpec.encrypt(paddedPlaintext)		
    if base64:		
        import base64		
        return base64.b64encode(ciphertext)		
    else:		
        return ciphertext.encode("hex")	
	
@csrf_exempt	
def get_input(request):
	input_data=ast.literal_eval(request.GET.get('input_data').encode('utf-8'));

	product=request.GET.get('product').encode('utf-8')
	plant=request.GET.get('site_id').encode('utf-8')
	url='https://henkelpilot.westeurope.cloudapp.azure.com/ocpu/library/qualityCorrelator/R/InputtoOutput'
	param_dict=json.dumps([input_data])
	json_data={'testData':param_dict,'MatNum': product,'SiteID':plant}
	#json_data={'MatNum': '1774264', 'SiteID': '2', 'testDataJson': '[{"0030_B": {"StdCycleTime": "50", "ChangeOverDone": "N", "RawMaterialInfo": {}}, "0050_B": {"StdCycleTime": "90", "ChangeOverDone": "N", "RawMaterialInfo": {}}, "0020_B": {"StdCycleTime": "70", "ChangeOverDone": "N", "RawMaterialInfo": {"826895": {"QualityAverage": {"Softening point": "101.30", "Color(Gardner)": "1"}, "ShelfLife": "365"}, "1211831": {"QualityAverage": {"Acid value(\\u9178\\u503c)": "0.07", "Melt point": "184.50"}, "ShelfLife": "365"}, "1231720": {"QualityAverage": {"Viscosity(HM)": "512", "Color(Yellow index)": "5.21429"}, "ShelfLife": "365"}, "1042117": {"QualityAverage": {"Assay%": "0.0080", "Melt point": "116.30", "Moisture(\\\\u6c34\\\\u542b\\\\u91cf%)": "0.18"}, "ShelfLife": "365"}, "889333": {"QualityAverage": {"Viscosity(VTS)": "870", "Mole weight": "215"}, "ShelfLife": "365"}, "603141": {"QualityAverage": {"Melt flow index": "7.70"}, "ShelfLife": "365"}, "538299": {"QualityAverage": {"Melt flow index": "1.59557"}, "ShelfLife": "1080"}}}, "0040_B": {"StdCycleTime": "60", "ChangeOverDone": "N", "RawMaterialInfo": {"826895": {"QualityAverage": {"Softening point": "101.30", "Color(Gardner)": "1"}, "ShelfLife": "365"}, "352945": {"QualityAverage": {"Softening point": "130.50", "Color(Gardner)": "1"}, "ShelfLife": "365"}, "83460": {"QualityAverage": {"Softening point": "96.30"}, "ShelfLife": "1460"}}}, "0060_B": {"StdCycleTime": "25", "ChangeOverDone": "N", "RawMaterialInfo": {}}}]'}

	res = requests.post(url, json=json_data,verify=False)
	res_url='https://henkelpilot.westeurope.cloudapp.azure.com'+res.content[0:res.content.find('val')+3]+'/json'

	res1=requests.get(res_url,verify=False)	

	return HttpResponse(json.dumps(ast.literal_eval(res1.content)), content_type="json")

@csrf_exempt	
def get_estimated_input(request):
	db = mongoConnection_to_ver2()
	correlate_dict=ast.literal_eval(request.POST.get('correlate_dict'));
	result = PrepPredictData.prepModelInputData(correlate_dict)
	result1=CalculateScore.Predict_Score('InputToOutput',result)
		
	#db.testcalculatescore.insert(correlate_dict)
	'''if(result != 'ModelNotBuilt'):
		x = {}
		for key in sorted(result['output_parameter'].iterkeys()):
			x[key] = result['output_parameter'][key]
		result['output_parameter'] = x	'''
			
	return HttpResponse(json.dumps(result1), content_type="json")
	#return HttpResponse("hy", content_type="json")
	
def get_structured_data(po_number,plant):
	SiteID=plant
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.src_processorders
	
	data = table_cursor.find({'ProcessOrderNumber':po_number}).limit(1)
	data_list = []
	for row in data:
		data_list.append(row)
	qualityscore=0	
	ProductionStartDateTime=''
	qualitysegment=''
	qualityscore_query=mongoConnection_to_ver2().report_fng_qualscore_qualitysegment.find({'ProcessOrder':po_number},{'AvgQualityScore':1,'QualitySegment':1,'ProductionStartDateTime':1,'_id':0})
	for r in qualityscore_query:
		qualityscore=float(r['AvgQualityScore'])
		qualitysegment=r['QualitySegment']
		if(r['ProductionStartDateTime']!=''):
			try:
				ProductionStartDateTime=r['ProductionStartDateTime'].strftime('%d-%m-%Y')	
			except Exception as e:
				print e		
		
	gen_info = ["ProcessOrderNumber", "SiteID", "SiteName", "MaterialNumber", "SBU", "Technology","ProductionStartDateTime","AvgQualityScore"]    
	pro_info = ["IsPhaseDelayed", "PhaseStartUserID", "PhaseResourceID","PhaseResourceName", "PreviousPO", "ChangeOverDoneBefore"]
	rawm_info = ["QualityAverage"]
	ccp_info = ["HighValue", "LowValue", "ActualValue"]
	#ccp_info = ["ActualValue"]

	for r in data_list:
		op_dict = {}
		op_dict['GeneralInfo'] = {}
		op_dict['Output_parameters']={}
		for k,v in r.iteritems():
			if k in gen_info:
				op_dict['GeneralInfo'][k] = r[k]
				if(k=='ProductionStartDateTime'):
					try:
						op_dict['GeneralInfo'][k]=r['ProductionStartDateTime'].strftime('%d %b %y')
					except Exception as e:
						op_dict['GeneralInfo'][k]=''
					op_dict['GeneralInfo']['QualityScore']=qualityscore
					op_dict['GeneralInfo']['QualitySegment']=qualitysegment
					
			if k == 'POSAPQCCharacterInfo':	
				for s in v:
					for kk, vv in s.iteritems():
					   for kkk,vvv in vv.iteritems():
							   if(kkk=='MeanValue'):
									op_dict['Output_parameters'][kk]=vvv															

		result_data=op_dict
	return result_data
	

def get_input_structured_data(request):
	db = mongoConnection_to_ver2()
	po_number=request.GET.get('po_number')
	materialNumber=request.GET.get('MaterialNumber')
	SiteID=request.GET.get('site_id').encode('utf-8')
	analysis=request.GET.get('analysis')
	
	if analysis=='Finished Goods':
		table_cursor=mongoConnection_to_ver2().report_correlator_rca
	elif analysis=='Stagged PO':
		table_cursor=mongoConnection_to_ver2().report_correlator_staged_po
	else:
		print "default"
		
	environment_dict={}
	gen_info = ["SiteName","SBU","Technology","MaterialNumber","QualitySegment", "AvgQualityScore", "ProductionStartDateTime", "BatchNumber"]    
	pro_info = ["IsPhaseDelayed"]
	rawm_info = ["QualityAverage"]
	ccp_info = ["HighValue", "LowValue", "ActualValue"]
	operator_info = ["PhaseStartUserID" ]
	machine_info = [ "PhaseResourceName","PhaseResourceID"]

	if(po_number[0]=='0' ):
		result_cursor=table_cursor.find({'BatchNumber':po_number},{'_id':0,'BatchNumber':1,'SiteID':1}).limit(1)
		for  result in result_cursor:
			po_number=result['BatchNumber']
			SiteID=result['SiteID']

	result_cursor = table_cursor.find({'BatchNumber':po_number,'SiteID':SiteID}).limit(1)
	data_list1 = []
	for row in result_cursor:
		data_list1.append(row)  

	OutputScore_dict={}
	GeneralInfo_dict=OrderedDict()
	TimeLineIndicator_dict={}
	for r in data_list1:
		for k,v in r.iteritems():
			if k in gen_info:
				if(k=='ProductionStartDateTime'):
					GeneralInfo_dict['ProductionDate']=v.strftime('%d-%m-%Y')
					ProductionStartDateTime=v.strftime('%d-%m-%Y')
				else:
					GeneralInfo_dict[k]=v
			if(k=='OutputScore'):
					for kk,vv in v.iteritems():
						for kkk,vvv in vv.iteritems():
							if(kkk=='ActualValue'):
								OutputScore_dict[kk]=vvv        

	parentChildDict = {}
	parameterMappingCursor = db.config_correlator_parameterMapping.find({'ActiveFlag':1,'Config_Level':'InputRMParameterMapping','MaterialNumber':materialNumber},{'Groups':1,'_id':0})
	
	for doc in parameterMappingCursor:
		for group in doc['Groups']:
			parent = doc['Groups'][group]['MasterChild']
			parentChildDict[parent] = []
			for i in range(len(doc['Groups'][group]['ChildList'])):
				child=doc['Groups'][group]['ChildList'][i]['ParameterName']
				parentChildDict[parent].append(child)
					
	
	data = table_cursor.find({'POPhaseInfo':{'$ne':[]},'BatchNumber':po_number}).limit(1) 
		
	op_dict = {}
	op_dict['GeneralInfo'] = {}
	op_dict['Source_Info'] = {}
	op_dict['prepRMData'] = {}
	op_dict['ChangeOverAdherence'] = {}
	op_dict['manpower_data'] = {}
	op_dict['machine_data'] = {}
	phaseSerialList={}
	data_list = {}
	for row in data:
		data_list = row
	GeneralInfo_list = ['QualitySegment','MaterialNumber','SiteID','BatchNumber','LineID','Technology','ProductionStartDateTime','Line_Display','SiteName','AvgQualityScore', 'Material_Display']

	for key in GeneralInfo_list:
		if(key == 'ProductionStartDateTime'):
			op_dict['GeneralInfo'][key] = data_list[key].strftime('%d-%m-%Y')
		else:
			op_dict['GeneralInfo'][key] = data_list[key]

	if(data_list.has_key('MaterialNumber')):
		op_dict['GeneralInfo']['MaterialNumber'] = data_list['MaterialNumber']

	if(data_list.has_key('Reporting_Data')):
		op_dict['PhaseInfo']=data_list['Reporting_Data'].get('PhaseInfo',{})

		for phase in op_dict['PhaseInfo']:
			if 'Count' not in phase and 'StatusIndicator' not in phase:
				phaseSerial=data_list['POPhaseInfo'][phase]['PhaseSerial']
				op_dict['PhaseInfo'][phase]['PhaseSerial']= phaseSerial
				phaseSerialList[phaseSerial]=phase

		op_dict['phaseSerialList']=phaseSerialList
		
		op_dict['Output_Parameters']=data_list['Reporting_Data'].get('Output_Parameters',{})
	for key, value in op_dict['PhaseInfo'].iteritems():
		if isinstance(value, dict):
			for kk,vv in value.iteritems():
				if(kk=='ProcessInfo'):
					for kkk,vvv in vv.iteritems():
						if(kkk == 'CycleTimeAdherance'):
							for k4,v4 in vvv.iteritems():
								if(k4 == 'PhaseStartDateTime'):
									op_dict['PhaseInfo'][key]['ProcessInfo']['CycleTimeAdherance']['PhaseStartDateTime'] = v4.strftime('%d-%m-%Y %H:%M:%S')
								if(k4 == 'PhaseEndDateTime'):
									op_dict['PhaseInfo'][key]['ProcessInfo']['CycleTimeAdherance']['PhaseEndDateTime'] = v4.strftime('%d-%m-%Y %H:%M:%S')


				if(kk=='RawMaterialInfo'):
					if isinstance(vv, dict):
						for kkk,vvv in vv.iteritems():
							if isinstance(vvv, dict):
								for kk4,vv4 in vvv.iteritems():
									if(kk4 == 'BatchInfo'):
										for k4,v4 in vv4.iteritems():
											if isinstance(v4, dict):
												for k5,v5 in v4.iteritems():
													if(k5 == 'ExpiryDate'):
														try:
															v5_time = datetime.strptime(v5.replace('Z', '').replace('T', ' '), '%Y-%m-%d %H:%M:%S')
															op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = v5_time.strftime('%d-%m-%Y')
														except:
															op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = None
													if(k5 == 'ManufactureDate'):
														try:
															v5_time = datetime.strptime(v5.replace('Z', '').replace('T', ' '), '%Y-%m-%d %H:%M:%S')
															op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = v5_time.strftime('%d-%m-%Y')
														except:
															op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = None

			op_dict['prepRMData'][key] = prepRMData(data_list['Reporting_Data']['PhaseInfo'][key]['RawMaterialInfo'],parentChildDict)
			op_dict['ChangeOverAdherence'] = data_list['Reporting_Data']['ChangeOverAdherence']
			op_dict['Source_Info'][key] = {}
			op_dict['Source_Info'][key]['PhaseStartUserName'] = data_list['POPhaseInfo'][key]['PhaseStartUserName']
			op_dict['Source_Info'][key]['PhaseStartUserID'] = data_list['POPhaseInfo'][key]['PhaseStartUserID']
			#op_dict['manpower_data'][key] = getManPowerData(data_list['Reporting_Data']['PhaseInfo'][key]['ManPower'])
			op_dict['machine_data'][key] = getMachineData(data_list['Reporting_Data']['PhaseInfo'][key]['MachineData'])
	return StreamingHttpResponse(json.dumps(op_dict,default=json_util.default), content_type="json")
	
manpower={}

def getManPowerData(record):
	item = record.copy()
	#for item in obj:
	# del item['_id']
	#del item['StatusIndicator']
	#del item['StatusCount']

	Total={}

	for type in item:
		#del item[type]['StatusIndicator']
		#del item[type]['StatusCount']
		if isinstance(item[type],dict):
			a,b,c,d,o,data = {},{},{},{},{},{}
			#for mn_id in item[type]:
			data['Status_Indicator']=item[type]['StatusIndicator'].encode('utf-8')
			Total[type]=0
			for categories in item[type]:

				if categories=='A':
					a['name']='A'
					# a['color']='A'
					a['color']='#00b050'
					a['y']=item[type][categories]
					Total[type]=Total[type]+a['y']
				elif categories=='B':
					b['name']='B'
					# b['color']='B'
					b['color']='#ffff00'
					b['y']=item[type][categories]
					Total[type]=Total[type]+b['y']
				elif categories=='C':
					c['name']='C'
					#c['color']='C'
					c['color']='#f8963f'
					c['y']=item[type][categories]
					Total[type]=Total[type]+c['y']
				elif categories=='D':
					d['name']='D'
					# d['color']='D'
					d['color']='#ff1919'
					d['y']=item[type][categories]
					Total[type]=Total[type]+d['y']
				elif categories=='OffSpec':
					o['name']='OffSpec'
					# d['color']='D'
					o['color']='#d3d3d3'
					o['y']=item[type][categories]
					Total[type]=Total[type]+o['y']
			data[type.encode('utf-8')]=[a,b,c,d,o]
			#Total[type]=a['y']+b['y']+c['y']+d['y']
			manpower[type.encode('utf-8')]=data
			manpower['Total']=Total
	return manpower


def getMachineData(record):
    obj = record.copy()
    MachineData={}
    issue=0
    warning=0
    ok=0
    NA=0

    #del obj['Status_Count']
    #del obj['Status_Indicator']
    # del obj['_id']

    param_list=['LowValue','POLevelAverage','ReadingsCount','OPCMachine','StatusIndicator','SurgeAverage','SagAverage','HighValue','SurgeFrequency','SagFrequency','ProductLevelAverage','PO_Product_Deviation_Percentage']
    for item in obj:
        if isinstance(obj[item],dict):
            data={}
            for param in param_list:
                if param.lower()=='statusindicator' and 'StatusIndicator' in obj[item]:
                    if obj[item][param].lower()=='ok':
                        ok+=1
                    elif obj[item][param].lower()=='issue':
                        issue+=1
                    elif obj[item][param].lower()=='warning':
                        warning+=1
                    elif obj[item][param].lower()=='na':
                        NA+=1
                if param in obj[item]:
                    data[param]=obj[item][param]
                else:
                    data[param]='NA'
            MachineData[item]=data

    MachineData['Ok']=ok
    MachineData['Issue']=issue
    MachineData['Warning']=warning
    MachineData['NA']=NA
    return MachineData


def create_chart_data(data):
    result = []
    for key,value in data.iteritems():
        if key != 'Status_Indicator':
            result.append({
                "color":key,
                "y":value,
                "name":key
                })

    return result

	
data=[]
CriticalGainData = []

#for item in model_imp_data_list:		#looping internal list in model_imp_data_list
def getParentChildData(ip_data,flag,op):
	global data
	global CriticalGainData	
	
	del data[:]
	del CriticalGainData[:]
	
	for i_data in ip_data.keys():			#looping phase data one by one
		if i_data.find('QuantityAdherence')>=0:
			split_data_arr = i_data.split('|')
			data.append((str(split_data_arr[1]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[2]+flag)))
			data.append((str(split_data_arr[1]+'@temp'+split_data_arr[2]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[3]+flag)))
			GainData = {'RawMaterial':str(split_data_arr[1]),'CriticalParameter':str(split_data_arr[3]),'Gain':ip_data[i_data]}
			CriticalGainData.append(GainData)
			
		elif i_data.find('QualityAdherence')>=0:
			split_data_arr = i_data.split('|')
			data.append((str(split_data_arr[1]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[2]+flag)))
			data.append((str(split_data_arr[1]+'@temp'+split_data_arr[2]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[3]+flag)))
			data.append((str(split_data_arr[1]+'@temp'+split_data_arr[3]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[4]+flag)))
			GainData = {'RawMaterial':str(split_data_arr[1]),'CriticalParameter':str(split_data_arr[3]),'Gain':ip_data[i_data]}
			CriticalGainData.append(GainData)
		
		elif i_data.find('ShelfLifeAdherence')>=0:
			split_data_arr = i_data.split('|')
			data.append((str(split_data_arr[1]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[2]+flag)))
			data.append((str(split_data_arr[1]+'@temp'+split_data_arr[2]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[3]+flag+'@sl')))
			GainData = {'RawMaterial':str(split_data_arr[1]),'CriticalParameter':str(split_data_arr[3]),'Gain':ip_data[i_data]}
			CriticalGainData.append(GainData)
		
		elif i_data.find('CycleTimeAdherence')>=0:
			split_data_arr = i_data.split('|')
			data.append((str(split_data_arr[1]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[3]+flag)))
			data.append((str(split_data_arr[1]+'@temp'+split_data_arr[3]+flag),str(split_data_arr[1]+'@temp'+split_data_arr[4]+flag)))
			GainData = {'Phase':str(split_data_arr[1]),'CriticalParameter':str(split_data_arr[4]),'Gain':ip_data[i_data]}
			CriticalGainData.append(GainData)
		
		elif i_data.find('ChangeOverAdherence')>=0:
			split_data_arr = i_data.split('|')
			data.append((str(split_data_arr[1]+flag),str(split_data_arr[2]+flag)))
			GainData = {'Phase':str(split_data_arr[1]),'CriticalParameter':str(split_data_arr[2]),'Gain':ip_data[i_data]}
			CriticalGainData.append(GainData)
			
		elif i_data.find('MachineData')>=0:
			split_data_arr = i_data.split('|')
			
			data.append((str(split_data_arr[1]+flag),str(split_data_arr[1].encode('utf-8')+'@temp'+split_data_arr[3].encode('utf-8')+flag)))
			data.append((str(split_data_arr[1].encode('utf-8')+'@temp'+split_data_arr[3].encode('utf-8')+flag),str(split_data_arr[1].encode('utf-8')+'@temp'+split_data_arr[4]+flag)))
			GainData = {'Phase':str(split_data_arr[1]),'CriticalParameter':str(split_data_arr[4]),'Gain':ip_data[i_data]}
			CriticalGainData.append(GainData)
			
	return op

def getTreeStructure(data,flag,Category):
	parents, children = zip(*data)
	
	root_nodes = {x for x in parents if x not in children}
		
	for node in root_nodes:
		data.append((Category+flag, node))
		
	tree = get_nodes(Category+flag)
	return tree
		
def get_nodes(node):
	d = {}
	d['name'] = node
	children = get_children(node)
	if children:
		d['children'] = [get_nodes(child) for child in children]
	return d

def get_children(node):
	return [x[1] for x in data if x[0] == node]		

def getTreeChartData(request):
	#mat_num,param_name,phase
	mat_num = request.GET.get('mat_num')
	param_name = request.GET.get('selected_param')
	Category = request.GET.get('Category')
	#Category='RawMaterial'
	res = ''
	global data
	del data[:]

	model_data_list=[]
	model_imp_data_list=[]
	model_imp_data_dict={}
	parameter_list=[]
	
	GainDataList = []
	
	del model_data_list[:]
	del model_imp_data_list[:]
	del parameter_list[:]
	del GainDataList[:]
	
	#get the data from model_config_BKP & report_rootCauseAnalysis after applying filters material_number	
	model_data = mongoConnection_to_ver2().model_config.find({'LastModelBuiltFlag':1,'MaterialNumber':mat_num},{'Postmodelling_Output_Parameters':1,'_id':0})
	
	for doc in model_data:
		model_data_list.append(doc)
		
	model_data.close()
	
	#preparing model_imp_data_list
	for row in model_data_list:
		if row.has_key('Postmodelling_Output_Parameters'):
			for op in row['Postmodelling_Output_Parameters'].keys():
				if (param_name.lower() != 'overall'):
					if (op.lower().find(param_name.lower())>=0):
						for ip_data in row['Postmodelling_Output_Parameters'][op]['input_column'].keys():
							if (ip_data.find(Category)>=0):
								if (row['Postmodelling_Output_Parameters'][op]['input_column'][ip_data]['Importance']['Model_Importance'] == 'Y'):
									model_imp_data_dict[ip_data]=row['Postmodelling_Output_Parameters'][op]['input_column'][ip_data]['Importance']['Gain']
																		
									
				else:
					for ip_data in row['Postmodelling_Output_Parameters'][op]['input_column'].keys():
						if (ip_data.find(Category)>=0):
							if (row['Postmodelling_Output_Parameters'][op]['input_column'][ip_data]['Importance']['Model_Importance'] == 'Y'):
								model_imp_data_dict[ip_data]=row['Postmodelling_Output_Parameters'][op]['input_column'][ip_data]['Importance']['Gain']
							
	
	#To make data in the parent child form pass both the list to getParentChildData
	p_c_res = getParentChildData(model_imp_data_dict,'|Y',1)

	#To get unique values use set
	data = set(data)
	data = list(data)			
	
	
	#to get final data in tree structure hieracrhy form
	if len(data)>0:
		res=getTreeStructure(data,'|Y', Category)
	else:
		res=''
	
	# parameter list loop
	for row in model_data_list:
		if row.has_key('Postmodelling_Output_Parameters'):
			for op in row['Postmodelling_Output_Parameters'].keys():
				param=op.split('|')[1]
				if param not in parameter_list:
					parameter_list.append(param)
		
	f_tree_chart_result = json.dumps(res, indent=4)

	
	#replacing @qt,@ql,@sl with empty '' string
	f_tree_chart_result = f_tree_chart_result.replace('@qt','').replace('@ql','').replace('@sl','').replace('MachineData','CCP')
	
	
	result = {} 
	
	CriticalGainDataSorted  = sorted(CriticalGainData,key=itemgetter('Gain'),reverse=True)
	
	result['tree_chart'] = f_tree_chart_result
	result['parameter'] = parameter_list
	
	result['CriticalGainData'] = CriticalGainDataSorted
		
	return HttpResponse(json.dumps(result),content_type="json")
	

#tree_chart_hierarchy = getTreeChartData('1149529','overall','0030_B')

#f_tree_chart_result = json.dumps(tree_chart_hierarchy, indent=4)

#replacing @qt,@ql,@sl with empty '' string
#f_tree_chart_result = f_tree_chart_result.replace('@qt','').replace('@ql','').replace('@sl','')

#print f_tree_chart_result

	
# def create_chart_data(data):
	# result = []
	# for key,value in data.iteritems():
		# if key != 'Status_Indicator':
			# result.append({
				# "color":key,
				# "y":value,
				# "name":key
				# })

	# return result
	

	
def getRCATreeChartData(request):
    mat_num = request.GET.get('mat_num')
    param_name = request.GET.get('selected_param')
    phase = request.GET.get('phase_num')

    sys.setrecursionlimit(2000)
    global data
    #rca_data = db.report_rootCauseAnalysis.find({},{'_id':0,'Model_Data':1}).limit(1)
    '''url = "mongodb://henkelUser:QpQc#123@193.96.100.95/correlator_ProdDump?authMechanism=SCRAM-SHA-1"
    conn = pymongo.MongoClient(url)
    table_cursor=conn.correlator_ProdDump'''
    table_cursor=mongoConnection_to_ver2()
    model_imp_data = table_cursor.config_correlator_model_importance.find({},{'_id':0,'Feature':1}).limit(1)
    rca_data = table_cursor.report_correlator_rca.find({},{'_id':0,'Model_Data':1}).limit(1)

    rca_data_list=[]
    model_imp_data_list=[]
    

    for row in rca_data:
        rca_data_keys = row['Model_Data'].keys()
        for item in rca_data_keys:
            rca_data_list.append(item)

    for row in model_imp_data:
        model_imp_data_list.append(row['Feature'])
        
    # remove data from rca_data_list which is already in model_imp_data_list
    rca_data_list = list(set(rca_data_list)-set(model_imp_data_list))

    res1 = getParentChildData(model_imp_data_list,'|Y',1)
   # res2 = getParentChildData(rca_data_list,'',2)

    data = set(data)
    data=list(data)         

    res=getTreeStructure(data)
    return res
	

def get_rca_structured_data(request):

	print "#######dvndjifnvjdfn#############################################################################"
	print "#######dvndjifnvjdfn#############################################################################"
	print "#######dvndjifnvjdfn#############################################################################"
	db=mongoConnection_to_ver2()
	module_name = request.GET.get('module')
	materialNumber = request.GET.get('materialNumber')
	po_number=request.GET.get('po_number').encode("utf-8")
	SiteID=request.GET.get('site_id').encode('utf-8')
	environment_dict={}

	gen_info = ["SiteName","SBU","Technology","MaterialNumber","QualitySegment", "AvgQualityScore", "ProductionStartDateTime", "BatchNumber"]    
	pro_info = ["IsPhaseDelayed"]
	rawm_info = ["QualityAverage"]
	ccp_info = ["HighValue", "LowValue", "ActualValue"]
	operator_info = ["PhaseStartUserID" ]
	machine_info = [ "PhaseResourceName","PhaseResourceID"]
	
	result_cursor_config_default = mongoConnection_to_ver2().config_correlator_defaults.find_one({'Config_Level':'BinaryDisplayTags'})
	
	if module_name == 'RCA':	
		'''url = "mongodb://correlatorUser1:QpQc#123@193.96.100.95/correlator_ProdDump?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=mongoConnection_to_ver2().report_correlator_rca
		
	elif module_name == 'SPO':
		'''url = "mongodb://correlatorUser1:QpQc#123@193.96.100.95/correlator_ProdDump?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		table_cursor=mongoConnection_to_ver2().report_correlator_staged_po
		
		pred_history = fetchPredictionHistory(po_number)

	if(po_number[0]=='0' ):
		result_cursor=table_cursor.find({'BatchNumber':po_number},{'_id':0,'BatchNumber':1,'SiteID':1}).limit(1)
		for  result in result_cursor:
			po_number=result['BatchNumber']
			SiteID=result['SiteID']

	result_cursor = table_cursor.find({'BatchNumber':po_number,'SiteID':SiteID}).limit(1)
	data_list1 = []
	for row in result_cursor:
		data_list1.append(row)  

	OutputScore_dict={}
	GeneralInfo_dict=OrderedDict()
	TimeLineIndicator_dict={}
	for r in data_list1:
		for k,v in r.iteritems():
			if k in gen_info:
				if((k=='ProductionStartDateTime') and (v != 'NA')):
					GeneralInfo_dict['ProductionDate']=v.strftime('%d-%m-%Y')
					ProductionStartDateTime=v.strftime('%d-%m-%Y')
				else:
					GeneralInfo_dict[k]=v
			if(k=='OutputScore'):
					for kk,vv in v.iteritems():
						for kkk,vvv in vv.iteritems():
							if(kkk=='ActualValue'):
								OutputScore_dict[kk]=vvv        

	
	if module_name == 'RCA':	
		#url = "mongodb://henkel_cor_User1:QpQc#123@193.96.100.95/correlator_ver1?authMechanism=SCRAM-SHA-1"
		'''url = "mongodb://correlatorUser1:QpQc#123@193.96.100.95/correlator_ProdDump?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		#table_cursor=conn.correlator_ver1.report_rootCauseAnalysis
		table_cursor=mongoConnection_to_ver2().report_correlator_rca
		GeneralInfo_list = ['QualitySegment','MaterialNumber','SiteID','BatchNumber','LineID','Technology','ProductionStartDateTime','Line_Display','SiteName','AvgQualityScore', 'Material_Display']
		
	elif module_name == 'SPO':
		'''url = "mongodb://correlatorUser1:QpQc#123@193.96.100.95/correlator_ProdDump?authMechanism=SCRAM-SHA-1"
		conn = MongoClient(url)'''
		#table_cursor=conn.correlator_ver1.report_rootCauseAnalysis
		table_cursor=mongoConnection_to_ver2().report_correlator_staged_po
		GeneralInfo_list = ['PredictedQualitySegment','MaterialNumber','SiteID','BatchNumber','LineID','Technology','ProductionStartDateTime','Line_Display','SiteName','PredictedAvgQualityScore', 'Material_Display']
		
	parentChildDict = {}
	parameterMappingCursor = db.config_correlator_parameterMapping.find({'ActiveFlag':1,'Config_Level':'InputRMParameterMapping','MaterialNumber':materialNumber},{'Groups':1,'_id':0})
	
	for doc in parameterMappingCursor:
		for group in doc['Groups']:
			parent = doc['Groups'][group]['MasterChild']
			parentChildDict[parent] = []
			for i in range(len(doc['Groups'][group]['ChildList'])):
				child=doc['Groups'][group]['ChildList'][i]['ParameterName']
				parentChildDict[parent].append(child)
				
	data = table_cursor.find({'POPhaseInfo':{'$ne':[]},'BatchNumber':po_number}).limit(1) 
	op_dict = {}
	config_defaults = {}
	op_dict['GeneralInfo'] = {}
	op_dict['encrpyted_username']={}
	op_dict['Source_Info'] = {}
	op_dict['prepRMData'] = {}
	op_dict['dataIssueReason'] = {}
	op_dict['ChangeOverAdherence'] = {}
	op_dict['manpower_data'] = {}
	op_dict['machine_data'] = {}
	phaseSerialList={}
	data_list = {}
	for row in data:
		data_list = row
	# GeneralInfo_list = ['QualitySegment','MaterialNumber','SiteID','BatchNumber','LineID','Technology','ProductionStartDateTime','ProductionEndDateTime']
	#GeneralInfo_list = ['QualitySegment','MaterialNumber','SiteID','BatchNumber','LineID','Technology','ProductionStartDateTime','Line_Display','SiteName','AvgQualityScore', 'Material_Display']
	for key in GeneralInfo_list:
		if(key == 'ProductionStartDateTime' and data_list[key] != 'NA'):
			op_dict['GeneralInfo'][key] = data_list[key].strftime('%d-%m-%Y')
		else:
			op_dict['GeneralInfo'][key] = data_list[key]

	if(data_list.has_key('MaterialNumber')):
		op_dict['GeneralInfo']['MaterialNumber'] = data_list['MaterialNumber']

	if(data_list.has_key('Reporting_Data')):
		op_dict['PhaseInfo']=data_list['Reporting_Data'].get('PhaseInfo',{})

		for phase in op_dict['PhaseInfo']:
			if 'Count' not in phase and 'StatusIndicator' not in phase:
				phaseSerial=data_list['POPhaseInfo'][phase]['PhaseSerial']
				op_dict['PhaseInfo'][phase]['PhaseSerial']= phaseSerial
				phaseSerialList[phaseSerial]=phase

		op_dict['phaseSerialList']=phaseSerialList
		op_dict['Output_Parameters']=data_list['Reporting_Data'].get('Output_Parameters',{})
		
		if module_name == 'SPO':
			op_dict['pred_history'] = pred_history
		
	for key, value in op_dict['PhaseInfo'].iteritems():
		if isinstance(value, dict):
			for kk,vv in value.iteritems():
				if(kk=='ProcessInfo'):
					for kkk,vvv in vv.iteritems():
						if(kkk == 'CycleTimeAdherance'):							
							op_dict['PhaseInfo'][key]['ProcessInfo']['CycleTimeAdherance']['PhaseStartDateTime'] = vvv['PhaseStartDateTime'].strftime('%Y-%m-%d %H:%M:%S')
							op_dict['PhaseInfo'][key]['ProcessInfo']['CycleTimeAdherance']['PhaseEndDateTime'] = vvv['PhaseEndDateTime'].strftime('%Y-%m-%d %H:%M:%S')

			if('ProcessInfo' in value):
				if('CycleTimeAdherance' in value['ProcessInfo']):							
					op_dict['PhaseInfo'][key]['ProcessInfo']['CycleTimeAdherance']['PhaseStartDateTime'] = value['ProcessInfo']['CycleTimeAdherance']['PhaseStartDateTime'].strftime('%Y-%m-%d %H:%M:%S')
					op_dict['PhaseInfo'][key]['ProcessInfo']['CycleTimeAdherance']['PhaseEndDateTime'] = value['ProcessInfo']['CycleTimeAdherance'].strftime('%Y-%m-%d %H:%M:%S')
							
							

			if('RawMaterialInfo' in value):
				#if isinstance(value['RawMaterialInfo'], dict):
				if len(value['RawMaterialInfo'].keys())>1:

					for kkk,vvv in value.iteritems():
						if isinstance(vvv, dict):
							for kk4,vv4 in vvv.iteritems():
								if('BatchInfo' in vvv):
									for k4,v4 in vv4.iteritems():
										if isinstance(v4, dict):
											if('ExpiryDate' in v4):
												try:
													v5_time = datetime.strptime(v5.replace('Z', '').replace('T', ' '), '%Y-%m-%d %H:%M:%S')
													op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = v5_time.strftime('%d-%m-%Y')

												except:
													op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = None
											if('ManufactureDate' in v4):
												try:
													v5_time = datetime.strptime(v5.replace('Z', '').replace('T', ' '), '%Y-%m-%d %H:%M:%S')
													op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = v5_time.strftime('%d-%m-%Y')
													# op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ManufactureDate'] = dateutil.parser.parse(v5).strftime('%d-%m-%Y')
												except:
													op_dict['PhaseInfo'][key]['RawMaterialInfo'][kkk]['BatchInfo'][k4]['ExpiryDate'] = None

			op_dict['prepRMData'][key] = prepRMData(data_list['Reporting_Data']['PhaseInfo'][key]['RawMaterialInfo'], parentChildDict)
#			if data_list['Reporting_Data']['ChangeOverAdherence']['PreviousPO']!='Y':
#				link='<a class="linking_mes_correlator" title="MES Link" href="http://ap-hipas.henkelgroup.net/ipas/ipas_Preweigh/Manufacture/ViewManufacturePO.aspx?id='+ r['SiteID']+'&processorder='+data_list['Reporting_Data']['ChangeOverAdherence']['PreviousPO']+'&archive=true&3app='+app+'&username='+encoded_username_url+'" target="blank" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'
#			else:
#				link='<a class="linking_mes_correlator no_data_available_mes" title="MES Link" href="#" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'		
			
			op_dict['ChangeOverAdherence'] = data_list['Reporting_Data']['ChangeOverAdherence']
			op_dict['Source_Info'][key] = {}
			op_dict['Source_Info'][key]['PhaseStartUserName'] = data_list['POPhaseInfo'][key]['PhaseStartUserName']
			op_dict['Source_Info'][key]['PhaseStartUserID'] = data_list['POPhaseInfo'][key]['PhaseStartUserID']
			op_dict['manpower_data'][key] = copy.deepcopy(getManPowerData(data_list['Reporting_Data']['PhaseInfo'][key]['ManPower']))
			op_dict['machine_data'][key] = getMachineData(data_list['Reporting_Data']['PhaseInfo'][key]['MachineData'])
			op_dict['critical_params'] = getCriticalParameterCount(data_list['MaterialNumber'])
			username = request.session['username']
			encrpytion_key = "71079162"
			encrpyted_username = AESencrypt(encrpytion_key, username)
			op_dict['encrpyted_username']=encrpyted_username
			config_defaults['ChangeOverDone'] = op_dict['ChangeOverAdherence']['ChangeOverDone']
			config_defaults['ChangeOverRequired'] = op_dict['ChangeOverAdherence']['ChangeOverRequired']
			catch_returnDisplayTagBinary = returnDisplayTagBinary(result_cursor_config_default,config_defaults)
			op_dict['ChangeOverAdherence']['ChangeOverRequired'] = catch_returnDisplayTagBinary['ChangeOverRequired']
			op_dict['ChangeOverAdherence']['ChangeOverDone'] = catch_returnDisplayTagBinary['ChangeOverDone']
			previous_exec_mes = getMESExecutionFlag(po_number,module_name)
			op_dict['ChangeOverAdherence']['ExecutionInfoExistsInMES'] = previous_exec_mes['ExecutionInfoExistsInMES']
			
	if module_name == 'RCA':
		op_dict['dataIssueReason'] = dataIssueReason(po_number)
	
	return StreamingHttpResponse(json.dumps(op_dict,default=json_util.default), content_type="json")

	
def getMESExecutionFlag(batchNumber,module_name):
	if(module_name == 'RCA'):
		MesExecutionFlag = mongoConnection_to_ver2().report_correlator_rca.find_one({'BatchNumber':batchNumber},{'ExecutionInfoExistsInMES':1})
		print MesExecutionFlag
	elif(module_name == 'SPO'):
		MesExecutionFlag = mongoConnection_to_ver2().report_correlator_staged_po.find_one({'BatchNumber':batchNumber},{'ExecutionInfoExistsInMES':1})
	return MesExecutionFlag

def dataIssueReason(BatchNumber):

	DataIssueReason_cursor = mongoConnection_to_ver2().report_correlator_rca.find_one({'BatchNumber':BatchNumber},{'DataIssueReason':1,'_id':0})

	DataIssueReasonList = []
	
	DataIssueReason_cursor['DataIssueCount']=5
	
	for DataIssueReason in DataIssueReason_cursor:
		DataIssueReasonList.append(DataIssueReason)

	return DataIssueReason_cursor
	

'''
funciton to convert the N to 'No' and Y to 'Yes'
'''
def returnDisplayTagBinary(configDisplayTags,Data):
    processedData={}
    for key in Data:
        if Data[key] in configDisplayTags['DisplayTags']:
            processedData[key]=configDisplayTags['DisplayTags'][Data[key]]
        else:
            processedData[key]=Data[key]
    return processedData
	
	
def fetchPredictionHistory(batchNumber):
	'''url = "mongodb://correlatorUser1:QpQc#123@193.96.100.95/correlator_ProdDump?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	table_cursor=conn.correlator_ProdDump.history_correlator_prediction'''
	table_cursor=mongoConnection_to_ver2().history_correlator_prediction
	predHistory=table_cursor.find_one({'ActiveFlag':1,'BatchNumber':batchNumber})
	processedHistory={}
	
	for prediction in predHistory['Prediction_History']:
		##Fetch Last Phase Completed(Phase with Max PhaseSerial)
		maxSerial=0
		maxPhase=''
		for phase in prediction['Phases_Completed']:
			if phase['PhaseName'] not in processedHistory:
				processedHistory[phase['PhaseName']]='NA'
			if phase['PhaseSerial']>maxSerial and phase['PhaseCompleted']=='Y':
				maxSerial=phase['PhaseSerial']
				maxPhase=phase['PhaseName']
		processedHistory[maxPhase]={}
		processedHistory[maxPhase]['PredictedQualityScore']=prediction['PredictedQualityScore']
		processedHistory[maxPhase]['PredictedQualitySegment']=prediction['PredictedQualitySegment']
		processedHistory[maxPhase]['TimeOfPrediction']=prediction['Time_Of_Prediction']
		processedHistory[maxPhase]['OutputScore']={}
		for parameter in prediction['OutputScore']:
			if prediction['OutputScore'][parameter].has_key('Score'):
				if prediction['OutputScore'][parameter]['Score']!='NA':
				#if prediction['OutputScore'][parameter]['PredictedQualityScore']!='NA':
					processedHistory[maxPhase]['OutputScore'][parameter]=prediction['OutputScore'][parameter]
	
	return processedHistory
	
def feedback_overall(request):
	feedback_all_1 = request.GET.get('feedback_all')
	feedback_all=ast.literal_eval(feedback_all_1)
	model_data = mongoConnection_to_ver2().rca_feedback.insert(feedback_all)
	return HttpResponse(json.dumps('1'), content_type="json")

def getCriticalParameterCount(mat_num):
	model_data_list=[]
	model_imp_data_list=[]
	#critical_param_list = []
	del model_data_list[:]
	del model_imp_data_list[:]
	#get the data from model_config & report_rootCauseAnalysis after applying filters material_number and parameter
	
	model_data = mongoConnection_to_ver2().model_config.find({'Postmodelling_Output_Parameters':{'$exists':True},'LastModelBuiltFlag':1,'MaterialNumber':mat_num},{'Postmodelling_Output_Parameters':1,'_id':0})
		
	for doc in model_data:
		model_data_list.append(doc)
	model_data.close()

	#preparing model_imp_data_list
	for row in model_data_list:
		if row.has_key('Postmodelling_Output_Parameters'):
			for op in row['Postmodelling_Output_Parameters'].keys():
				for ip_data in row['Postmodelling_Output_Parameters'][op]['input_column'].keys():
					if (row['Postmodelling_Output_Parameters'][op]['input_column'][ip_data]['Importance']['Model_Importance'] == 'Y'):
						model_imp_data_list.append(ip_data)
						
	model_imp_data_list = set(model_imp_data_list)					
	return list(model_imp_data_list)
		
	
def get_staggedpos_structured_data(request):
	po_number=request.GET.get('po_number')
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	qualityscore=0
	table_cursor=conn.henkel.src_StagedProcessOrders
	data = table_cursor.find({'StagingPhaseInfo':{'$ne':[]},'BatchNumber':po_number}).limit(1)
	data_list = []
	for row in data:
		data_list.append(row)
	gen_info = ["ProcessOrderNumber", "SiteID", "SiteName", "MaterialNumber", "SBU", "Technology"]    
	pro_info = ["IsPhaseDelayed", "ChangeOverDone"]
	rawm_info = ["QualityAverage"]
	ccp_info = ["ActualValue"]
	operator_info = ["PhaseStartUserID" , "PhaseStartUserName"]
	machine_info = [ "PhaseResourceName","PhaseResourceID"]

	for r in data_list:
		op_dict = {}
		op_dict['GeneralInfo'] = {}
		for k,v in r.iteritems():
			if k == 'StagingPhaseInfo':
					for kk, vv in v.iteritems():
						op_dict[kk] = {}
						op_dict[kk]['RawMaterialInfo'] = {}
						for kp, kv in vv.iteritems():
							if kp == 'RawMaterialInfo':
								 for kd, pd in kv.iteritems():
										 op_dict[kk]['RawMaterialInfo'][kd] = {}
										 for kdd, pdd in pd.iteritems():
											 if kdd in rawm_info:
													for eq_key,eq_val in pd[kdd].iteritems():
																op_dict[kk]['RawMaterialInfo'][kd][eq_key]=eq_val	

	result_data={'op_dict':op_dict}
	return HttpResponse(json.dumps(result_data), content_type="json")


@csrf_exempt
def compare_actual_predicted(request):
	plant_list=request.GET.getlist('plant_list[]')
	sbu_list=request.GET.getlist('sbu_list[]')
	technology_list=request.GET.getlist('technology_list[]')
	product_list=request.GET.getlist('product_list[]')
	date_list=request.GET.getlist('date_list[]')
	
	if(len(date_list)==2):
		StartDate='c("'+datetime.strptime(date_list[0], "%Y-%m-%d").strftime("%Y-%m-%d %H:%M:%S")+'")'
		EndDate='c("'+datetime.strptime(date_list[1], "%Y-%m-%d").strftime("%Y-%m-%d %H:%M:%S")+'")'
	else:
		StartDate='c("'+datetime.strptime(date_list[0], "%Y-%m-%d").strftime("%Y-%m-%d %H:%M:%S")+'")'
		EndDate='c("'+datetime.strptime(date_list[0], "%Y-%m-%d").strftime("%Y-%m-%d %H:%M:%S")+'")'
	
	SiteID=''
	if(len(plant_list)>0):
		string_val=''
		for i in range(0,len(plant_list)):
			if(i!=len(plant_list)-1):
				string_val=string_val+'"'+str(plant_list[i])+'",'
			else:
				string_val=string_val+'"'+str(plant_list[i])+'"'
		SiteID="c("+string_val+")"	
	
	SBU=''
	if(len(sbu_list)>0):
		string_val=''
		for i in range(0,len(sbu_list)):
			if(i!=len(sbu_list)-1):
				string_val=string_val+'"'+str(sbu_list[i])+'",'
			else:
				string_val=string_val+'"'+str(sbu_list[i])+'"'
		SBU="c("+string_val+")"	
		
		
	Technology=''
	if(len(technology_list)>0):
		string_val=''
		for i in range(0,len(technology_list)):
			if(i!=len(technology_list)-1):
				string_val=string_val+'"'+str(technology_list[i])+'",'
			else:
				string_val=string_val+'"'+str(technology_list[i])+'"'
		Technology="c("+string_val+")"	
		
	
	MaterialNumber=''
	if(len(product_list)>0):
		string_val=''
		for i in range(0,len(product_list)):
			if(i!=len(product_list)-1):
				string_val=string_val+'"'+str(product_list[i])+'",'
			else:
				string_val=string_val+'"'+str(product_list[i])+'"'
		MaterialNumber="c("+string_val+")"		
	

	url='https://henkelpilot.westeurope.cloudapp.azure.com/ocpu/library/correlator/R/conf_matrix'
	data = {'SiteID':SiteID, 'SBU':SBU,'Technology':Technology,'MaterialNumber':MaterialNumber,'StartDate':StartDate,'EndDate':EndDate}

	res = requests.post(url, data=data,verify=False)
	res_url='https://henkelpilot.westeurope.cloudapp.azure.com'+res .content[0:res .content.find('val')+3]+'/json'
	parameters_res=requests.get(res_url,verify=False)
	result_data=[]
	if(ast.literal_eval(parameters_res.text.encode('utf-8'))[0]!=0):
		parameters_json=ast.literal_eval(ast.literal_eval(parameters_res.text.encode('utf-8'))[0])
		
		segment_mapping={'E':0,'G':1,'B':2,'C':3}
		if(len(parameters_json.keys())>0):
			outer_flagging={'E':0,'G':0,'B':0,'C':0}
			for k,v in parameters_json.iteritems():
				inner_flagging={'E':0,'G':0,'B':0,'C':0}
				outer_flagging[k]=1
				for kk,vv in v.iteritems():
					result_data.append([segment_mapping[k],segment_mapping[kk],vv])
					inner_flagging[kk]=1
				for key,val in inner_flagging.iteritems():
					if(val==0):
						result_data.append([segment_mapping[k],segment_mapping[key],0])
			for key,val in outer_flagging.iteritems():
				if(val==0):
					result_data.append([segment_mapping[key],0,0])
					result_data.append([segment_mapping[key],1,0])
					result_data.append([segment_mapping[key],2,0])
					result_data.append([segment_mapping[key],3,0])

	return HttpResponse(json.dumps(result_data), content_type="json")
	
def get_threshold_data(request):
	result_list=[]
	product_list=request.GET.get('product_list')
	product_cond=product_list
	result_cursor=mongoConnection_to_ver2().rule_correlator_aggregate_count.find({'MaterialNumber':product_cond,'ActiveFlag':1},{'_id':0,'Material_Display':1,'MaterialNumber':1,'Optimal_Value.UpdatedBy':1,'Optimal_Value.UpdatedDate':1,'Optimal_Value.Optimal_Value_Rule_Revision_Count':1,'Model_Training_Product_Level.UpdatedBy':1,'Model_Training_Product_Level.UpdatedDate':1,'Model_Training_Product_Level.Model_Rule_Revision_Count':1})
	
	for r in result_cursor:
		result_list.append(r)
	return HttpResponse(json.dumps(result_list,default=json_util.default), content_type="application/json")

def get_weight_data(request):
	result_list=[]
	product_list=request.GET.get('product_list')
	Config_Level=request.GET.get('Config_Level')
	product_cond=product_list
	if Config_Level=='RawMaterialConfig':
		result_cursor=mongoConnection_to_ver2().config_correlator_rca.aggregate([{'$match':{'MaterialNumber':product_cond,'Config_Level':Config_Level,'ActiveFlag':1}},{'$project':{'_id':0,'Phase':1,'RawMaterial_Display':1,'QualityParameter':1,'OptimalValue':1,'UpperLimit':1,'LowerLimit':1,'UpdatedDatetime':1,'UpdatedBy':1}}])
	else:
		result_cursor=mongoConnection_to_ver2().config_correlator_rca.aggregate([{'$match':{'MaterialNumber':product_cond,'Config_Level':Config_Level,'ActiveFlag':1}},{'$project':{'_id':0,'Phase':1,'MachineParameter':1,'OptimalValue':1,'UpperLimit':1,'LowerLimit':1,'UpdatedDatetime':1,'UpdatedBy':1}}])
	for r in result_cursor:
		result_list.append(r)
		
	return HttpResponse(json.dumps(result_list,default=json_util.default), content_type="application/json")	
	
def get_segment_data(request):
	result_list=[]
	product_list=request.GET.get('product_no')
	Config_Level=request.GET.get('Config_Level')
	Config_Name = request.GET.get('product_name')
	model_config_cursor =  mongoConnection_to_ver2().model_config.find({'MaterialNumber':product_list,'ActiveFlag':1},{'Premodelling_Input_Parameters':1,'_id':0})
	
	model_config_dict={}
	for vector in model_config_cursor:
		for key,val in vector['Premodelling_Input_Parameters'].iteritems():
			model_config_dict[key]=val
	model_config_cursor.close()
	
	input_list=[]
	
	for item in model_config_dict.keys():

		if item.find(Config_Level)>0 and model_config_dict[item]['DefaultUsageFlag']=='Y' and  model_config_dict[item]['ActiveFlag']==1:
			
			#d={}
			d=OrderedDict()
			vector=item.split('|')

			if Config_Level=='ProcessInfo':
				d['item_str'] = item
				d['Phase']=vector[1]
				d['ProductNumber']=product_list
				d['ProductName']=Config_Name
				d['Vector']=vector[-1]
				d['PreModelUsageDecisionFlag']=model_config_dict[item]['PreModelUsageDecisionFlag']
				d['UpdatedDate']=model_config_dict[item]['UpdatedDateTime']
				d['UpdatedBy']=model_config_dict[item]['UpdatedBy']
				
			elif Config_Level=='MachineData':
				d['item_str'] = item
				d['Phase']=vector[1]
				d['ProductNumber']=product_list
				d['ProductName']=Config_Name
				d['ParameterName']=vector[-2]
				d['Vector']=vector[-1]
				d['PreModelUsageDecisionFlag']=model_config_dict[item]['PreModelUsageDecisionFlag']
				d['UpdatedDate']=model_config_dict[item]['UpdatedDateTime']
				d['UpdatedBy']=model_config_dict[item]['UpdatedBy']
	
			elif Config_Level=='RawMaterial':
				d['item_str'] = item
				#d['Phase']=vector[1]
				d['ProductNumber']=product_list
				d['ProductName']=Config_Name
				d['vector']=vector[-1]
				#d['VectorCategory']=vector[4]
				d['VectorCategory']=vector[2]
				d['UpdatedDate']=model_config_dict[item]['UpdatedDateTime']
				d['UpdatedBy']=model_config_dict[item]['UpdatedBy']
	
				if vector[2]=='QualityAdherence':
					d['ParameterName']=vector[-2]				
				else:
					d['ParameterName']='NA'
					
				#d['RawMaterialName']=vector[3]
				d['RawMaterialName']=vector[1]
				d['PreModelUsageDecisionFlag']=model_config_dict[item]['PreModelUsageDecisionFlag']
				
			input_list.append(d)

	return HttpResponse(json.dumps(input_list,default=json_util.default), content_type="application/json")
	

	
def get_segment_output_data(request):
	
	result_list=[]
	product_list=request.GET.get('product_no')
	config_Name = request.GET.get('product_name')
	
	model_config_cursor = mongoConnection_to_ver2().model_config.find({'MaterialNumber':product_list,'ActiveFlag':1},{'Premodelling_Output_Parameters':1,'ActiveFlag':1,'_id':0})
	#model_config_cursor = connection_config_premodel_BKP().find({'MaterialNumber':product_list},{'Premodelling_Output_Parameters':1,'ActiveFlag':1,'_id':0})
	
	model_config_dict={}
	for vector in model_config_cursor:
		for key,val in vector['Premodelling_Output_Parameters'].iteritems():
			model_config_dict[key]=val
	model_config_cursor.close()
	
	
	output_list=[]
	
	
	for item in model_config_dict.keys():
		
		if model_config_dict[item]['DefaultUsageFlag']=='Y' and  model_config_dict[item]['ActiveFlag']==1:
			
			d=OrderedDict()
			vector=item.split('|')
			d['item_str'] = item			
			d['ProductNumber']=product_list
			d['ProductName']=config_Name
			d['vector']=vector[1]
			d['ParameterName']=vector[-1]
			d['PreModelUsageDecisionFlag']=model_config_dict[item]['PreModelUsageFlag']
			d['UpdatedDate']=model_config_dict[item]['UpdatedDateTime']
			d['UpdatedBy']=model_config_dict[item]['UpdatedBy']
			
			output_list.append(d)

	
		
	return HttpResponse(json.dumps(output_list,default=json_util.default), content_type="application/json")	
def fetchRMList(request):
	product=request.GET.get('product')
	batchnumber=request.GET.get('batchnumber')
	phase_name=request.GET.get('phase_name')
	analysis=request.GET.get('analysis')
	if analysis=='Finished Goods':
		sourceCollectionName='report_correlator_rca'
	elif analysis=='Stagged PO':
		sourceCollectionName='report_correlator_staged_po'
	filters_dict={}	
	ParamDict={}
	#ParamList=[]
	
	db=mongoConnection_to_ver2()

	optimalDataCursor=db.config_correlator_rca.find({'MaterialNumber':product,'Phase':phase_name,'Config_Level':'RawMaterialConfig','ActiveFlag':1})
	
	RMDict={}
	RMList=[]
	
	for RMData in optimalDataCursor:
		if RMData['RawMaterialNumber'] not in RMDict:
			newDict={}
			newDict['ID']=RMData['RawMaterialNumber']
			newDict['Display']=RMData['RawMaterial_Display']
			newDict['SelectedRM']=0
			RMDict[RMData['RawMaterialNumber']]=newDict.copy()
	optimalDataCursor.close()
	
	if analysis!='Default':
		po_data=db[sourceCollectionName].find_one({'BatchNumber':batchnumber})
		for RM in po_data['Analysis_Parameters']['PhaseInfo'][phase_name]['RawMaterialInfo']:
			if 'Count' not in RM and 'Indicator' not in RM:
				if RM not in RMDict:
					RMData=po_data['Analysis_Parameters']['PhaseInfo'][phase_name]['RawMaterialInfo'][RM]
					newDict={}
					newDict['ID']=RM
					newDict['Display']=RMData['MaterialDescription']
					newDict['SelectedRM']=1
					RMDict[RM]=newDict.copy()
				else:
					RMDict[RM]['SelectedRM']=1
	for RM in RMDict:
		RMList.append(RMDict[RM])
	return HttpResponse(json.dumps(RMList), content_type="json")	

def myconverter(o):
    if isinstance(o, datetime):
        return o.__str__()
	
def fetchRMDetails(request):
	product=request.GET.get('product')
	batchNumber=request.GET.get('batchNumber')
	phase=request.GET.get('phase_name')
	analysis=request.GET.get('analysis')
	ExistingRMList=request.GET.getlist('ExistingRMList[]')
	SelectedRMList=request.GET.getlist('SelectedRMList[]')
	if analysis=='Finished Goods':
		sourceCollectionName='report_correlator_rca'
	elif analysis=='Stagged PO':
		sourceCollectionName='report_correlator_staged_po'
		
	toProcessList=list(set(SelectedRMList)-set(ExistingRMList))
	#toProcessList=set(SelectedRMList).symmetric_difference(set(ExistingRMList))
	resultDict={}	
	
	db=mongoConnection_to_ver2()
	po_data=db[sourceCollectionName].find_one({'BatchNumber':batchNumber})
	for RM in toProcessList:
		if RM in po_data['Reporting_Data']['PhaseInfo'][phase]['RawMaterialInfo']:
			resultDict[RM]=po_data['Reporting_Data']['PhaseInfo'][phase]['RawMaterialInfo'][RM].copy()		
			for param in resultDict[RM]['QualityAdherence']:
				if isinstance(resultDict[RM]['QualityAdherence'][param],dict):
					resultDict[RM]['QualityAdherence'][param]['Display_Value']=resultDict[RM]['QualityAdherence'][param]['POLevelAverage']
		else:	
			##Fetch From Optimal Value/Default Vector Values
			resultDict[RM]={}
			resultDict[RM]['QuantityAdherence']={}
			resultDict[RM]['QuantityAdherence']={}
			resultDict[RM]['QuantityAdherence']={}
			
			##QuantityAdherence Default Values
			QuantityAdherence={}
			QuantityAdherence['Quantity_Tolereance_Flag']='Y'
			QuantityAdherence['ToleranceQty']=0
			QuantityAdherence['Quantity_Deviation_Percentage']=0
			QuantityAdherence['RequiredQuantity']=0
			QuantityAdherence['ConsumedQuantity']=0
			QuantityAdherence['BatchCount']=0
			QuantityAdherence['Unit']='NA'
			
			##ShelfLifeAdherence Default Values
			ShelfLifeAdherence={}
			ShelfLifeAdherence['RemainingShelfLife_Percentage']=100
			ShelfLifeAdherence['TotalShelfLife']=365
			ShelfLifeAdherence['RemainingShelfLife_Days']=365
			
			##QualityAdherence Default Values
			
			##Fetch Optimal Values
			optimalDataCursor=db.config_correlator_rca.find({'MaterialNumber':product,'Phase':phase,'RawMaterialNumber':RM,'Config_Level':'RawMaterialConfig','ActiveFlag':1})
			QualityAdherence={}
			for param in optimalDataCursor:
				paramName=param['QualityParameter']
				del param['EndDateTime']
				del param['StartDateTime']
				del param['CreatedDateTime']
				del param['_id']
				QualityAdherence[paramName]=param
				
				QualityAdherence[paramName]['Display_Value']=QualityAdherence[paramName]['OptimalValue']
				
				resultDict[RM]['MaterialNumber']=param['RawMaterialNumber']
				resultDict[RM]['MaterialDescription']=param['RawMaterialDescription']
				resultDict[RM]['Material_Display']=param['RawMaterial_Display']
			
			resultDict[RM]['QuantityAdherence']=QuantityAdherence
			resultDict[RM]['ShelfLifeAdherence']=ShelfLifeAdherence
			resultDict[RM]['QualityAdherence']=QualityAdherence
			
			
			
	return HttpResponse(json.dumps(resultDict,default = myconverter), content_type="json")	
	
	
def get_status_rules_data(request):
	result_list=[]
	product_no=request.GET.get('product_no')
	config_Name = request.GET.get('value')
	db = mongoConnection_to_ver2()
	#status_rules_dict = connection_status_rules().find_one({'MaterialNumber':product_no,'ActiveFlag':1},{'_id':0})
	status_rules_dict = db.config_correlator_defaults.find_one({'Config_Level':'EvaluationCriteria'},{'_id':0})
	
	CategoryStatusDict={}
	
	for rule in status_rules_dict['Rule_Status']:
		if status_rules_dict['Rule_Status'][rule]['Category']==config_Name:
			CategoryStatusDict[rule]=status_rules_dict['Rule_Status'][rule]

	return HttpResponse(json.dumps(CategoryStatusDict,default=json_util.default),content_type="application/json")
	
def get_status_by_category(request):
	db = mongoConnection_to_ver2()
	result_dict={}
	product_no= request.GET.get('product_no')
	configName = request.GET.get('category_name')
	
	status_rules_dict = db.config_correlator_statusIndicator.find_one({'MaterialNumber':product_no,'ActiveFlag':1},{'_id':0})
	#status_rules_dict = connection_defaults().find_one({'Config_Level':'EvaluationCriteria'})
	defaultsOperatorList = db.config_correlator_defaults.find_one({'Config_Level':'Operator_List'})
	
	result_dict=status_rules_dict['Rule_Status'][configName]
	
	result_dict_mod=result_dict.copy()

	for indicator in result_dict['EvaluationOutput_Criteria']:
		newEvaluationCriteria=result_dict['EvaluationOutput_Criteria'][indicator]
		for operator in defaultsOperatorList['OperatorList']:
			value=defaultsOperatorList['OperatorList'][operator]
			newEvaluationCriteria.replace(value,operator)
		result_dict_mod['EvaluationOutput_Criteria'][indicator]=newEvaluationCriteria
	
	return HttpResponse(json.dumps(result_dict_mod,default=json_util.default),content_type="application/json")
	
def get_split_string(request):
	
	result_dict={}
	str1 = request.GET.get('str')
	
	str = str1.strip()
	
	rawListSplit=['']
	rawListofExpressions=[]
	processedListofExpressions=[]
	rawList=re.split("\s",str)
	
	for expression in rawList:
		expression=expression.strip()
		rawListSplit.append(expression)
	
	for i in range(0,len(rawListSplit)-1,2):
		
		exp={}
		exp['Conjunction']=rawListSplit[i]
		exp['Expression']=rawListSplit[i+1]
		rawListofExpressions.append(exp)

	for expressionSet in rawListofExpressions:
		expressionSplit=re.split('<=|>=|<|>|!=|==',expressionSet['Expression'])
		operand1=expressionSplit[0]
		operand2=expressionSplit[1]
		operator=''
		if '<=' in expressionSet['Expression']:
			operator='<='
		elif '>=' in expressionSet['Expression']:
			operator='>='
		elif '<' in expressionSet['Expression']:
			operator='<'
		elif '>' in expressionSet['Expression']:
			operator='>'
		elif '==' in expressionSet['Expression']:
			operator='=='
		elif '!=' in expressionSet['Expression']:
			operator='!='

		expressionSet['Operand1']=operand1
		expressionSet['Operator']=operator
		expressionSet['Operand2']=operand2
		processedListofExpressions.append(expressionSet)
		
	return HttpResponse(json.dumps(processedListofExpressions,default=json_util.default),content_type="application/json")	

def parseEvaluationString(request):

	check_val=request.GET.get('dict_val')
	outputCriteriaDict=ast.literal_eval(check_val)
	
	evalString=''
	outputCriteriaDict_mod={}
	for indicator in outputCriteriaDict:
	   if outputCriteriaDict[indicator]!='NA':
		   outputCriteriaDict_mod[indicator]=outputCriteriaDict[indicator]

	##Initialise Issue Case
	if 'Issue' in outputCriteriaDict_mod:
	   evalString='if '+outputCriteriaDict_mod['Issue']+':\n\t'+'status_indicator='+"'Issue'\n"
	if 'Warning' in outputCriteriaDict_mod:
	   evalString+='elif '+outputCriteriaDict_mod['Warning']+':\n\t'+'status_indicator='+"'Warning'\n"
	if 'Ok' in outputCriteriaDict_mod:
	   evalString+='elif '+outputCriteriaDict_mod['Ok']+':\n\t'+'status_indicator='+"'Ok'\n"
	if 'NA' in outputCriteriaDict_mod:
	   evalString+='elif '+outputCriteriaDict_mod['NA']+':\n\t'+'status_indicator='+"'NA'\n"
	evalString+='else:\n\t'+'status_indicator='+"'NA'\n"

	return HttpResponse(json.dumps(evalString,default=json_util.default),content_type="application/json")	
	#return evalString
#print parseEvaluationString({'Issue':'Expression1','Warning':'NA'})	

def alterStatusRule1(request):
	product_no = request.GET.get('product_no');
	Category =  request.GET.get('category_name');
	updatedBy = request.session['user_alias']
	check_val1 = request.GET.get('dict_value')
	evalCriteriaOutputDict = ast.literal_eval(check_val1)
	
	result = AlterStatus.alterStatusRule(product_no,evalCriteriaOutputDict,Category,updatedBy)
	
	return HttpResponse(json.dumps(result), content_type="application/json")
	

@csrf_exempt	
def rejected_datatable(request):
	#startpoint = request.POST.get("start")
	pagelength = request.POST.get("length")
	draw = request.POST.get("draw")
	#endcounter = int(startpoint) + int(pagelength)
	#endcounter = pagelength
	startpoint=int(request.POST.get("start"))
	endcounter=int(startpoint)+int(pagelength)
	pipeline=get_rejected_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	module=request.POST.getlist('analysis[]')
	page_view=data_list[0].encode('utf-8')
	analysis=module[0].encode('utf-8')
	
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	if(analysis == 'Finished Goods'):
		db = mongoConnection_to_ver2().rejected_correlator_fng_processorders
	elif(analysis == 'Stagged PO'):
		db = mongoConnection_to_ver2().rejected_correlator_staged_po
		
	if type=='total':	
		if page_view=='rejected_view':
				batchnumber=request.POST.get('batchnumber')
				if(batchnumber!='all'):			
					pipeline['$match']['$and'].append({'BatchNumber':batchnumber})
				del pipeline['$match']['$and'][0]
				result_cursor=db.aggregate([ pipeline, {'$project':{'_id':0,'Technology':'$Technology','ResourceName':'$ResourceName','PlineName':'$PlineName','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrderNumber':'$ProcessOrderNumber','po':'$BatchNumber','product':'$MaterialNumber','Reason':'$Reason','RejectedTime':'$RejectedTime'}} ,{'$sort':{'avg':1}}])
				
	po_list=[]
	product_list=[]
	po_avgscr=[]
	processorder_list=[]
	line_list=[]
	resource_list=[]
	plantid_list=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_rejection_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	for r in result_cursor:	
		if(counter>=startpoint and counter<endcounter):
			templist=[]
			segmenttemp=''
			link=''
			link1=''
			linking_div=''
			plantid_list.append(r['SiteName'])
			po_sbu_list.append(r['SBU'])
			po_technology_list.append(r['Technology'])
			product_list.append(r['product'])
			line_list.append(r['PlineName'])
			resource_list.append(r['ResourceName'])
			po_list.append(r['po'])
			processorder_list.append(r['ProcessOrderNumber'])
			po_rejection_list.append(r['Reason'])
			po_siteid_list.append(r['SiteID'])	
			templist=[r['SiteName'],r['SBU'],r['Technology'],r['product'],r['PlineName'],r['ResourceName'],r['po'],r['ProcessOrderNumber'],r['Reason'],r['RejectedTime'].strftime('%Y-%m-%d %H:%M:%S')]
			data.append(templist)
		counter=counter+1
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['product_numbers']=product_list
	temp_dict['line']=line_list
	temp_dict['resource']=resource_list
	temp_dict['po_numbers']=po_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['rejection_reason']=po_rejection_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['color']='#F3778B'
	temp_dict['recordsFiltered']=counter
	temp_dict['recordsTotal']=counter
	temp_dict['data']=data
	temp_dict['draw']=int(draw)
	temp_dict['start']=startpoint
	temp_dict['length']=endcounter

	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po[0]), content_type="application/json")
	
	
def get_rejected_pipeline(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	date_list=[]
	filter_list=[{'ProductionStartDateTime':{'$ne':''}} ]
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	line=''
	product=''
	resource=''
	date='1900-01-01'
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]


	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data)
	else:	
		product=data_list[0].encode('utf-8')	
		
	data_list=[]
	data_list=request.POST.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]		
	
	if (len(plant_list)>=2 ):
		filter_list.append({'SiteID':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteID':plant } )
				
	if (len(product_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':product_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'MaterialNumber':product }	)
			
		
	if (len(date_list)>=2 ):
		filter_list.append( {'ProductionStartDateTime': {'$lte':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } } )
	else:
		#start=datetime(date.year(),date[1],date[2],0,0,0,0)
        #end=datetime(date[0],date[1],date[2],11,59,59,999999)
		filter_list.append ( {'ProductionStartDateTime':{'$lt':datatime.datetime.strptime(date, '%Y-%m-%d')+timedelta(days=1),'$gte':datatime.datetime.strptime(date, '%Y-%m-%d') }})
		#filter_list.append ( {'ProductionStartDateTime':datetime.datetime.strptime(date, '%Y-%m-%d')} )
		
	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline
	
@csrf_exempt	
def get_rejected_data(request):
	pipeline=get_rejected_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	module=request.POST.getlist('analysis[]')
	analysis=module[0].encode('utf-8')
	if(analysis == 'Finished Goods'):
		db = mongoConnection_to_ver2().rejected_correlator_fng_processorders
	elif(analysis == 'Stagged PO'):
		db = mongoConnection_to_ver2().rejected_correlator_staged_po
	if type=='total':
		if page_view=='rejected_view':
			batchnumber=request.POST.get('batchnumber')
			if(batchnumber!='all'):			
				pipeline['$match']['$and'].append({'BatchNumber':batchnumber})
			del pipeline['$match']['$and'][0]
			
			result_cursor=db.aggregate([ pipeline, {'$project':{'_id':0,'Technology':'$Technology','ResourceName':'$ResourceName','PlineName':'$PlineName','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrderNumber':'$ProcessOrderNumber','po':'$BatchNumber','product':'$MaterialNumber','Reason':'$Reason','RejectedTime':'$RejectedTime'}} ,{'$sort':{'avg':1}}])
				
	po_list=[]
	product_list=[]
	po_avgscr=[]
	processorder_list=[]
	line_list=[]
	resource_list=[]
	plantid_list=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_rejection_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	if (type=='total' ):
		if(page_view=='rejected_view'):		
			for r in result_cursor:
				plantid_list.append(r['SiteName'])
				po_sbu_list.append(r['SBU'])
				po_technology_list.append(r['Technology'])
				product_list.append(r['product'])
				line_list.append(r['PlineName'])
				resource_list.append(r['ResourceName'])
				po_list.append(r['po'])
				processorder_list.append(r['ProcessOrderNumber'])
				po_rejection_list.append(r['Reason'])
				po_siteid_list.append(r['SiteID'])
				deatil_list=[]
				deatil_list.append(r['SiteName'])
				deatil_list.append(r['SBU'])
				deatil_list.append(r['Technology'])
				deatil_list.append(r['product'])
				deatil_list.append(r['PlineName'])
				deatil_list.append(r['ResourceName'])
				deatil_list.append(r['po'])
				deatil_list.append(r['ProcessOrderNumber'])
				deatil_list.append(r['Reason'])
				deatil_list.append(r['RejectedTime'].strftime('%Y-%m-%d %H:%M:%S'))
				deatils_list.append(deatil_list)
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['product_numbers']=product_list
	temp_dict['line']=line_list
	temp_dict['resource']=resource_list
	temp_dict['po_numbers']=po_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['rejection_reason']=po_rejection_list
	temp_dict['download_list']=deatils_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['color']='#F3778B'
	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po), content_type="application/json")
	
@csrf_exempt	
def data_issue_datatable(request):
	#startpoint = request.POST.get("start")
	pagelength = request.POST.get("length")
	draw = request.POST.get("draw")
	#endcounter = int(startpoint) + int(pagelength)
	#endcounter = pagelength
	startpoint=int(request.POST.get("start"))
	endcounter=int(startpoint)+int(pagelength)
	pipeline=get_rejected_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	module=request.POST.getlist('analysis[]')
	page_view=data_list[0].encode('utf-8')
	analysis=module[0].encode('utf-8')
	
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	if(analysis == 'Finished Goods'):
		db = mongoConnection_to_ver2().data_issue_correlator_rca
	elif(analysis == 'Stagged PO'):
		db = mongoConnection_to_ver2().data_issue_correlator_staged_po

	if type=='total':	
		if page_view=='data_issue_view':
				batchnumber=request.POST.get('batchnumber')
				if(batchnumber!='all'):			
					pipeline['$match']['$and'].append({'BatchNumber':batchnumber})
				del pipeline['$match']['$and'][0]
				
				pipeline=pipeline['$match']
				result_cursor=db.find(pipeline,{'_id':0,'SiteName':1,'SBU':1,'Technology':1,'MaterialNumber':1,'PlineName':1,'ProcessOrderNumber':1,'BatchNumber':1,'DataIssueReason':1})

	temp_dict={}
	top_po=[]
	color='#F3778B'
	counter=0
	data=[]
	for r in result_cursor:
		if(counter>=startpoint and counter<endcounter):
			templist=[]
			for phase in r['DataIssueReason']:
				for issue in r['DataIssueReason'][phase]['Process']:
					templist=[r['SiteName'],r['SBU'],r['Technology'],r['MaterialNumber'],r['PlineName'],r['BatchNumber'],r['ProcessOrderNumber'],phase,'Process',issue]
				for issue in r['DataIssueReason'][phase]['Machine']:
					templist=[r['SiteName'],r['SBU'],r['Technology'],r['MaterialNumber'],r['PlineName'],r['BatchNumber'],r['ProcessOrderNumber'],phase,'Machine',issue]
				for issue in r['DataIssueReason'][phase]['RawMaterialInfo']:
					templist=[r['SiteName'],r['SBU'],r['Technology'],r['MaterialNumber'],r['PlineName'],r['BatchNumber'],r['ProcessOrderNumber'],phase,'RawMaterialInfo',issue]
			data.append(templist)
		counter=counter+1
	temp_dict['color']='#F3778B'
	temp_dict['recordsFiltered']=counter
	temp_dict['recordsTotal']=counter
	temp_dict['data']=data
	temp_dict['draw']=int(draw)
	temp_dict['start']=startpoint
	temp_dict['length']=endcounter

	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po[0]), content_type="application/json")
	
	
@csrf_exempt	
def get_data_issue_data(request):
	pipeline=get_rejected_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	module=request.POST.getlist('analysis[]')
	analysis=module[0].encode('utf-8')
	if(analysis == 'Finished Goods'):
		db = mongoConnection_to_ver2().data_issue_correlator_rca
	elif(analysis == 'Stagged PO'):
		db = mongoConnection_to_ver2().data_issue_correlator_staged_po
	if type=='total':
		if page_view=='data_issue_view':
			batchnumber=request.POST.get('batchnumber')
			if(batchnumber!='all'):			
				pipeline['$match']['$and'].append({'BatchNumber':batchnumber})
			del pipeline['$match']['$and'][0]
			pipeline=pipeline['$match']
			result_cursor=db.find(pipeline,{'_id':0,'SiteName':1,'SBU':1,'Technology':1,'MaterialNumber':1,'PlineName':1,'ProcessOrderNumber':1,'BatchNumber':1,'DataIssueReason':1})

	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	if (type=='total' ):
		if(page_view=='data_issue_view'):
			r_list=[]
			'''for r in result_cursor:
				r_list.append(r)
			result_cursor.close()'''			
			for r in result_cursor:
				for phase in r['DataIssueReason']:
					for issue in r['DataIssueReason'][phase]['Process']:
						deatil_list=[]
						deatil_list.append(r['SiteName'])
						deatil_list.append(r['SBU'])
						deatil_list.append(r['Technology'])
						deatil_list.append(r['MaterialNumber'])
						deatil_list.append(r['PlineName'])
						deatil_list.append(r['BatchNumber'])
						deatil_list.append(r['ProcessOrderNumber'])
						deatil_list.append(phase)
						deatil_list.append('Process')
						deatil_list.append(issue)
					for issue in r['DataIssueReason'][phase]['Machine']:
						deatil_list=[]
						deatil_list.append(r['SiteName'])
						deatil_list.append(r['SBU'])
						deatil_list.append(r['Technology'])
						deatil_list.append(r['MaterialNumber'])
						deatil_list.append(r['PlineName'])
						deatil_list.append(r['BatchNumber'])
						deatil_list.append(r['ProcessOrderNumber'])
						deatil_list.append(phase)
						deatil_list.append('Machine')
						deatil_list.append(issue)
					for issue in r['DataIssueReason'][phase]['RawMaterialInfo']:
						deatil_list=[]
						deatil_list.append(r['SiteName'])
						deatil_list.append(r['SBU'])
						deatil_list.append(r['Technology'])
						deatil_list.append(r['MaterialNumber'])
						deatil_list.append(r['PlineName'])
						deatil_list.append(r['BatchNumber'])
						deatil_list.append(r['ProcessOrderNumber'])
						deatil_list.append(phase)
						deatil_list.append('RawMaterialInfo')
						deatil_list.append(issue)
				deatils_list.append(deatil_list)
	'''response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="errorbills_data.csv"'
	writer = csv.writer(response)
	writer.writerow(["SiteName", "SBU", "Technology","MaterialNumber","LineName","BatchNumber","ProcessOrder","Phase","Category","Details"])
	for errorrow in deatils_list:
		writer.writerow(errorrow)
	return response'''
	temp_dict['download_list']=deatils_list
	temp_dict['color']='#F3778B'
	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po), content_type="application/json")
	

@csrf_exempt	
def alterInputFlag(request):
	result = []
	
	try:
		check_val2 = request.POST.get('input_list_value')
		input_flag_list = ast.literal_eval(check_val2)
		
		
		MaterialNumber = input_flag_list['Material_no']
		Data = input_flag_list['Data1']
			
		model_config_data = mongoConnection_to_ver2().model_config.find_one({'MaterialNumber':MaterialNumber,'ActiveFlag':1})
		
		
		
		for param in Data:
			paramName = param['input_row_id']
			
			value = param['input_flag_value']
					
			model_config_data['Premodelling_Input_Parameters'][paramName.decode('utf-8')]['UpdatedBy']=request.session['user_alias']	
			model_config_data['Premodelling_Input_Parameters'][paramName.decode('utf-8')]['PreModelUsageDecisionFlag']=value
			model_config_data['Premodelling_Input_Parameters'][paramName.decode('utf-8')]['UpdatedDateTime']=datetime.now()
		
		#connection_testData().insert_one(model_config_data)
		
		SCD('model_config',[model_config_data],['MaterialNumber'],{'MaterialNumber':MaterialNumber})
		result = 1
		return HttpResponse(json.dumps(result), content_type="application/json")
	except:
		result = 0
		return HttpResponse(json.dumps(result), content_type="application/json")
	

def alterPOCount(request):
	result = []
	try:
		check_val3 = request.GET.get('po_count_value')
		PO_Count_list = ast.literal_eval(check_val3)
		
		MaterialNumber = PO_Count_list['MaterialNumber']
		
		model_config_data = mongoConnection_to_ver2().rule_correlator_aggregate_count.find_one({'MaterialNumber':MaterialNumber,'ActiveFlag':1})
			
		if(int(PO_Count_list['Optimal_Value_Rule_Revision_Count']) != model_config_data['Optimal_Value']['Optimal_Value_Rule_Revision_Count']):
		
			Optimal_Value_Count = PO_Count_list['Optimal_Value_Rule_Revision_Count']
			model_config_data['Optimal_Value']['Optimal_Value_Rule_Revision_Count']=Optimal_Value_Count
			model_config_data['Optimal_Value']['UpdatedBy']=request.session['user_alias']
			model_config_data['Optimal_Value']['UpdatedDate']=datetime.now()
		
		
		if(int(PO_Count_list['Model_Rule_Revision_Count']) != model_config_data['Model_Training_Product_Level']['Model_Rule_Revision_Count']):
			Model_Rule_Count = PO_Count_list['Model_Rule_Revision_Count'];
			model_config_data['Model_Training_Product_Level']['Model_Rule_Revision_Count']=Model_Rule_Count
			model_config_data['Model_Training_Product_Level']['UpdatedBy']=request.session['user_alias']
			model_config_data['Model_Training_Product_Level']['UpdatedDate']=datetime.now()
			
		SCD('rule_correlator_aggregate_count',[model_config_data],['MaterialNumber'],{'MaterialNumber':MaterialNumber})
		result = 1
		return HttpResponse(json.dumps(result), content_type="application/json")
	except:
		print 'Error in SCD'
		result = 0
		return HttpResponse(json.dumps(result), content_type="application/json")
	
		


def alterOptimalValue(request):
	try:
		result = []
		check_val4 = request.GET.get('optimal_list_value')
		Optimal_value_list = ast.literal_eval(check_val4)
		MaterialNumber = Optimal_value_list['Material_no']
		config_level = Optimal_value_list['Config_level'];
		Data = Optimal_value_list['Data2'];
		if config_level == 'RawMaterialConfig':
			for param in Data:
					#print Data[param]['optimal_value']
					model_config_data = mongoConnection_to_ver2().config_correlator_rca.find_one({'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase'],'QualityParameter':Data[param]['quality_parameter'],'ActiveFlag':1})
					
					model_config_data['OptimalValue'] = float(Data[param]['optimal_value'])
					model_config_data['UpdatedBy']=request.session['user_alias']
					model_config_data['UpdatedDate']=datetime.now()
					
					SCD('config_correlator_rca',[model_config_data],['MaterialNumber','Phase','QualityParameter'],{'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase'] })
				#model_config_data['RawMaterial_Display'] = param['quality_parameter']

		
		if config_level == 'MachineParameterConfig':
			#model_config_data = mongoConnection().config_correlator_rca.find_one({'MaterialNumber':MaterialNumber,'ActiveFlag':1})	
			for param in Data:

					model_config_data = mongoConnection_to_ver2().config_correlator_rca.find_one({'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase'],'MachineParameter':Data[param]['quality_parameter'],'ActiveFlag':1})
			
					model_config_data['OptimalValue'] = float(Data[param]['optimal_value'])
					model_config_data['UpdatedBy']=request.session['user_alias']
					model_config_data['UpdatedDate']=datetime.now()
			
					SCD('config_correlator_rca',[model_config_data],['MaterialNumber','Phase','MachineParameter'],{'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase']})
				
		result = 1
		return HttpResponse(json.dumps(result), content_type="application/json")
	except:
		print 'Error in SCD'
		result = 0
		return HttpResponse(json.dumps(result), content_type="application/json")
		


# def alterOptimalValue(request):
	# #try:
	# result = []
	# check_val4 = request.GET.get('optimal_list_value')
	# Optimal_value_list = ast.literal_eval(check_val4)
	
	# MaterialNumber = Optimal_value_list['Material_no'];
	# config_level = Optimal_value_list['Config_level'];
	# processing_type = request.GET.get('ProcessingType');
	# load_type = request.GET.get('LoadType');
	# job_status = 'Pending'
	# Data = Optimal_value_list['Data2'];
	
	# load_list = []
	# job_status_val = ''
	# shedulerJobIDCursor = connection_load_scheduler().aggregate([{'$group':{'_id':0,'JobID':{'$max':'$JobID'}}}])
	
	# for doc in shedulerJobIDCursor:
		# load_list.append(doc)
	
	# if len(load_list) == 0:
		# JobID = 1
	# else:
		# JobID = load_list[0]['JobID'] + 1

	# connection_load_scheduler().insert_one({'MaterialNumber':MaterialNumber,'Username':request.session['user_alias'],'ProcessingType':processing_type,'LoadType':load_type,'JobStatus':job_status,'CreatedTime':datetime.now(),'JobID':JobID})
	
	
	# LoadId = 0
	# LaodIdCursor = connection_audit_log().aggregate([{'$group':{'_id':0,'LoadID':{'$max':'$LoadID'}}}])
	# for re in LaodIdCursor:
		# LoadId = re['LoadID']
	
	# print '*******'
	# print LoadId
	# JobCursor = connection_audit_log().find_one({'LoadID':LoadId},{"LoadID":1,"JobStatus":1})
	# job_status_val = JobCursor['JobStatus']
	
	# print '@@@@@'
	# print job_status_val
	
	# if config_level == 'RawMaterialConfig':
		# for param in Data:
				
			# model_config_data = connection_config_correlator().find_one({'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase'],'QualityParameter':Data[param]['quality_parameter'],'ActiveFlag':1})
			
			# model_config_data['OptimalValue'] = float(Data[param]['optimal_value'])
			# model_config_data['UpdatedBy']=request.session['user_alias']
			# model_config_data['UpdatedDate']=datetime.now()
			
			# #SCD('config_correlator_rca',[model_config_data],['MaterialNumber','Phase','QualityParameter'],{'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase'] })
			# #model_config_data['RawMaterial_Display'] = param['quality_parameter']

	
	# if config_level == 'MachineParameterConfig':
		# #model_config_data = connection_config_correlator().find_one({'MaterialNumber':MaterialNumber,'ActiveFlag':1})	
		# for param in Data:

				# model_config_data = connection_config_correlator().find_one({'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase'],'MachineParameter':Data[param]['quality_parameter'],'ActiveFlag':1})
		
				# model_config_data['OptimalValue'] = float(Data[param]['optimal_value'])
				# model_config_data['UpdatedBy']=request.session['user_alias']
				# model_config_data['UpdatedDate']=datetime.now()
				# #SCD('config_correlator_rca',[model_config_data],['MaterialNumber','Phase','MachineParameter'],{'MaterialNumber':MaterialNumber,'Config_Level':config_level,'Phase':Data[param]['phase']})
	
	# if load_type == 'RPN' and job_status_val.lower() != 'started':
		# print 'Function call'
		# schedule_load(MaterialNumber , load_type)
	
	# result = 1
	# return HttpResponse(json.dumps(result), content_type="application/json")
	# # except:
		# # print 'Error in SCD'
		# # result = 0
		# # return HttpResponse(json.dumps(result), content_type="application/json")
		

# def schedule_load(MaterialNumber , load_type):
	
	# os.chdir('/home/sarajesh/Correlator_Data_Load_Code_Optimised/')
	# filename = 'correlatorscheduling.py'
	# print 'Schedule Load'
	# arg = '{"po_list":[],"product_list":['+str(MaterialNumber)+'],"site_list":[],"load_type":"SL","UpdatedTime":[],"relationMapConfigFlag":"Y"}'
	# runstatus=call(['python2.7',filename, str(arg)], shell=True)
	# #subprocess.call("correlatorscheduling.py ", shell=True)
	# #subprocess.run('python /home/sarajesh/Correlator_Data_Load_Code_Optimised/correlatorscheduling.py )
	# #python 2.7 correlatorscheduling.py '{"po_list":[],"product_list":['+MaterialNumber+'],"site_list":[],"load_type":'+load_type+',"UpdatedTime":[],"relationMapConfigFlag":"Y"}'
	
	# #python 2.7 correlatorscheduling.py '{"po_list":[],"product_list":['+MaterialNumber+'],"site_list":[],"load_type":'+load_type+',"UpdatedTime":[],"relationMapConfigFlag":"Y"}'


@csrf_exempt	
def alterOutputFlag(request):
	result = []
	try:
		check_val5 = request.POST.get('output_list_value')
		output_flag_list = ast.literal_eval(check_val5)
		MaterialNumber = output_flag_list['Material_no']
		Data = output_flag_list['Data3']

		#model_config_data = mongoConnection().model_config.find_one({'MaterialNumber':MaterialNumber,'ActiveFlag':1})
		model_config_data = mongoConnection_to_ver2().model_config.find_one({'MaterialNumber':MaterialNumber,'ActiveFlag':0})

		for param in Data:
			paramName = param['output_row_id']
			value = param['output_flag_value']
			model_config_data['Premodelling_Output_Parameters'][paramName.decode('utf-8')]['UpdatedBy']=request.session['user_alias']	
			model_config_data['Premodelling_Output_Parameters'][paramName.decode('utf-8')]['PreModelUsageFlag']=value
			model_config_data['Premodelling_Output_Parameters'][paramName.decode('utf-8')]['UpdatedDateTime']=datetime.now()
		
		SCD('model_config',[model_config_data],['MaterialNumber'],{'MaterialNumber':MaterialNumber})
		result = 1
		return HttpResponse(json.dumps(result), content_type="application/json")
	except:
		result = 0
		return HttpResponse(json.dumps(result), content_type="application/json")

def get_bathnumber_list(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	date_list=[]
	date=''
	pipeline=get_filter_pipeline(request)
	change_type=request.GET.get("change_type", "")
	change_type=change_type.encode('utf-8')
	page_view=request.GET.get('page_view')
	change_value=request.GET.get('change_value')
	if(change_value == 'Finished Goods'):
		db = mongoConnection_to_ver2().report_correlator_rca
	elif(change_value == 'Stagged PO'):
		db = mongoConnection_to_ver2().report_correlator_staged_po
	data_list=[]
	data_list=request.GET.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]
	if (len(date_list)>=2 ):
		pipeline['ProductionStartDateTime']= {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } 
	else:
		pipeline['ProductionStartDateTime']={'ProductionStartDateTime':datetime.strptime(date, '%Y-%m-%d')}
	batchnumber_list=[]
	if(change_value == 'Finished Goods'):
		result_cursor=db.find(pipeline,{'BatchNumber':1,'QualitySegment':1})
		for r in result_cursor:
			tempdict={}
			tempdict['BatchNumber']=r['BatchNumber']
			tempdict['QualitySegment']=r['QualitySegment']
			batchnumber_list.append(tempdict)
	elif(change_value == 'Stagged PO'):
		result_cursor=db.find(pipeline,{'BatchNumber':1,'PredictedQualitySegment':1})
		for r in result_cursor:
			tempdict={}
			tempdict['BatchNumber']=r['BatchNumber']
			tempdict['QualitySegment']=r['PredictedQualitySegment']
			batchnumber_list.append(tempdict)
	filters_dict['batchnumber_list']=batchnumber_list
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")
	

def getModelBuiltStatus(request):
	Product=request.GET.get('Product')
	db=mongoConnection_to_ver2()
	config=db.rule_correlator_aggregate_count.find_one({'MaterialNumber':Product})

	if config['Model_Training_Product_Level']['Model_Last_Revision_Count']>0:
		result = ['Model is Built for '+Product,1]
	else:
		result = ['Model not Built for '+Product,0]
	return HttpResponse(json.dumps(result), content_type="application/json")
	
def Accuracy_treemap_axis_values(request):
	db=mongoConnection_to_ver2()
	dict={}
	po_count=[]
	param_count=[]
	result_cursor=db.config_correlator_defaults.find({'Config_Level':'Confidence_Level'},{'Parameter_Count':1,'PO_Count':1,'_id':0})
	for r in result_cursor:
		for i in r['PO_Count']:
			po_count.append('['+str(r['PO_Count'][i]['LowerLimit'])+'-'+str(r['PO_Count'][i]['UpperLimit'])+']')
		for j in r['Parameter_Count']:
			param_count.append('['+str(r['Parameter_Count'][j]['LowerLimit'])+'-'+str(r['Parameter_Count'][j]['UpperLimit'])+']')
	dict['Parameter_Count'] = param_count
	dict['PO_Count'] = po_count
	return HttpResponse(json.dumps(dict), content_type="application/json")
	
def Accuracy_treemap_param_value(request):
	product=request.GET.get('product_ID')
	paramList=request.GET.get('product_List')
	db=mongoConnection_to_ver2()
	dict=[]
	if paramList=='overall':
		result_cursor = db.model_config.find({'LastModelBuiltFlag':1,'MaterialNumber':product},{'ConfidenceLevel':1,'_id':0})
		for r in result_cursor:
			for k,v in r.iteritems():
				for k1,v1 in v.iteritems():
					if k1=='ParamCount':
						for k2,v2 in v1.iteritems():
							Parameter_Count = int(k2.split('_')[1])-1
					elif k1=='POCount':
						for k2,v2 in v1.iteritems():
							PO_Count = int(k2.split('_')[1])-1
		dict.append([Parameter_Count,PO_Count])
	else:
		result_cursor = db.model_config.find({'LastModelBuiltFlag':1,'MaterialNumber':product},{'Postmodelling_Output_Parameters.Output_Parameters|'+paramList+'|GroupActualValue.ConfidenceLevel':1,'_id':0})
		for r in result_cursor:
			for k,v in r.iteritems():
				for k1,v1 in v.iteritems():
					for k2,v2 in v1.iteritems():
						for k3,v3 in v2.iteritems():
							if k3=='ParamCount':
								for k4,v4 in v3.iteritems():
									Parameter_Count = int(k4.split('_')[1])-1
							if k3=='POCount':
								for k4,v4 in v3.iteritems():
									PO_Count = int(k4.split('_')[1])-1
		dict.append([Parameter_Count,PO_Count])
	return HttpResponse(json.dumps(dict), content_type="application/json")
	
from collections import defaultdict
	
def Accuracy_table_values(request):
	product=request.GET.get('product_ID')
	#product = '1855126'
	db = mongoConnection_to_ver2()
	dict = {}
	result_cursor = db.report_correlator_staged_po.find({'MaterialNumber':product,'AccuracyData':{'$exists':True}})
	OverAllActualAvgQualityScore=[]
	OverAllPredictedAvgQualityScore=[]
	OverallMAPE=[]
	actual={}
	predicted={}
	for row in result_cursor:
		OverAllActualAvgQualityScore.append(row['AccuracyData']['Actual']['AvgQualityScore'])
		OverAllPredictedAvgQualityScore.append(row['AccuracyData']['Predicted']['AvgQualityScore'])
		OverallMAPE.append(row['AccuracyData']['PercentageError']['overall'])
		row['AccuracyData']['Actual']['Parameter']
		for param,val in row['AccuracyData']['Actual']['Parameter'].iteritems():
			if param not in actual:
				actual[param]=[]
				predicted[param]=[]        
			actual[param].append(val['GroupActualValue'])
			predicted[param].append(row['AccuracyData']['Predicted']['Parameter'][param]['GroupPredictedValue'])

	result={}
	result["Overall_Score"]={}
	if len(OverAllActualAvgQualityScore)>0:
		result["Overall_Score"]['ActualScore']=sum(OverAllActualAvgQualityScore)/len(OverAllActualAvgQualityScore)
	if len(OverAllPredictedAvgQualityScore)>0:
		result["Overall_Score"]['PredictedScore']=sum(OverAllPredictedAvgQualityScore)/len(OverAllPredictedAvgQualityScore)
	if len(OverallMAPE)>0:
		result["Overall_Score"]['OverallMAPE']=sum(OverallMAPE)/len(OverallMAPE)
	for param in actual:
		result[param]={}
		#dict[param]['PredictedlValue']=0
		#dict[param]['ActualValue']=0

		result[param]['ActualValue']=sum(actual[param])/len(actual[param])
		result[param]['PredictedlValue']=sum(predicted[param])/len(predicted[param])
	return HttpResponse(json.dumps(result), content_type="application/json")
	
	
def getAccuracyMatrix(request):
	product=request.GET.get('product_ID')
	#product='1990392'
	#paramList=request.GET.get('product_List')

	db=mongoConnection_to_ver2()
	result_cursor = db.report_correlator_staged_po.find({"AccuracyData":{'$exists':True},'MaterialNumber':product})
	
	Mapping={}
	Mapping['A']=0
	Mapping['B']=1
	Mapping['C']=2
	Mapping['D']=3
	
	AccuracyDataList=[]
	recordList=[]
	
	for record in result_cursor:
		recordList.append(record)
		
	result_cursor.close()
	
	for record in recordList:
		tuple=[]
		tuple.append(Mapping[record['AccuracyData']['Actual']['QualitySegment']])
		tuple.append(Mapping[record['AccuracyData']['Predicted']['QualitySegment']])
		AccuracyDataList.append(copy.deepcopy(tuple))
	

	print AccuracyDataList
	
	return HttpResponse(json.dumps(AccuracyDataList), content_type="application/json")
	
	
def get_raw_material_list(request):
	result_list = []
	product_no=request.GET.get('product_no')
	config_name = request.GET.get('value')
	
	result_list	 = connection_parent_child_data().distinct('RawMaterial_Display',{'MaterialNumber':product_no,'Config_Level':config_name})
	return HttpResponse(json.dumps(result_list,default=json_util.default),content_type="application/json")
	
	
def getOPMapping(request):
	db=mongoConnection_to_ver2()
	result_list=[]
	product_no=request.GET.get('product_no')
	Config_Level=request.GET.get('Config_Level')
	rawmaterial_no = request.GET.get('raw_material_no')
	
	if Config_Level == 'OutputParameterMapping':
		mappingDict = db.config_correlator_parameterMapping.find_one({'MaterialNumber':product_no,'Config_Level':Config_Level,'ActiveFlag':1})
		
		allParentChildList=[]
		for Group in mappingDict['Groups']:
			for child in mappingDict['Groups'][Group]['ChildList']:
				parentChildList=[]
				parentChildList.append(child['ParameterName'])
				parentChildList.append(mappingDict['Groups'][Group]['MasterChild'])
				parentChildList.append(mappingDict['UpdatedBy'])
				parentChildList.append(mappingDict['UpdatedDatetime'])
				allParentChildList.append(copy.deepcopy(parentChildList))
		
		return HttpResponse(json.dumps(allParentChildList,default=json_util.default), content_type="application/json")
	else:
		mappingDict = connection_parent_child_data().find_one({'MaterialNumber':product_no,'Config_Level':Config_Level,'RawMaterialNumber':rawmaterial_no.strip(),'ActiveFlag':1})
	
		allParentChildList=[]
		for Group in mappingDict['Groups']:
			for child in mappingDict['Groups'][Group]['ChildList']:
				parentChildList=[]
				parentChildList.append(child['ParameterName'])
				parentChildList.append(mappingDict['Groups'][Group]['MasterChild'])
				parentChildList.append(mappingDict['UpdatedBy'])
				parentChildList.append(mappingDict['UpdatedDatetime'])
				allParentChildList.append(copy.deepcopy(parentChildList))
		
		return HttpResponse(json.dumps(allParentChildList,default=json_util.default), content_type="application/json")
	

def alterOPMapping(request):
	result_list=[]
	
	try:
		check_val = request.GET.get('updated_parent_list')
		parent_dict = ast.literal_eval(check_val)
		product_no = request.GET.get('Material_no');
		Config_level = request.GET.get('Config_level');	
		raw_material_no = request.GET.get('raw_material_no').strip();	
	
		#print Config_level
		#print raw_material_no
	
		'''
		{'VISCOSITY': {'parent_name': 'Group_COLOURVALUE', 'child_name': 'VISCOSITY'}, 'SOFTENINGPOINT': {'parent_name': 'Group_APPEARANCE', 'child_name': 'SOFTENINGPOINT'}}
		'''
		
		if Config_level == 'InputRMParameterMapping':
			
			mappingDict = connection_parent_child_data().find_one({'MaterialNumber':product_no,'Config_Level':Config_level,'RawMaterialNumber':raw_material_no,'ActiveFlag':1})
			
			for param in parent_dict:
				for Group in mappingDict['Groups']:
					for child in mappingDict['Groups'][Group]['ChildList']:
						if child['ParameterName'] == parent_dict[param]['child_name']:
							mappingDict['Groups'][Group]['MasterChild'] =  parent_dict[param]['parent_name']
							mappingDict['UpdatedBy'] = request.session['user_alias']
							mappingDict['UpdatedDatetime'] = datetime.now()
							#print child['ParameterName']
							#print mappingDict['Groups'][Group]['MasterChild']
			
			SCD('config_correlator_parameterMapping',[mappingDict],['MaterialNumber'],{'MaterialNumber':product_no,'Config_Level':Config_level,'RawMaterialNumber':raw_material_no})
			
		
		
		if Config_level == 'OutputParameterMapping':
		
			mappingDict = connection_parent_child_data().find_one({'MaterialNumber':product_no,'Config_Level':Config_level,'ActiveFlag':1})
			for param in parent_dict:
				for Group in mappingDict['Groups']:
					for child in mappingDict['Groups'][Group]['ChildList']:
						if child['ParameterName'] == parent_dict[param]['child_name']:
							mappingDict['Groups'][Group]['MasterChild'] =  parent_dict[param]['parent_name']
							mappingDict['UpdatedBy'] = request.session['user_alias']
							mappingDict['UpdatedDatetime'] = datetime.now()
							#print child['ParameterName']
							#print mappingDict['Groups'][Group]['MasterChild']
			
			SCD('config_correlator_parameterMapping',[mappingDict],['MaterialNumber'],{'MaterialNumber':product_no,'Config_Level':Config_level})

		
		result_list = 1
		return HttpResponse(json.dumps(result_list), content_type="application/json")
	except:
		print 'Error in SCD'
		result_list=0
		return HttpResponse(json.dumps(result_list), content_type="application/json")	