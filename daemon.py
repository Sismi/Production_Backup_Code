"""Daemonizer."""

import sys
import time
import os
import atexit
from signal import SIGTERM


class Daemon(object):
    """
    A generic daemon class.

    Usage: subclass the Daemon class and override the run() method
    """

    def __init__(self, pidfile, stdin='/dev/null',
                 stdout='/dev/null', stderr='/dev/null'):
        """Constructor for the daemon."""
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        print pidfile
	self.pidfile = pidfile

    def daemonize(self):
        """Method to daemonize the process.

        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try:
	    print 'demonize'
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0)
		pass
        except OSError, e:
	    print e.strerror
            sys.stderr.write(
                "fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)
	# decouple from parent environment
        print 'demon2'
	os.chdir("/")
        os.setsid()
        os.umask(0)

        # do second fork
	print 'daemon3'
        try:
            pid = os.fork()
	    print pid,'>>>>'
            if pid > 0:
                # exit from second parent
                sys.exit(0)
	        pass
        except OSError, e:
            print 'daemon4', e.strerror
	    sys.stderr.write(
                "fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = file(self.stdin, 'r')
        so = file(self.stdout, 'a+')
        se = file(self.stderr, 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())
	print 'daemon5'
        # write pidfile
        atexit.register(self.delpid)
	print 'daemon6'
        pid = str(os.getpid())
	print pid
        file(self.pidfile, 'w+').write("%s\n" % pid)

    def delpid(self):
        """Method to delete the pid."""
        os.remove(self.pidfile)

    def start(self):
        """Start the daemon."""
        # Check for a pidfile to see if the daemon already runs
        print 'start 2'
	try:
            pf = file(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError as e:
	    print e
            pid = None

        if pid:
            message = "pidfile %s already exist. Daemon already running?\n"
            sys.stderr.write(message % self.pidfile)
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        self.run()

    def stop(self):
        """Stop the daemon."""
        # Get the pid from the pidfile
        try:
            pf = file(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        if not pid:
            message = "pidfile %s does not exist. Daemon not running?\n"
            sys.stderr.write(message % self.pidfile)
            # not an error in a restart
            return

        # Try killing the daemon process
        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError, err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print str(err)
                sys.exit(1)

    def restart(self):
        """Restart the daemon."""
        self.stop()
        self.start()

    def run(self):
        """Method to run the daemon.

        You should override this method when you subclass Daemon. It will be
        called after the process has been daemonized by start() or restart().
        """
        pass


class DjangoDaemon(Daemon):
    """Customised Class to run the process as a daemon."""

    def run(self):
        """Method to run the Daemon."""
        # Code here
	print 'start...'
        os.eviron.setdefault('DJANGO_SETTINGS_MODULE', "henkel.settings")
        from django.core.management import execute_from_command_line
        execute_from_command_line(['manage.py', 'runserver', '0.0.0.0:80'])
        while True:
            time.sleep(1)


if __name__ == "__main__":
    # Main function that takes start|stop|restart as arguments to run
    # the application as a daemon.
    daemon = DjangoDaemon('/tmp/henkel/django-daemon.pid')
    print sys.argv
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
