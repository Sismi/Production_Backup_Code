function tree_clicked(phase_num,module) {
	$('#tree_stats').html('');
	$('.statistical_overlay').css('display','block');
	var json_data = '';
	selected_param = $('#param-select').val();
	$.ajax({
	method: "GET",
	url: "/qualitycorrelator1/getTreeChartData/",
	contentType: "json",
	data:{'mat_num':global_data['GeneralInfo']['MaterialNumber'],'selected_param':selected_param,'Category':phase_num},
	success: function(result_data) {
		//stats_phase_display1();
		json_data = JSON.parse(result_data['tree_chart']);
		json_parameter = (result_data['parameter']);
		param_addition_statistical(json_parameter,global_param_number);
		global_param_number = 1;
		if (json_data == "") {
			$('.statistical_overlay').css('display','none');
			$("#tree_stats").html("<span id='no-spo-stats'>No details for Statistical Analysis</span>");
			$("#no-spo-stats").css({"position":"relative","top":"40%","left":"50%"});
		}
		else {
			form_tree(json_data);
		}
		if(module == 'InputToOutput'){
			$('.phase_bar_div_stats .phase_number').each(function() {
				if($(this).text() == phase_num) {
					$(this).css('background-color','#9acd32');
					$(this).parent().parent().find('a span .phase').css('background-color','#9acd32');
				}
			})
		}
	}
	});
}
	
function param_addition_statistical(param_list,global_param_number) {
	if(global_param_number == 0) {
		list1 = [];
		$('#param-select').html('<option value="overall" selected>Overall</option>');
		$('select#param-select').find('option').each(function() {
			if(!($(this).val() == 'overall')) {
				list1.push($(this).val());
			}
		});
		
		for(var f=0;f<param_list.length;f++){
			this_text = param_list[f];
			$('#param-select').append('<option value="'+this_text+'">'+this_text+'</option>');
		}
	}
}

function stats_phase_display1() {
	$('.phase_bar_div_stats').html('<table class="table-striped"><tr><th>Parameter</th><th>Gain</th></tr><tr><td>Visc</td><td>2.0</td></tr><tr><td>Visc2HM</td><td>4.0</td></tr></table>');
	$(".stats_phase_class li").last().css("height", "0px");
}

function form_tree(json_data) {
	$(".modal-body #tree_stats").html('');
	var width = 1000;
	var height = 450;
	var maxLabel = 150;
	var duration = 500;
	var radius = 5;
	var i = 0;
	var root;

	var tree = d3.layout.tree()
	    .size([height, width]);

	var diagonal = d3.svg.diagonal()
	    .projection(function(d) { return [d.y, d.x]; });

	var svg = d3.select(".modal-body #tree_stats").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	        .append("g")
	        .attr("transform", "translate(" + maxLabel + ",0)");

	root = json_data;
	root.x0 = height / 2;
	root.y0 = height / 2;
	root.children.forEach(collapse);

	function update(source) 
	{
	    // Compute the new tree layout.
	    var nodes = tree.nodes(root).reverse();
	    var links = tree.links(nodes);

	    // Normalize for fixed-depth.
	    nodes.forEach(function(d) { d.y = d.depth * maxLabel; });

	    // Update the nodes�
	    var node = svg.selectAll("g.node")
	        .data(nodes, function(d){ 
	            return d.id || (d.id = ++i); 
	        });
	    // Enter any new nodes at the parent's previous position.
	    var nodeEnter = node.enter()
	        .append("g")
	        .attr("class", "node")
	        .attr("transform", function(d){ return "translate(" + source.y0 + "," + source.x0 + ")"; })
	        .on("click", click);
	    nodeEnter.append("circle")
	        .attr("r", 0)
	        .style("fill", function(d){ 
	            return d._children ? "lightsteelblue" : "white"; 
	        })
			.style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; })
			.style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; });

	    nodeEnter.append("text")
	        .attr("x", function(d){ 
	            var spacing = computeRadius(d) + 5;
	            return d.children || d._children ? -spacing : spacing; 
	        })
	        .attr("dy", "11")
			.attr("dx", "5")
	        .attr("text-anchor", function(d){ return d.children || d._children ? "end" : "start"; })
	        .text(function(d){ return d.name.split('|')[0]; })
	        .style("fill-opacity", 0);

	    // Transition nodes to their new position.
	    var nodeUpdate = node.transition()
	        .duration(duration)
	        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });
	    

	    nodeUpdate.select("circle")
	        .attr("r", function(d){ return computeRadius(d); })
	        .style("fill", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; })
	        .style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; });

	    nodeUpdate.select("text").style("fill-opacity", 1);

	    // Transition exiting nodes to the parent's new position.
	    var nodeExit = node.exit().transition()
	        .duration(duration)
	        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
	        .remove();

	    nodeExit.select("circle").attr("r", 0);
	    nodeExit.select("text").style("fill-opacity", 0);

	    // Update the links�
	    var link = svg.selectAll("path.link")
	        .data(links, function(d){ return d.target.id; });

	    // Enter any new links at the parent's previous position.
	    link.enter().insert("path", "g")
	        .attr("class", "link")
	        .attr("d", function(d){
	            var o = {x: source.x0, y: source.y0};
	            return diagonal({source: o, target: o});
	        });

	    // Transition links to their new position.
	    link.transition()
	        .duration(duration)
	        .attr("d", diagonal);

	    // Transition exiting nodes to the parent's new position.
	    link.exit().transition()
	        .duration(duration)
	        .attr("d", function(d){
	            var o = {x: source.x, y: source.y};
	            return diagonal({source: o, target: o});
	        })
	        .remove();

	    // Stash the old positions for transition.
	    nodes.forEach(function(d){
	        d.x0 = d.x;
	        d.y0 = d.y;
	    });
	}

	function computeRadius(d)
	{
	    if(d.children || d._children) return radius + (radius * nbEndNodes(d) / 10);
	    else return radius;
	}

	function nbEndNodes(n)
	{
	    nb = 0;    
	    if(n.children){
	        n.children.forEach(function(c){ 
	            nb += nbEndNodes(c); 
	        });
	    }
	    else if(n._children){
	        n._children.forEach(function(c){ 
	            nb += nbEndNodes(c); 
	        });
	    }
	    else nb++;
	    
	    return nb;
	}

	function click(d)
	{
	    if (d.children){
	        d._children = d.children;
	        d.children = null;
	    } 
	    else{
	        d.children = d._children;
	        d._children = null;
	    }
	    update(d);
	}

	function collapse(d){
	    if (d.children){
	        d._children = d.children;
	        d._children.forEach(collapse);
	        d.children = null;
	    }
	}

	function expand(d){   
	    var children = (d.children)?d.children:d._children;
	    if (d._children) {        
	        d.children = d._children;
	        d._children = null;       
	    }
	    if(children)
	      children.forEach(expand);
	}

	function expandAll(){
	    expand(root); 
	    //update(root);
	}
	expandAll();
	update(root);
	$('.statistical_overlay').css('display','none');
}
function product_model_built_status(){
	console.log("yes");
	product = $('.product').val();
	$.ajax({
		method: "GET",
		url: "/qualitycorrelator1/getModelBuiltStatus/",
		data:{'Product':product},
		success: function(result_data) {
			console.log('rs' , result_data);
			$('#model_built_status').html(result_data)
		}
	});
}
function statiscs_table(product_ID,product_List){
	
	$.ajax({
		type:'GET',
		url:'/qualitycorrelator1/Accuracy_table_values',
		data:{'product_ID':product_ID,'product_List':product_List},
		success: function(res){
			/*var res = {"Group_SOFTENINGPOINT": {"Predicted": 106.40447998046902, "ActualValue": 103.5, "MAPE": 2.8062608506946916}, "Group_VISCOSITYHM": {"Predicted": 11541.9990234375, "ActualValue": 13000.0, "MAPE": 11.215392127403847}, "paramList": ["Group_SOFTENINGPOINT", "Group_VISCOSITYHM"]};*/
			var avg_mape = 0;
			for(k in res){
				if(k!='paramList'){
					avg_mape=avg_mape+res[k]['MAPE'];
				}
			}
			var len = Object.keys(res.paramList).length;
			avg_mape = avg_mape/len;
			avg_mape = avg_mape.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

			
				var str = '<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" style="padding-left:0px;">'+
							  '<div class="info-box">'+
								'<span class="info-box-icon bg-aqua"><i class="fa fa-percent" aria-hidden="true"></i></span>'+
								'<div class="info-box-content">'+
								  '<span class="info-box-text">MAPE</span>'+
								  '<span class="info-box-number">'+avg_mape+'<small></small></span>'+
								'</div>'+
							  '</div>'+
							'</div>	'+
							'<table class="table table-bordered"><thead><tr><th>Name</th><th>Actual Value</th><th>Predicted Value</th></tr></thead><tbody>'
			for(i=0;i<len;i++){
				var name = res.paramList[i];
				str = str + '<tr><td>'+res.paramList[i]+'</td><td>'+res[name].ActualValue+'</td><td>'+res[name].Predicted.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+'</td></tr>'
			}
			str = str +'</tbody></table>';
			$('#statistical-table').html(str);
			
		},
		error: function(e){
		}
	});
}

function treemap(data,po_count,param_count,xcoordinate,ycoordinate,id){

	for(key in data){
	  if(data[key].x == xcoordinate && data[key].y == ycoordinate){
		data[key]['color'] = 'url(#highcharts-default-pattern-1)';
	  }
	}	
	console.log(data)
	$('#'+id).highcharts({
		chart: {
			type: 'heatmap',
			backgroundColor: 'rgb(247, 247, 247)'
		},
		title: { text: '' },
		legend: { enabled: false },
		credits: { enabled: false },
		xAxis: { 
			categories: param_count,
			title: { text: '' },
			labels: { 
			enabled: true,
				style: {
					color: 'black',  
					fontWeight: '600'
				}
			},
			minPadding:0,
			maxPadding:0,
		},  
		plotOptions: {
			columnrange: {
				 dataLabels: {
					style: {
						textOutline: false,
						textShadow: false
					},
				 }
			},
			series: {
				animation: {
					duration: 200
				}
			}
		},			
		tooltip: {
			enabled: false,
			shared: true,
			useHTML: true,
			headerFormat: '<table>',
			pointFormat: '<tr><td style="text-align: right;font-size:12px;">{point.title}</td></tr>',
			footerFormat: '</table>',
			valueDecimals: 2

		},			
		yAxis: {
			categories: po_count,
			title: { text: '' },
			labels: { 
				enabled: true,
				suffix:'',
				style: {
					color: 'black',  
					fontWeight: '600'						
				}				
			}
		},
		series: [{
			borderWidth: 2,
			borderColor:'rgba(255,255,255,.9)',
			data: data,
			dataLabels: {
				enabled: true,
				color: '#000000',
				formatter: function () {
					return this.point.title;
				},
				style: {
					fontWeight:'100',
				}
			}
		}]

	});
}