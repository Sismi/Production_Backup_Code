/*
JS file for User Admin module
*/

var selected_pages_list = [];
$(document).ready(function() {
    $('.admin_li_btn').click();
    $('#editdatepicker').Zebra_DatePicker({
        direction: -1,
        format: 'm/d/Y',
        view: 'years',
        show_icon: true,
    });
	$("#edituserstatus").attr("disabled","true");
    $(".main-container-sidebar").css("display","block");
	$(".main-container1, .kpi-container, .kpi-container-1").css("background-color","white");

    $('#approved-date').Zebra_DatePicker({
        direction: -1,
        format: 'm/d/Y',
        view: 'years',
        show_icon: true,
    });
	create_syncdate();
/*	
	$('#sitename_list, #sbu_list, #technology_list, #fgproducts_list, #linename_list, #resources_list').click(function(event){		
        console.log('scrolldown to be called');
		scrollDown();		
		console.log('scrolldown called already');
        event.preventDefault();		
    });
	$('#resources_list .btnOk, #resources_list .btnCancel').click(function(event){		
        console.log('scrollup to be called');
		scrollUp();		
		console.log('scrollup called already');
        event.preventDefault();		
    });
    $('.treewrapper').click(function(event){		
        console.log('scrolldown1 to be called');
		scrollDown1();		
		console.log('scrolldown1 called already');
        event.preventDefault();		
    });
*/
    //$('html, body').animate({scrollTop:$('#formnewusergroup').position().top}, 'slow');

    if(window.location.href.indexOf("cat=usergroup") > -1) {
        if($($('.rawmaterial_li ').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.rawmaterial_li ').find('i')[0]).removeClass('fa-caret-down');
            $($('.rawmaterial_li ').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.finishedgood_li').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down');
            $($('.finishedgood_li').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up')) {
            $($('.menu_ul')[2]).slideToggle('');
            $($('.admin_li').find('i')[0]).removeClass('fa-caret-up');
            $($('.admin_li').find('i')[0]).addClass('fa-caret-down'); 
            $('.rawmaterial_li').find('ul').css('display','none');      
            $('.finishedgood_li').find('ul').css('display','none');
            $('.admin_li').find('ul').css('display','block');
        }
        page_index=$(".not_mobile")[0];
        $(page_index).addClass("clicked_li");

        $('.admin_li .fa-users').parent().addClass('clicked_li');
        $("#user-role-group-main, #user-role-group-list").css("display","block");
        $('.sub-group-2').css('display','none');
        $('.main-heading').html('Application user module');
    	$(".editgroup, .deletegroup").addClass("disable_href");
        $("#user-main, .user-details, .sub-group-2").css("display","none");
        $("#user-main").removeClass("col-lg-10").addClass("col-lg-9");
        list_all_user_groups();
        $('#user-main .fa-trash').addClass('deleteuser').removeClass('delete-from-user');
        $('#user-main .fa-trash').parent().attr('href','#myModaluser');
    }
    else if(window.location.href.indexOf("cat=user") > -1) {
        if($($('.rawmaterial_li ').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.rawmaterial_li ').find('i')[0]).removeClass('fa-caret-down');
            $($('.rawmaterial_li ').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.finishedgood_li').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down');
            $($('.finishedgood_li').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up')) {
            $($('.menu_ul')[2]).slideToggle('');
            $($('.admin_li').find('i')[0]).removeClass('fa-caret-up');
            $($('.admin_li').find('i')[0]).addClass('fa-caret-down'); 
            $('.rawmaterial_li').find('ul').css('display','none');      
            $('.finishedgood_li').find('ul').css('display','none');
            $('.admin_li').find('ul').css('display','block');
        }
        page_index=$(".not_mobile")[3];
        $(page_index).addClass("clicked_li");

        $('.admin_li .fa-user').parent().addClass('clicked_li');
		//$('#user-modification .admin-headings').html('Addiiton of New User');
        $('#user-main .fa-trash').removeClass('deleteuser').addClass('delete-from-user');
        $('#user-main .fa-trash').parent().attr('href','#myModaluserdeletion');
        $('.main-heading').html('Users');
        $("#user-role-group-main, #user-role-group-list, #user-main, .sub-group-2").css("display","none");
        $(".kpi-container").css("display","block");

        $('[data-toggle="tooltip"]').tooltip();

        $(".add-new-user-button").on("click", function() {
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
			//$('.kpis_min_height').css('color','black');
			selected_user_color();
			$(this).find('span').css('color','white');
			$('#user-modification .admin-headings').html('Addition of New User');
            $("#user-main-module, .sub-users-1, #user-main").css("display","none");
			$(".canceluser").html("Clear");
			$("#user-password_modification").css("display","none");
            $("#user-modification").css("display","block");
			//disable_form_elements('user-modification');
            //$("#addnewuserform").find("input").val("").prop("disabled",false);
            $("#addnewuserform .saveedituser").removeClass('saveedituser').addClass('savenewuser');
			$(".savenewuser").html("Submit");
            $("#addnewuserform").find("input").val("").removeAttr("readonly");
			//disable_form_elements('addnewuserform');
            $("#editdatepicker, #editUseremail, #editusername, #edituserpassword, #confirmpassword, #editfirstname, #editlastname, #edituseralias, #edituserstatus, #editmesstatus").removeAttr('readonly').prop('disabled',false).removeProp('disabled','disabled').removeAttr('disabled','disabled');
            $("#addnewuserform .form-group:lt(4) label:nth-child(1)").addClass("required");
            $("#henkel-com").removeClass("required").css("display","block");
			$($('#confirmpassword').parent().parent()).css('display','block');
            $("#editUseremail").parent().addClass("col-lg-6 col-md-4").removeClass("col-lg-9 col-md-8");
            $("#addun, #addemail, #addpasswrd, #addfn").html("");
            $("#edituserstatus").attr("readonly","readonly").attr("checked","checked").prop('disabled',true);
			//$('#editmesstatus').removeAttr('readonly').removeAttr('onclick');
        });

        $(".edit-existing-user-button").on("click", function() {
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
			selected_user_color();
			$(this).find('span').css('color','white');
			$("#user-password_modification").css("display","none");
			$(".canceluser").html("Cancel");
			$($('#confirmpassword').parent().parent()).css('display','none');
            $("#editUseremail").parent().removeClass("col-lg-6 col-md-4").addClass("col-lg-9 col-md-8");
            $("#henkel-com").css("display","none");
            var flag_user = 'Editing';
            get_users_lists_into_dropdown(flag_user);
			$('#editmesstatus').removeAttr('readonly').removeAttr('onclick');
			$("#edituserstatus").attr("readonly","readonly").prop('disabled',true);
        });

        $(".block-unblock-user-button").on("click", function() {
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
			selected_user_color();
			$(this).find('span').css('color','white');
            $("#henkel-com").css("display","none");
			$(".canceluser").html("Cancel");
			$("#user-password_modification").css("display","none");
			$($('#confirmpassword').parent().parent()).css('display','none');
            $("#editUseremail").parent().removeClass("col-lg-6 col-md-4").addClass("col-lg-9 col-md-8");
            var flag_user = 'Blocking/UnBlocking';
            // $("#edituserstatus").addClass("hasBorder");
            // $("#edituserstatus").css({"border":"1px solid red","background":"yellow"});
            get_users_lists_into_dropdown(flag_user);
			$('#editmesstatus').attr('readonly','readonly').prop('disabled',true);
            //$('#edituserstatus').removeAttr('onclick').prop('disabled',false);
			$("#edituserstatus").removeAttr('readonly').prop('disabled',false).removeProp('disabled','disabled').removeAttr('disabled','disabled');
        });

        $(".reset-password-button").on("click", function() {
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
			selected_user_color();
			$(this).find('span').css('color','white');
            $("#henkel-com, #user-modification").css("display","none");
			//$("#user-password_modification").css("display","block");
			$(".canceluser").html("Cancel");
			$($('#confirmpassword').parent().parent()).css('display','block');
            $("#editUseremail").parent().removeClass("col-lg-6 col-md-4").addClass("col-lg-9 col-md-8");
            var flag_user = 'Resetting Password';
            get_users_lists_into_dropdown(flag_user);
			$('#editmesstatus').attr('readonly','readonly');
			$("#edituserstatus").attr("readonly","readonly").prop('disabled',true);
			$("#edituserpassword, #confirmpassword").removeAttr('readonly').prop('disabled',false).removeProp('disabled','disabled').removeAttr('disabled','disabled');
        });

        $(".delete-multiple-users").on("click", function() {
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
			selected_user_color();
			$(this).find('span').css('color','white');
            $("#user-main-module, #user-modification").css("display","none");
			$("#user-password_modification").css("display","none");
            $("#user-main").removeClass("col-lg-9").addClass("col-lg-10").css("display","block");
            $(".delete-from-user").css("display","block");
            $(".user1").css("display","none");
            list_all_users();
        });

        $(".view-users-table").on("click", function() {
            // $(".delete-multiple-users").click();    
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
			selected_user_color();
			$(this).find('span').css('color','white');
            $("#user-main-module, #user-modification").css("display","none");
			$("#user-password_modification").css("display","none");
            $("#user-main").removeClass("col-lg-9").addClass("col-lg-10").css("display","block");
            $(".delete-from-user, .user1").css("display","none");
            list_all_users();
            $("#user-list-datatable input").prop("disabled",true);
        });
    }
    else if(window.location.href.indexOf("cat=datagroup") > -1) {
        if($($('.rawmaterial_li ').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.rawmaterial_li ').find('i')[0]).removeClass('fa-caret-down');
            $($('.rawmaterial_li ').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.finishedgood_li').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down');
            $($('.finishedgood_li').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up')) {
            $($('.menu_ul')[2]).slideToggle('');
            $($('.admin_li').find('i')[0]).removeClass('fa-caret-up');
            $($('.admin_li').find('i')[0]).addClass('fa-caret-down'); 
            $('.rawmaterial_li').find('ul').css('display','none');      
            $('.finishedgood_li').find('ul').css('display','none');
            $('.admin_li').find('ul').css('display','block');
        }
        page_index=$(".not_mobile")[1];
        $(page_index).addClass("clicked_li");


        $('.admin_li .fa-archive').parent().addClass('clicked_li');
        $("#data-group-main").css('display','block');
        $("#user-main .fa-plus-circle.user1").removeClass("user1").addClass("datauser1");
        $(".user1 .deleteuser").css("display","none");
        $(".datauser1 .deletedatauser").css("display","block");
        $('#user-main').css('display','none');
        $('.main-heading').html('Data user module');
        $('.editdatagroup, .deletedatagroup').addClass('disable_href');
        list_all_data_user_groups();
    }
    else if(window.location.href.indexOf("cat=licensing") > -1) {
        if($($('.rawmaterial_li ').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.rawmaterial_li ').find('i')[0]).removeClass('fa-caret-down');
            $($('.rawmaterial_li ').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.finishedgood_li').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down');
            $($('.finishedgood_li').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up')) {
            $($('.menu_ul')[2]).slideToggle('');
            $($('.admin_li').find('i')[0]).removeClass('fa-caret-up');
            $($('.admin_li').find('i')[0]).addClass('fa-caret-down'); 
            $('.rawmaterial_li').find('ul').css('display','none');      
            $('.finishedgood_li').find('ul').css('display','none');
            $('.admin_li').find('ul').css('display','block');
        }
        page_index=$(".not_mobile")[2];
        $(page_index).addClass("clicked_li");

        $('.admin_li .fa-key').parent().addClass('clicked_li');
        $("#admin-licensing, .kpi-container-1").css('display','block');
        $("#user-main .fa-plus-circle.user1").removeClass("user1").addClass("datauser1");
        $(".user1 .deleteuser").css("display","none");
        license_select_values();
        $(".datauser1 .deletedatauser").css("display","block");
        $('#user-main').css('display','none');
        $('.main-heading').html('Admin licensing');
        $('.editdatagroup, .deletedatagroup').addClass('disable_href');
        $('#approved-date').removeAttr('readonly');

        $(".add-new-license").on("click", function() {
			selected_user_color();
			$(".kpi-container-1 div").removeClass("clicked_li_dg_ag");
			$(this).find('.user-mod').addClass('clicked_li_dg_ag');
			$(this).find('span').css('color','white');
			//check_procurement_licenses();
			$.ajax({
                type:"GET",
                url:"check_procurement_licenses",
                data: {"page_url": "QualityPulse_FG"},
                success:function(result){ 					
					if (result == 'Error') {
						$("#license-main").css("display","none");
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Limit crossed");
						$("#common-modal .modal-body").html("All the procured licenses are used already. Please revoke some to add new licenses."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");    
					}
					else {
						$("#license-main, .messages").css("display","none");
						$("#license-modification").css("display","block");
						$("approved-date").removeAttr("readonly");
					}
					
                    //return result;
                    // $('#user-role-group-list li.selected').click();
                }
            });
        });

        $(document).on("click",".remove-license", function() { 
            $("#license-modification, .messages").css("display","none");
			selected_user_color();
			$(this).find('span').css('color','white');
            $(".kpi-container-1 div").removeClass("clicked_li_dg_ag");
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
            $("#license-main, #license-list-datatable, .deletelicense").css("display","block");
            list_all_licenses();
        });
		
		$(document).on("click",".view-all-licenses", function() {
            $("#license-modification, .messages, .deletelicense").css("display","none");
            $(".kpi-container-1 div").removeClass("clicked_li_dg_ag");
			selected_user_color();
			$(this).find('span').css('color','white');
            $(this).find('.user-mod').addClass('clicked_li_dg_ag');
            $("#license-main, #license-list-datatable").css("display","block");
            list_all_licenses();
        });
    }
	else if(window.location.href.indexOf("cat=update_profile") > -1) {
        if($($('.rawmaterial_li ').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.rawmaterial_li ').find('i')[0]).removeClass('fa-caret-down');
            $($('.rawmaterial_li ').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.finishedgood_li').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down');
            $($('.finishedgood_li').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up')) {
            $($('.menu_ul')[2]).slideToggle('');
            $($('.admin_li').find('i')[0]).removeClass('fa-caret-up');
            $($('.admin_li').find('i')[0]).addClass('fa-caret-down'); 
            $('.rawmaterial_li').find('ul').css('display','none');      
            $('.finishedgood_li').find('ul').css('display','none');
            $('.admin_li').find('ul').css('display','block');
        }
        page_index=$(".not_mobile")[14];
        $(page_index).addClass("clicked_li");
		$('#user-modification .admin-headings').html('Update your profile');
        $('.admin_li .fa-pencil-sqaure-o').parent().addClass('clicked_li');
        $("#admin-licensing, .kpi-container-1").css('display','none');
        $("#user-main .fa-plus-circle.user1").removeClass("user1").addClass("datauser1");
        $(".user1 .deleteuser").css("display","none");
		var user_id = $('#userid').text();
        individual_user_details(user_id);
        $(".datauser1 .deletedatauser").css("display","none");
        $("#user-modification").css("display","block");
		$("#editUseremail").parent().removeClass("col-lg-6 col-md-4").addClass("col-lg-9 col-md-8");
        $("#henkel-com").css("display","none");
		$('#edituserpassword').attr("readonly","readonly");
		$('#confirmpassword').attr("readonly","readonly");
        $('#editmesstatus').attr("readonly","readonly");
		$('#user-main').css('display','none');
        $('.sub-users-1').css('display','none');
        $('.main-heading').html('Update Profile');
        $('.editdatagroup, .deletedatagroup').addClass('disable_href');
        $('#approved-date').removeAttr('readonly');

    }
	else if(window.location.href.indexOf("cat=reset_password") > -1) {
        if($($('.rawmaterial_li ').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.rawmaterial_li ').find('i')[0]).removeClass('fa-caret-down');
            $($('.rawmaterial_li ').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.finishedgood_li').find('i')[0]).hasClass('fa-caret-down')) {
            $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down');
            $($('.finishedgood_li').find('i')[0]).addClass('fa-caret-up'); 
        }
        if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up')) {
            $($('.menu_ul')[2]).slideToggle('');
            $($('.admin_li').find('i')[0]).removeClass('fa-caret-up');
            $($('.admin_li').find('i')[0]).addClass('fa-caret-down'); 
            $('.rawmaterial_li').find('ul').css('display','none');      
            $('.finishedgood_li').find('ul').css('display','none');
            $('.admin_li').find('ul').css('display','block');
        }
        page_index=$(".not_mobile")[15];
        $(page_index).addClass("clicked_li");

        $('.admin_li .fa-repeat').parent().addClass('clicked_li');
        $("#admin-licensing, .kpi-container-1").css('display','none');
        $("#user-main .fa-plus-circle.user1").removeClass("user1").addClass("datauser1");
        $(".user1 .deleteuser").css("display","none");
		var user_id = $('#user_id').text();
        individual_user_details(user_id);
        $(".datauser1 .deletedatauser").css("display","none");
        $("#user-password_modification").css("display","block");
        $('#user-main').css('display','none');
        $('.main-heading').html('Update Profile');
        $('.editdatagroup, .deletedatagroup').addClass('disable_href');
        $('#approved-date').removeAttr('readonly');
		$('#editmesstatus').attr('readonly','readonly');
    }
});

/*	function scrollDown() {		
      var vheight = $(window).height();		
	  var scroll_top;
	  if (((Math.floor($(window).scrollTop() / vheight)+1) * vheight) > 400) {
			scroll_top =  400;		
		}
		else {
			scroll_top =  (Math.floor($(window).scrollTop() / vheight)+1) * vheight;		
		}
		console.log(scroll_top);
      $('#formnewdatausergroup').animate({
		scrollTop : scroll_top
      }, 500);  		
    };

	// The scroll-down function		
    function scrollDown() {		
      var vheight = $(window).height();		
      $('#formnewdatausergroup').animate({		
        scrollTop: (Math.floor($(window).scrollTop() / vheight)+1) * vheight		
      }, 500);  		
    };
	
	
	function scrollUp() {
	  var vheight = $(window).height();
	  $('html, body').animate({
		scrollTop: (Math.ceil($(window).scrollTop() / vheight)-1) * vheight
	  }, 500);
	};
	*/
	// function to disable all the form elements
	function disable_form_elements(form_name) {
		$('#editusername').removeAttr('readonly').removeProp('readonly').removeProp("disabled");
	}
	
	
    // The scroll-down function		
    function scrollDown1() {		
      var vheight = $(window).height();		
      $('#formnewusergroup').animate({		
        scrollTop: (Math.floor($(window).scrollTop() / vheight)+1) * vheight		
      }, 500);  		
    };		
	
    //$('#plant-list-selection').SumoSelect(); 		
    //$('#technology-list-selection').SumoSelect({selectAll:true,selectAlltext : 'All',okCancelInMulti: true,triggerChangeCombined: true}).sumo.selectItem(0);		
    		
    $('#plant-list-selection').change(function(){		
        $('#technology-list-selection').SumoSelect().sumo.disable()		
        $.ajax({		
        method: "GET",		
        url: "/qualitypulse/get_filter_data_config/",		
        data:{plant:$('#plant-list-selection').val()},		
        success: function(result_data) {    		
            filter_render_config('#technology-list-selection',result_data);		
            // $('revision_count_tbody').html('');		
        }		
        });             		
    });
	
    function filter_render_config(classname,data)		
    {		
        $('#technology-list-selection').empty()		
        for (i=0;i<data.length;i++)		
        {		
        $('#technology-list-selection').append('<option  value="'+data[i]+'">'+data[i]+'</option>')    		
        }		
        if(data.length<1)		
        {		
        $('#technology-list-selection').append('<option  value="all">No options</option>')		
        }		
        $('#technology-list-selection').SumoSelect().sumo.reload()		
        $('#technology-list-selection').SumoSelect().sumo.enable() 		
        $('#technology-list-selection').SumoSelect({selectAll:true,selectAlltext : 'All',okCancelInMulti: true,triggerChangeCombined: true}).sumo.selectItem(0);		
        $('.select_search_class').remove()		
    }

    // Display this only when the logged in user belongs to the Admin group
        $('.admin_li_btn').click(function() {
            // $($('.menu_ul')[1]).slideToggle(''); 
            if($($('.admin_li').find('i')[0]).hasClass('fa-caret-up') ) {
                $($('.admin_li').find('i')[0]).removeClass('fa-caret-up').addClass('fa-caret-down');
                //$($('.admin_li ').find('i')[0]).addClass('fa-caret-down'); 
                $('.admin_li').find('ul').css('display','block');
                $('.Raw_Material_li').find('ul').css('display','none');
                $('.finishedgood_li').find('ul').css('display','none');
                // $('.top_bar a').css('visibility','hidden');
                $($('.finishedgood_li').find('i')[0]).removeClass('fa-caret-down').addClass('fa-caret-up');
                $($('.Raw_Material_li').find('i')[0]).removeClass('fa-caret-down').addClass('fa-caret-up');
            }
            else { 
                $($('.admin_li').find('i')[0]).removeClass('fa-caret-down').addClass('fa-caret-up');
                //$($('.admin_li ').find('i')[0]).addClass('fa-caret-up'); 
                $('.admin_li').find('ul').css('display','none');
            }
        });

    //  function to fetch all the user role groups from DB on load
    function list_all_user_groups() {
        $.ajax({
            type:"GET",
            async: false,
            url:"user_group",
            success:function(result){
                str='';
                for(var i=0;i < result.collect.length;i++){
                    str=str+'<li groupId="'+result.collect[i].label+'" class="single-user-group" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;border-bottom: 1px solid rgba(128, 128, 128, 0.19);" tooltip="'+result.collect[i].value+'" title="'+result.collect[i].value+'">'+result.collect[i].value+'</li>';   
                }
                $('#user-role-group-list').hide().html(str).fadeIn('slow').css('border','1px solid rgba(128, 128, 128, 0.19)');                     
            }
        }); 
    }

    //  function to fetch all the data user groups from DB on load
    function list_all_data_user_groups() {
        $.ajax({
            type:"GET",
            async: false,
            url:"data_user_group",
            success:function(result){
                str='';
                for(var i=0;i < result.collect.length;i++){
                    str=str+'<li datagroupId="'+result.collect[i].label+'" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;border-bottom: 1px solid rgba(128, 128, 128, 0.19);" title="'+result.collect[i].value+'" tooltip="'+result.collect[i].value+'" class="single-data-user-group">'+result.collect[i].value+'</li>';   
                }
                $('#data-user-group-list').hide().html(str).fadeIn('slow').css('border','1px solid rgba(128, 128, 128, 0.19)');                     
            }
        }); 
    }
	
	function selected_user_color() {
		$('.kpis_heading').each(function(){
			$(this).css('color','black');
		});
	}

    //  function to fetch all the users from DB for multiple delete
    function list_all_users() {
		$('.admin-headings').html('List of Users');
        $.ajax({
            type:"GET",
            async: false,
            url:"list_all_users",
            success:function(result){  
                var counter=0;
                var str='<thead><tr><th>Select</th>';
                for(var k in result[0]) {
                    counter++;
                    if(counter==6){
                        break;
                    }
                    col_name=k.replace(/_/g,' ');
                    str = str.toLowerCase().replace(/\b[a-z]/g, function(str) {
                        return str.toUpperCase();
                    });
                    str += '<th title="'+col_name+'">'+col_name+'</th>';
                }

                str += '</tr></thead>';
                
                str += '<tbody>';
                for(var j=0; j < result.length; j++) {
                    str += '<tr><td><input value="'+result[j]["user_id"]+'" type="checkbox"></td><td>'+result[j]["username"]+'</td><td>'+result[j]["firstname"]+'</td><td>'+result[j]["email_address"]+'</td><td>'+result[j]["created_date"]+'</td><td>'+result[j]["user_alias"]+'</td>';
                    str +='</tr>';
                }
                str += '</tbody>';
                $('#user-list-datatable').html(str);
                $("#user-list-datatable").dataTable().fnDestroy();    
                if($('.clicked_li_dg_ag .kpis_heading').text() == 'View all users') {
                    $('#user-list-datatable').DataTable(
                    {
                        'iDisplayLength': 7,
                        "bRetrive" : true,
						"scrollX": true,
						"bDeferRender": true,
                        "scrollY": "300px",
                        "aoColumnDefs" : [ {
                            'bSortable' : false,
                            'aTargets' : [ 0 ],
                            "visible": false, 
                            "targets": 0
                            } ] ,
                        "oLanguage": {
                            "sZeroRecords": "No Record Found"
                        }
                    });
                }
                else if($('.clicked_li_dg_ag .kpis_heading').text() == 'Delete Users' ) {
                    $('#user-list-datatable').DataTable(
                    {
                        'iDisplayLength': 7,
                        "bRetrive" : true,
						"scrollX": true,
						"bDeferRender": true,
                        "scrollY": "300px",
                        "aoColumnDefs" : [ {
                            'bSortable' : false,
                            'aTargets' : [ 0 ],
                            } ] ,
                        "oLanguage": {
                            "sZeroRecords": "No Record Found"
                        }
                    });
                }
                $('.dataTables_filter input').attr("placeholder", "Search");
                $('#user-list-datatable').trigger('update');
            }
        });
    }

	    //  function to fetch all the users from DB for multiple delete
    function list_all_licenses() {
		$('.admin-headings').html('List of Licenses');
        var param = '';
        if($('.clicked_li_dg_ag .kpis_heading').text() == 'Remove License(s)' ) {
            param = 'Granted';
        }
        else if($('.clicked_li_dg_ag .kpis_heading').text() == 'View All Licenses' ) {
            param = 'All';
        }
        $.ajax({
            type:"GET",
            // async: false,
            data : {'param':param},
            url:"list_all_licenses",
            success:function(result){  
                var counter=0;
                global_var = result;
                var str='<thead><tr><th>Select</th><th title="SiteName">SiteName</th><th title="Technology">Technology</th><th title="Approved By">Approved By</th><th title="UploadFilePath">Upload File Path</th><th title="ApprovedDate">Approved Date</th><th title="CreatedBy">Created By</th><th title="Description">Description</th>';
               /*  for(var k in result[0]) {
                    /*if(counter==6){
                        break;
                    }
                    col_name=k.replace(/_/g,' ');
                    //str += '<th title="'+col_name+'">'+col_name+'</th>';
					str += '<th title="SiteName">SiteName</th><th title="Technology">Technology</th><th title="Approved By">Apprived By</th><th title="UploadFilePath">Upload File Path</th><th title="ApprovedDate">Approved Date</th><th title="CreatedBy">Created By</th><th title="Description">Description</th>';
                } */

                str += '</tr></thead>';
                
                str += '<tbody>';
				if($('.clicked_li_dg_ag .kpis_heading').text() == 'Remove License(s)' ) {
					for(var j=0; j < result.length; j++) {
						str += '<tr><td><input value="TechnologyID='+result[j]["TechnologyID"]+'&SiteID='+result[j]["SiteID"]+'&SiteName='+result[j]["SiteName"]+'&TechnologyName='+result[j]["Technology"]+'&CreatedBy='+result[j]["Created By"]+'&UpdatedBy='+result[j]["Updated By"]+'&UploadFilePath='+result[j]["UploadFilePath"]+'&Description='+result[j]["Description"]+'&ApprovedBy='+result[j]["Approved By"]+'&CreatedDate='+result[j]["CreatedTimeStamp"]+'&UpdatedDate='+result[j]["Updated Date"]+'&ApprovedDate='+result[j]["Approved Date"]+'" type="checkbox"></td><td><div style="width:85px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" title="'+result[j]["SiteName"]+'">'+result[j]["SiteName"]+'</div></td><td>'+result[j]["Technology"]+'</td><td>'+result[j]["Approved By"]+'</td><td><div style="width:83px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" title="'+result[j]["UploadFilePath"]+'">'+result[j]["UploadFilePath"]+'</div></td><td>'+result[j]["Approved Date"]+'</td><td>'+result[j]["Created By"]+'</td><td style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" title="'+result[j]["Description"]+'">'+result[j]["Description"]+'</td>';
						str +='</tr>';
					}
				}
				else if($('.clicked_li_dg_ag .kpis_heading').text() == 'View All Licenses') {
					for(var j=0; j < result.length; j++) {
						str += '<tr><td><input value="TechnologyID='+result[j]["TechnologyID"]+'&SiteID='+result[j]["SiteID"]+'&SiteName='+result[j]["SiteName"]+'&TechnologyName='+result[j]["Technology"]+'&CreatedBy='+result[j]["Created By"]+'&UpdatedBy='+result[j]["Updated By"]+'&UploadFilePath='+result[j]["UploadFilePath"]+'&Description='+result[j]["Description"]+'&ApprovedBy='+result[j]["Approved By"]+'&CreatedDate='+result[j]["CreatedTimeStamp"]+'&UpdatedDate='+result[j]["Updated Date"]+'&ApprovedDate='+result[j]["Approved Date"]+'" type="checkbox"></td><td style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" title="'+result[j]["SiteName"]+'">'+result[j]["SiteName"]+'</td><td>'+result[j]["Technology"]+'</td><td>'+result[j]["Approved By"]+'</td><td><div style="width:90px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" title="'+result[j]["UploadFilePath"]+'">'+result[j]["UploadFilePath"]+'</div></td><td>'+result[j]["Approved Date"]+'</td><td>'+result[j]["Created By"]+'</td><td style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" title="'+result[j]["Description"]+'">'+result[j]["Description"]+'</td>';
						str +='</tr>';
					}
				}
                str += '</tbody>';
                $('#license-list-datatable').html(str);
                $("#license-list-datatable").dataTable().fnDestroy();    
                if($('.clicked_li_dg_ag .kpis_heading').text() == 'View All Licenses') {
                    $('#license-list-datatable').DataTable(
                    {
                        "iDisplayLength": 4,
						"bRetrive" : true,
						"scrollX": false,
						"bDeferRender": true,
						"scrollY": "300px",
                        "aoColumnDefs" : [ {
                            'bSortable' : false,
                            'aTargets' : [ 0 ],
                            "visible": false, 
                            "targets": 0
                            } ] ,
                        "oLanguage": {
                            "sZeroRecords": "No Record Found"
                        }
                    });
					$('#license-list-datatable').find('checkbox').css('display', 'none');
                }
                else if($('.clicked_li_dg_ag .kpis_heading').text() == 'Remove License(s)' ) {
                    $('#license-list-datatable').DataTable(
                    {
                        "iDisplayLength": 4,
						"bRetrive" : true,
						"scrollX": true,
						"bDeferRender": true,
						"scrollY": "300px",
                        "aoColumnDefs" : [ {
                            'bSortable' : false,
                            'aTargets' : [ 0 ],
                            } ] ,
                        "oLanguage": {
                            "sZeroRecords": "No Record Found"
                        }
                    });
                }
                $('.dataTables_filter input').attr("placeholder", "Search");
				$("#license-list-datatable").css("table-layout","fixed").css("word-wrap","break-word");
                $('#license-list-datatable').trigger('update');
            }
        });
    }
	
	
    // function to populate dropdown with all users
    function get_users_lists_into_dropdown(flag_user) {
        var flag_user_str = flag_user;
        $("#user-modification, #user-main").css("display","none");
        $("#all-users-list-select").html("");
        $("#user-main-module").css("display","block");
        $("#flag-user").text(flag_user_str);
        $("#all-users-list-select").select2({
          placeholder: 'Select an option',
        });
        populate_dropdown_all_users();
    }

    // function to populate dropdown with all users (ajax call)
    function populate_dropdown_all_users() {
        $.ajax({
            type:"GET",
            async: false,
            url:"user_list",
            success:function(result){
                str='';
                x = '';
                for(var i=0;i < result.collect.length;i++){
                    //str=str+'<li groupId="'+result.collect[i].label+'" class="single-user-group">'+result.collect[i].value+'</li>';   
                    x += "<option value='"+result.collect[i].label+"'>"+result.collect[i].value+"</option>"
                }
                $('#all-users-list-select').html('<option disabled="disabled" selected="selected" style="font-weight:bold;">Select a user</option>');
                $("#all-users-list-select").hide().append(x).fadeIn('slow');                     
            }
        });
    }

    function individual_user_details(user_id) {
        $.ajax({
            type:"GET",
            url:"user_description",
            async: false,
            data: {"user_id":user_id},
            success:function(result) { 
                var str=[]; 
                str[0] =result[0]["username"];
                str[1] =result[0]["firstname"];
                str[2] =result[0]["lastname"];
                str[3] =result[0]["email"];
                str[4] =result[0]["contact"];
                str[5] =result[0]["address"];
                str[6] =result[0]["password"];
                str[7] =result[0]["datepicker"];
                str[8] =result[0]["user_alias"];
                str[9] =result[0]["flag_active"];
				str[10] = result[0]["mes_user"];
				str[11] = result[0]["status"];

                var datetime = str[7].split("T");
                document.getElementById("editusername").value = str[0];
				document.getElementById("editusername1").value = str[0];
                $("#editusername").attr("readonly","readonly");
				$("#editusername1").attr("readonly","readonly");
                document.getElementById("editfirstname").value = str[1];
                document.getElementById("editlastname").value = str[2];
                document.getElementById("editUseremail").value = str[3];
				document.getElementById("editUseremail1").value = str[3];
                $("#editUseremail").attr("readonly","readonly");
				$("#editUseremail1").attr("readonly","readonly");
                $("#editUseremail").attr("title", str[3]);
				$("#editUseremail1").attr("title", str[3]);
                document.getElementById("edituserpassword").value = str[6];
				document.getElementById("confirmpassword").value = str[6];
                document.getElementById("editdatepicker").value = datetime[0]; 
                $("#editdatepicker").attr("disabled",true);
                document.getElementById("edituseralias").value = str[8];

                if(str[9].toString() == 'True' || str[9].toString() == 'true') {
                    $("#edituserstatus").prop('checked', true);
                }
                else if(str[9].toString() == 'False' || str[9].toString() == 'false') {
                    $("#edituserstatus").prop("checked", false);
                    $('#edituserstatus').attr('value', false).removeAttr("checked");
					$("#editmesstatus").prop("checked", false);		
                    $('#editmesstatus').attr('value', false);
                }
				if(str[11].toString() == 'True'|| str[11].toString() == 'true') {
                    $("#editmesstatus").prop('checked', true);
                }
                else{
                    $("#editmesstatus").prop("checked", false);
				}
				$("#editmesstatus").attr("readonly","readonly");
            }
        });
    }
	$(document).on('click', '#resetuserpassword', function(){

			var validUdFormEmail = 0, validUdFormusername = 0, validUdFormfirstname = 0, validUdFormpassword=0, validUdFormConfirmPswd=0, validUdFormCurrentpassword=0 ; 
			
			/*var inp1 = $("#editfirstname");
				   if(inp1.val()!= ""){
						$('#addfn').html("");
						validUdFormfirstname = 1;
					}
					else {
						$('#addfn').html("Enter firstname").css('color','red');
						$('#editfirstname').val("");
						 validUdFormfirstname = 0; 
					}*/
			if($('#currentpassword').val()!=""){
				validUdFormCurrentpassword=1;
				$('#currpassword').html('');
			}
			else{
				validUdFormCurrentpassword=0;
				$('#currpassword').html("Current Password can\'t be empty").css('color','red');
			}
			var inp2 = $("#newuserpassword");
			var pswd=inp2.val();
			var currnt = $('#currentpassword').val();
			   if($("#newuserpassword").val()!="" && pswd!=currnt){
						validUdFormpassword = 1;
						$('#addpasswrd1').html('');
				}
				else {
					validUdFormpassword = 0; 
					$('#addpasswrd1').html("Please Enter New Password");
					$('#addpasswrd1').css('color','red');
				}	
			var c_pswd = $("#confirmpassword1").val();
               if(pswd==c_pswd){
					$('#confirmpassword1').html("");
                  validUdFormConfirmPswd = 1;
				  $('#addconfirmpassword1').html("");
                }
                else {
                    $('#addconfirmpassword1').html("Password didn\'t match").css('color','red');
                    validUdFormConfirmPswd = 0; 
                }
				
			if( validUdFormpassword==1 && validUdFormConfirmPswd==1 && validUdFormCurrentpassword==1){
				$("#resetpassswrdform input").removeAttr('disabled');

				var str = $("#resetpassswrdform").serialize()
				var u_id = $('#user_id').text();
				str+='&user_id='+u_id;
				
				$.ajax({
					url: "resetUserPassword",
					type: "get",
					data: str,
					processData: false,
					contentType: false,
					//dataType : 'json',
					success: function(response){
						if(response[0]['flag']==1){
							//$('#addconfirmpassword1').html("Password Reset Succesfully").css('color','blue');
							$("#common-modal").modal("show");
							$("#common-modal .modal-title").html("Success message");
							$("#common-modal .modal-body").html("Password Reset Succesfully."); 
							$("#common-modal .modal-no").css("display","none");
							$("#common-modal .modal-yes").html("Okay");
						}
						else{
							$('#addconfirmpassword1').html("Current Password was incorrect").css('color','red');
						}
						
						$('#currentpassword, #newuserpassword, #confirmpassword1').val('');
						
					}
				});
				
			}
    });	

    $("#editusername").focusout(function(){
        if($(this).val() == "") {
            event.preventDefault;
			return false;
        }
        var user_text = $(this).val();
        var label_text = 'Username';
        if(!($(this).is('[readonly]'))) {
            $.ajax({
                type:"GET",
                async: false,
                url:"validate_user_details",
                data: {"user_text":user_text,"label_text":label_text},
                success:function(result){       
                    if(result == '0') {
                        $("#addun").html("Username already present. Choose another.").css("color","red");
                        $("#editusername").val("");
                        $("#editUseremail").val("");
                        $("#editusername").focus();
                    } else {
                        $("#addun").html("Username can be used by you.").css("color","green");
                    }
                }
            });
        }  
    });

    $("#editusername").keyup(function() {
        $("#editUseremail").val($(this).val().replace(/\s+/g, ''));
    });

    $("#Name1").focusout(function(){
        var user_text = $(this).val();
		console.log(user_text);
		if(user_text == "" || !(user_text)) {
            console.log('prevent default');
			return false;
			event.preventDefault;
        }
        var label_text = 'UserGroupname';
        var group_initials = 'AG';
        $.ajax({
            type:"GET",
            async: false,
            url:"validate_group_details",
            data: {"user_text":user_text,"label_text":label_text, "group_initials":group_initials},
            success:function(result){       
                if(result == '0') {
                    $("#groupnamealert").html("Group name already present").css("color","red");
                    $("#Name1").val("");
                }
				else {
                    $("#groupnamealert").html("Group name can be used by you.").css("color","green");
                }
            }
        });  
    });

    // function to capture the selected user id & do the functionality
    $("#all-users-list-select").change(function() {
        var flag_value = $('#flag-user').text();
		$('#user-modification .admin-headings').html('Modification of existing User');
        $("#addnewuserform .savenewuser").removeClass('savenewuser').addClass('saveedituser');
        $("#editUseremail").parent().removeClass("col-lg-6 col-md-4").addClass("col-lg-9 col-md-8");
        var user_id =  ($(this).val() != '') ? $(this).val() :  $('#selected_user_id').html();
        individual_user_details(user_id);
		$('#selected_user_id').html(user_id);
        $("#addun, #addemail, #addpasswrd, #addfn").html("");
        $("#edituserpassword").attr("readonly","readonly");
		$("#confirmpassword").attr("readonly","readonly");
        //$("#edituserstatus").attr("onclick","return false;");

        if(flag_value == 'Editing') {
            $("#edituseralias").removeAttr("readonly");
            $("#editfirstname").removeAttr("readonly");
            $("#editlastname").removeAttr("readonly");
            //$("#edituserstatus").attr("readonly","readonly").attr("checked","checked").prop("checked", true);
			$("#edituserstatus").attr("readonly","readonly");
			$("#editmesstatus").attr("readonly","readonly").attr("onclick","return false;").attr("disabled",true);
			$(".saveedituser").html("Submit");
			$(".admin-headings").html("Edit existing user");
        }
        else if (flag_value == 'Blocking/UnBlocking') {
            $("#edituseralias").attr("readonly","readonly");
            $("#editfirstname").attr("readonly","readonly");
            $("#editlastname").attr("readonly","readonly");
            $('#edituserstatus').removeAttr('onclick').removeAttr("readonly");
			$(".saveedituser").html("Block");
			$(".admin-headings").html("Block/Unblock user");
			$("#editdatepicker, #editUseremail, #editusername, #edituserpassword, #confirmpassword, #editfirstname, #editlastname, #edituseralias").attr("readonly","readonly").attr("checked","checked").attr("onclick","return false;").attr("disabled",true);
			$("#edituserstatus").removeAttr('readonly').prop('disabled',false).removeAttr('onclick');
			
			//$("#editdatepicker, #editUseremail, #editusername, #edituserpassword, #confirmpassword, #editfirstname, #editlastname, #edituseralias").removeAttr('readonly').prop('disabled',false).removeProp('disabled','disabled').removeAttr('disabled','disabled');
        }
        else if (flag_value == 'Resetting Password') {
            $("#edituserpassword").removeAttr("readonly");
			$("#confirmpassword").removeAttr("readonly");
			$("#edituserconfirmpassword").removeAttr("readonly");
            $("#edituseralias").prop("disabled", true);
            $("#editfirstname").prop("disabled", true);
            $("#editlastname").prop("disabled", true);
            $("#edituserstatus").attr("readonly","readonly");
			$(".saveedituser").html("Reset");
			$(".admin-headings").html("Reset password for user");
			$("#editmesstatus").attr("readonly","readonly").attr("checked","checked");
			$("#edituserpassword, #confirmpassword").removeAttr('readonly').prop('disabled',false).removeProp('disabled','disabled').removeAttr('disabled','disabled');
        }

        $("#updatemsg").html("");       
        $("#addnewuserform .form-group label").removeClass("required");
        $("#user-modification").css("display", "block");
        $(".sub-users-1").css("display", "none");
        $(".sub-users-2").css("display", "block");
        $(".users1").css("display", "none");
        $(".users2").css("display", "block");
        $(".edituser").css("display","none");
        $(".edituser1").css("display","block");
        $(".saveuser").css("display", "block");
        $("#alertemailmsg").html("");
        $("#alertcontactmsg").html("");
        $("#editfn").html("");
        $("#editun").html("");
        $("#editemail").html("");
        $("#editpasswrd").html("");
    }); 

    // click event to display user name on click of a particular user role group
    $(document).on('click', '#user-role-group-list li', function(){
        $("#savenewusergroup").html("");
        $('#user-role-group-list li').removeClass("selected").css("background-color", "");
        $(this).addClass("selected");
        $(".icon-object-group a").removeClass("disable_href");
        $("#updatedmsg").html("");
        $("#user-modification, #user-role-group-modification").css("display", "none");
        $("#user-role-group-list li.selected").css('background-color','gray');
        $('.deletegroup, .editgroup').removeClass('disable_href').css("cursor", "pointer");
        var group_id=$(this).attr('groupId');
        var pass_url = "list_related_users";
        var populating_id = $("#user-list-datatable");
        var label_text = '';
        $("#user-main").css("display","block");
        var group_name = $("#user-role-group-list li.selected").text();
        $("#user-heading").html("List of users in \"" + group_name + "\"");
        datatable_formation(group_id, pass_url, populating_id, label_text);
    });

    // click event to display user name on click of a particular user role group
    $(document).on('click', '#data-user-group-list li', function(){
        $("#savenewusergroup").html("");
        $('#data-user-group-list li').removeClass("selected").css("background-color", "");
        $(this).addClass("selected");
        $(".icon-object-group a").removeClass("disable_href");
        $("#updatedmsg").html("");
        $("#user-main").css("display","block");
        $("#user-main .fa-trash").removeClass("delete-from-user deleteuser").addClass("delete-from-data-group");
        $("#user-data-group-name").html('"'+$(this).text()+'"'); 
        $("#user-modification, #data-user-group-modification").css("display", "none");
        $("#data-user-group-list li.selected").css('background-color','gray');
        $('.deletedatagroup, .editdatagroup').removeClass('disable_href').css("cursor", "pointer");
        var group_id=$(this).attr('datagroupId');
        var group_name = $("#data-user-group-list li.selected").text();
        $("#user-heading").html("List of users in \"" + group_name + "\"");
        var populating_id = $("#user-list-datatable");
        var pass_url = 'list_related_datausers';
        var label_text = 'DG';
        datatable_datauser_formation(group_id, pass_url, populating_id, label_text);
    });

    function datatable_formation(group_id, pass_url, populating_id, label_text) {
		$(".admin-headings").css("display","block");
        if(label_text != '')
        {
            $.ajax({
                type:"GET",
                async: false,
                url:pass_url,
                data:{"group_id":group_id, "label_text":label_text},
                success:function(result){  
                    if (result.length == 0) {
						$(".admin-headings").css("display","none");
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Warning!");
                        $("#common-modal .modal-body").html("No users present in the selected group"); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        $("#user-list-datatable_wrapper").css("display","none");
                        return false;
                    }
                    else {
                        var counter=0;
                        var str='<thead><tr><th>Select</th>';
                        for(var k in result[0]) {
                            counter++;
                            if(counter==6){
                                break;
                            }
                            col_name=k.replace(/_/g,' ');
                            str += '<th title="'+col_name+'">'+col_name+'</th>';
                        }

                        str += '</tr></thead>';
                        
                        str += '<tbody>';
                        for(var j=0; j < result.length; j++) {
                            str += '<tr><td><input value="'+result[j]["user_id"]+'" type="checkbox"></td><td>'+result[j]["username"]+'</td><td>'+result[j]["firstname"]+'</td><td>'+result[j]["email_address"]+'</td><td>'+result[j]["created_date"]+'</td><td>'+result[j]["user_alias"]+'</td>';
                            str +='</tr>';
                        }
                        str += '</tbody>';
                        $(populating_id).html(str);
                        $(populating_id).dataTable().fnDestroy();    
                        $(populating_id).DataTable(
                        {
                            'iDisplayLength': 7,
                            "bRetrive" : true,
							"bDeferRender": true,
							"scrollX": true,
                            "scrollY": "300px",
                            "aoColumnDefs" : [ {
                                'bSortable' : false,
                                'aTargets' : [ 0 ]
                                } ] ,

                            "oLanguage": {
                                "sZeroRecords": "No Record Found"
                            }
                        });
                        $('.dataTables_filter input').attr("placeholder", "Search");
                        $(populating_id).trigger('update');
                       }
                   }
                });
            }
            else {
                $.ajax({
                type:"GET",
                url:pass_url,
                async: false,
                data:{"group_id":group_id},
                success:function(result){  
                    if (result.length == 0) {
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Warning!");
                        $("#common-modal .modal-body").html("No users present in the selected group"); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        $("#user-list-datatable_wrapper").css("display","none");
                        return false;
                    }
                    else 
                    {
                        var counter=0;
                        var str='<thead><tr><th>Select</th>';
                        for(var k in result[0]) {
                            counter++;
                            if(counter==6){
                                break;
                            }
                            col_name=k.replace(/_/g,' ');
                            str += '<th title="'+col_name+'">'+col_name+'</th>';
                        }

                        str += '</tr></thead>';
                        
                        str += '<tbody>';
                        for(var j=0; j < result.length; j++) {
                            str += '<tr><td><input value="'+result[j]["user_id"]+'" type="checkbox"></td><td>'+result[j]["username"]+'</td><td>'+result[j]["firstname"]+'</td><td>'+result[j]["email_address"]+'</td><td>'+result[j]["created_date"]+'</td><td>'+result[j]["user_alias"]+'</td>';
                            str +='</tr>';
                        }
                        str += '</tbody>';
                        $(populating_id).html(str);
                        $(populating_id).dataTable().fnDestroy();    
                        $(populating_id).DataTable(
                        {
                            'iDisplayLength': 7,
                            "bRetrive" : true,
							"bDeferRender": true,
							"scrollX": true,
                            "scrollY": "300px",
                            "aoColumnDefs" : [ {
                                'bSortable' : false,
                                'aTargets' : [ 0 ]
                                } ] ,

                            "oLanguage": {
                                "sZeroRecords": "No Record Found"
                            }
                        });
                        $('.dataTables_filter input').attr("placeholder", "Search");
                        $(populating_id).trigger('update');
                    }   
                }
            });
        }
    }

    function datatable_datauser_formation(group_id, pass_url, populating_id, label_text) {
        if(label_text != '') {
            $.ajax({
                type:"GET",
                url:pass_url,
                async: false,
                data:{"group_id":group_id, "label_text":label_text},
                success:function(result){  
                    var counter=0;
                    var str='<thead><tr><th>Select</th>';
                    for(var k in result[0]) {
                        counter++;
                        if(counter==6){
                            break;
                        }
                        col_name=k.replace(/_/g,' ');
                        str += '<th title="'+col_name+'">'+col_name+'</th>';
                    }

                    str += '</tr></thead>';
                    
                    str += '<tbody>';
                    if (result.length == 0) {
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Warning!");
                        $("#common-modal .modal-body").html("No users present in the selected group"); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        $("#user-list-datatable_wrapper").css("display","none");
                        return false;
                    }
                    else {
                        for(var j=0; j < result.length; j++) {
                            str += '<tr><td><input value="'+result[j]["user_id"]+'" type="checkbox"></td><td>'+result[j]["username"]+'</td><td>'+result[j]["firstname"]+'</td><td>'+result[j]["email_address"]+'</td><td>'+result[j]["created_date"]+'</td><td>'+result[j]["user_alias"]+'</td>';
                            str +='</tr>';
                        }
                        str += '</tbody>';
                        $(populating_id).html(str);
                        $(populating_id).dataTable().fnDestroy();    
                        $(populating_id).DataTable(
                        {
                            'iDisplayLength': 7,
                            "bRetrive" : true,
							"bDeferRender": true,
							"scrollX": true,
                            "scrollY": "300px",
                            "aoColumnDefs" : [ {
                                'bSortable' : false,
                                'aTargets' : [ 0 ]
                                } ] ,

                            "oLanguage": {
                                "sZeroRecords": "No Record Found"
                            }
                        });
                        $('.dataTables_filter input').attr("placeholder", "Search");
                        $(populating_id).trigger('update');
                    }
                }
            });
        }
        else {
            $.ajax({
                type:"GET",
                url:pass_url,
                async: false,
                data:{"group_id":group_id},
                success:function(result){  
                    var counter=0;
                    var str='<thead><tr><th>Select</th>';
                    for(var k in result[0]) {
                        counter++;
                        if(counter==6){
                            break;
                        }
                        col_name=k.replace(/_/g,' ');
                        str += '<th title="'+col_name+'">'+col_name+'</th>';
                    }

                    str += '</tr></thead>';
                    
                    str += '<tbody>';
                    if (result.length == 0) {
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Warning!");
                        $("#common-modal .modal-body").html("No users present in the selected group"); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        $("#user-list-datatable_wrapper").css("display","none");
                        return false;
                    }
                    else {
                        for(var j=0; j < result.length; j++) {
                            str += '<tr><td><input value="'+result[j]["user_id"]+'" type="checkbox"></td><td>'+result[j]["username"]+'</td><td>'+result[j]["firstname"]+'</td><td>'+result[j]["email_address"]+'</td><td>'+result[j]["created_date"]+'</td><td>'+result[j]["user_alias"]+'</td>';
                            str +='</tr>';
                        }
                        str += '</tbody>';
                        $(populating_id).html(str);
                        $(populating_id).dataTable().fnDestroy();    
                        $(populating_id).DataTable(
                        {
                            'iDisplayLength': 7,
                            "bRetrive" : true,
							"bDeferRender": true,
							"scrollX": true,
                            "scrollY": "300px",
                            "aoColumnDefs" : [ {
                                'bSortable' : false,
                                'aTargets' : [ 0 ]
                                } ] ,

                            "oLanguage": {
                                "sZeroRecords": "No Record Found"
                            }
                        });
                        $('.dataTables_filter input').attr("placeholder", "Search");
                        $(populating_id).trigger('update');
                        var group_name = $("#data-user-group-list li.selected").text();
                        $("#user-heading").html("List of users in \"" + group_name + "\"");
                    }
                }
            });
        }
    }

    $('.new-user-form').on('click', function() {
        $("#user-main").css("display", "block");
        $(".sub-users-1").css("display","none");
        $("#user-modification, .sub-users-2").css("display", "block");
        $("#sublevel2_user").css("display", "none");
        $("#sublevel1_user").toggle();
        $("#addfn").html("");
        $("#addun").html("");
        $("#addemail").html("");
        $("#addpasswrd").html("");
        $("#updatedmsg").html("");
        var custId = 1;
        $.ajax({
            type:"GET",
            // async: false,
            url:"user_group",
            success:function(result){
                str='';
                for(var i=0;i < result.collect.length; i++){
                    str=str+'<option value="'+result.collect[i].label+'">'+result.collect[i].value+'</option>';
                }
                $('#usergrouplist').hide().html(str).fadeIn('slow');                 
            }
        });  

        $.ajax({
            type:"GET",
            // async: false,
            url:"user_list",
            success:function(result){       
                str='';
                for(var i=0;i < result.collect.length; i++){
                    str=str+'<li userid="'+result.collect[i].label+'"class="single-user" >'+result.collect[i].value+'</li>';                  
                }
                $('#add-new-user').hide().html(str).fadeIn('slow');
                
            }
        });   
    }); 

        // function to check whether the selected user role group is already selected or not
        $("#selected-user-groups").on('click','.fa-times',function() {
            for (var i = $("#usergrouplist option").length - 1; i >= 0; i--) {
                if($($("#usergrouplist option")[i]).text() == $(this).parent().text())
                {
                    $(this).parent().parent().remove();     
                }
            }
        });

        // ON click of '+' in add user role group
        $(document).on('click', '.group1', function(){
            $("#Name1").val("").prop("disabled",false);
			$('#user-role-group-modification .admin-headings').html('Addition of new App User Group');
            $("#description").val("");
            $("#comments").val("");
            $("#savenewusergroup").html("");
            selected_pages_list = [];
            $("#groupnamealert").html("");
            $("#user-modification, #user-main").css("display","none");
            $("#user-role-group-modification").css("display", "block");
            createRegionTreeSelect($('#pages-list-select'));
        });

        function license_select_values() {
            $.ajax({
                type:"GET",
                // async: false,
                url:"licenses_select2_values",
                success:function(result){       
                    license_sumo_select(result, 'plant', 'sitename-list-select', 'SiteName', 'SiteID');
                    license_sumo_select(result, 'technology', 'technology-list-select', 'Technology', 'TechnologyID');
                    
                    // $('#tech-list-selection').SumoSelect({selectAll:true,selectAlltext : 'All',okCancelInMulti: true,triggerChangeCombined: true});
                    // add_search();
                }
            });
        }

        function license_sumo_select(result, data, populating_id, filter_val, filter_val_id) {
            var str = '<select class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="'+data+'-list-selection" name="'+data+'-list" style="width:100px; !important">';
            if(result[filter_val].length == 0) {
            }
            else {
                for(var i = 0; i < result[filter_val].length; i++) {
                    str += "<option value='"+result[filter_val][i][filter_val_id]+"_"+result[filter_val][i][filter_val]+"'>"+result[filter_val][i][filter_val]+"</option>";
                }
            }
            str += "</select>";
            $('#'+populating_id).html(str);
            $('#'+data+'-list-selection').SumoSelect();
        }

        // ON click of '+' in data user group
        $(document).on('click', '.datagroup1', function(){
            $("#dataName1").val("").prop("disabled",false);
			$('#data-user-group-modification .admin-headings').html('Addition of new Data User roup');
            $("#data-description").val("");
            $("#data-comments").val("");
            $("#savenewdatausergroup").html("");
            $("#datagroupnamealert").html("");
            $("#user-modification, #user-main").css("display","none");
            $("#data-user-group-modification").css("display", "block");
            fetch_select2_values();
        });

        function fetch_select2_values() {
            $.ajax({
                type:"GET",
                async: false,
                url:"fetch_select2_values",
                success:function(result){  
                    individual_sumo_select(result, 'SiteName', $("#sitename_list"));
                    individual_sumo_select(result, 'FGProducts', $("#fgproducts_list"));
                    individual_sumo_select(result, 'Technology', $("#technology_list"));
                    individual_sumo_select(result, 'LineName', $("#linename_list"));
                    individual_sumo_select(result, 'Resources', $("#resources_list"));
                    individual_sumo_select(result, 'SBU', $("#sbu_list"));
                    add_search();
                }
            });
        }

        function individual_sumo_select(result, data, populating_id) {
            var str = '<select id="'+data+'_list" multiple style="width:150px; !important">';
            if(result[data].length == 0) {
            }
            else {
                for(var i = 0; i < result[data].length; i++) {
                    str += "<option value='"+result[data][i][4]+"'>"+result[data][i][1]+"</option>";
                }
            }
            str += "</select>";
            $(populating_id).html(str);
            $('#'+data+'_list').SumoSelect({selectAll:true,selectAlltext : 'All',okCancelInMulti: true,triggerChangeCombined: true});
        }


        // ON click of + in add user
        $('.user1').click(function() {
            group_id = $('#user-role-group-list li.selected').attr('groupId');
            var pass_url = "absent_user_list";
            var populating_id = $("#user-list-datatable");
            $(this).css("display","none");
            var label_text = 'AG';
            $(".deleteuser").css("display","none");
            datatable_formation(group_id, pass_url, populating_id, label_text);
            $(".absent-list-save, .absent-list-back").css({"display":"inline-block","position":"relative","float":"right","margin-bottom":"10px"});
            var group_name = $("#user-role-group-list li.selected").text();
            $("#user-heading").html("List of users NOT PRESENT in \"" + group_name + "\"");
        });

        $(document).on("click", ".absent-list-save", function() {
            $(".absent-list-save, .absent-list-back").css("display","none");
            var pass_url = "list_related_users";
            var populating_id = $("#user-list-datatable");
            var label_text = '';
            $(".newuser_yes").click();
            if(window.location.href.indexOf("cat=datagroup") > -1) {
                var group_name = $("#data-user-group-list li.selected").text();
				label_text = 'DG';
                pass_url = 'list_related_datausers';
                $(".datauser1, .delete-from-data-group").css("display","block");
                datatable_datauser_formation(group_id, pass_url, populating_id, label_text);
            }
            else if(window.location.href.indexOf("cat=usergroup") > -1) {
                var group_name = $("#user-role-group-list li.selected").text();
                pass_url = 'list_related_users';
				label_text = 'AG';
                $(".user1, .deleteuser").css("display","block");
                datatable_formation(group_id, pass_url, populating_id, label_text);
            }
        })

        $(document).on("click", ".absent-list-back", function() {
            $(".absent-list-save, .absent-list-back").css("display","none");
            if(window.location.href.indexOf("cat=datagroup") > -1) {
                $(".datauser1, .delete-from-data-group").css("display","block");
                var group_name = $("#data-user-group-list li.selected").text();
                $("#user-heading").html("List of users in \"" + group_name + "\"");
                $("#data-user-group-list li.selected").click();
            }
            else if(window.location.href.indexOf("cat=usergroup") > -1) { 
                $(".user1, .deleteuser").css("display","block");
                var group_name = $("#user-role-group-list li.selected").text();
                $("#user-heading").html("List of users in \"" + group_name + "\"");
                $("#user-role-group-list li.selected").click();
                $(".user1").css("display","block");
            }
        });

        // ON click of + in add user
        $(document).on('click','.datauser1', function() {
            var group_id = $('#data-user-group-list li.selected').attr('datagroupId');
            var pass_url = "absent_user_list";
            var populating_id = $("#user-list-datatable");
            $(".datauser1, .delete-from-data-group").css("display","none");
            var label_text = 'DG';
            var group_name = $("#data-user-group-list li.selected").text();
            $("#user-heading").html("List of users NOT PRESENT in \"" + group_name + "\"");
            datatable_datauser_formation(group_id, pass_url, populating_id, label_text);
            $(".absent-list-save, .absent-list-back").css({"display":"inline-block","position":"relative","float":"right","margin-bottom":"10px"});
            var group_name = $("#data-user-group-list li.selected").text();
            $("#user-heading").html("List of users NOT PRESENT in \"" + group_name + "\"");
        });

        $(".deleteuser").on("click", function() {
            //alert("working");
			var table = $('#user-list-datatable').DataTable();
			//alert( 'Rows '+table.rows( '.selected' ).count()+' are selected' );
			//return false;
        });

        $(".delete-from-data-group").on("click", function() {
            //alert("delete");
        });
		
		function check_procurement_licenses() {
            $.ajax({
                type:"GET",
                url:"check_procurement_licenses",
                data: {"page_url": "QualityPulse_FG"},
                success:function(result){ 
					if (result == 'Error') {
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Limit crossed");
						$("#common-modal .modal-body").html("All the procured licenses are used already. Please revoke some to add new licenses."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");    
		//				return false;
					}
					else {
						$(".kpi-container-1 div").removeClass("clicked_li_dg_ag");
						$(this).find('.user-mod').addClass('clicked_li_dg_ag');
						$("#license-main, .messages").css("display","none");
						$("#license-modification").css("display","block");
						$("approved-date").removeAttr("readonly");
					}
                    //return result;
                    // $('#user-role-group-list li.selected').click();
                }
            });
        }

        $(".deletelicense").on("click",function() {
            $("#common-modal").modal("show");
            // var selected_license = $('#license-list-datatable tr').find('input:checked').parent().parent();
            /* var selected_license = $('#license-list-datatable tr input:checked');
            console.log(selected_license.length);
            str = '';
            for(var i=0; i < selected_license.length;i++) {
                console.log(selected_license[i].value);
                // x = $(selected_license[i]).find('td');
                // console.log(x);
                // str += selected_license[i] + '&';
            } */
            $("#common-modal .modal-title").html("Confirmation message");
            $("#common-modal .modal-body").html("Are you sure you want to revoke the selected licenses?"); 
            $("#common-modal .modal-no").html("Cancel");
            $("#common-modal .modal-yes").html("Okay").addClass('revokelicense_yes1');
			$('.revokelicense_yes1').on('click', function() {
				submit_action()
			});
            //$('.revokelicense_yes').click();
        });
		
        /* $(".revokelicense_yes").on('click', function() {
            console.log('inside revokelicense_yes');
            var selected_license = $('#license-list-datatable tr input:checked');
            var license_str = '';
            for(var i=0; i < selected_license.length; i++) {
                license_str += selected_license[i].value;
                console.log(global_var);
                $.ajax({
                    type:"GET",
                    async: false,
                    url:"revoke_license",
                    data: {"license_str": license_str},
                    success:function(result){       
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Success message");
                        $("#common-modal .modal-body").html("Licenses successfully revoked."); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        // $('#user-role-group-list li.selected').click();
						//list_all_licenses();
                    }
                });  
            }
        }); */
		
		
		function submit_action(){
            var selected_license = $('#license-list-datatable tr input:checked');
            var license_str = '';
            for(var i=0; i < selected_license.length; i++) {
                license_str += selected_license[i].value;
                $.ajax({
                    type:"GET",
                    async: false,
                    url:"revoke_license",
                    data: {"license_str": license_str},
                    success:function(result){       
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Success message");
                        $("#common-modal .modal-body").html("Licenses successfully revoked."); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        // $('#user-role-group-list li.selected').click();
						//list_all_licenses();
                    }
                });  
            }
		}
		

        $(".deleteuser_yes").on("click", function() {
            var selected_users = $('#user-list-datatable tr input:checked');
            var user_str = '';
            for(var i=0; i < selected_users.length; i++) {
                user_str += selected_users[i].value + ',';
            }

            if(window.location.href.indexOf("cat=datagroup") > -1) {
                var label_text = "DG";
                var group_id = $("#data-user-group-list li.selected").attr("datagroupId");
            }
            else if(window.location.href.indexOf("cat=usergroup") > -1) {
                var label_text = "AG";
                var group_id = $("#user-role-group-list li.selected").attr("groupId");
            }

            user_str = user_str.slice(0, -1);
            
            $.ajax({
                type:"GET",
                async: false,
                url:"new_user_absent_list",
                data: {"group_id": group_id, "user_str":user_str, "action":'D', "label_text":label_text},
                success:function(result){       
                    if(window.location.href.indexOf("cat=datagroup") > -1) {
                        $("#data-user-group-list li.selected").click();
                    }
                    if(window.location.href.indexOf("cat=usergroup") > -1) {
                        $("#user-role-group-list li.selected").click();
                    }
                }
            });  
        });

        function success_deletion() {
            $.ajax({
                type:"GET",
                // async: false,
                url:"list_related_users",
                data:{"group_id":group_id},
                success:function(response){  
                    $("#common-modal").modal("show");
                    $("#common-modal .modal-title").html("Success message");
                    $("#common-modal .modal-body").html("Users successfully added."); 
                    $("#common-modal .modal-no").css("display","none");
                    $("#common-modal .modal-yes").html("Okay");
                    $('#user-role-group-list li.selected').click();
                }
            });
        }

        $(".deletefromuser_yes").on("click", function() {
            var selected_users = $('#user-list-datatable tr input:checked');
            var group_id = $("#user-role-group-list li.selected").attr("groupId");
            var user_str = '';
            for(var i=0; i < selected_users.length; i++) {
                user_str += selected_users[i].value + ',';
            }
            user_str = user_str.slice(0, -1);
            $.ajax({
                type:"GET",
                async: false,
                url:"delete_from_user",
                data: {"user_str":user_str},
                success:function(result){       
                    $("#common-modal").modal("show");
                    $("#common-modal .modal-title").html("Success message");
                    $("#common-modal .modal-body").html("Users removed successfully."); 
                    $("#common-modal .modal-no").css("display","none");
                    $("#common-modal .modal-yes").html("Okay");
                    list_all_users();
                }
            }); 
        }); 

        // search on alpahabtical order for user role group
        $("#groupsearch").on("keyup", function(){
            var value = $(this).val().toLowerCase();
            $("#user-role-group-list > li").each(function() {
                if ($(this).text().toLowerCase().search(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        });

        // search on alpahabtical order for user role group
        $("#datagroupsearch").on("keyup", function(){
            var value = $(this).val().toLowerCase();
            $("#data-user-group-list > li").each(function() {
                if ($(this).text().toLowerCase().search(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        });

        // search on alpahabtical order for user 
        $("#usersearch").on("keyup", function(){
            var value = $(this).val().toLowerCase();
            $("#add-new-user > li").each(function() {
                if ($(this).text().toLowerCase().search(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        });

        // cancels the present user form
        $('.canceluser').click(function() {
            $("#user-modification").css("display","none");
            $('#select2-all-users-list-select-container').text('');
			$(".clicked_li_dg_ag").trigger('click');
        });

        $('.cancellicense').click(function() {
            $("license-modification input").val("");
			$("#addtl #addpl #addapprovedby #addapproveddate #adddescription").html("");
            $("#license-modification").css("display","none");
            $(".kpi-container div").removeClass("clicked_li_dg_ag");
        });

        $(document).on('click', '.canceldatagroup', function() {
            $("#data-user-group-modification").css("display", "none");
        });

        // cancels the present user role group form
        $('.cancelgroup').click(function() {
            $("#user-role-group-modification").css("display","none");
        });

        // add new user role group
        $(document).on('click', '.addnewgroup', function(){
            event.preventDefault();
            var selected_pages = $("#pages-list-select input:checked");
            var user_str = '';
            if(selected_pages.length >= 1) {
                for(var i=0; i < selected_pages.length; i++) {
                    var page_id = selected_pages[i].id.match(/\d+/);
                    user_str += parseInt(page_id) + ',';
                }
            }
            user_str = user_str.slice(0, -1);
            var validUdFormName = 0;
            var inp = $("#Name1");
            if(inp.val()!= "" || inp.val() != " ") {
                validUdFormName = 1;
            }
            else {
                $("#groupnamealert").html("Groupname is mandatory").css("color","red");
                $('#Name1').val("");
                validUdFormName = 0; 
            }
            if(selected_pages.length != 0) {
                validUdFormName = 1;
            }
            else {
                $("#savenewusergroup").html("Please select associated pages.").css("color","red");
                validUdFormName = 0; 
            }
            if(validUdFormName == 1){
                var str = $("#formnewusergroup").serialize();
                var description_text = $("#description").val();
                var comments_text = $("#comments").val();
                var name_text = $("#Name1").val();
                $.ajax({
                    url: "new_user_role_group",
                    type: "get",
                    async: false,
                    data: {"pages_list": user_str, "name": name_text, "description": description_text, "comments": comments_text},
                    success: function(response){
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Success message");
                        $("#common-modal .modal-body").html("Group created/altered successfully."); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        list_all_user_groups();
                        $('.deletegroup, .editgroup').addClass('disable_href').css("cursor","default");
                        $("#savenewusergroup").html("");
                        $("#groupnamealert").html("");
                        $("#user-role-group-modification").css("display","none");
                    }
                });
            }   
        });

        function individual_param_list() {
            ids_list=["#SiteName_list","#FGProducts_list","#Technology_list","#LineName_list","#Resources_list","#SBU_list"]
            var value_list = '';
            for (j=0;j<ids_list.length;j++)
            {
                value_id=$(ids_list[j]).val()
                if(value_id) {
                    for(var i = 0; i < value_id.length; i++) {
                        value_list += value_id[i] + ',';
                    }
                }
            }
            return value_list;            
        }

        // add new user role group
        $(document).on('click', '.addnewdatagroup', function(){
            event.preventDefault();
            var user_str = individual_param_list();
            user_str = user_str.slice(0, -1);

            var validUdFormName = 0;
            var inp = $("#dataName1");
            if(inp.val()!= "") {
                $("#datagroupnamealert").html("");
                validUdFormName = 1;
            }
            else {
                $("#datagroupnamealert").html("Groupname is mandatory").css("color","red");
                $('#dataName1').val("");
                validUdFormName = 0; 
            }
            if(user_str.length != 0) {
                validUdFormName = 1;
            }
            else {
                $("#savenewdatausergroup").html("Please select values for filter.").css("color","red");
                validUdFormName = 0; 
            }
            if(validUdFormName == 1){
                var str = $("#formnewdatausergroup").serialize();
                var description_text = $("#data-description").val();
                var comments_text = $("#data-comments").val();
                var name_text = $("#dataName1").val();
                $.ajax({
                    url: "new_data_user_group",
                    type: "get",
                    async: false,
                    //data: {"form_details": str,"pages_list":user_str},
                    data: {"values_list": user_str, "name": name_text, "description": description_text, "comments": comments_text},
                    success: function(response){
                        //alert("Application user group table changed successfully.");
                        $("#common-modal").modal("show");
                        $("#common-modal .modal-title").html("Success message");
                        $("#common-modal .modal-body").html("Group created/altered successfully."); 
                        $("#common-modal .modal-no").css("display","none");
                        $("#common-modal .modal-yes").html("Okay");
                        list_all_data_user_groups();
                        $('.deletegroup, .editgroup').addClass('disable_href').css("cursor","default");
                        $("#savenewdatausergroup").html("");
                        $("#datagroupnamealert").html("");
                        $("#data-user-group-modification").css("display","none");
                        $('#formnewdatausergroup input').val('');
                    }
                });
            }   
        });

        //---------------------
        $(".deletegroup").click(function(){
            var group_id= $('#user-role-group-list li.selected').attr('groupId');
            var selected_group = $("#user-role-group-list li.selected").text();
            var selection_text = 'AG';
            $("#populated-groupname").html(selected_group); 
            $.ajax({
                url: "count_users",
                async: false,
                type: "get",
                data: {"group_id":group_id, "selection_text":selection_text},
                success: function(response){
                    $("#groupuser1").html(response);
                }
            });
        });

        //---------------------
        $(document).on("click", ".deletedatagroup",function(){
            var group_id= $('#data-user-group-list li.selected').attr('datagroupId');
            var selected_group = $("#data-user-group-list li.selected").text();
            var selection_text = 'DG';
            $("#populated-groupdataname").html(selected_group); 
            $.ajax({
                url: "count_users",
                async: false,
                type: "get",
                data: {"group_id":group_id, "selection_text": selection_text},
                success: function(response){
                    $("#groupdatauser1").html(response);
                }
            });
        });

        // delete user role group
        $('.usergroup_yes').click(function(){
            var group_id= $('#user-role-group-list li.selected').attr('groupID');
            var name= $('#user-role-group-list li.selected').text();
            var x = window.location.href.indexOf("cat=usergroup")
            var y = window.location.href.indexOf("cat=datagroup")
            if (x > -1) {
                var selection_text = 'AG'
            }
            else if (y > -1) {
                var selection_text = 'DG'
            }
            $.ajax({
                url: "delete_data_user_group",
                type: "get",
                async: false,
                data: {"group_id":group_id, "selection_text":selection_text},
                success: function(response){
                    list_all_user_groups();
                }
            }); 
            $('.deletegroup').addClass('disable_href');
            $('.editgroup').addClass('disable_href');
            $('#sublevel2_user').css("display","none");
            $('#sublevel1_user').css("display","none");
            $('#myModalusergroup').modal('hide');
            $('#user-main').css('display',"none");
            $('#level4').css('display',"none");
        });

        // delete user role group
        $('.datausergroup_yes').click(function(){
            var group_id= $('#data-user-group-list li.selected').attr('datagroupId');
            var name= $('#data-user-group-list li.selected').text();
            var x = window.location.href.indexOf("cat=usergroup");
            var y = window.location.href.indexOf("cat=datagroup");
            if (x > -1) {
                var selection_text = 'AG'
            }
            else if (y > -1) {
                var selection_text = 'DG'
            }
            $.ajax({
                url: "delete_data_user_group",
                type: "get",
                async: false,
                data: {"group_id":group_id, "selection_text":selection_text},
                success: function(response){
                    // list_all_user_groups();
                    list_all_data_user_groups();
                }
            }); 
            $('.deletegroup').addClass('disable_href');
            $('.editgroup').addClass('disable_href');
            $('#sublevel2_user, #data-user-group-modification').css("display","none");
            $('#sublevel1_user').css("display","none");
            $('#myModalusergroup').modal('hide');
            $('#user-main').css('display',"none");
            $('#level4').css('display',"none");
        });


        $('.newuser_yes').click(function() {
            if(window.location.href.indexOf("cat=datagroup") > -1) {
                var group_id=$('#data-user-group-list li.selected').attr("datagroupId");
                var label_text = "DG";
            }
            else if(window.location.href.indexOf("cat=usergroup") > -1) {
                var group_id=$('#user-role-group-list li.selected').attr("groupId");
                var label_text = "AG";
            }
            var new_users = $('#user-list-datatable tr input:checked');
            var pass_url = "list_related_users";
            var populating_id = $("#user-list-datatable");
            
            var user_str = '';
            $('#user-list-datatable tr input:checked').each(function() {
                user_str += $(this).val() + ',';
            });
            user_str = user_str.slice(0,-1);
            $.ajax({
                type:"GET",
                url:"new_user_absent_list",
                async: false,
                data: {"group_id": group_id, "user_str":user_str, "action":'I', "label_text":label_text},
                success:function(result){       
                    if(window.location.href.indexOf("cat=datagroup") > -1) {
                        $('#data-user-group-list li.selected').click();
                    }
                    else if(window.location.href.indexOf("cat=usergroup") > -1) {
                        $('#user-role-group-list li.selected').click();
                    }
                }
            });
        });

        function createRegionTreeSelect(id) {
            $.ajax({
                    type: "GET",
                    async: false,
                    url: "getRegionHierarchyData",
                    data:{
                        //filter_data
                        },
                    dataType:'json',
                    success: function(result) {
                        var idToNodeMap = {};
                        var root = [];
                        for(var i = 0; i < result.length; i++) {
                            var datum = result[i];
                            datum.children = [];
                            datum.has_children = false;
                            idToNodeMap[datum.clientId] = datum;
                            
                            if(typeof datum.parentClientId === "undefined" || datum.parentClientId==0) {
                                root.push(datum);        
                            } else {
                                parentNode = idToNodeMap[datum.parentClientId];
                                parentNode.children.push(datum);
                                parentNode.has_children=true;
                            }
                        }
                        var prop={ id: 0, title: "Select All", has_children: true, level: 0, children: root };
                        $(id).html('');
                        var JSONObject = prop;
                        $(id).chosentree({
                            width: 500,
                            deepLoad: true,
                            inputName: 'abc',
                            input_placeholder: 'Search',
                            input_type: 'search',
                            showtree: true,
                            showroot: true,
                            autosearch: true,
                            allLoaded: true,
                            load: function (node, callback) {
                                callback(JSONObject);
                            },
                            treeloaded: function () {
                                $('.chzntree-search').show();
                                for(var i=0; i < selected_pages_list.length; i++) {
                                    $('input:checkbox[id="choice_'+selected_pages_list[i]+'"]').prop('checked', true);
                                } 
                            }
                        });
                        
                    }
                    });
            }


        // to display in edit form of user role group
        $(document).on('click', '.editgroup', function(){
            $("#user-role-group-modification, #formnewusergroup").css("display","block");
            $('#editusergroupname').html("");
			$('#user-role-group-modification .admin-headings').html('Modification of existing App User roup');
            $("#savenewusergroup").html("");
            var groupname=$('#user-role-group-list li.selected').text();
            var group_id=$('#user-role-group-list li.selected').attr("groupId");
            $('#user-main').css('display','none');
            $("#user-role-group-modification, #formnewusergroup, #editgroupuserdetails").css("display", "block");
            $('#usergrouptoggle1').change(function(){
                a = $(this).prop('checked');
            });
            
            $.ajax({
                type:"GET",
                async: false,
                url:"edit_user_role_group",
                data:{"group_id":group_id},
                success:function(result){ 
                    var str=[]; 
                    str[0] =result[0]["userrolegroup_name"];
                    str[1] =result[0]["description"];
                    str[2] =result[0]["comments"];
                    str[3]=result[0]["pages_list"];
                    selected_pages_list = str[3];
                    createRegionTreeSelect($('#pages-list-select'));
                    $("#Name1").val(str[0]).prop("disabled",true);
                    $("#description").val(str[1]);
                    $('#usergrouptoggle1').prop('checked',false).change();
                    if(str[3]=='true')
                    {
                        $('#usergrouptoggle1').prop('checked',true).change();   
                    }else
                    {
                        $('#usergrouptoggle1').prop('checked',false).change();
                    }
                    $("#comments").val(str[2]);
                   
                    $("#groupcomments").val(str[2]); 
                }
            });
            
            $("#sublevel1_staff").css("display", "none");
            $("#sublevel2_staff").css("display", "none");
            $("#user-role-group-modification, #editgroupuserdetails").css("display", "block");
            $("#user-main").css("display", "none");
        });

        // to display in edit form of data user group
        $(document).on('click', '.editdatagroup', function(){
            //event.preventDefault();
            // $(".addnewdatagroup").addClass()
            $("#data-user-group-modification, #formnewdatausergroup").css("display","block");
            $('#editdatausergroupname').html("");
			$('#data-user-group-modification .admin-headings').html('Modification of existing Data User roup');
            $("#savenewdatausergroup, #datagroupnamealert").html("");
            var groupname=$('#data-user-group-list li.selected').text();
			fetch_select2_values();
            var group_id=$('#data-user-group-list li.selected').attr("datagroupId");
            $('#user-main').css('display','none');
            $("#data-user-group-modification, #formnewdatausergroup, #editgroupuserdetails").css("display", "block");
            $('#usergrouptoggle1').change(function(){
                a = $(this).prop('checked');
            });
            
            $.ajax({
                type:"GET",
                url:"edit_data_user_group",
                async: false,
                data:{"group_id":group_id},
                success:function(result){ 
                    var str=[]; 
                    str[0] =result[0]["userrolegroup_name"];
                    str[1] =result[0]["description"];
                    str[2] =result[0]["comments"];
                    str[3]=result[0]["values_list"];
                    $("#dataName1").val(str[0]).prop("disabled",true);
                    $("#data-description").val(str[1]);
                    $("#data-comments").val(str[2]);
                    var id_mapping = {"SiteName":"#SiteName_list","FGProducts":"#FGProducts_list","Technology":"#Technology_list","LineName":"#LineName_list","Resources":"#Resources_list","SBU":"#SBU_list"};
					for(key in str[3]) {
                        var to_be_select_values = str[3][key];
                        id = id_mapping[key];
                        for(var x=0; x < to_be_select_values.length; x++) {
                            $(id).SumoSelect().sumo.selectItem(to_be_select_values[x].toString());
                        }
                        $(id).SumoSelect().sumo.reload();
                    }
                }
            });
            
            // $("#sublevel1_staff").css("display", "none");
            // $("#sublevel2_staff").css("display", "none");
            $("#data-user-group-modification, #editgroupuserdetails").css("display", "block");
            $("#user-main").css("display", "none");
        });

    $('#edituserstatus').change(function(){
         cb = $(this);
         cb.val(cb.prop('checked'));
     });

    //--------------------add new user-----------------------------------
    $(document).on('click', '.savenewuser', function(){
        // event.preventDefault();
        var validUdFormEmail = 0, validUdFormusername = 0, validUdFormfirstname = 0, validUdFormpassword = 0; 
        //validating email----------
         //validating contact----------
        var inp = $("#editusername");
		   if(inp.val()!= ""){
			$('#addun').html("");
			  validUdFormusername = 1;
			}

			else {
				$('#addun').html("Enter username").css('color','red');
				$('#editusername').val("");
				 validUdFormusername = 0; 
			}

        var inp1 = $("#editfirstname");
		   if(inp1.val()!= ""){
				$('#addfn').html("");
				validUdFormfirstname = 1;
			}

			else {
				$('#addfn').html("Enter firstname").css('color','red');
				$('#editfirstname').val("");
				 validUdFormfirstname = 0; 
			}
	   if (window.location.href.indexOf("cat=user") > -1) {
		   if ($('#editUseremail').val() != "" && $('#editUseremail').val().indexOf('@henkel.com') == -1) {
				$('#addemail').html("");
				validUdFormEmail = 1
			}
			else {
			   $('#addemail').html("Invalid email").css('color','red');
				 $('#editUseremail').val("");
				validUdFormEmail = 0;
			}
			

			var inp2 = $("#edituserpassword").val();
		   if(inp2!= ""){
			$('#addpasswrd').html("");
			  validUdFormpassword = 1;
			}

			else {
				$('#addpasswrd').html("Enter password").css('color','red');
				$('#edituserpassword').val("");
				 validUdFormpassword = 0; 
			}

			var c_pswd = $("#confirmpassword").val();
		    if(inp2==c_pswd){
				$('#confirmpassword').html("");
				validUdFormConfirmPswd = 1;
				$('#addconfirmpassword').html("");
			}

			else {
				$('#addconfirmpassword').html("Password didn\'t match").css('color','red');
				validUdFormConfirmPswd = 0; 
			}
			

        
			if(validUdFormEmail == 1 && validUdFormfirstname == 1 && validUdFormusername == 1 && validUdFormpassword == 1 && validUdFormConfirmPswd == 1){
				 var str = $("#addnewuserform :input[type!='checkbox']").serialize();
				 
				if ($('#editmesstatus').is(':checked') == true) {						var str = $("#addnewuserform").serialize()
					str += '&editmesstatus=on';
				} else {
					str += '&editmesstatus=off';
				}
				
				$.ajax({
					url: "new_user_addition",
					type: "get",
					async: false,
					data: str,
					processData: false,
					contentType: false,
					dataType : 'json',
					success: function(response){
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Success message");
						$("#common-modal .modal-body").html("User added successfully to default groups. Please change groups if required."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");
						$('#addnewuserform input').val('');
						$("#addun, #addemail, #addpasswrd, #addfn").html("");
						$("#all-users-list-select").val("");
					}
				});
			}
	   }
	   else{
			if(validUdFormfirstname == 1 && validUdFormusername == 1){
				
				$("#addnewuserform input").removeAttr('disabled');

				var str = $("#addnewuserform").serialize()
				var user_id = $('#user_id').text();
				str+='&user_id='+user_id;
				if ($('#edituserstatus').is(':checked') == true) {
					   str += '&edituserstatus=on';
				} else {
					   str += '&edituserstatus=off';
				}

				if($('.clicked_li_dg_ag .kpis_heading').text() == 'Reset Password') {
					var label_text = 'Reset';
					str += '&label_text='+label_text;
				} else {
					var label_text = 'Edit';
					str += '&label_text='+label_text;
				}
				if ($('#editmesstatus').is(':checked') == true) {		
				   str += '&editmesstatus=on';		
				} else {		
					   str += '&editmesstatus=off';
				}
				$.ajax({
					url: "edit_single_user",
					type: "get",
					data: str,
					async: false,
					processData: false,
					contentType: false,
					dataType : 'json',
					success: function(response){
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Success message");
						$("#common-modal .modal-body").html("User updated successfully."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");
						$('#addnewuserform input').val('');
						$("#addun, #addemail, #addpasswrd, #addfn").html("");
						$("#all-users-list-select").val("");
						//$("#user-modification").css("display","none");
						var user_id = $('#user_id').text();
						individual_user_details(user_id);
					}

				});
				/*
				var str = $("#addnewuserform :input[type!='checkbox']").serialize()
				
				$.ajax({
					url: "new_user_addition",
					type: "get",
					async: false,
					data: str,
					processData: false,
					contentType: false,
					dataType : 'json',
					success: function(response){
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Success message");
						$("#common-modal .modal-body").html("User added successfully. Please add user the user to specific user-role group and data group so that it can show required information or else it won't login."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");
						$('#addnewuserform input').val('');
						$("#addun, #addemail, #addpasswrd, #addfn").html("");
						$("#all-users-list-select").val("");
					}
				});*/
				
			}
	   }
        });
		
	$("#addnewlicenseform").on("submit", function(e) {
        e.preventDefault();
        func_name();
    })

    //$( "#addnewlicenseform" ).submit(function( event ) {
    function func_name() {
      if ($('#plant-list-selection :selected').text() != "") {
            $('#addtl').html("");
            validpl = 1
        }
        else {
           $('#addtl').html("Select a plant").css('color','red');
            validtl = 0;
        }

        //validating technology list----------
        if ($('#technology-list-selection :selected').text() != "") {
            $('#addpl').html("");
            validtl = 1
        }
        else {
           $('#addpl').html("Select a technology").css('color','red');
            validtl = 0;
        }
        
        //validating approved by----------
        var inp = $("#approved-by");
        if(inp.val()!= "") {
        $('#addapprovedby').html("");
            validapprovedby = 1;
        }
        else {
            $('#addapprovedby').html("Enter approver's name").css('color','red');
            $('#approved-by').val("");
            validapprovedby = 0; 
        }
        //validating approved date----------
        var inp1 = $("#approved-date");
       if(inp1.val()!= ""){
            $('#addapproveddate').html("");
            validapproveddate = 1;
        }
        else {
            $('#addapproveddate').html("Enter approved date").css('color','red');
            $('#addapproveddate').val("");
             validapproveddate = 0; 
        }
        var inp2 = $("#description-license");
        if(inp2.val()!= ""){
            $('#adddescription').html("");
            validadddescription = 1;
        }
        else {
            $('#adddescription').html("Enter description").css('color','red');
            $('#description-license').val("");
             validadddescription = 0; 
        }
        // var validpl = 0, validtl = 0, validapproveddate = 0, validapprovedby = 0, validadddescription = 0;
        if(validtl == 1 && validapproveddate == 1 && validapprovedby == 1 && validadddescription == 1){
            // var str = $("#addnewlicenseform").serialize();
            var data = new FormData($('#addnewlicenseform').get(0));
            var desc = $("#description").val();
            var plant_list = $('#plant-list-selection').val();
            var tech_list = $('#technology-list-selection').val();
            var approved_by = $("#approved-by").val();
            var approved_date = $("#approved-date").val();
			// var x = parseInt($(this));		
            var temp_list = plant_list.split(',');		
            var plant_str = '';		
            for(var i=0; i< temp_list.length; i++) {		
                plant_str += parseInt(temp_list[i]);		
            }		
            var temp_list = tech_list.split(',');		
            var tech_str = '';		
            for(var i=0; i< temp_list.length; i++) {		
                tech_str += parseInt(temp_list[i]);		
            }		
			
            $.ajax({
                url: "new_license_addition",
                type: "POST",
                // async: false,
                data: data,
                processData: false,
                contentType: false,
                dataType : 'json',
                success: function(response){
					 if(response == 1) {
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Success message");
						$("#common-modal .modal-body").html("License added successfully."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");
						$('#addnewlicenseform input').val('');
						$("#addpl, #adddescription, #addapproveddate, #addapprovedby").html("");
                    // $("#all-users-list-select").val("");
					 }
					 else {
						$("#common-modal").modal("show");
						$("#common-modal .modal-title").html("Error message");
						$("#common-modal .modal-body").html("Some error occured. Please contact administrator."); 
						$("#common-modal .modal-no").css("display","none");
						$("#common-modal .modal-yes").html("Okay");
						$('#addnewlicenseform input').val('');
						$("#addpl, #adddescription, #addapproveddate, #addapprovedby").html("");
					 }
                }
            });
            return false;
        }
        else {
            //alert("false");
            return false;
        }

    }

    //--------------------add new license-----------------------------------
    $(document).on('click', '.savenewlicensez', function(){
        event.preventDefault();
        var validpl = 0, validtl = 0, validapproveddate = 0, validapprovedby = 0, validadddescription = 0; 
        // validating plant list----------
        if ($('#plant-list-selection :selected').text() != "") {
            $('#addtl').html("");
            validpl = 1
        }
        else {
           $('#addtl').html("Select a plant").css('color','red');
            validtl = 0;
        }

        //validating technology list----------
        if ($('#technology-list-selection :selected').text() != "") {
            $('#addpl').html("");
            validtl = 1
        }
        else {
           $('#addpl').html("Select a technology").css('color','red');
            validtl = 0;
        }
        
        //validating approved by----------
        var inp = $("#approved-by");
        if(inp.val()!= "") {
        $('#addapprovedby').html("");
            validapprovedby = 1;
        }
        else {
            $('#addapprovedby').html("Enter approver's name").css('color','red');
            $('#approved-by').val("");
            validapprovedby = 0; 
        }
        //validating approved date----------
        var inp1 = $("#approved-date");
       if(inp1.val()!= ""){
            $('#addapproveddate').html("");
            validapproveddate = 1;
        }
        else {
            $('#addapproveddate').html("Enter approved date").css('color','red');
            $('#addapproveddate').val("");
             validapproveddate = 0; 
        }
        var inp2 = $("#description-license");
        if(inp2.val()!= ""){
            $('#adddescription').html("");
            validadddescription = 1;
        }
        else {
            $('#adddescription').html("Enter description").css('color','red');
            $('#description-license').val("");
             validadddescription = 0; 
        }
        // var validpl = 0, validtl = 0, validapproveddate = 0, validapprovedby = 0, validadddescription = 0;
        if(validpl == 1 && validtl == 1 && validapproveddate == 1 && validapprovedby == 1 && validadddescription == 1){
            var str = $("#addnewlicenseform").serialize();
			
			if ($('#editmesstatus').is(':checked') == true) {		
                   str += '&editmesstatus=on';		
            } else {		
                   str += '&editmesstatus=off';		
            }
			
            var data = new FormData($('#addnewlicenseform').get(0));
            var desc = $("#description").val();
            var plant_list = $('#plant-list-selection').val();
            var tech_list = $('#technology-list-selection').val();
            var approved_by = $("#approved-by").val();
            var approved_date = $("#approved-date").val();
            // var formData = new FormData($("#addnewlicenseform"));
            $.ajax({
                url: "new_license_addition",
                type: "POST",
                // async: false,
                data: {'str':str, 'plant_list':plant_list,'tech_list':tech_list},
                processData: false,
                contentType: false,
                dataType : 'json',
                success: function(response){
                    $("#common-modal").modal("show");
                    $("#common-modal .modal-title").html("Success message");
                    $("#common-modal .modal-body").html("License added successfully."); 
                    $("#common-modal .modal-no").css("display","none");
                    $("#common-modal .modal-yes").html("Okay");
                    $('#addnewlicenseform input').val('');
                    // $('#addnewlicenseform select').val('');
                    $("#addpl, #adddescription, #addapproveddate, #addapprovedby").html("");
                    // $("#all-users-list-select").val("");
                }
            });
            
        }
        });
 
 $(document).on('click', '.saveedituser', function(){
        $('#select2-all-users-list-select-container').text('');
        $('#select2-all-users-list-select-container').attr('title', '');
        // event.preventDefault();
        var validUdFormEmail = 0, validUdFormusername = 0, validUdFormfirstname = 0, validUdFormpassword = 0; 
        
        var inp1 = $("#editfirstname");
               if(inp1.val()!= ""){
                    $('#addfn').html("");
                    validUdFormfirstname = 1;
                }
                else {
                    $('#addfn').html("Enter firstname").css('color','red');
                    $('#editfirstname').val("");
                     validUdFormfirstname = 0; 
                }
        var inp2 = $("#edituserpassword");
               if(inp2.val()!= ""){
                $('#addpasswrd').html("");
                  validUdFormpassword = 1;
                }
                else {
                    $('#addpasswrd').html("Enter password").css('color','red');
                    $('#edituserpassword').val("");
                     validUdFormpassword = 0; 
                }
        
        if(validUdFormfirstname == 1 && validUdFormpassword == 1){
            $("#addnewuserform input").removeAttr('disabled');

            var str = $("#addnewuserform").serialize()

            var user_id = $('#all-users-list-select').val();
            str+='&user_id='+user_id;
            if ($('#edituserstatus').is(':checked') == true) {
                   str += '&edituserstatus=on';
            } else {
                   str += '&edituserstatus=off';
			}
			if ($('#editmesstatus').is(':checked') == true) {		
				   str += '&editmesstatus=on';		
			} else {		
				   str += '&editmesstatus=off';
            }

            if($('.clicked_li_dg_ag .kpis_heading').text() == 'Reset Password') {
                var label_text = 'Reset';
                str += '&label_text='+label_text;
            } else {
                var label_text = 'Edit';
                str += '&label_text='+label_text;
            }
			
			var flag_value = $('#flag-user').text();
			var display_message = '';
			if(flag_value == 'Editing') {
				display_message = 'User edited successfully';
			}
			else if (flag_value == 'Blocking/UnBlocking') {
				display_message = 'User blocked successfully';
			}
			else if (flag_value == 'Resetting Password') {
				display_message = 'Password reset successfully';
			}

            $.ajax({
                url: "edit_single_user",
                type: "get",
                data: str,
                async: false,
                processData: false,
                contentType: false,
                dataType : 'json',
                success: function(response){
                    $("#common-modal").modal("show");
                    $("#common-modal .modal-title").html("Success message");
                    $("#common-modal .modal-body").html(display_message); 
					$(".modal-body").css("height","80px");
                    $("#common-modal .modal-no").css("display","none");
                    $("#common-modal .modal-yes").html("Okay");
                    $('#addnewuserform input').val('');
                    $("#addun, #addemail, #addpasswrd, #addfn").html("");
                    $("#all-users-list-select").val("");
                    $("#user-modification").css("display","none");
                }
            });
            
        }
    });
	function create_syncdate(){
		 $.ajax({
			url: "get_syncdate",
			method: "POST",
			complete: function(result) {
				$('.get_date').html(result['responseText']);
			}
		});
	}