function overall_quality(Data,id)
{
	Highcharts.setOptions({
        global: {
            useUTC: false,
            
        },
        lang: {
          decimalPoint: '.',
          thousandsSep: ','
        }
    });
	
	$(id).highcharts({
		chart: {type: 'pie',
		options3d: {enabled: false,alpha: 55,beta:1},
		events: {
            load: function(event) {
				  var total = 0;
					for(var i=0, len=this.series[0].yData.length; i<len; i++){
						total += this.series[0].yData[i];
					}
				  var text = this.renderer.text(  'Total: ' + total,  this.plotLeft,  this.plotTop +5 ).attr({  zIndex: 5   }).add()
				}
              }
		
		
		},
		title: {text: ''},
		exporting: { enabled: false },
		credits: { enabled: false},
		tooltip: {pointFormat: '{series.name}: <b>{point.y:,2f}</b>'},
		legend: {enabled: false,align: 'left',x: 10,verticalAlign: 'bottom',y: 5,shadow: false},
		plotOptions: {
			
			pie: {
					borderWidth:1.5,
					allowPointSelect: true,cursor: 'pointer',innerSize: '45%',depth: 25,showInLegend: true,
					dataLabels: {enabled: true,distance:8,format: ' {point.percentage:.1f} %',style: {color:  'black','font-size':'10px'}}
				 },	
			series: {
					cursor: 'pointer', 
					point: { events: {click: function () {  modal_window_data(this.name)  } } }
					}	
		},
		series: [{name: 'Batches',colorByPoint: true,data:Data}]
	});
}


function overall_quality_modal(Data,id)
{
	$(id).highcharts({
		chart: {type: 'pie',options3d: {enabled: false,alpha: 55,beta:1}},
		title: {text: ''},
		exporting: { enabled: false },
		credits: { enabled: false},
		tooltip: {pointFormat: '{series.name}: <b>{point.y}</b>'},
		legend: {enabled: false,align: 'left',x: 10,verticalAlign: 'bottom',y: 5,shadow: false},
		plotOptions: {
			
			pie: {
					borderWidth:1.5,
					allowPointSelect: true,cursor: 'pointer',innerSize: '45%',depth: 25,showInLegend: true,
					dataLabels: {enabled: true,distance:8,format: ' {point.percentage:.1f} %',style: {color:  'black','font-size':'10px'}}
				 },	
			series: {
					cursor: 'pointer', 
					point: { events: {click: function () {   } } }
					}	
		},
		series: [{name: 'Batches',colorByPoint: true,data:Data}]
	});
}

function customer_complaints(Data,id)
{
	$(id).highcharts({
		chart: {type: 'pie',options3d: {enabled: false,alpha: 55,beta:1}},
		title: {text: ''},
		colors: ["#87CEEB","#32CD32","#BA55D3","#F08080","#4682B4","#9ACD32","#40E0D0","#FF69B4","#F0E68C","#D2B48C","#8FBC8B","#6495ED"],
		exporting: { enabled: false },
		credits: { enabled: false},
		tooltip: {pointFormat: '{series.name}: <b>{point.y}</b>'},
		legend: {enabled: false,align: 'left',x: 10,verticalAlign: 'bottom',y: 5,shadow: false},
		plotOptions: {
			pie: {
					borderWidth:1.5,
					allowPointSelect: true,cursor: 'pointer',innerSize: '45%',depth: 25,showInLegend: true,
					dataLabels: {enabled: true,distance:8,format: ' {point.percentage:.1f} %',style: {color:  'black','font-size':'10px'}}
				 },	
			series: {
					cursor: 'pointer', 
					point: { events: {click: function () {complaint_modal_window_data(this.name)} } }
					}	
		},
		series: [{name: 'Batches',colorByPoint: true,data:Data}]
	});
}



function customer_complaints_modal_chart(Data,id)
{
	$(id).highcharts({
		chart: {type: 'pie',options3d: {enabled: false,alpha: 55,beta:1}},
		title: {text: ''},
		exporting: { enabled: false },
		credits: { enabled: false},
		tooltip: {pointFormat: '{series.name}: <b>{point.y}</b>'},
		legend: {enabled: false,align: 'left',x: 10,verticalAlign: 'bottom',y: 5,shadow: false},
		plotOptions: {
			pie: {
					borderWidth:1.5,
					allowPointSelect: true,cursor: 'pointer',innerSize: '45%',depth: 25,showInLegend: true,
					dataLabels: {enabled: true,distance:8,format: ' {point.percentage:.1f} %',style: {color:  'black','font-size':'10px'}}
				 },	
			series: {
					cursor: 'pointer', 
					point: { events: {click: function () {   } } }
					}	
		},
		series: [{name: 'Batches',colorByPoint: true,data:Data}]
	});
}





function overall_quality1(Data,id)
{
	$(id).highcharts({
		chart: {type: 'pie',options3d: {enabled: false,alpha: 55,beta:1},
			events: {
				load: function(event) {
					  var total = 0;
						for(var i=0, len=this.series[0].yData.length; i<len; i++){
							total += this.series[0].yData[i];
						}
					  var text = this.renderer.text(  'Total: ' + total,  this.plotLeft,  this.plotTop +5 ).attr({  zIndex: 5   }).add()
					}
			}
		},
		title: {text: ''},
		
		exporting: { enabled: false },
		credits: { enabled: false},
		tooltip: {pointFormat: '{series.name}: <b>{point.y}</b>'},
		legend: {enabled: false,align: 'left',x: 10,verticalAlign: 'bottom',y: 5,shadow: false},
		plotOptions: {
			pie: {
					allowPointSelect: true,cursor: 'pointer',innerSize: '45%',depth: 25,showInLegend: true,
					dataLabels: {enabled: true,distance:8,format: ' {point.percentage:.1f} %',style: {color:  'black'}}
				 },	
			series: {
					cursor: 'pointer', 
					point: { events: {click: function () {  parameter_window_data(this.name)  } } }
					}	
		},
		series: [{name: 'Batches',colorByPoint: true,data:Data}]
	});
}



function Trend_Of_Quality(Data,id)
{
	
	if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview')
	{
		label_value='# RM Batch Contribution'
		
	}
	else
	{
		label_value='# PO Percentage Contribution'
		
	}
	
         $(id).highcharts({
			chart: { 
				zoomType: 'x',
				spacingTop: 25, 
				alignTicks:false,
				
			},
			title: { text: ''},
			subtitle: { text: '' },
			exporting: { enabled: false },
			credits: { enabled: false},
			xAxis: [
				{ categories:Data[0]["x_categories"], 
				  labels: { 
					enabled:true,
					rotation: -15
				  },
			}],
			yAxis: [
					{ // Secondary yAxis
					gridLineDashStyle:'ShortDashDot',
					min: 0,max:100,endOnTick:false,tickInterval: 50,
					title: {text: 'Quality Score ',style: {color: '#F3778B'}},
					labels: { format: '{value}% ',style: {color: '#F3778B' } },
					opposite: true
					},
					{ //primary axis
					gridLineDashStyle:'ShortDashDot',
					min: 0,max:100,endOnTick:false,tickInterval: 50,
					labels: { format: '{value}%',style: { color: '#7CB5EC' } },
					title: {text:label_value,style: { color: '#7CB5EC' }}
					}
					],
			plotOptions: {
			column: {stacking: 'percent',dataLabels: {enabled: false,color:'white',style: {textShadow: '0 0 3px black'}}}
			},
			tooltip: { shared: true },
			legend: {enabled: false,align: 'left',  x: 10, verticalAlign: 'bottom',  y: 5, shadow: false },
			series: Data[0]['series']
			 
		 });
}
	
	
function Trend_Of_Quality_Line(Data,id)
{	
	if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview')
	{
		label_value='Raw Material Batches'
		
	}
	else
	{
		label_value='Process Orders'
		
	}

	val=$('input[name=inlineRadioOptions]:checked').val()
		if(val=="$SiteName"){value='Plant'}
		if(val=="$LineName"){value='Line'}
		if(val=="$Resources"){value='Resource'}
		if(val=="$SBU"){value='SBU'}
		if(val=="$Technology"){value='Technology'}
		if(val=="$MaterialNumber"){if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview'){ value='Raw Material'}  else{value='Product'} }
		if(val=="$Operators"){value='Operator'}
		if(val=="$VendorName"){value='Vendor'}
		Highcharts.setOptions({
			global: {
				useUTC: false,
				
			},
			lang: {
			  decimalPoint: '.',
			  thousandsSep: ','
			}
		});
         $(id).highcharts({
			chart: {type: 'column',zoomType: 'x',alignTicks:false,},
			title: { text: 'Quality Segment Spread By '+value+' ',style: { color: 'rgba(127, 119, 119, 0.87)','font-size': '12px !important' }},
			subtitle: { text: '' },
			xAxis: {categories: Data[0]["x_categories"],labels: { rotation: -10}},
			yAxis: {
				gridLineDashStyle:'ShortDashDot',
				min: 0,max:100,title: {text: ''},stackLabels: {enabled: true,style: {fontWeight: 'bold',color: 'gray'}},
				labels: { format: '{value}%',style: { color: '#7CB5EC' } },
				title: {text:label_value,style: { color: '#7CB5EC' }}
			},
			plotOptions: {
			column: {stacking: 'percent',dataLabels: {enabled: false,color:'white',style: {textShadow: '0 0 3px black'}}},	
			},
			exporting: { enabled: false },
			credits: { enabled: false},
			legend: {enabled: false,align: 'left', x: 40,verticalAlign: 'bottom',y: 5, shadow: false},
			tooltip: {
				headerFormat: '<b>{point.x}</b><br/>',
				pointFormat: '{series.name}: {point.y:,2f}<br/>Total: {point.stackTotal:,2f}'
			},
			series:Data[0]["series"],
		});


}

function quality_variation_by_line(Data,id)
{
	if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview')
	{
		label_value='Raw Material Batches'
		
	}
	else
	{
		label_value='Process Orders'
		
	}	
	
	
		val=$('input[name=inlineRadioOptions]:checked').val()
		if(val=="$SiteName"){value='Plant'}
		if(val=="$LineName"){value='Line'}
		if(val=="$Resources"){value='Resource'}
		if(val=="$SBU"){value='SBU'}
		if(val=="$Technology"){value='Technology'}
		if(val=="$MaterialNumber"){if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview'){ value='Raw Material'}  else{value='Product';} }
		if(val=="$Operators"){value='Operator'}
		if(val=="$VendorName"){value='Vendor'}
		$(id).highcharts({
			chart: { zoomType: 'x',alignTicks:false,},
			title: { text: ''+label_value+' Spread By '+value+' ',style: { color: 'rgba(127, 119, 119, 0.87)','font-size': '12px !important' }},
			subtitle: { text: '' },
			exporting: { enabled: false },
			credits: { enabled: false},
			xAxis: [{ categories:Data['categories'], crosshair: true ,labels: { rotation: -10}}],
			yAxis: [
					{ //primary axis
					//gridLineDashStyle:'ShortDashDot',
					//min: 0,
					endOnTick:false,tickInterval: 50,
					labels: { format: '{value}',style: { color: '#7CB5EC' } },
					title: {text:label_value ,style: {color: '#7CB5EC'}}
					},
					{ // Secondary yAxis
					gridLineDashStyle:'ShortDashDot',
					//min: 0,max:100,
					min: 0,max:100,endOnTick:false,tickInterval: 50,
					title: {text: 'Quality Score',style: {color: '#F3778B'}},
					labels: { format: '{value}% ',style: {color: '#F3778B' } },
					opposite: true
					}
	

					],
			tooltip: { shared: true },
			legend: {enabled: false,align: 'left',  x: 10, verticalAlign: 'bottom',  y: 5, shadow: false },
			series: Data['series']
		});


}


function quality_variation_by_line_trend(Data,id)
{
	 //console.log('DAta ',Data[0]); 		
	 //console.log((Data[0]["categories"]).sort());
	 $(id).highcharts({
        chart: {type: 'line', zoomType: 'x'},
        title: {text: ''},
        subtitle: { text: ''},
        exporting: { enabled: false },
			  credits: { enabled: false},
        xAxis: {
            categories:Data[0]["categories"],labels: { rotation: -10}
        },
        yAxis: {
			gridLineDashStyle:'ShortDashDot',
            title: { text: ' '},
            labels: {format: '{value}',
            style: { color: '#222' }
            },
        },
        plotOptions: {
         line: { dataLabels: {enabled: false}, enableMouseTracking: true}
        },
        series: Data[0]["series"]
    });


}

function Trend_Of_Parameter(data,id)
{
	var param='';
	if(window.location.pathname.split('/')[2]=='cpk_view'){
		param=$('#parameter-dropdown').attr('data-k');
	}
	else if(window.location.pathname.split('/')[2]=='configuration_view'){
		param =parameter[0];
	}
	Highcharts.setOptions({
        global: {
            useUTC: false,  
        },
        lang: {
          decimalPoint: '.',
          thousandsSep: ','
        }
    });
	$(id).highcharts({
        title: {
            text: 'Control Chart for '+param+' ',
			style: { color: 'rgba(127, 119, 119, 0.87)','font-size': '12px !important' },
            x: -20 //center
        },
		credits:{
			enabled:false
		},
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories:data['result_dict']
        },
        yAxis: {
			gridLineDashStyle: 'ShortDashDot',
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            },
			]
        },
		chart: {
            zoomType: 'x',
			width:870
        },
        tooltip: {pointFormat: '{series.name}: <b>{point.y:,3f}</b>'},
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: param,
            data:data['result_list1'],
			color:''
        },
		{
			name: 'USL',
            data:data['result_val1'],
			color:'#F7A25C'
        },
		{    
			
            name: 'Mean',
            data:data['result_val3']
        },
		{    
			
            name: 'LSL',
            data:data['result_val5'],
			color:'#F7A25C'
        }]
    }); 
	
}

function Trend_Of_Parameter_benchmark(data,id,parameter)
{
	var param='';
	if(window.location.pathname.split('/')[2]=='cpk_view'){
		param=$(".parameter").val();
	}
	else if(window.location.pathname.split('/')[2]=='configuration_view'){
		param =parameter[0];
	}
	Highcharts.setOptions({
        global: {
            useUTC: false,  
        },
        lang: {
          decimalPoint: '.',
          thousandsSep: ','
        }
    });
	 $(id).highcharts({
		chart:{
			 height:800
		},
        title: {
            text: 'Control Chart for '+param+' ',
			style: { color: 'rgba(127, 119, 119, 0.87)','font-size': '12px !important' },
            x: -20 //center
        },
		credits:{
			enabled:false
		},
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories:data['result_dict']
        },
        yAxis: {
			gridLineDashStyle: 'ShortDashDot',
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            },
			]
        },
		chart: {
            zoomType: 'x'
        },
        tooltip: {pointFormat: '{series.name}: <b>{point.y:,3f}</b>'},
        legend: {
            enabled:true
        },
        series: [{
            name: 'Actual Value',
            data:data['result_list1'],
			color:''
        },
		{
			name: 'USL',
            data:data['result_val1'],
			color:'#F7A25C'
        },
		{    
			
            name: 'Mean',
            data:data['result_val3']
        },
		{    
			
            name: 'Mid Point',
            data:data['result_val4']
        },
		{    
			
            name: 'LSL',
            data:data['result_val5'],
			color:'#F7A25C'
        },
		{    
			
            name: 'Median',
            data:data['Median'],
			color:'#E40303'
        }]
    }); 
	$(id).highcharts().reflow();
	
}

function Trend_Of_Parameter_graph1(Data,id)
{
	Highcharts.setOptions({
        global: {
            useUTC: false,  
        },
        lang: {
          decimalPoint: '.',
          thousandsSep: ','
        }
    });
	$(id).highcharts({
        chart: {
            zoomType: 'x',
			width:850
        },
        title: {
            text: 'Trend of Cpk and Standard Deviation(SD) for ' +$('#parameter-dropdown').attr('data-k')+ ' '+$('#offspec-dropdown').attr('data-k'),
			style: { color: 'rgba(127, 119, 119, 0.87)','font-size': '12px !important' }
        },
		credits:{
			enabled:false
		},
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: Data['result_xaxis'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
			gridLineDashStyle: 'ShortDashDot',
            labels: {
                format: '{value}',
                style: {
                    color: 'black'
                }
            },
            title: {
                text: 'Cpk',
                style: {
                    color: 'black'
                }
            }
			
        }, { // Secondary yAxis
			gridLineDashStyle: 'ShortDashDot',
            title: {
                text: 'SD',
                style: {
                    color: '#7CB5EC'
                }
            },
            labels: {
                format: '{value} ',
                style: {
                    color: '#7CB5EC'
                }
            },
			
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
			enabled:false,
            layout: 'vertical',
            align: 'left',
            x: 10,
            verticalAlign: 'top',
            y: 10,
            floating: true,
            backgroundColor: 'black' || '#FFFFFF'
        },
        series: [{
            name: 'Cpk',
            type: 'line',
           color:' black',
            data: Data['result_cpk'],
            tooltip: {
                valueSuffix: ''
            }

        }, {
            name: 'SD',
            type: 'line',
			yAxis: 1,
			color:'#7CB5EC',
            data: Data['result_sd'],
            tooltip: {
                valueSuffix: ''
            }
        }]
    });

}




