//*********************************for getting filters data on change *********************************//

var start_date;
var end_date;
var main_start_date = '';
var main_end_date = '';
function  filter_change_data(change)
{
var plant_list=[]
var sbu_list=[]
var line_list=[]
var product_list=[]
var technology_list=[]
var resource_list=[]
var rawmaterial_list=[]
var vendor_list=[]
if (change=='plant')
{	
	page_view=window.location.pathname.split('/')[2]
	if(page_view=='rejected_view'|page_view=='configuration_benchmark')
	{	
		$(".plant_li").each(function(){
			if (this.checked) {plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		sbu_list=['all']; technology_list=['all']; product_list=['all']; line_list=['all']; resource_list=['all']; change_type='plant';
	}
	else if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview'|window.location.pathname.split('/')[2]=='RM_grid_view')
	{
		if($('.plant').val()==null){plant_list=['all']	}else{ plant_list=$('.plant').val()}
		sbu_list=['all']; technology_list=['all']; rawmaterial_list=['all']; vendor_list=['all']; change_type='plant';		
	} 
	
	else if(window.location.pathname.split('/')[2]=='cpk_view' |window.location.pathname.split('/')[2]=='grid_view' |window.location.pathname.split('/')[2]=='configuration_view')
	{
		plant_list =[$('#plant-dropdown').attr('data-k')];sbu_list=['all'];technology_list=['all'];product_list=['all'];line_list=['all'];resource_list=['all'];change_type='plant'				
	}
	else if(window.location.pathname.split('/')[2]=='plant_view' |window.location.pathname.split('/')[2]=='resource_view' | window.location.pathname.split('/')[2]=='line_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		sbu_list=['all'];technology_list=['all'];product_list=['all'];line_list=['all'];resource_list=['all'];change_type='plant';
	}
	else	
	{
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		sbu_list=['all'];technology_list=['all'];product_list=['all'];line_list=['all'];resource_list=['all'];change_type='plant'	
	}		

}



else if (change=='sbu')
{
	var page_view=window.location.pathname.split('/')[2]	
	if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview'|window.location.pathname.split('/')[2]=='RM_grid_view')
	{
		if($('.plant').val()==null){plant_list=['all']	}else{ plant_list=$('.plant').val()}
		if($('.sbu').val()==null){sbu_list=['all']} else{ sbu_list=$('.sbu').val()};
		technology_list=['all'];rawmaterial_list=['all']; vendor_list=['all']; change_type='sbu';		
	}
	else if(window.location.pathname.split('/')[2]=='plant_view' | window.location.pathname.split('/')[2]=='resource_view' | window.location.pathname.split('/')[2]=='line_view' | window.location.pathname.split('/')[2]=='grid_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		technology_list=['all'];product_list=['all'];line_list=['all'];resource_list=['all'];change_type='sbu';
	}
	else	
	{
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		technology_list=['all'];product_list=['all'];line_list=['all'];resource_list=['all'];change_type='sbu';
	}	

}

else if (change=='technology')
{
	var page_view=window.location.pathname.split('/')[2]
	if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview'|window.location.pathname.split('/')[2]=='RM_grid_view')
	{
		if($('.plant').val()==null){plant_list=['all']	}else{ plant_list=$('.plant').val()}
		if($('.sbu').val()==null){sbu_list=['all']} else{ sbu_list=$('.sbu').val()};
		if($('.technology').val()==null){technology_list=['all']} else{ technology_list=$('.technology').val()}
		rawmaterial_list=['all']; vendor_list=['all']; change_type='technology';		
	}	
	else if(window.location.pathname.split('/')[2]=='plant_view'| window.location.pathname.split('/')[2]=='resource_view' | window.location.pathname.split('/')[2]=='line_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		product_list=['all'];line_list=['all'];resource_list=['all'];change_type='technology'
	}
	else if(window.location.pathname.split('/')[2]=='grid_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		technology_list =[$('#technology-dropdown').attr('data-k')];
		product_list=['all'];line_list=['all'];resource_list=['all'];change_type='technology'
	}
	else	
	{
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		product_list=['all'];line_list=['all'];resource_list=['all'];change_type='technology'	
	}		

}



else if (change=='vendor')
{
	var page_view=window.location.pathname.split('/')[2]
	if(page_view==''| page_view=='grid_view')
	{	
		
		if($('.plant').val()==null){plant_list=['all']}else{ plant_list=$('.plant').val()}
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}
		if($('.product').val()==null){product_list=['all']}else{product_list=$('.product').val()}
		line_list=['all'];change_type='vendor'
	}		
	else 
	{	
		if($('.plant').val()==null){plant_list=['all']}else{ plant_list=[$('.plant').val()]}
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}	
		if($('.product').val()==null){product_list=['all']}else{product_list=$('.product').val()}		
		line_list=['all'];change_type='vendor';
	}
}

else if (change=='rawmaterial')
{
	var page_view=window.location.pathname.split('/')[2]
	if(page_view==''| page_view=='grid_view')
	{	
		
		if($('.plant').val()==null){plant_list=['all']}else{ plant_list=$('.plant').val()}
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}
		if($('.product').val()==null){product_list=['all']}else{product_list=$('.product').val()}
		line_list=['all'];resource_list=['all'];change_type='rawmaterial'
	}		
	else 
	{	
		if($('.plant').val()==null){plant_list=['all']}else{ plant_list=[$('.plant').val()]}
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}	
		if($('.product').val()==null){product_list=['all']}else{product_list=$('.product').val()}		
		line_list=['all'];change_type='rawmaterial';
	}
}

else if (change=='product')
{
	var page_view=window.location.pathname.split('/')[2]
	if(window.location.pathname.split('/')[2]=='product_view')
	{	
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		product_list =[$('#product-dropdown').attr('data-k')];
		line_list=['all'];resource_list=['all'];change_type='product';
	}
	else if(window.location.pathname.split('/')[2]=='rejected_view'){
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		sbu_list=['all'];technology_list=['all'];
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}		
		line_list=['all'];resource_list=['all'];change_type='product';}
	else if(window.location.pathname.split('/')[2]=='configuration_benchmark')
	{
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		sbu_list=['all'];technology_list=['all'];
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}		
		line_list=['all'];resource_list=['all'];change_type='product';
	}
	else if(window.location.pathname.split('/')[2]=='cpk_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}	
		product_list =[$('#product-dropdown').attr('data-k')];		
		line_list=['all'];resource_list=['all'];change_type='product';
	}
	else if(window.location.pathname.split('/')[2]=='grid_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		technology_list =[$('#technology-dropdown').attr('data-k')];
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}	
		line_list=['all'];resource_list=['all'];change_type='product';
	}
	else if(window.location.pathname.split('/')[2]=='configuration_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		product_list =[$('#product-dropdown').attr('data-k')];
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}		
		line_list=['all'];resource_list=['all'];change_type='product';
	}
	else if(window.location.pathname.split('/')[2]=='plant_view'| window.location.pathname.split('/')[2]=='resource_view' | window.location.pathname.split('/')[2]=='line_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}		
		line_list=['all'];resource_list=['all'];change_type='product';
	}
	else 
	{	
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
		line_list=['all'];resource_list=['all'];change_type='product';
	}
}



else if (change=='line')
{
	var page_view=window.location.pathname.split('/')[2]
	if(window.location.pathname.split('/')[2]=='plant_view')
	{	
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
		$(".line_li").each(function(){
			if(this.checked){line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		resource_list=['all'];change_type='line';
	}
	else if(window.location.pathname.split('/')[2]=='product_view')
	{	
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		product_list =[$('#product-dropdown').attr('data-k')];
		$(".line_li").each(function(){
			if(this.checked){line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		resource_list=['all'];change_type='line'
	}
	else if(window.location.pathname.split('/')[2]=='cpk_view' ||window.location.pathname.split('/')[2]=='configuration_view' ||window.location.pathname.split('/')[2]=='rejected_view' ||window.location.pathname.split('/')[2]=='configuration_benchmark')
	{
		if($('.plant').val()==null){plant_list=['all']}else{ plant_list=[$('.plant').val()]}
		if($('.sbu').val()==null){sbu_list=['all']}else{sbu_list=$('.sbu').val()}
		if($('.technology').val()==null){technology_list=['all']}else{technology_list=$('.technology').val()}	
		if($('.product').val()==null){product_list=['all']}else{product_list=$('.product').val()}		
		if($('.line').val()==null){line_list=['all']} else{line_list=[$('.line').val()]}
		resource_list=['all'];change_type='line'
	}
	else if(window.location.pathname.split('/')[2]=='line_view'|window.location.pathname.split('/')[2]=='resource_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}		
		line_list =[$('#line-dropdown').attr('data-k')];
		resource_list=['all'];change_type='line'
	}
	else if(window.location.pathname.split('/')[2]=='grid_view'){
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		technology_list =[$('#technology-dropdown').attr('data-k')];
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}	
		$(".line_li").each(function(){
			if(this.checked){line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		resource_list=['all'];change_type='product';
	}
	else 
	{
		$(".plant_li").each(function(){
			if(this.checked){plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if(this.checked){sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if(this.checked){technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".product_li").each(function(){
			if(this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
		$(".line_li").each(function(){
			if(this.checked){line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		resource_list=['all'];change_type='line'
	}
}


if(window.location.pathname.split('/')[2]=='rawmaterial_view'|window.location.pathname.split('/')[2]=='vendor_view'|window.location.pathname.split('/')[2]=='RM_overview'|window.location.pathname.split('/')[2]=='RM_grid_view')
{
	
	data={'plant_list':plant_list,'sbu_list':sbu_list,'technology_list':technology_list,'rawmaterial_list':rawmaterial_list,'vendor_list':vendor_list,'change_type':change_type}

	return data	
	
}
else{
	data={'plant_list':plant_list,'sbu_list':sbu_list,'technology_list':technology_list,'product_list':product_list,'line_list':line_list,'resource_list':resource_list,'change_type':change_type}

return data	
}




}




// Ajax function for getting filter date on change 
function  ajax_filter_change_data(change)
{
	var result;
	Data=filter_change_data(change);
	$.ajax({
	method: "POST",
	url: "/qualitypulse/get_filter_data/",
	data:Data,
	success: function(result_data) {
		result=result_data;
	}
	});

}



//function for getting filter data on apply buttton click

function filter_data()
{
date_list=[]	
if(start_date==end_date)
{
date_list=[start_date]
}
else{
date_list=[start_date,end_date]
}	
/*sbu_list=$('.sbu').val()
technology_list=$('.technology').val()*/
	var data={}
	var product_list =[];
	var plant_list =[];
	var resource_list =[];
	var line_list =[];
	var technology_list =[];
	var sbu_list =[];
	var data={}
	if(window.location.pathname.split('/')[2]=='plant_view')
	{
		plant_list =[$('#plant-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if (this.checked) {sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if (this.checked) {technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".line_li").each(function(){
			if (this.checked) {line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		$(".resource_li").each(function(){
			if (this.checked) {resource_list.push($(this).val());}
		});
		if(resource_list.length==0){resource_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
	}
	else if(window.location.pathname.split('/')[2]=='rawmaterial_view')
	{
		plant_list=$('.plant').val()
		rawmaterial_list=[$('.rawmaterial').val()]
		sbu_list=$('.sbu').val()
		technology_list=$('.technology').val()
		vendor_list=$('.vendor').val()
		data={'plant_list':plant_list,'vendor_list':vendor_list,'rawmaterial_list':rawmaterial_list,'date_list':date_list,'sbu_list':sbu_list,'technology_list':technology_list}
		return data
	}
	else if(window.location.pathname.split('/')[2]=='vendor_view')
	{
		plant_list=$('.plant').val()
		rawmaterial_list=$('.rawmaterial').val()
		vendor_list=[$('.vendor').val()]
		sbu_list=$('.sbu').val()
		technology_list=$('.technology').val()
		data={'plant_list':plant_list,'vendor_list':vendor_list,'rawmaterial_list':rawmaterial_list,'date_list':date_list,'sbu_list':sbu_list,'technology_list':technology_list}
		
		return data
	}
	else if(window.location.pathname.split('/')[2]=='RM_overview'|window.location.pathname.split('/')[2]=='RM_grid_view')
	{
		plant_list=$('.plant').val()
		rawmaterial_list=$('.rawmaterial').val()
		vendor_list=$('.vendor').val()
		data={'plant_list':plant_list,'vendor_list':vendor_list,'rawmaterial_list':rawmaterial_list,'date_list':date_list,'sbu_list':sbu_list,'technology_list':technology_list}
		
		return data
	}
	else if(window.location.pathname.split('/')[2]=='line_view')
	{
		plant_list =[$('#plant-dropdown').attr('data-k')];
		line_list =[$('#line-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if (this.checked) {sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if (this.checked) {technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".resource_li").each(function(){
			if (this.checked) {resource_list.push($(this).val());}
		});
		if(resource_list.length==0){resource_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
	}
	else if(window.location.pathname.split('/')[2]=='resource_view')
	{
		plant_list =[$('#plant-dropdown').attr('data-k')];
		line_list =[$('#line-dropdown').attr('data-k')];
		resource_list =[$('#resource-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if (this.checked) {sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if (this.checked) {technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
	}	
	else if(window.location.pathname.split('/')[2]=='product_view')
	{
		$(".plant_li").each(function(){
			if (this.checked) {plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if (this.checked) {sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if (this.checked) {technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".line_li").each(function(){
			if (this.checked) {line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		$(".resource_li").each(function(){
			if (this.checked) {resource_list.push($(this).val());}
		});
		if(resource_list.length==0){resource_list = ['all'];}
		product_list =[$('#product-dropdown').attr('data-k')];
	}
	else if(window.location.pathname.split('/')[2]=='cpk_view')
	{
		plant_list =[$('#plant-dropdown').attr('data-k')];
		product_list =[$('#product-dropdown').attr('data-k')];
		line_list=['all'];
		resource_list=['all'];
		sbu_list=['all'];
		technology_list=['all'];
		/*plant_list=[$('.plant').val()]
		line_list=[$('.line').val()]
		resource_list=[$('.resource').val()]
		product_list=[$('.product').val()]
		sbu_list=$('.sbu').val()
		technology_list=$('.technology').val()*/
	}
	else if(window.location.pathname.split('/')[2]=='configuration_benchmark')
	{
		$(".plant_li").each(function(){
			if (this.checked) {plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
		line_list=['all']
		resource_list=['all']
		sbu_list=['all']
		technology_list=['all']
	}
	else if(window.location.pathname.split('/')[2]=='rejected_view')
	{
		$(".plant_li").each(function(){
			if (this.checked) {plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
		line_list=['all']
		resource_list=['all']
		sbu_list=['all']
		technology_list=['all']
	}
	else if(window.location.pathname.split('/')[2]=='grid_view')
	{
		plant_list =[$('#plant-dropdown').attr('data-k')];
		technology_list =[$('#technology-dropdown').attr('data-k')];
		$(".sbu_li").each(function(){
			if (this.checked) {sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".line_li").each(function(){
			if (this.checked) {line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		$(".resource_li").each(function(){
			if (this.checked) {resource_list.push($(this).val());}
		});
		if(resource_list.length==0){resource_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
	}
	else if(window.location.pathname.split('/')[2]=='configuration_view')
	{
		plant_list=$('.plant').val()
		line_list=$('.line').val()
		resource_list=$('.resource').val()
		product_list=$('.product').val()
		sbu_list=$('.sbu').val()
		technology_list=$('.technology').val()
	}
	else{
		$(".plant_li").each(function(){
			if (this.checked) {plant_list.push($(this).val());}
		});
		if(plant_list.length==0){plant_list = ['all'];}
		$(".sbu_li").each(function(){
			if (this.checked) {sbu_list.push($(this).val());}
		});
		if(sbu_list.length==0){sbu_list = ['all'];}
		$(".technology_li").each(function(){
			if (this.checked) {technology_list.push($(this).val());}
		});
		if(technology_list.length==0){technology_list = ['all'];}
		$(".line_li").each(function(){
			if (this.checked) {line_list.push($(this).val());}
		});
		if(line_list.length==0){line_list = ['all'];}
		$(".resource_li").each(function(){
			if (this.checked) {resource_list.push($(this).val());}
		});
		if(resource_list.length==0){resource_list = ['all'];}
		$(".product_li").each(function(){
			if (this.checked) {product_list.push($(this).val());}
		});
		if(product_list.length==0){product_list = ['all'];}
	}
	
data={'plant_list':plant_list,'line_list':line_list,'resource_list':resource_list,'product_list':product_list,'date_list':date_list,'sbu_list':sbu_list,'technology_list':technology_list}

return data

}

/*function intialise_datepicker()
{
	$('#reportrange').daterangepicker({
	ranges: {
	   'Today': [moment(), moment()],
	   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	   'This Month': [moment().startOf('month'), moment().endOf('month')],
	   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	   //  },opens:'left',"startDate": moment().subtract(12, 'month').startOf('month'),"endDate": moment().subtract(1, 'month')..endOf('month'),
	},opens:'left',"startDate": moment().subtract(12, 'month').startOf('month'),"endDate": moment().subtract(34, 'days'),'minDate':"09/01/2015",'maxDate':"09/15/2016"
    }, cb);
	cb(moment().subtract(12, 'month').startOf('month'), moment());
	//cb(datasync_date.subtract(12, 'month').startOf('month'), datasync_date);
	function cb(start, end) {
		start_date=start.format('YYYY-MM-DD')
		end_date=end.format('YYYY-MM-DD')
		$('#reportrange span').html(start.format('YY-MM-DD') + ' - ' + end.format('YY-MM-DD'));
	}
	
}*/
function intialise_datepicker(max_date, min_date, endo_date, max1_date, min1_date, end1_date)
{
	var now_date;
	if (main_end_date == '' && main_start_date == '') {
		main_start_date =(new Date(min1_date).getMonth()+1)+'-'+new Date(min1_date).getDate()+'-'+new Date(min1_date).getFullYear();
		main_end_date = (new Date(max1_date).getMonth()+1)+'-'+new Date(max1_date).getDate()+'-'+new Date(max1_date).getFullYear();
	}
	var dateString = min1_date;
	var momentObj = moment(dateString, 'MM-DD-YYYY');
	var momentString = momentObj.format('YYYY-MM-DD');
	x = new Date(2011,10,30);
	min1_date_obj = new Date(end1_date);
	max1_date_obj = new Date(max1_date);
	now_date = new Date();
	var full_year = now_date.getFullYear();
	var full_year1=  full_year.toString().substring(2);
	var now_month = now_date.getMonth()+1;
	if (now_month < 10) { now_month = '0' + now_month; }
	var now_dates = now_date.getDate();
	if (now_dates < 10) { now_dates = '0' + now_dates; }
	var today = full_year1+'-'+now_month+'-'+now_dates;
	var ystrday = full_year1+'-'+now_month+'-'+now_dates-1;
	var sevendays = full_year1+'-'+now_month+'-'+now_dates-6;
	var thrty = full_year1+'-'+now_month+'-'+now_dates-29;
	var start_month = full_year1+'-'+now_month-1+'-'+01;
	var endof_month = full_year1+'-'+now_month-1+'-'+30;
	
	
	$('#reportrange').daterangepicker({
        ranges: {
           //'Today': [today, today],
           //'Yesterday': [ystrday, ystrday],
           //'Last 7 Days': [sevendays, today],
           //'Last 30 Days': [thrty, moment()],
           //'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]


        },opens:'left',"startDate": min1_date_obj,"endDate": max1_date_obj,'minDate':main_start_date,'maxDate':main_end_date
    }, cb);
	// cb(moment().subtract(12, 'month').startOf('month'), moment().subtract(0, 'month').endOf('month'));
	// cb(moment().subtract(12, 'month').startOf('month'), sync_date);
	cb(endo_date, max_date, end1_date, max1_date);
	function cb(start, end, start_global, end_global) {
		start_date=start_global
		end_date=end_global
		if (start_date=='Custom Range'){
			var date = new Date(start);
			var start_year = date.getFullYear();
			var start_year1=  start_year.toString().substring(2);
			var start_month = date.getMonth()+1;
			if (start_month < 10) { start_month = '0' + start_month; }
			var start_dates = date.getDate();
			if (start_dates < 10) { start_dates = '0' + start_dates; }
			var startt = start_year1+'-'+start_month+'-'+start_dates;

			var date2 = new Date(end);
			var end_year = date2.getFullYear();
			var end_year1=  end_year.toString().substring(2);
			var end_month = date2.getMonth()+1;
			if (end_month < 10) { end_month = '0' + end_month; }
			var end_dates = date2.getDate();
			if (end_dates < 10) { end_dates = '0' + end_dates; }
			var endd = end_year1+'-'+end_month+'-'+end_dates;

			$('#reportrange span').html(startt + ' - ' + endd);
			start_date=start_year+'-'+start_month+'-'+start_dates;
			end_date=end_year+'-'+end_month+'-'+end_dates;

		}
		else if (start_date=='Last Month'){
			var date = new Date(start);
			var start_year = date.getFullYear();
			var start_year1=  start_year.toString().substring(2);
			var start_month = date.getMonth()+1;
			if (start_month < 10) { start_month = '0' + start_month; }
			var start_dates = date.getDate();
			if (start_dates < 10) { start_dates = '0' + start_dates; }
			var startt = start_year1+'-'+(start_month)+'-'+start_dates;

			var date2 = new Date(end);
			var end_year = date2.getFullYear();
			var end_year1=  end_year.toString().substring(2);
			var end_month = date2.getMonth()+1;
			if (end_month < 10) { end_month = '0' + end_month; }
			var end_dates = date2.getDate();
			if (end_dates < 10) { end_dates = '0' + end_dates; }
			var endd = end_year1+'-'+(end_month)+'-'+end_dates;

			$('#reportrange span').html(startt + ' - ' + endd);
			start_date=start_year+'-'+start_month+'-'+start_dates;
			end_date=end_year+'-'+end_month+'-'+end_dates;

		}else{
			$('#reportrange span').html(start + ' - ' + end);
		}
	}
	
}

function add_search()
{
$('.optWrapper').prepend('<div class="select_search_class" style="width: 100%;height: 30px;lineHeight:15px;padding: 4px;border-bottom: 1px solid #EFEFEF;"><input type="text" id="select_search " placeholder="search " style="width: 1; border-radius: 5px;width: 100%;border: 1px solid #EFEFEF;padding-left: 5px;"></div> ')
$('.optWrapper input').bind("keyup paste", function (e) {
            var value = $(this).val().toUpperCase();
            var $rows = $('.optWrapper.open li');
            if (value == '') { $rows.show(); return false; }
		    $rows.each(function (index) {
                select_val = $(this).find('label').context.textContent.toUpperCase()
                if (select_val.indexOf(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });	
			});
}



