function tree_clicked(phase_num,module) {
	$('#tree_stats').html('');
	$('.statistical_overlay').css('display','block');
	var json_data = '';
	selected_param = $('#param-select').val();
	selected_param = ((!selected_param)? 'overall' : selected_param);
	$.ajax({
	method: "GET",
	url: "/qualitycorrelator/getTreeChartData/",
	contentType: "json",
	data:{'mat_num':global_data['GeneralInfo']['MaterialNumber'],'selected_param':selected_param,'Category':phase_num},
	success: function(result_data) {
		global_critical_gain = result_data['CriticalGainData'];
		json_data = JSON.parse(result_data['tree_chart'].replace(/[\w_]+@temp/g, ''));
		json_parameter = (result_data['parameter']);
		global_param_list = result_data['parameter'];
		param_addition_statistical(json_parameter,global_param_number);
		global_param_number = 1;
		if (json_data == "") {
			$('.statistical_overlay').css('display','none');
			$("#tree_stats").html("<span id='no-spo-stats'>No details for Statistical Analysis</span>");
			$("#no-spo-stats").css({"position":"relative","top":"40%","left":"50%"});
		}
		else {
			form_tree(json_data);
		}
		if(global_critical_gain.length>0){
			stats_phase_display1(global_critical_gain);
		}
		else{
			$("#tree_stats, #gain-display").html("<span id='no-spo-stats'>No data to Display</span>");
			//$("#tree_stats").html("<span id='no-spo-stats'>No data to Display</span>");
		}
		xyz();
		if(module == 'InputToOutput'){
			$('.phase_bar_div_stats .phase_number').each(function() {
				if($(this).text() == phase_num) {
					$(this).css('background-color','#9acd32');
					$(this).parent().parent().find('a span .phase').css('background-color','#9acd32');
				}
			})
		}
	}
	});
}

function nav_tab_structure_stats_accuracy(Module) {
	if(Module == 'RCA'){
		str = '<ul class="nav nav-tabs" id="stats-accuracy-nav" role="tablist"><li class="nav-item stats-analysis" style="width:20%;"><a class="nav-link" data-toggle="tab" href="#stats-analysis" role="tab">Model</a></li><li class="nav-item gain-display" style="width:20%;"><a class="nav-link" data-toggle="tab" href="#gain-display" role="tab"> Gain</a></li><li class="nav-item accuracy" style="width:20%;"><a class="nav-link" data-toggle="tab" href="#accuracy" role="tab">Accuracy</a></li><div id="param-nav-select" style="display:inline-block;margin-top: 15px;"></div><span id="product-details" style="display:inline-block;position:absolute;margin-top: 5px;"></span></ul><div class="tab-content" style="height:100%;"><div class="tab-pane" id="stats-analysis" role="tabpanel"></div><div class="tab-pane" id="gain-display" role="tabpanel"></div><div class="tab-pane" id="accuracy" role="tabpanel"></div>';
	}
	else if(Module == 'InputToOutput'){
		str = '<ul class="nav nav-tabs" id="stats-accuracy-nav" role="tablist"><li class="nav-item stats-analysis" style="width:20%;"><a class="nav-link" data-toggle="tab" href="#stats-analysis" role="tab">Model</a></li><li class="nav-item gain-display" style="width:20%;"><a class="nav-link" data-toggle="tab" href="#gain-display" role="tab"> Gain</a></li><li class="nav-item accuracy" style="width:20%;"><a class="nav-link" data-toggle="tab" href="#accuracy" role="tab">Accuracy</a></li><div id="param-nav-select" style="display:inline-block;margin-top: 15px;"></div><span id="product-details" style="display:inline-block;position:absolute;margin-top: 5px;"></span></ul><div class="tab-content" style="height:74%;"><div class="tab-pane" id="stats-analysis" role="tabpanel"></div><div class="tab-pane" id="gain-display" role="tabpanel"></div><div class="tab-pane" id="accuracy" role="tabpanel"></div>';
	}
	return str;
}

function xyz(param_number_parameter) {
	/*$('#modalImportance').removeClass('active-tab');
	$(this).addClass('active-tab');
	$('#modal_importance_tab').css({'display':'none'});  */
	$('.phase_bar_div_stats').html('');
	var str = '<div class="col-lg-6 col-md-6 col-sm-6" style="height:75%;border-right:1px solid #ccc;">'+
						'<div class="col-lg-12 col-md-12 col-sm-12" style="border: 1px solid #ddd;padding: 18px;box-shadow: 0px 0px 7px rgba(0,0,0,0.1) inset;border-radius: 2px;text-align:center;">'+
							//	'<b>Parameter List : </b> <select id="param-select"></select>'+						
						'</div>'+
						'<div class="col-1g-12 col-md-12 col-sm-12" style="margin-top: 9px;padding-left: 0px;"> <b> Confidence : </b> </div>'+
						'<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0px;">'+
							
							'<div style="width:100%;height:28px;text-align:center;color: black;box-shadow: 0px 0px 7px #337ab7 inset;border-radius: 2px;">'+
							'<p style="padding:5px;color: #337ab7;">Parameter Count</p>'+
							'</div>'+	
							'<div class="col-lg-2 col-md-2 col-sm-2" style="height:375px;width:5%;color: black;box-shadow: 0px 0px 7px #337ab7 inset;border-radius: 2px;margin-bottom: 5px;">'+
							'<p style="transform: rotate(-90deg);position:relative;top:47%;color: #337ab7;">Count</p><p style="transform: rotate(-90deg);position: relative;top: 46%;color: #337ab7;">PO </p>'+
							'</div>'+
							'<div class="col-lg-10 col-md-10 col-sm-2" id="statistical-treemap" style="height:375px;min-width:250px;margin: 0 auto;width:94%;padding:0px;">'+							
							'</div>'+
							
						'</div>'+
					'</div>'+
					'<div class="col-lg-6 col-md-6 col-sm-6" style="height:75%;">'+
						'<div class="col-lg-12 col-md-12 col-sm-12" style="border: 1px solid #ddd;padding: 10px;box-shadow: 0px 0px 7px rgba(0,0,0,0.1) inset;border-radius: 2px;margin-bottom: 5px;">'+
							'<b style="color: black;position: relative;left: 45%;"> Accuracy  </b>'+
						'</div>'+
						
						'<div id="statistical-table" style="height:270px;width:100%;">'+							
						'</div>'+
						
						'<div>'+
							'<div style="width:100%;height:28px;text-align:center;color: black;box-shadow: 0px 0px 7px #337ab7 inset;border-radius: 2px;">'+
								'<p style="padding:5px;">Actual Quality Segment</p>'+
							'</div>'+
							'<div class="col-lg-2 col-md-2 col-sm-2" style="height:200px;width:5%;color: black;box-shadow: 0px 0px 7px #337ab7 inset;border-radius: 2px;"><p style="transform: rotate(-90deg);position:relative;top:25%;">Segment</p><p style="transform: rotate(-90deg);position: relative;top: 34%;">Quality </p><p style="transform: rotate(-90deg);position: relative;top: 48%;">Predicted</p>'+
							'</div>'+
							'<div class="col-lg-10 col-md-10 col-sm-10" id="table2-accuracy" style="height:200px;width:94%;padding:0px;margin:0 auto;">'+							
							'</div>'+
						'</div>'+
					'</div>';
					
			
			$("#common-modal .modal-body #accuracy").html(str); 
	
			param_addition_statistical(global_param_list,param_number_parameter);
			global_param_number = 1;
			var product_ID = $('.product').val();
			var product_List = $('#param-select').val();
			
			statiscs_table(product_ID,product_List);
			
			
			getAccuracytable2(product_ID);
					
			$.ajax({
				type:'GET',
				url:'/qualitycorrelator/Accuracy_treemap_axis_values',
				success: function(res){
				
					$.ajax({
						type:'GET',
						url:'/qualitycorrelator/Accuracy_treemap_param_value',
						data:{'product_ID':product_ID,'product_List':product_List},
						success: function(result){
							var xcoordinate = result[0][0];
							var ycoordinate = result[0][1];	
							
							var data = [
								{x:0, y:2, value:1, title:'Medium',color: 'rgba(241, 196, 15, 0.5)'},  /*rgba(231, 76, 60, 0.8) , #e74c3c */
								{x:1, y:2, value:1, title:'High',color: 'rgba(46, 204, 113, 0.5)'},
								{x:2, y:2, value:1, title:'High',color: 'rgba(46, 204, 113, 0.5)'},				/*rgba(241, 196, 15, 0.7) , #f1c40f */
								{x:0, y:1, value:1, title:'Low',color: 'rgba(231, 76, 60, 0.5)'},   /*url(#highcharts-default-pattern-1)*/
								{x:1, y:1, value:1, title:'Medium',color:'rgba(241, 196, 15, 0.5)'},
								{x:2, y:1, value:1, title:'High',color: 'rgba(46, 204, 113, 0.5)'},					/*rgba(46, 204, 113, 0.8) , #2ecc71 */
								{x:0, y:0, value:1, title:'Low',color:'rgba(231, 76, 60, 0.5)'},
								{x:1, y:0, value:1, title:'Low',color: 'rgba(231, 76, 60, 0.5)'},
								{x:2, y:0, value:1, title:'Medium',color: 'rgba(241, 196, 15, 0.5)'}                
							];		
					
							treemap(data,res.PO_Count,res.Parameter_Count,xcoordinate,ycoordinate,'statistical-treemap');	
						},
						error: function(e){
							console.log('error',e);
						}
					});	
				},
				error: function(e){
					console.log('error',e);
				}
			});
}

function getAccuracytable2(product_ID){
	$.ajax({
		type:'GET',
		url:'/qualitycorrelator/getAccuracyMatrix',
		data:{'product_ID':product_ID},
		success: function(res){
			var data = [[0, 0, 0], [0, 1, 0], [0, 2, 0], [0, 3, 0], [1, 0, 0], [1, 1, 0], [1, 2, 0], [1, 3, 0],[2, 0, 0], [2, 1, 0], [2, 2, 0], [2, 3, 0],[3, 0, 0], [3, 1, 0], [3, 2, 0], [3, 3, 0]];
			var len_data = data.length;
			var len_res = res.length;
			
			for(i=0;i<len_res;i++){
				for(j=0;j<len_data;j++){		
					if(data[j][0] == res[i][0] && data[j][1] == res[i][1]){
						data[j][2] = data[j][2] + 1;
					}
				}		
			}

			getAccuracyMatrixChart('table2-accuracy',data);
		},
		error: function(e){
		}
	});
}
	
function param_addition_statistical(param_list,global_param_number) {
	if(global_param_number == 0) {
		list1 = [];
		$('#stats-analysis #param-select').html('<option value="overall" selected>Overall</option>');
		$('#accuracy #param-select').html('<option value="overall" selected>Overall</option>');
		$('#stats-analysis select#param-select').find('option').each(function() {
			if(!($(this).val() == 'overall')) {
				list1.push($(this).val());
			}
		});
		
		$('#accuracy select#param-select').find('option').each(function() {
			if(!($(this).val() == 'overall')) {
				list1.push($(this).val());
			}
		});
		
		for(var f=0;f<param_list.length;f++){
			this_text = param_list[f];
			$('#stats-analysis #param-select').append('<option value="'+this_text+'">'+this_text+'</option>');
		}
		
		for(var f=0;f<param_list.length;f++){
			this_text = param_list[f];
			$('#accuracy #param-select').append('<option value="'+this_text+'">'+this_text+'</option>');
		}
		
		$('#param-nav-select').html($('#param-stats-select').html());
		global_param_number = 1;
	} 
}

function stats_phase_display1(gain_list) {
	var table_gain = '';
	var dynamic_table_key = '';
	if(Object.prototype.hasOwnProperty.call(gain_list[0], 'RawMaterial')) {
		dynamic_table_key = 'RawMaterial';
	}
	else {
		dynamic_table_key = 'Phase';
	}
	
	var table_gain = '<table class="table table-bordered gain_table" style="width:100%;"><thead><th>Rank</th><th>'+dynamic_table_key+'</th><th>CriticalParameter</th><th>Gain</th></tr></thead><tbody id="gain_tbody"></tbody></table>';
	var tbody_gain = '';
	$("#common-modal .modal-body #gain-display").html(table_gain);
	var count = 0;
	$(gain_list).each(function() {
		tbody_gain += '<tr><td></td><td>'+this[dynamic_table_key]+'</td><td>'+this["CriticalParameter"]+'</td><td>'+this["Gain"]+'</td></tr>';
	});
	$('#gain_tbody').html(tbody_gain);
	table_gain += '';
	
	$('.gain_table').DataTable({
		"oLanguage": {
			"sZeroRecords": "No Record Found"
		},
		"fnRowCallback" : function(nRow, aData, iDisplayIndex){
			$("td:first", nRow).html(iDisplayIndex +1);
			return nRow;
		},
		"aoColumnDefs" : [ {
			'bSortable' : false,
			'aTargets' : [ 0,1,2,3 ]
		} ] ,
		"aaSorting": [[3,"desc"]]
	});
	
	$(".stats_phase_class li").last().css("height", "0px");
	
}

function form_tree(json_data) {
	$(".modal-body #tree_stats").html('');
	var width = 1000;
	var height = 450;
	var maxLabel = 150;
	var duration = 500;
	var radius = 5;
	var i = 0;
	var root;

	var tree = d3.layout.tree()
	    .size([height, width]);

	var diagonal = d3.svg.diagonal()
	    .projection(function(d) { return [d.y, d.x]; });

	var svg = d3.select(".modal-body #tree_stats").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	        .append("g")
	        .attr("transform", "translate(" + maxLabel + ",0)");

	root = json_data;
	root.x0 = height / 2;
	root.y0 = height / 2;
	root.children.forEach(collapse);
	even_odd = 0;
	function update(source) 
	{
	    // Compute the new tree layout.
	    var nodes = tree.nodes(root).reverse();
	    var links = tree.links(nodes);

	    // Normalize for fixed-depth.
	    nodes.forEach(function(d) { d.y = d.depth * maxLabel; });

	    // Update the nodes�
	    var node = svg.selectAll("g.node")
	        .data(nodes, function(d){ 
	            return d.id || (d.id = ++i); 
	        });
	    // Enter any new nodes at the parent's previous position.
	    var nodeEnter = node.enter()
	        .append("g")
	        .attr("class", "node")
	        .attr("transform", function(d){ return "translate(" + source.y0 + "," + source.x0 + ")"; })
	        .on("click", click);
	    nodeEnter.append("circle")
	        .attr("r", 0)
	        .style("fill", function(d){ 
	            return d._children ? "lightsteelblue" : "white"; 
	        })
			.style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; })
			.style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; });
	
	    nodeEnter.append("text")
	        .attr("y", function(d){ 
	            var spacing = computeRadius(d) + 5;
				x = (even_odd % 2 == 0) ? -spacing : spacing;
				even_odd++;
	            return x; 
	        })
	        .attr("x", "10")
	        .attr("dy", "11")
			.attr("dx", "5")
			//.attr("y", "5")
	        .attr("text-anchor", function(d){ return d.children || d._children ? "end" : "start"; })
	        .text(function(d){ return d.name.split('|')[0]; })
	        .style("fill-opacity", 0);

	    // Transition nodes to their new position.
	    var nodeUpdate = node.transition()
	        .duration(duration)
	        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });
	    

	    nodeUpdate.select("circle")
	        .attr("r", function(d){ return computeRadius(d); })
	        //.style("fill", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; })
	        .style("fill", function(d) { return (d.name.split('|')[1] == 'Y') ? '#19b698' : '#d3d3d3'; })
	        //.style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#1a237e' : '#d3d3d3'; });
	        .style("stroke", function(d) { return (d.name.split('|')[1] == 'Y') ? '#19b698' : '#d3d3d3'; });

	    nodeUpdate.select("text").style("fill-opacity", 1);

	    // Transition exiting nodes to the parent's new position.
	    var nodeExit = node.exit().transition()
	        .duration(duration)
	        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
	        .remove();

	    nodeExit.select("circle").attr("r", 0);
	    nodeExit.select("text").style("fill-opacity", 0);

	    // Update the links�
	    var link = svg.selectAll("path.link")
	        .data(links, function(d){ return d.target.id; });

	    // Enter any new links at the parent's previous position.
	    link.enter().insert("path", "g")
	        .attr("class", "link")
	        .attr("d", function(d){
	            var o = {x: source.x0, y: source.y0};
	            return diagonal({source: o, target: o});
	        });

	    // Transition links to their new position.
	    link.transition()
	        .duration(duration)
	        .attr("d", diagonal);

	    // Transition exiting nodes to the parent's new position.
	    link.exit().transition()
	        .duration(duration)
	        .attr("d", function(d){
	            var o = {x: source.x, y: source.y};
	            return diagonal({source: o, target: o});
	        })
	        .remove();

	    // Stash the old positions for transition.
	    nodes.forEach(function(d){
	        d.x0 = d.x;
	        d.y0 = d.y;
	    });
	}

	function computeRadius(d)
	{
	    if(d.children || d._children) return radius + (radius * nbEndNodes(d) / 10);
	    else return radius;
	}

	function nbEndNodes(n)
	{
	    nb = 0;    
	    if(n.children){
	        n.children.forEach(function(c){ 
	            nb += nbEndNodes(c); 
	        });
	    }
	    else if(n._children){
	        n._children.forEach(function(c){ 
	            nb += nbEndNodes(c); 
	        });
	    }
	    else nb++;
	    
	    return nb;
	}

	function click(d)
	{
	    if (d.children){
	        d._children = d.children;
	        d.children = null;
	    } 
	    else{
	        d.children = d._children;
	        d._children = null;
	    }
	    update(d);
	}

	function collapse(d){
	    if (d.children){
	        d._children = d.children;
	        d._children.forEach(collapse);
	        d.children = null;
	    }
	}

	function expand(d){   
	    var children = (d.children)?d.children:d._children;
	    if (d._children) {        
	        d.children = d._children;
	        d._children = null;       
	    }
	    if(children)
	      children.forEach(expand);
	}

	function expandAll(){
	    expand(root); 
	    //update(root);
	}
	expandAll();
	update(root);
	$('.statistical_overlay').css('display','none');
}
function product_model_built_status(){	
	$('#input_correlate').prop('disabled', true);
	product = $('.product').val();
	$.ajax({
		method: "GET",
		url: "/qualitycorrelator/getModelBuiltStatus/",
		data:{'Product':product},
		success: function(result_data){
			console.log(result_data);
			$('#model_built_status').html(result_data[0]);
			if(result_data[1] == 1){
				$('#input_correlate').prop('disabled', false);
			}
			if(result_data[1] == 0){
				$('#input_correlate').prop('disabled', true);
			}
		}
	});
}
function statiscs_table(product_ID,product_List){
	
	$.ajax({
		type:'GET',
		url:'/qualitycorrelator/Accuracy_table_values',
		data:{'product_ID':product_ID,'product_List':product_List},
		success: function(res){
			/*var res = {"Group_SOFTENINGPOINT": {"Predicted": 106.40447998046902, "ActualValue": 103.5, "MAPE": 2.8062608506946916}, "Group_VISCOSITYHM": {"Predicted": 11541.9990234375, "ActualValue": 13000.0, "MAPE": 11.215392127403847}, "paramList": ["Group_SOFTENINGPOINT", "Group_VISCOSITYHM"]};*/
			
			/*var res = {
				"Group_SOFTENINGPOINT": {"PredictedScore": 74.86,"ActualScore":79.2}, 
				"Group_VISCOSITYHM": {"PredictedScore": 984.87,"ActualScore":893.0}, 
				"Overall_Score":{"PredictedScore": 0.5323,"ActualScore": 0.43862,"OverAllMape": 0.07345692092701067}
			}*/
			
			var avg_mape = 0;
			avg_mape = res.Overall_Score.OverallMAPE.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
			act_score = res.Overall_Score.ActualScore.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
			pred_score = res.Overall_Score.PredictedScore.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
			
			total_keys=[]
			len = [];
			for(key in res){
				total_keys.push(key);
			}
			
			if($.inArray( "Overall_Score", total_keys )){
				var index = $.inArray( "Overall_Score", total_keys );
				total_keys.splice(total_keys.indexOf('Overall_Score'),index);
			}
			
			len = total_keys.length;
			str1 = '';
			for(i=0;i<len;i++){
				var name =  total_keys[i];
				str1 += '<tr><td>'+name+'</td><td>'+res[name].ActualValue+'</td><td>'+res[name].PredictedlValue.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+'</td></tr>';
			}
			
			var str = '<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:5px;padding-left:0px;"> <b> Actual Score :</b> </div>'+
					'<table class="table table-bordered mape_table" style="width:100%;"><thead><tr><th>Name</th><th>Actual Value</th><th>Predicted Value</th></tr></thead><tbody>'+
			str1;
			str += '</tbody></table>';
			str +='<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:5px;padding-left:0px;"> <b> Overall Score :</b>'+
				'</div>'+
				'<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;">'+
						  '<div class="info-box">'+
							'<span class="info-box-icon bg-aqua"><i class="fa fa-percent" aria-hidden="true"></i></span>'+
							'<div class="info-box-content">'+
							  '<span class="info-box-text">MAPE</span>'+
							  '<span class="info-box-number">'+avg_mape+'<small></small></span>'+
							'</div>'+
						  '</div>'+
						'</div>'+
						'<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;">'+
						  '<div class="info-box">'+
							'<span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>'+
							'<div class="info-box-content">'+
							  '<span class="info-box-text">Actual </span>'+
							  '<span class="info-box-number">'+act_score+'<small></small></span>'+
							'</div>'+
						  '</div>'+
						'</div>'+
						'<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;">'+
						  '<div class="info-box">'+
							'<span class="info-box-icon bg-aqua"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i></span>'+
							'<div class="info-box-content">'+
							  '<span class="info-box-text">Predicted </span>'+
							  '<span class="info-box-number">'+pred_score+'<small></small></span>'+
							'</div>'+
						  '</div>'+
						'</div>';
			
			$('#statistical-table').html(str);
			$('.mape_table').DataTable({
				"order": [[ 0, "desc" ]], "paging":   false, 'bFilter': false, 'bInfo': false
			});
			//$('#statistical-treemap').highcharts().reflow();
		},
		error: function(e){
		}
	});
}

function treemap(data,po_count,param_count,xcoordinate,ycoordinate,id){
	//po_count = po_count.reverse();
	for(key in data){
		if(data[key].x == xcoordinate && data[key].y == ycoordinate){
			if(data[key].x == 0 && data[key].y == 2){ data[key]['color'] = '#f1c40f'; }  //Medium
			if(data[key].x == 0 && data[key].y == 1){ data[key]['color'] = '#e74c3c'; }  //Low
			if(data[key].x == 0 && data[key].y == 0){ data[key]['color'] = '#e74c3c'; }  //Low
			if(data[key].x == 1 && data[key].y == 2){ data[key]['color'] = '#2ecc71'; }  //High
			if(data[key].x == 1 && data[key].y == 1){ data[key]['color'] = '#f1c40f'; }  //Medium
			if(data[key].x == 1 && data[key].y == 0){ data[key]['color'] = '#e74c3c'; }  //Low
			if(data[key].x == 2 && data[key].y == 2){ data[key]['color'] = '#2ecc71'; }  //High
			if(data[key].x == 2 && data[key].y == 1){ data[key]['color'] = '#2ecc71'; }  //High
			if(data[key].x == 2 && data[key].y == 0){ data[key]['color'] = '#f1c40f'; }  //Medium
			//data[key]['color'] = 'url(#highcharts-default-pattern-1)';
		}
	}		

	$('#'+id).highcharts({
		chart: {
			type: 'heatmap',
			backgroundColor: '#fff',
			marginBottom: 30
		},
		title: { text: '' },
		legend: { enabled: false },
		credits: { enabled: false },
		xAxis: { 
			categories: param_count,
			title: { text: '' },
			labels: { 
			enabled: true,
				style: {
					color: 'black',  
					fontSize:'10px',
					fontWeight: '600'
				}
			},
			minPadding:0,
			maxPadding:0,
		},  
		plotOptions: {
			columnrange: {
				 dataLabels: {
					style: {
						textOutline: false,
						textShadow: false
					},
				 }
			},
			series: {
				animation: {
					duration: 200
				}
			}
		},			
		tooltip: {
			enabled: false,
			shared: true,
			useHTML: true,
			headerFormat: '<table>',
			pointFormat: '<tr><td style="text-align: right;font-size:12px;">{point.title}</td></tr>',
			footerFormat: '</table>',
			valueDecimals: 2

		},			
		yAxis: {
			categories: po_count,
			title: { text: '' },
			labels: { 
				enabled: true,
				suffix:'',
				style: {
					color: 'black',  
					fontSize:'10px',
					fontWeight: '600'						
				}				
			}
		},
		series: [{
			borderWidth: 2,
			borderColor:'rgba(255,255,255,.9)',
			data: data,
			dataLabels: {
				enabled: true,
				color: '#fff',
				formatter: function () {
					return this.point.title;
				},
				style: {
					fontWeight:'100',
				}
			}
		}]

	});
	
}

function getAccuracyMatrixChart(id,data){
	$('#'+id).highcharts({

		chart: {
			type: 'heatmap',
			plotBorderWidth: 1,
			marginBottom: 30,
			marginLeft: 50,
		},
		title: {
			text: ''
		},
		credits: { enabled: false },
		xAxis: {
			categories: ['A', 'B', 'C', 'D'],
			title: {text: ''},
			labels: { 
				enabled: true,
				style: {
					color: 'black',  
					fontSize:'10px',
					fontWeight: '600'
				}
			},
		},
		tooltip: {
			enabled: false,
		},
		yAxis: {
			categories: ['A', 'B', 'C', 'D'],
			title: {text: ''},
			labels: { 
				enabled: true,
				suffix:'',
				style: {
					color: 'black',  
					fontSize:'10px',
					fontWeight: '600'						
				}				
			}
		},
		colorAxis: {
			min: 0,
			minColor: '#40C4FF',
			maxColor: '#00B0FF'
		},

		legend: {
			enabled: false
		},
		plotOptions: {
			series: { borderColor: '#fff' },
			animation: { duration: 200 }
		},		
		series: [{
			borderWidth: 1,
			data: data,
			dataLabels: {
				enabled: true,
				color: '#000000',
				style: {
					fontWeight:'100',
				}
			}
		}]

	});
}