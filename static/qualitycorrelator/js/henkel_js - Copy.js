//*********************************for getting filters data on change *********************************//

function  filter_change_data(change)
{
plant_list=[]
line_list=[]
product_list=[]
resource_list=[]
if (change=='plant')
{
	
	if(window.location.pathname.split('/')[2]=='')
	{	
		plant_list=$('.plant').val()
		line_list=['all']
		resource_list=['all']
		change_type='plant'	
	}	
	else
	{	
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']}
		else{ plant_list=[$('.plant').val()] }
		line_list=['all']
		resource_list=['all']
		change_type='plant'	
	}	

}

if (change=='line')
{
	
	if(window.location.pathname.split('/')[2]=='')
	{	
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']	}
		else{ plant_list=$('.plant').val()	}
		line_list=$('.line').val()
		resource_list=['all']
		change_type='line'
	}	
	else if(window.location.pathname.split('/')[2]=='plant_view')
	{	
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']}
		else{ plant_list=[$('.plant').val()] }
		
		line_list=$('.line').val()
		if(line_list==null){line_list=['all']}
		resource_list=['all']
		change_type='line'
	}	
	else 
	{	
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']}
		else{ plant_list=[$('.plant').val()] }
		
		line_val=$('.line').val()
		if(line_val==null){line_list=['all']}
		else{ line_list=[$('.line').val()] }
		resource_list=['all']
		change_type='line'
	}

	
	
}

if (change=='resource')
{
	if(window.location.pathname.split('/')[2]=='')
	{
		plant_val=$('.plant').val()
		if(plant_val==null)
		{
		plant_list=['all']					
		}
		else
		{
		plant_list=$('.plant').val()			
			
		}
		line_val=$('.line').val()
		if(line_val==null)
		{
		line_list=['all']					
		}
		else
		{
		line_list=$('.plant').val()			
			
		}		
	resource_list=$('.resource').val()
	change_type='resource'			
	}
	else if(window.location.pathname.split('/')[2]=='plant_view')
	{	
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']}
		else{ plant_list=[$('.plant').val()] }
		line_list=$('.line').val()
		if(line_list==null){line_list=['all']}
		resource_list=$('.resource').val()
		if(resource_list[0]==null){resource_list[0]='all'}
		change_type='resource'
	}	
	else if(window.location.pathname.split('/')[2]=='line_view')
	{	
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']}
		else{ plant_list=[$('.plant').val()] }
		
		line_val=$('.line').val()
		if(line_val==null){line_list=['all']}
		else{ line_list=[$('.line').val()] }
		
		resource_list=$('.resource').val()
		change_type='line'
	}
	else
	{
		plant_val=$('.plant').val()
		if(plant_val==null){plant_list=['all']}
		else{ plant_list=[$('.plant').val()] }
		
		line_val=$('.line').val()
		if(line_val==null){line_list=['all']}
		else{ line_list=[$('.line').val()] }
		
		resource_val=$('.resource').val()
		if(resource_val==null){resource_list=['all']}
		else{ resource_list=[$('.resource').val()] }
		change_type='line'
		
	}
	
	

}

data={'plant_list':plant_list,'line_list':line_list,'resource_list':resource_list,'change_type':change_type}
return data


}
// Ajax function for getting filter date on change 
function  ajax_filter_change_data(change)
{
	var result;
	Data=filter_change_data(change);
	console.log(Data)
	$.ajax({
	method: "POST",
	url: "/qualitypulse/get_filter_data/",
	data:Data,
	success: function(result_data) {
		console.log(result_data);
		result=result_data;
	}
	});

}



//function for getting filter data on apply buttton click

function filter_data()
{
var data={}
	plant_list=$('.plant').val()
	if(plant_list==null){ 
	plant_list=['all']
	}

	line_list=$('.line').val()
	if(line_list[0]==null){ 
	line_list=['all']
	}

	resource_list=$('.resource').val()
	if(resource_list==null){ 
	resource_list=['all']
	}


	product_list=$('.product').val()
	if(product_list==null){ 
	product_list=['all']
	}
	
data={'plant_list':plant_list,'line_list':line_list,'resource_list':resource_list,'product_list':product_list}
return data
}

function intialise_datepicker()
{
	$('#reportrange').daterangepicker({
		ranges: {
		   'Today': [moment()],
		   'Yesterday': [moment().subtract(1, 'days')],
		   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		   'This Month': [moment().startOf('month'), moment().endOf('month')],
		   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	}, cb);
	cb(moment().subtract(29, 'days'), moment());
	function cb(start, end) {
		$('#reportrange span').html(start.format('YYYY-MM-D') + ' To ' + end.format('YYYY-MM-D'));
	}
}
