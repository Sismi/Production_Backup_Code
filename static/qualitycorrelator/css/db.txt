USE [BillingAssurance]
GO

/****** Object:  StoredProcedure [ba].[sp_update_billing_efficiency]    Script Date: 11/27/2015 6:36:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [ba].[sp_update_billing_efficiency] as
begin

	declare @var_start_dts datetime;
	declare @var_end_dts datetime;
	declare @var_insert_count int;
	declare @var_delete_count int;
	declare @var_update_count int;
	declare @var_loadid int;

	set @var_start_dts=getdate();

	set @var_loadid=(select isnull(max(loadid),-1)+1 from BillingAssurance.ba.ETLLog);
	
	set @var_insert_count=0;

	declare @ruleid int
	declare @errortype nvarchar(100)
	declare @rulequery nvarchar(4000)
	declare @SQLString nvarchar(4000)
	declare @ParmDefinition nvarchar(1000)
	DECLARE db CURSOR FOR
	SELECT ruleid,errortype,rulequery FROM BillingAssurance.ba.[rule] where status = 1
	OPEN db
	FETCH NEXT FROM db INTO @ruleid,@errortype,@rulequery
	WHILE @@FETCH_STATUS = 0
	BEGIN
	 PRINT @errortype
	 PRINT @rulequery
	 if @rulequery!='NA'
	 begin
		set @SQLString=N'insert into BillingAssurance.ba.BillingEfficiency(ruleid,BillNumber,load_id) select distinct b.ruleid,a.billnumber,' + cast(@var_loadid as nvarchar(10)) + 
		N' from (' + @rulequery + N') a join (select @prm_RuleID ruleid) b on 1=1 join (select * from BillingAssurance.ba.Bill where [BillingEfficiencyStatus] is null) c on a.billnumber=c.billnumber'
		SET @ParmDefinition = N'@prm_RuleID int'
		EXECUTE sp_executesql @SQLString,@ParmDefinition,@prm_RuleID=@ruleid
		set @var_insert_count=@var_insert_count+@@ROWCOUNT
		--print @SQLString
	 end
	 FETCH NEXT FROM db INTO @ruleid,@errortype,@rulequery
	END
	CLOSE db
	DEALLOCATE db
	
	update [BillingAssurance].[ba].[Bill]
	set [BillingEfficiencyStatus]='processed'
	where [BillingEfficiencyStatus] is null
	
	set @var_update_count=@@ROWCOUNT
	
	set @var_delete_count=0
	set @var_end_dts=getdate()
	
	INSERT INTO [BillingAssurance].[ba].[ETLLog]
           ([LoadName]
           ,[start_dts]
           ,[end_dts]
           ,[insertcount]
           ,[updatecount]
           ,[deletecount])
     VALUES
           ('Billing Efficiency',@var_start_dts,@var_end_dts,@var_insert_count,@var_update_count,@var_delete_count)	
		   
	print @@ERROR 
	
end





GO


