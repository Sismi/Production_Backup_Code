/*
JS file for User Admin module
*/

$(document).ready(function() {
	alert('hello');

	$.ajax({
		type:"GET",
		url:"user_group",
		success:function(result){
			console.log(result);
			str='';
			for(var i=0;i < result.collect.length;i++){
				str=str+'<li custId="'+result.collect[i].value+'" class="allusergroup">'+result.collect[i].label+'</li>';	
			}
			$('#usergroupdetails').hide().html(str).fadeIn('slow');						

			for(var i=0;i < result.collect.length; i++){
					str=str+'<option value="'+result.collect[i].label+'">'+result.collect[i].value+'</option>';			
			}
			$('#usergrouplist').hide().html(str).fadeIn('slow');
        }
	});

	$("i.editgroup, i.deletegroup").addClass("disable_href");
        $("#user-main, .user-details").css("display","none");

        //  ajax call to fetch all the user role groups from DB on load
        $.ajax({
            type:"GET",
            url:"user_group",
            success:function(result){
                str='';
                for(var i=0;i < result.collect.length;i++){
                    str=str+'<li groupId="'+result.collect[i].label+'" class="single-user-group">'+result.collect[i].value+'</li>';   
                }
                $('#user-role-group-list').hide().html(str).fadeIn('slow');                     
            }
        });

        // ajax call to fetch all the users from DB on load
        $.ajax({
            type:"GET",
            url:"user_list",
            success:function(result){
                str='';
                for(var i=0;i < result.collect.length;i++){
                    str=str+'<li userId="'+result.collect[i].label+'" class="single-user">'+result.collect[i].value+'</li>';   
                }
                $('#user-list').hide().html(str).fadeIn('slow');    
            }
        });

        // ajax call to fetch all the pages list on load
        $.ajax({
            type:"GET",
            url:"pages_list",
            success:function(result){
                str='';
                for(var i=0;i < result.collect.length; i++){
                    str=str+'<option value="'+result.collect[i].label+'">'+result.collect[i].value+'</option>';
                }
                $('#pages-list-select').hide().html(str).fadeIn('slow');
            }
        });

        // click event to display user name on click of a particular user role group
        $(document).on('click', '#user-role-group-list li', function(){
            console.log("clicked a role group");
            $("#user-role-group-list li").css("background-color", "transparent");
            $("#savenewusergroup").html("");
            $("#updatedmsg").html("");
            $("#usergroupdetails li").css("color", "black");
            $("#user-main").css("display","block");
            //$("#level3").removeClass("col-lg-4").addClass("col-lg-3").css("display", "none").css("text-align", "center");
            $("#user-modification, #user-role-group-modification").css("display", "none");
            $(this).css('background','#eeeeee');
            $(this).css('color','grey');
            $('.deletegroup, .editgroup').removeClass('disable_href').css("cursor", "pointer");
            $('.deleteuser, .edituser').addClass('disable_href').css("cursor","default");
            var user_id=$(this).attr('groupId');
            console.log(user_id);
            //$("#user-list").css("display","block");
            $('#user-role-group-list li').removeClass("selected");
            $(this).addClass("selected");
            $.ajax({
                type:"GET",
                url:"user_list_selected",
                data:{"user_id":user_id},
                success:function(result){       
                    str='';
                    console.log(result);
                    for(var i=0;i < result.collect.length; i++){
                        str=str+'<li userid="'+result.collect[i].label+'"class="single-user" >'+result.collect[i].value+'</li>';                  
                    }
                    $('#user-list').hide().html(str).fadeIn('slow');
                }
            });
            $("#user-list").css("display", "block");
            //$("#sublevel1_staff").css("display", "none");
            //$("#sublevel3_staff").css("display", "none");
            //$("#sublevel2_staff").css("display", "block");
            console.log("endsssss");
        });

        // function to show/hide content based on user category selected(user role group or user)
        $("#main-user-category input[name='user-category']").click(function () {
            if($("input[name=user-category]:checked").val() == 'user')
            {
                alert($("input[name=user-category]:checked").val());

                $.ajax({
                    type:"GET",
                    url:"user_list",
                    success:function(result){       
                        str='';
                        for(var i=0;i < result.collect.length; i++){
                            str=str+'<li userid="'+result.collect[i].label+'"class="single-user" >'+result.collect[i].value+'</li>';                  
                        }
                        $('#user_list').hide().html(str).fadeIn('slow');
                        
                    }
                });

                $("#user-role-group-main").css("display","none");
                //$("#user-main").removeClass("col-lg-4").addClass("col-lg-6");
                //$("#user-main").removeClass("col-lg-4").addClass("col-lg-3").css("display","block");
                //$("#sublevel1_staff").css("display", "none");
                $("#user-main").css("display","block");
                $("#user-role-group-modification").css("display","none");
                //$("#sublevel2_staff").css("display", "block").css("text-align", "center");
           //   $("#sublevel2_group").css("display", "block");
                //$("#staffdetails").css("display", "block");
            }
            else
            {
                var companyname = 1;
                console.log(companyname);
                alert($("input[name=user-category]:checked").val());
                $.ajax({
                type:"GET",
                url:"user_group",
                data:{"companyname": companyname},
                success:function(result){
                    console.log(result);
                    str='';
                    for(var i=0;i < result.collect.length;i++){
                        str=str+'<li groupId="'+result.collect[i].value+'" class="single-user-group">'+result.collect[i].label+'</li>';   
                    }
                    $('#usergroupdetails').hide().html(str).fadeIn('slow');         
                    
                    
                }
                });
                $("#user-main").css("display","none");
                $("#user-modification").css("display", "none");
                //$("#level4").css("display", "none");
                //$("#sublevel2_user").css("display","none");
                $("#sublevel2_user").css("display", "block");
                $("#sublevel1_user").css("display", "block");
                $("#user-role-group-main").css("display","block");
            }
        });

        // function to add the selected group for the present user
        $("#usergrouplist").change(function()
        {
            var value = $("#usergrouplist option:selected").val();    
            
            $("#selected-user-groups").css("display", "block");
            var flag = 0;
            for (var i = $("#selected-user-groups span").length - 1; i >= 0; i--) {
                if($($("#selected-user-groups span")[i]).text() == $('#usergrouplist option:selected').text())
                {
                    flag = 1;
                }
                if(value == 0)
                {
                    alert("Select a valid option from the list.");
                    //break;
                    return false;
                }
            };
            if(flag == 0)
            {
                var each_group = "<span style='width: 140px; padding-left: 5px; background-color: grey; border-radius: 3px; margin-right: 5px;'><label style='padding-right:3px;'>"+$('#usergrouplist option:selected').text()+"<i class='fa fa-times' style='cursor: pointer; padding-left: 5px;' aria-hidden='true'></i></label></span>";
                $("#selected-user-groups").append(each_group);  
            }
        });

        // function to add the selected pages for the present user role group
        $("#pages-list-select").change(function()
        {
            var value = $("#pages-list-select option:selected").val();    
            
            $("#selected-pages").css("display", "block");
            var flag = 0;
            for (var i = $("#selected-pages span").length - 1; i >= 0; i--) {
                if($($("#selected-pages span")[i]).text() == $('#pages-list-select option:selected').text())
                {
                    flag = 1;
                }
                if(value == 0)
                {
                    alert("Select a valid option from the list.");
                    //break;
                    return false;
                }
            };
            if(flag == 0)
            {
                var each_group = "<span style='width: 140px; padding-left: 5px; background-color: grey; border-radius: 3px; margin-right: 5px;'><label style='padding-right:3px;'>"+$('#pages-list-select option:selected').text()+"<i class='fa fa-times' style='cursor: pointer; padding-left: 5px; padding-top:3px;' aria-hidden='true'></i></label></span>";
                $("#selected-pages").append(each_group);  
            }
        });

        // function to check whether the selected user role group is already selected or not
        $("#selected-user-groups").on('click','.fa-times',function() {
            for (var i = $("#usergrouplist option").length - 1; i >= 0; i--) {
                if($($("#usergrouplist option")[i]).text() == $(this).parent().text())
                {
                    $(this).parent().parent().remove();     
                }
            }
        });

        // function to check whether the selected page is already selected or not
        $("#selected-pages").on('click','.fa-times',function() {
            for (var i = $("#pages-list-select option").length - 1; i >= 0; i--) {
                if($($("#pages-list-select option")[i]).text() == $(this).parent().text())
                {
                    $(this).parent().parent().remove();     
                }
            }
        });

        // ON click of '+' in add user role group
        $(document).on('click', '.group1', function(){
            $("#savenewusergroup").html("");
            $("#groupnamealert").html("");
            //$("#level3").removeClass("col-lg-3").addClass("col-lg-4");
            $("#user-main").css("display", "none");
            $("#user-modification").css("display","none");
            $("#user-role-group-modification").css("display", "block");
            //$("#sublevel3_staff").css("display", "none");
            //$("#sublevel1_staff").toggle();
            $("#level4").css("display", "none");

            $.ajax({
                type:"GET",
                url:"pages_list",
                //data: custId,
                success:function(result) {
                    str='';
                    console.log("***");
                    console.log(result);
            //      <div class="child-checkbox"><input type="checkbox" id="check_fg_1" value="first_checkbox">Overview</div>
            /*        for(var i=0;i < result.collect.length; i++){
                            str=str+'<div class="child-checkbox"><input type="checkbox" id="'+result.collect[i].label+'" class="allstaff">'+result.collect[i].value+'</div>';           
                    }
                    $('#user-rights-list').hide().html(str).fadeIn('slow');
                    console.log(str);
*/
                    for(var i=0;i < result.collect.length; i++){
                        str=str+'<option value="'+result.collect[i].label+'">'+result.collect[i].value+'</option>';
                    }
                    $('#user-rights-list').hide().html(str).fadeIn('slow');
                }
            });
        });

        // ON click of + in add user
        $(document).on('click', '.user1', function() {
            console.log("on click of + in user");
            $(".sub-users-1").css("display","none");
            $("form-group label").addClass("col-lg-4 col-md-6 col-sm-12 col-xs-12");
            $("form-group input").addClass("col-lg-8 col-md-6 col-sm-12 col-xs-12");
            $("#user-modification, .sub-users-2").css("display", "block");
            $("#sublevel2_user").css("display", "none");
            $("#sublevel1_user").toggle();
            $("#addfn").html("");
            $("#addun").html("");
            $("#addemail").html("");
            $("#addpasswrd").html("");
            $("#updatedmsg").html("");
            var custId = 1;
            $.ajax({
                type:"GET",
                url:"user_group",
                success:function(result){
                    str='';
                    for(var i=0;i < result.collect.length; i++){
                        str=str+'<option value="'+result.collect[i].label+'">'+result.collect[i].value+'</option>';
                    }
                    $('#usergrouplist').hide().html(str).fadeIn('slow');                 
                }
            });
            console.log("ok ok ok");
        }); 

        // search on alpahabtical order for user role group
        $("#groupsearch").on("keyup", function(){
            var value = $(this).val().toLowerCase();
            $("#user-role-group-list > li").each(function() {
                if ($(this).text().toLowerCase().search(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        });

        // search on alpahabtical order for user
        $("#usersearch").on("keyup", function(){
            var value = $(this).val().toLowerCase();

            $("#user-list > li").each(function() {
                if ($(this).text().toLowerCase().search(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        });

        // cancels the present user form
        $('.canceluser').click(function() {
            $("#user-modification").css("display","none");
        });

        // cancels the present user role group form
        $('.cancelgroup').click(function() {
            $("#user-role-group-modification").css("display","none");
        });

        // description of user 
        window.username=""; 
        $(document).on('click', '#user-list li', function(){
            console.log("on click of each user");
            $("#user-list li").css("background-color", "transparent");
            $("#updatedmsg, #alertcontactmsg, #alertemailmsg").html("");
            //$("#alertcontactmsg").html("");
            //$("#alertemailmsg").html("");
            $("#user-list li").css("color", "black");
            $(this).css('background','#eeeeee');
            $(".users2, .sub-users-2, #user-role-group-modification").css("display",'none');
            $(".edituser, #user-modification, .sub-users-1").css("display",'block');
            //$("#user-modification").css("display","block");
            $(this).css('color','grey');
            $('.deleteuser, .edituser').removeClass('disable_href').css("cursor","pointer");
            user_id=$(this).attr("userid");
            console.log(user_id);
            $('#user-list li').removeClass("selected");
            $(this).addClass("selected");
            
            $.ajax({
                type:"GET",
                url:"user_description",
                data:{"user_id":user_id},
                success:function(result){
                    var str=''; 
                    console.log(result);
                    var datetime=result[0]["datepicker"].split("T");
                    str=str+'<tr><th>Username </th><td>'+result[0]["username"]+'</td></tr>'+
                    '<tr><th>Firstname </th><td>'+result[0]["firstname"]+'</td></tr>'+
                    '<tr><th>Lastname</th><td>'+result[0]["lastname"]+'</td></tr>'+
                    '<tr><th>Email </th><td>' +result[0]["email"]+'</td></tr>'+ 
                    '<tr><th>Contact </th><td>' +result[0]["contact"]+'</td></tr>'+
                    '<tr><th>Address </th><td> ' +result[0]["address"]+'</td></tr>'+
                    '<tr><th>Joining Date </th><td> ' +datetime[0]+'</td></tr>'+
                    '<tr><th>Created By </th><td> ' +result[0]["created_by"]+'</td></tr>'+
                    '<tr><th>User Alias </th><td> ' +result[0]["user_alias"]+'</td></tr>';

                    $('#user-description').hide().html(str).fadeIn(1000).css("text-align","center");
                }
            });
            $("#level4").css("display", "block");
            $("#sublevel1_user").css("display", "none");
            $("#sublevel2_user").css("display", "block");
            $(".users1").css("display", "block");
            $(".saveuser").css("display", "none"); 
        });

        // add new user role group
        $(document).on('click', '.addnewgroup', function(){
            var validUdFormName = 0;
            var inp = $("#Name1");
            console.log(inp);
            if(inp.val()!= "") {
                validUdFormName = 1;
            }
            else {
                $('#groupnamealert').html("Enter Groupname");
                $('#Name1').val("");
                validUdFormName = 0; 
            }
            if(validUdFormName == 1){
                var group_custid=$('#overallcustomer li.selected').attr("custid");
                var str = $("#formnewusergroup").serialize();
                console.log(str);
                $.ajax({
                    url: "new_user_role_group",
                    type: "get",
                    data: str,
                    success: function(response){
                        $.ajax({
                            type:"GET",
                            url:"user_group",
                            data:{"companyname":group_custid},
                            success:function(result){
                                str='';
                                for(var i=0;i < result.collect.length;i++){
                                    str=str+'<li custId="'+result.collect[i].value+'" class="allusergroup">'+result.collect[i].label+'</li>';   
                                }
                                $('#usergroupdetails').hide().html(str).fadeIn('slow');                    
                                $('#formnewusergroup input').val("");
                                $('#savenewusergroup').html("Added successfully");
                            }
                        });
                    }
                });
            }   
            $('.deletegroup').addClass('disable_href');
            $('.editgroup').addClass('disable_href');
            //$("#sublevel1_staff").css("display","none");
        });

        // add new user to the DB
        $(document).on('click', '.savenewuser', function(){
            event.preventDefault();
            console.log("save new user");
            var validUdFormEmail = 0, validUdFormusername = 0, validUdFormfirstname = 0, validUdFormpassword = 0; 
            
            //validating email----------
        /*    if (validateemail('Useremail')) {
                validUdFormEmail = 1
            }
            else {
                $('#addemail').html("Invalid email");
                $('#Useremail').val("");
                validUdFormEmail = 0;
            }
          */  
            //validating contact----------
            var inp = $("#username");
            if(inp.val()!= ""){
                validUdFormusername = 1;
            }
            else {
                $('#addun').html("Enter username");
                $('#username').val("");
                validUdFormusername = 0; 
            }

            var inp1 = $("#firstname");
            if(inp1.val()!= ""){
                validUdFormfirstname = 1;
            }
            else {
                $('#addfn').html("Enter firstname");
                $('#firstname').val("");
                 validUdFormfirstname = 0; 
            }

            var inp2 = $("#userpassword");
            if(inp2.val()!= ""){
                validUdFormpassword = 1;
            }
            else {
                $('#addpasswrd').html("Enter password");
                $('#userpassword').val("");
                validUdFormpassword = 0; 
            }
            
            if(validUdFormEmail == 1 && validUdFormfirstname == 1 && validUdFormusername == 1 && validUdFormpassword == 1) {
            
                var str = $("#addnewuserform").serialize();
                console.log(str);
                // check this
                var user_role_group_id = $("#usergroupdetails li.selected").attr('custid');
                console.log(str);
                $.ajax({
                    url: "new_user?catid="+user_role_group_id,
                    type: "get",
                    data: str,
                    
                    processData: false,
                    contentType: false,
                    dataType : 'json',
                    success: function(response){
                        $.ajax({
                            
                            type:"GET",
                            url:"user_list_selected",
                            data:{"group_id":user_role_group_id},
                            success:function(result){
                                str='';
                                for(var i=0;i < result.collect.length; i++){
                                    str=str+'<li user_id="'+result.collect[i].value+'"class="single-user" >'+result.collect[i].label+'</li>';      
                                }
                                $('#user-list').hide().html(str).fadeIn('slow');
                                $('#sub-users-2').css("display","none");
                                $("#updatedmsg").html("Added successfully");
                                $('#addnewuserform input').val("");
                            }
                        });
                    }
                });
                
            }
            console.log("save new user endssss");
        });

        //---------------------
        $(".deletegroup").click(function(){
            var custid= $('#user-role-group-list li.selected').attr('groupId');
            console.log(custid);
            $.ajax({
                url: "count_users",
                type: "get",
                data: {"custid":custid},
                success: function(response){
                    $("#groupuser1").html(response);
                }
            });
        });

        // delete user role group
        $('.usergroup_yes').click(function(){
            var name1= $('#usergroupdetails li.selected').attr('custid');
            var name= $('#usergroupdetails li.selected').text();
            $.ajax({
                url: "delete_user_role_group",
                type: "get",
                data: {"id":name1, "usergroup":name},
                success: function(response){
                    
                var name= $('#overallcustomer li.selected').attr('custid'); 
                    $.ajax({
                        
                        type:"GET",
                        url:"user_group",
                        data:{"companyname":name},
                        success:function(result){
                        
                            str='';
                            for(var i=0;i < result.collect.length;i++){
                                
                                str=str+'<li custId="'+result.collect[i].value+'" class="allusergroup">'+result.collect[i].label+'</li>';   
                            }
                            
                            $('#usergroupdetails').html(str);
                        }
                        });
                }
            }); 
            //$("#sublevel3_staff").css("display","none");
            //$("#sublevel2_staff").css("display","none");
            $('.deletegroup').addClass('disable_href');
            $('.editgroup').addClass('disable_href');
            $('#sublevel2_user').css("display","none");
            $('#sublevel1_user').css("display","none");
            
            $('#level3').css('display',"none");
            $('#level4').css('display',"none");
        });

        // Showing the details of a user to make further edition
        $(document).on('click', '.edituser', function(){
            $('#updatemsg').html("");
                var user_id= $('#user-list li.selected').attr("userid");
                $.ajax({
                    type:"GET",
                    url:"user_description",
                    data: {"user_id":user_id},
                    success:function(result) { 
                        var str=[]; 
                        str[0] =result[0]["username"];
                        str[1] =result[0]["firstname"];
                        str[2] =result[0]["lastname"];
                        str[3] =result[0]["email"];
                        str[4] =result[0]["contact"];
                        str[5] =result[0]["address"];
                        str[6] =result[0]["password"];
                        str[7] =result[0]["datepicker"];
                        str[8] =result[0]["user_alias"];
                        var datetime = str[7].split("T");
                        console.log(str);
                        document.getElementById("editusername").value = str[0];
                        document.getElementById("editfirstname").value = str[1];
                        document.getElementById("editlastname").value = str[2];
                        document.getElementById("editUseremail").value = str[3];
                        //document.getElementById("editusercontact").value = str[4];
                        //document.getElementById("edituseraddress").value = str[5];
                        document.getElementById("edituserpassword").value = str[6];
                        document.getElementById("editdatepicker").value = datetime[0]; 
                        //document.getElementById("edituseralias").value = str[8];
                    }
                });
                $("#updatemsg").html("");       
                $("#user-modification").css("display", "block");
                $("#sub-users-1").css("display", "none");
                $("#sub-users-2").css("display", "block");
                $(".users1").css("display", "none");
                $(".users2").css("display", "block");
                $(".edituser").css("display","none");
                $(".edituser1").css("display","block");
                $(".saveuser").css("display", "block");
                $("#alertemailmsg").html("");
                $("#alertcontactmsg").html("");
                $("#editfn").html("");
                $("#editun").html("");
                $("#editemail").html("");
                $("#editpasswrd").html("");
            }); 

        // to display in edit form of user role group
        $(document).on('click', '.editgroup', function(){
            $('#editusergroupname').html("");
            $("#savenewusergroup").html("");
            var groupname=$('#usergroupdetails li.selected').text();
            var group_custid=$('#usergroupdetails li.selected').attr("custid");
            $('#usergrouptoggle1').change(function(){
                a = $(this).prop('checked');
                
            });
            
            $.ajax({
            type:"GET",
            url:"editgroup",
            data:{"groupname":groupname,"group_custid":group_custid},
            success:function(result){ 
                 var str=[]; 
                    str[0] =result[0]["name"];
                    str[1] =result[0]["select"];
                    str[2] =result[0]["region"];
                    str[3] =result[0]["toggle"];
                    
                    $("#usergroupname").val(str[0]) ;
                    $("#usergroupselect").val(str[1]);
                    $('#usergrouptoggle1').prop('checked',false).change();
                    if(str[3]=='true')
                    {
                    $('#usergrouptoggle1').prop('checked',true).change();   
                    }else
                    {
                    $('#usergrouptoggle1').prop('checked',false).change();
                    }
                    $("#usergroupRegion1").val(str[2]);
                    
                    $("#usergroupRegion1").val(str[2]); 
            }
            });
            $("#sublevel1_staff").css("display", "none");
            $("#sublevel2_staff").css("display", "none");
            $("#sublevel3_staff").css("display", "block");
            $("#level4").css("display", "none");
            
        });
        
});

$(document).ready(function() {
    console.log( "ready!" );
});