from django.shortcuts import render
from django.db.backends.util import CursorWrapper
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from datetime import datetime
import pymongo
import logging
import json
import ast

#---------------------------------connection function--------------------------------------#
def finishedgood_connection():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_fng_qualscore_qualitysegment
	return table_cursor

def staggedpo_connection():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.Report_StagedPO_QulaityScoreSeg	
	return table_cursor


def FinishedGoodScore(request):
	data=[]
	if request.method == 'GET' and 'SiteID' in request.GET:
		SiteID=request.GET.get('SiteID').encode('utf-8')
	if request.method == 'GET' and 'ProcessOrder' in request.GET:
		ProcessOrder=request.GET.get('ProcessOrder').encode('utf-8')
	if ((SiteID is not None and SiteID != '')and (ProcessOrder is not None and ProcessOrder != '')):
		result_cursor=finishedgood_connection().find({'SiteID':SiteID,'ProcessOrder':ProcessOrder},{'_id':0,'AvgQualityScore':1,'QualitySegment':1})
		for result in result_cursor:
			data.append(result)
	return HttpResponse(json.dumps(data), content_type="application/json")	
	
	
def StaggedPOScore(request):
	data=[]
	if request.method == 'GET' and 'SiteID' in request.GET:
		SiteID=request.GET.get('SiteID').encode('utf-8')
	if request.method == 'GET' and 'ProcessOrder' in request.GET:
		ProcessOrder=request.GET.get('ProcessOrder').encode('utf-8')
	if ((SiteID is not None and SiteID != '')and (ProcessOrder is not None and ProcessOrder != '')):
		result_cursor=staggedpo_connection().find({'SiteID':SiteID,'ProcessOrder':ProcessOrder},{'_id':0,'AvgQualityScore':1,'QualitySegment':1})
		for result in result_cursor:
			data.append(result)
	return HttpResponse(json.dumps(data), content_type="application/json")	