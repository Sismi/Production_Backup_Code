from django.conf.urls import url
from QPQC_data_api import views
urlpatterns = [
	url(r'^FinishedGoodScore/$', views.FinishedGoodScore, name="FinishedGoodScore"),
	url(r'^StaggedPOScore/$', views.StaggedPOScore, name="StaggedPOScore"),	
]