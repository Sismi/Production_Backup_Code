"""
WSGI config for henkel project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "henkel.settings")

#os.environ['PYTHON_EGG_CACHE'] = '/home/sarajesh/.python-eggs'

os.environ['PYTHON_EGG_CACHE'] ='/tmp'

application = get_wsgi_application()
