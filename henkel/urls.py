from django.conf.urls import include, url
from django.contrib import admin
from qualitypulse import views
from qualitycorrelator import views
from QPQC_data_api import views
from user_mod import urls as user_url

urlpatterns = [
    url(r'^qualitypulse/', include('qualitypulse.urls')),
	url(r'^qualitycorrelator/', include('qualitycorrelator.urls')),
	url(r'^QPQC_data_api/', include('QPQC_data_api.urls')),
	url(r'^user_mod/', include(user_url)),
]
