#import pandas as pd
#import psycopg2
#import numpy as np
import datetime as dt


#Step 1 : Construct the Dynamic API Call 
#   location="30.8066095696,121.4766826549"
#Create a function to pull historic data
    #Once this is done, pump the data to Weather data
#------------------------------------------------------
#Call the function for 5 days of data [Astronomy Data + Weather Data]

def weather_historical(location,start_date,end_date):
	import urllib2 as url
	loc=location
	base_link="http://api.worldweatheronline.com/free/v2/past-weather.ashx"
	key="?key=72db368510ed58d56e3800d34a7b4"
	location = "&q=" + location
	format = "&format=csv"
	start_date="&date="+ start_date
	end_date = "&enddate=" + end_date
	tp = "&tp=3"
	api_link="http://api.worldweatheronline.com/free/v2/past-weather.ashx?key=72db368510ed58d56e3800d34a7b4&q=30.8066095696,121.4766826549&date=2017-01-12&enddate=2017-01-23&tp=3&format=csv"
	#Extracting data through the API call
	txt = url.urlopen(api_link).read()
	
	aaa=txt.split('\n') #Converting the single line text to multiple lines
	
	print aaa
	'''weather_header=aaa[5] 
	weather_header=weather_header.replace('#','')
	weather_header=weather_header.split(',')
	astronomy_header=aaa[2]
	astronomy_header=astronomy_header.replace('#','')
	astronomy_header=astronomy_header.split(',')
	#Casting as a Dataframe for Pandas operations
	df=pd.DataFrame(aaa)
	df.columns=['Column']
	print '====================================='
	print df
	new=pd.DataFrame(df['Column'].str.split(',').tolist(),columns=['a1','a2','a3','a4','a5','a6','a7','a8','a9','a10','a11','a12','a13','a14','a15','a16','a17','a18','a19','a20','a21','a22','a23','a24','a25','a26'])
	#Creating the range for Astronomy Data
	print '====================================='
	print new
	b=range(8,len(new),9) 
	astronomy_range=b
	astronomy_range=astronomy_range[0:len(astronomy_range)-1]
	#Creating a range for Weather Data
	a=[]
	for x in range(8,len(new)-1,9):
		a=a+(range(x+1,x+9,1))
	weather_range=a
	print '\n'
	print '====================================='
	#print weather_range
	#Creating Weather Data
	weather=new.ix[weather_range,:]
	weather.columns=weather_header
	weather['Location']=loc
	weather.to_dict(orient="index")
	print weather
	print '====================================='
	#Creating Astronomy Data
	astronomy=new.ix[astronomy_range,0:9]
	astronomy.columns=astronomy_header
	astronomy['Location']=loc
	#Consolidating the Astronomy and Weather Dataframes into a single list
	list=[weather]
	print list'''
	return(list)

#import connectDB # this is simply a module that returns a connection to the db
class ReadFaker:
    """
    This could be extended to include the index column optionally. Right now the index
    is not inserted
    """
    def __init__(self, data):
        self.iter = data.itertuples()
    def readline(self, size=None):
        try:
            line = self.iter.next()[1:]  # element 0 is the index
            row = '\t'.join(x.encode('utf8') if isinstance(x, unicode) else str(x) for x in line) + '\n'
        # in my case all strings in line are unicode objects.
        except StopIteration:
            return ''
        else:
            return row
    read = readline

def insert(df, table, con=None, columns = None):
    time1 = dt.datetime.now()
    close_con = False
    if not con:
        try:
            conn_string = "host='localhost' dbname='henkel_temp' user='postgres' password='password'"
            psycopg2.connect(conn_string)   ###dbLoader returns a connection with my settings
            close_con = True
        except psycopg2.Error, e:
            print e.pgerror
            print e.pgcode
            return "failed"
    inserted_rows = df.shape[0]
    data = ReadFaker(df)
    try:
        curs = con.cursor()
        print 'inserting %s entries into %s ...' % (inserted_rows, table)
        if columns is not None:
            curs.copy_from(data, table, null='nan', columns=[col for col in columns])
        else:
            curs.copy_from(data, table, null='nan')
        con.commit()
        curs.close()
        if close_con:
            con.close()
    except psycopg2.Error, e:
        print e.pgerror
        print e.pgcode
        con.rollback()
        if close_con:
            con.close()
        return "failed"
    time2 = dt.datetime.now()
    print time2 - time1
    return inserted_rows


def sql_execute(sql):
    import psycopg2
    conn_string = "host='henkelpilot.westeurope.cloudapp.azure.com' dbname='henkel_temp' user='postgres' password='password'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    cursor.execute(sql)
    results=cursor.fetchall()
    cursor.close()
    return(results)



def sql_insert(df,table):
    import psycopg2
    conn_string = "host='henkelpilot.westeurope.cloudapp.azure.com' dbname='henkel_temp' user='postgres' password='password'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    insert(df,table,con)
    cursor.close()
    results=1
    return(results)
#--------------------------------------------------------------------------------------------------------


#--------------------------------------------------------------------------------------------------------

#Defining functions for MySQL execute and insert
def sql_execute(sql):
    import psycopg2
    conn_string = "host='henkelpilot.westeurope.cloudapp.azure.com' dbname='henkel_temp' user='postgres' password='password'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    cursor.execute(sql)
    results=cursor.fetchall()
    cursor.close()
    return(results)



def sql_insert(df,table):
    import psycopg2
    conn_string = "host='henkelpilot.westeurope.cloudapp.azure.com' dbname='henkel_temp' user='postgres' password='password'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    insert(df,table,con)
    cursor.close()
    results=1
    return(results)

#-----------------------------------------------------------------------------------------------------------
'''
#Daily Run
#Historic Data
import datetime as dt
start_date=str(dt.date.today() - dt.timedelta(days=12))
end_date=str(dt.date.today() - dt.timedelta(days=1))
sql="select * from weather_historical_parameters"
data=sql_execute(sql)

for i in range(0,len(data)):
	max_date=sql_execute("select max(date) from weather_historical where location = '"+data[i][0]+"'")
	historical_data=weather_historical(data[i][0],start_date,end_date)
	historical_data[0]=historical_data[0][pd.to_datetime(historical_data[0]['date']) > pd.to_datetime(max_date[0][0])]
	sql_insert(historical_data[0],'weather_historical')
'''
#-----------------------------------------------------------------------------------------------------------

historical_data = weather_historical('30.8066095696,121.4766826549','2017-01-22','2017-02-01')
historical_data[0]=historical_data[0][pd.to_datetime(historical_data[0]['date']) > '2017-01-21']
#print historical_data[0]
