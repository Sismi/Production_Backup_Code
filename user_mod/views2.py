from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
import psycopg2
import json
import requests
import pymongo
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext, loader
from collections import OrderedDict
import datetime
from django.core.urlresolvers import reverse
#import qualitypulse.pages_display
from qualitypulse.views import pages_display
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import cgi, os
import cgitb; cgitb.enable()

def uploadExclusionFile(request):
    filename=''
    if request.method == 'POST':
        form = cgi.FieldStorage()
        fileitem = request.FILES['uploadexcfile']
        filename = fileitem.name
        path = default_storage.save('fuploads/'+filename, ContentFile(fileitem.read()))
    return HttpResponse(filename, content_type="application/json")

# Create your views here.
def login_user(request):  
    username = request.POST.get("username")
    password = request.POST.get("password")  
    data = request.POST.copy 

    if request.method == 'POST' and username != '' and password != '':
        data = request.POST.copy
        cur = connection.cursor()
        print data
#        url="http://test.vegamview.com:8081/iPAS_DstoreService/DstoreService.svc/Login?username="+username+"&password="+password+""
 #       print url
 #       res=requests.get(url)
 #       print res.status_code
 #       print res.content
 #       mes_value = json.loads(res.content)
        query1 = "select * from public.usp_userauthentication('"+username+"','"+password+"')"
        print query1
        cur.execute(query1)
        result = cur.fetchone()
        print result
        cur.close()
        if(result[0] == 1):
            print "successful"
           # s = request.session()
            request.session['username'] = username
            request.session['user_id'] = result[1]
            print request.session['username']
            print request.session['user_id']
           # print s
            user_alias = result[3]
            # request.session['user_alias'] = result[3]
            if (user_alias == ''):
                request.session['user_alias'] = username
            else:
                request.session['user_alias'] = user_alias
            display_name = username
            user_id = result[1]
            query2 = "select * from user_getassociatedapppages("+str(user_id)+")"
            print query2
            cur1 = connection.cursor()
            cur1.execute(query2)
            result1 = cur1.fetchall()
            print result1
            finished_goods_list = []
            raw_materials_list = []
            administrator_list = []
            for i in result1:
                d = OrderedDict()
                if i[4] == 'Finished Goods':
                    print finished_goods_list
                    finished_goods_list.append(i)
                if i[4] == 'Raw Materials':
                    print raw_materials_list
                    raw_materials_list.append(i)
                if i[4] == 'Administrator':
                    print administrator_list
                    administrator_list.append(i)
            pagedatarray=OrderedDict()
            firstpagetoshow=''
            if(len(finished_goods_list)>0):
                pagedatarray['Finished Good']=finished_goods_list
                firstpagetoshow=finished_goods_list[0][3]
            if(len(raw_materials_list)>0):
                pagedatarray['Raw Materials']=raw_materials_list
                if(firstpagetoshow==''):
                    firstpagetoshow=raw_materials_list[0][3]      
            if(len(administrator_list)>0):
                pagedatarray['Administrator']=administrator_list 
              
            print firstpagetoshow
            return HttpResponseRedirect(firstpagetoshow)
        else:
            return render(request, "login_page.html",{'invalid': True })
    else:
        return render(request, "login_page.html",{'invalid': False })

def logout(request):
    del request.session['user_id']
    return HttpResponseRedirect("/user_mod/login/")

#---------------------------------connection function--------------------------------------#
def connection_mongo():
    conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
    cursor=conn.henkel
    cursor.authenticate('henkelUser','QpQc#123')
    table_cursor=conn.henkel.Report_QualScore_QualSegment   
    return table_cursor

def admin(request):
    if 'user_id'  in request.session:
        pagedatarray = pages_display(request)
        template = loader.get_template('admin.html')
        result_cursor=connection_mongo().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
        for r in result_cursor:
            syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
        print pagedatarray
        print syncdate
        context = RequestContext(request, {'appuser':request.session['user_alias'],'appuserid':request.session['user_id'], "pages_dict": pagedatarray,'syncdate':syncdate })
        return HttpResponse(template.render(context), content_type="text/html")
    else:
        return HttpResponseRedirect("/user_mod/login/")

def fetch_select2_values(request):
    list_sitename=[]
    list_sbu=[]
    list_siteid=[]
    list_technology=[]
    list_linename=[]
    list_fgproducts=[]
    cur1 = connection.cursor()
    cur2 = connection.cursor()
    cur3 = connection.cursor()
    cur4 = connection.cursor()
    cur5 = connection.cursor()
    cur6 = connection.cursor()
    query1 = "select * from public.param_name_value('SiteName')"
    query2 = "select * from public.param_name_value('SBU')"
    query3 = "select * from public.param_name_value('Resources')"
    query4 = "select * from public.param_name_value('Technology')"
    query5 = "select * from public.param_name_value('LineName')"
    query6 = "select * from public.param_name_value('FGProducts')"
    cur1.execute(query1)
    cur2.execute(query2)
    cur3.execute(query3)
    cur4.execute(query4)
    cur5.execute(query5)
    cur6.execute(query6)
    sitename_list = cur1.fetchall()
    sbu_list = cur2.fetchall()
    siteid_list = cur3.fetchall()
    technology_list = cur4.fetchall()
    linename_list = cur5.fetchall()
    fgproducts_list = cur6.fetchall()
    list_sitename.append(sitename_list)
    list_sbu.append(sbu_list)
    list_siteid.append(siteid_list)
    list_technology.append(technology_list)
    list_linename.append(linename_list)
    list_fgproducts.append(fgproducts_list)
    cur1.close()
    cur2.close()
    cur3.close()
    cur4.close()
    cur5.close()
    cur6.close()
    pagedata={'SiteName':sitename_list,'SBU':sbu_list,'Resources':siteid_list,'Technology':technology_list,'LineName':linename_list,'FGProducts':fgproducts_list}
    return HttpResponse(json.dumps(pagedata), content_type="application/json")

def user_group(request):
    companyvalue=[]
    cur = connection.cursor()
    query1='Select * from UserAppRoleGroup_ListUserGroups()'
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def data_user_group(request):
    companyvalue=[]
    cur = connection.cursor()
    query1='Select * from user_data_rolegroup_listusergroups()'
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def user_list(request):
    companyvalue=[]
    cur = connection.cursor()
    query1="select * from public.users_listusers()"
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def absent_user_list(request):
    companyvalue=[]
    groupId=request.GET.get('group_id')
    label_text=request.GET.get('label_text')
    print groupId
    cur = connection.cursor()
    query1="Select * from public.user_data_APP_rolegroup_listusernotgroup("+groupId+",'"+label_text+"')"
    cur.execute(query1)
    result= cur.fetchall()
    print result
    for i in result:
        d=OrderedDict()
        d['username']=i[1]
        d['firstname']=i[2]
        d['email_address']=i[5]
        d['user_alias']=i[4]
        d['created_date']=i[7].isoformat()
        d['user_id']=i[0]
        companyvalue.append(d)
    cur.close()
    #pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(companyvalue),content_type="application/json")

def pages_list(request):
    companyvalue=[]
    cur = connection.cursor()
    query1='select pk_page_id, page_name from "AppPages"'
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def GetUsersList_SameGroup(request):
    GroupId=request.POST.get('group_id')
    Data=[]
    cursor = Connection.cursor()
    query="Select pk_user_id ,username from Users where flag_active=1 and fk_userrolegroup_id='"+GroupId+"'"
    cursor.execute(query)
    result=cursor.fetchall()
    for row in result:
        DataDict={}
        DataDict['GroupId']=row[0]
        DataDict['GroupName']=row[1]
        Data.append(DataDict)
    return HttpResponse(json.dumps(Data), content_type="application/json")  

def user_list_selected(request):
    companyvalue=[]
    userrolegroup_id=request.GET.get("group_id")
    cur = connection.cursor()
    query1= "select * from public.userapprolegroup_listusers('"+userrolegroup_id+"')"
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        d['password']=i[2]
        d['firstname']=i[3]
        d['email_address']=i[4]
        d['created_date']=i[5]
        d['flag_active']=i[6]
        d['fk_userrolegroup_id']=i[7]
        d['check']='<input type="checkbox" />'
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def list_related_users(request):
    companyvalue = []
    userrolegroup_id = request.GET.get("group_id")
    cur = connection.cursor()
    query1= "select * from public.userapprolegroup_listusers('"+userrolegroup_id+"')"
    cur.execute(query1)
    #cur.execute(query1)
    result = cur.fetchall()
    for i in result:
        d = OrderedDict()
        d['username']=i[1]
        if i[2]:
            d['firstname'] = i[2]
        else:
            d['firstname']= ''
        d['email_address']=i[6]
        d['created_date']=i[8].isoformat()
        d['user_alias']=i[7]
        d['user_id']=i[0]
        companyvalue.append(d)
    cur.close()
    return HttpResponse(json.dumps(companyvalue), content_type='application/json')

def list_all_licenses(request):
    print "inside list licesnes"
    companyvalue = []
    conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
    cursor=conn.henkel
    cursor.authenticate('henkelUser','QpQc#123')
    table_cursor=conn.henkel.test_licensing
    result_cursor = table_cursor.find({'ActiveFlag':1},{'_id':0})
    for row in result_cursor:
        print row
        d = OrderedDict()
        d['SiteName']=row['SiteName']
        d['Technology']=row['Technology']
        d['Approved By']=row['Approved By']
        d['Upload File']=row['Upload File']
        d['Approved Date']=row['Approved Date']
        d['Description']=row['Description']
        d['SiteID']=row['SiteID']
        d['id']=str(row['SiteID'])+'_'+str(row['Technology'])
        companyvalue.append(d)

    return HttpResponse(json.dumps(companyvalue), content_type='application/json')

def list_related_datausers(request):
    companyvalue = []
    userrolegroup_id = request.GET.get("group_id")
    cur = connection.cursor()
    query1= "select * from user_data_rolegroup_listusers('"+userrolegroup_id+"')"
    cur.execute(query1)
    #cur.execute(query1)
    result = cur.fetchall()
    for i in result:
        d = OrderedDict()
        d['username']=i[1]
        if i[2]:
            d['firstname'] = i[2]
        else:
            d['firstname']= ''
        d['email_address']=i[6]
        d['created_date']=i[8].isoformat()
        d['user_alias']=i[7]
        d['user_id']=i[0]
        companyvalue.append(d)
    cur.close()
    return HttpResponse(json.dumps(companyvalue), content_type='application/json')

def user_description(request):
    user = request.user
    username = user.username
    userdetail=[]
    user_id=request.GET.get("user_id")
    cur = connection.cursor()
    query1 = "select * from public.users_users_details('"+user_id+"');"
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['username']=i[1]
        d['firstname']=i[3]
        d['email']=i[6]
        d['datepicker']=i[8].isoformat()
        d['user_alias']=i[5]
        d['lastname']=i[4]        
        d['flag_active']=i[11]
        d['contact']=i[5]
        d['address']=i[7]
        d['created_by']=i[7]
        #d['status']=i[12]
        d['password']=i[2]
        

        userdetail.append(d)
    cur.close()
    return HttpResponse(json.dumps(userdetail),content_type="application/json")

def new_user_role_group(request):
    #groupname1=request.GET.get("idkey")
    name=request.GET.get("name")
    description=request.GET.get("description")
    comments=request.GET.get("comments")
    pages_list=request.GET.get("pages_list")
    name = name.title()
    cur = connection.cursor()
    query1 = "select * from public.insert_usergroup_apppages('"+pages_list+"','"+name+"','"+description+"','"+comments+"')"
    print query1
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def new_data_user_group(request):
    name=request.GET.get("name")
    description=request.GET.get("description")
    comments=request.GET.get("comments")
    values_list=request.GET.get("values_list")
    name = name.title()
    cur = connection.cursor()
    query1 = "select * from insert_user_data_group_dataparam('"+values_list+"','"+name+"','"+description+"','"+comments+"')"
    print query1
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

#mongodb
@csrf_exempt
def new_license_addition(request):
    try:
        print request.FILES.get('file-upload')
        approvedby=request.POST.get("approved-by")
        approveddate=request.POST.get("approved-date")
        descriptionlicense=request.POST.get("description-license")
        plant_list=request.POST.getlist("plant-list")
        technology_list=request.POST.getlist("technology-list")
        print approvedby
        print approveddate
        print descriptionlicense
        print plant_list
        print technology_list
        conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
        cursor=conn.henkel
        cursor.authenticate('henkelUser','QpQc#123')
        table_cursor=conn.henkel.test_licensing
        one_dict = {}
        print "start"
        one_dict['Approved Date']=approveddate
        one_dict['ActiveFlag']=1
        one_dict['Approved By']=approvedby
        one_dict['Description']=descriptionlicense
        one_dict['SiteID']=plant_list
        one_dict['Technology']=technology_list
        one_dict['Upload File']=technology_list
        print "end"
        print one_dict
        table_cursor.insert(one_dict)
        return HttpResponse(1,content_type="application/json")
    except Exception as e:
        print e

def __unicode__(self):
    return self.user.username

def new_user_addition(request):
    #form = cgi.FieldStorage()
    #catid=request.GET.get("catid")
    print request
    name=request.GET.get("editusername")
    firstname=request.GET.get("editfirstname")
    lastname=request.GET.get("editlastname")
    email=request.GET.get("editUseremail")
    user_alias=request.GET.get("edituseralias")
    #address=request.GET.get("useraddress")
    password1=request.GET.get("edituserpassword")
    created_date=request.GET.get("editdatepicker")
    user_flag=request.GET.get("edituserstatus")
    # if user_flag == 'on':
    #     user_flag = 'true'
    # else:
    #     user_flag = 'false'
    #user = request.user
    print name
    print firstname
    print lastname
    print email
    print user_alias
    print password1
    print created_date
    print user_flag
    #print user
    #username = user.username
    created_by = request.session['username']
    # created_by = 'System'
    #print created_by
    cur = connection.cursor()
    #query1= "insert into users (fk_group_id,user_name,first_name,last_name,email,phone_num,address,password,join_date) values ('"+str(catid)+"','"+str(name)+"','"+str(firstname)+"','"+str(lastname)+"' ,'"+str(email)+"','"+str(contact)+"','"+str(address)+"','"+str(password1)+"','"+str(datepicker)+"' )"
    query1 = "select * from public.insert_user('"+name+"','"+password1+"','"+firstname+"','"+lastname+"','"+user_alias+"','"+email+"','"+created_by+"','"+created_date+"');"
    cur.execute(query1)
    print query1
    cur.close()
    return HttpResponse(1,content_type="application/json")

def new_user_absent_list(request):
    #form = cgi.FieldStorage()
    group_id = request.GET.get("group_id")
    user_id_list = request.GET.get("user_str")
    action = request.GET.get("action")
    label_text=request.GET.get("label_text")
    catid=request.GET.get("catid")
    name=request.GET.get("username")
    firstname=request.GET.get("firstname")
    lastname=request.GET.get("lastname")
    email=request.GET.get("Useremail")
    contact=request.GET.get("usercontact")
    address=request.GET.get("useraddress")
    password1=request.GET.get("userpassword")
    datepicker=request.GET.get("datepicker")
    '''name = name.title()
    firstname = firstname.title()
    lastname = lastname.title()'''
    cur = connection.cursor()
    #query1= "insert into users (fk_group_id,user_name,first_name,last_name,email,phone_num,address,password,join_date) values ('"+str(catid)+"','"+str(name)+"','"+str(firstname)+"','"+str(lastname)+"' ,'"+str(email)+"','"+str(contact)+"','"+str(address)+"','"+str(password1)+"','"+str(datepicker)+"' )"
    query = "select * from public.insert_usergroup_datagroup_users("+group_id+",'"+str(user_id_list)+"','"+str(action)+"','"+label_text+"')"
    cur.execute(query)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def delete_data_user_group(request):
    #custid = request.GET.get("id")
    group_id= request.GET.get("group_id")
    selection_text = request.GET.get("selection_text")
    cur = connection.cursor()
    #query = "delete from usergroups_detail where category_id = '"+custid+"' and catname='"+usergroup+"'" 
    query1 = "Select * from user_data_app_rolegroup_deletefromusergroup('"+group_id+"','"+selection_text+"')"
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def delete_from_user(request):
    users_list = request.GET.get("user_str")
    print users_list
    cur = connection.cursor()
    query1 = "select * from public.users_deletefromuser('"+users_list+"')"
    print query1
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def validate_user_details(request):
    user_text=request.GET.get("user_text")
    label_text = request.GET.get("label_text")
    print user_text
    print label_text
    cur = connection.cursor()
    query1 = "select * from validate_user_details('"+user_text+"','"+label_text+"')"
    cur.execute(query1)
    row=cur.fetchone()
    print row
    print query1
    cur.close()
    return HttpResponse(row,content_type="text/html")

def validate_group_details(request):
    user_text=request.GET.get("user_text")
    label_text = request.GET.get("label_text")
    group_initials = request.GET.get("group_initials")
    print user_text
    print label_text
    print group_initials
    cur = connection.cursor()
    query1 = "select * from public.validate_UserGroup_AppData('"+user_text+"','"+label_text+"','"+group_initials+"')"
    cur.execute(query1)
    row=cur.fetchone()
    print row
    print query1
    cur.close()
    return HttpResponse(row,content_type="text/html")

def count_users(request):
    group_id=request.GET.get("group_id")
    selection_text = request.GET.get("selection_text")
    print group_id
    print selection_text
    cur = connection.cursor()
    query2 = "select * from public.user_data_App_rolegroup_count_users('"+group_id+"', '"+selection_text+"')"
    #query1 = "select * from public.userapprolegroup_count_users('"+group_id+"')"
    cur.execute(query2)
    print query2
    row=cur.fetchone()
    print row
    cur.close()
    return HttpResponse(row,content_type="text/html")

def count_data_users(request):
    group_id=request.GET.get("group_id")
    cur = connection.cursor()
    query1 = "select * from public.user_data_rolegroup_count_users('"+group_id+"')"
    cur.execute(query1)
    row=cur.fetchone()
    cur.close()
    return HttpResponse(row,content_type="text/html")

def edit_user_role_group(request):
    groupdetail=[]
    groupdetail1=[]
    groupname=request.GET.get("groupname")
    group_custid=request.GET.get("group_id")
    cur1 = connection.cursor()
    cur2 = connection.cursor()
    #query1= "select * from 'UserAppRoleGroup' where userrolegroup_name='"+groupname+"' and pk_userrolegroup_id='"+group_id+"' "
    query1 = "Select * from UserAppRoleGroup_UserGroupDetails('"+group_custid+"')"
    query2 = "select * from UserAppRoleGroup_GetAssociatedPage('"+group_custid+"')"
    cur1.execute(query1)
    cur2.execute(query2)
    print query2
    print query1
    x = cur2.fetchall()
    result= cur1.fetchall()
    for row in x:
        groupdetail1.append(row[0])

    for i in result:
        d=OrderedDict()
        d['userrolegroup_id']=i[0]
        d['userrolegroup_name']=i[1]
        d['description']=i[3]
        d['comments']=i[4]
        d['pages_list']=groupdetail1
        groupdetail.append(d)
    cur1.close()
    cur2.close()
    print groupdetail
    return HttpResponse(json.dumps(groupdetail),content_type="application/json")

def edit_data_user_group(request):
    groupdetail=[]
    groupdetail1=[]
    # groupdetail2=[]
    group_custid=request.GET.get("group_id")
    cur1 = connection.cursor()
    cur2 = connection.cursor()
    #query1= "select * from 'UserAppRoleGroup' where userrolegroup_name='"+groupname+"' and pk_userrolegroup_id='"+group_id+"' "
    query1 = "Select * from user_data_rolegroup_usergroupdetails('"+group_custid+"')"
    query2 = "select * from user_data_rolegroup_getassociateddataparamvalue('"+group_custid+"')"
    cur1.execute(query1)
    cur2.execute(query2)
    x = cur2.fetchall()
    result= cur1.fetchall()
    print result
    print x
    requireddata={}
    for row in x:
        key=row[2]
        if (row[2] in requireddata.keys()):
            
            requireddata[key].append(row[0])
        else:
            requireddata[key]=[]
            requireddata[key].append(row[0])
    print requireddata


    for i in result:
        d=OrderedDict()
        d['userrolegroup_id']=i[0]
        d['userrolegroup_name']=i[1]
        d['description']=i[3]
        d['comments']=i[4]
        d['values_list']=requireddata
        groupdetail.append(d)
    cur1.close()
    cur2.close()
    print groupdetail
    return HttpResponse(json.dumps(groupdetail),content_type="application/json")

def edit_single_user(request):
    groupdetail=[]
    print request
    groupdetail1=[]
    name=request.GET.get("editusername")
    firstname=request.GET.get("editfirstname")
    lastname=request.GET.get("editlastname")
    email=request.GET.get("editUseremail")
    user_alias=request.GET.get("edituseralias")
    #address=request.GET.get("useraddress")
    password1=request.GET.get("edituserpassword")
    created_date=request.GET.get("editdatepicker")
    user_flag=request.GET.get("edituserstatus")
    label_text=request.GET.get("label_text")
    user_id=request.GET.get("user_id")
    updated_by = request.session['username']
    # updated_by='System'

    if user_flag == 'on':
        print user_flag
        user_flag = True
    elif user_flag == 'off':
        print user_flag
        user_flag = False
        print user_flag
    print user_id

    if not lastname:
        lastname = ''
    elif lastname:
        lastname= lastname

    cur1 = connection.cursor()

    cur2 = connection.cursor()
    #query1= "select * from 'UserAppRoleGroup' where userrolegroup_name='"+groupname+"' and pk_userrolegroup_id='"+group_id+"' "
    #'"+email+"',
    query1 = "Select * from userapprolegroup_editblockreset("+user_id+",'"+firstname+"','"+lastname+"','"+user_alias+"','"+updated_by+"',"+str(user_flag)+",'"+password1+"','"+label_text+"')"
    #query2 = "select * from UserAppRoleGroup_GetAssociatedPage('"+group_custid+"')"
    print query1
    cur1.execute(query1)
    cur1.close()
    return HttpResponse(1,content_type="application/json")

def getRegionHierarchyData(request):
    query='SELECT * FROM "Hierarchy"'
    cur = connection.cursor()
    cur.execute(query)
    result = cur.fetchall()
    hierarchy_array=[]
    for row in result:
        d = OrderedDict()
        d['clientId'] = row[0]
        d['id'] = row[0]
        d['value'] = row[4]
        d['parentClientId'] = row[1]
        d['clientName'] = row[2]
        d['level'] = row[3]
        d['title'] = row[2]
        hierarchy_array.append(d)
    cur.close()
    return HttpResponse(json.dumps(hierarchy_array), content_type="application/json")

def list_all_users(request):
    companyvalue = []
    cur = connection.cursor()
    query1= "select * from public.users_listusers()"
    cur.execute(query1)
    result = cur.fetchall()
    for i in result:
        print i
        d = OrderedDict()
        d['username']=i[1]
        if i[2]:
            d['firstname'] = i[3]
        else:
            d['firstname']= ''
        d['email_address']=i[6]
        d['created_date']=i[8].isoformat()
        d['user_alias']=i[5]
        d['user_id']=i[0]
        d['flag_active']=i[11]
        companyvalue.append(d)
    cur.close()
    return HttpResponse(json.dumps(companyvalue), content_type='application/json')

	
def resetUserPassword(request):
	data=[]
	currentpassword=request.GET.get("currentpassword")
	password=request.GET.get("newuserpassword")
	user_id=request.GET.get("user_id")
	updated_by='System'
	#connection = pyodbc.connect('DRIVER={SQL Server};SERVER=13.94.252.185;PORT=1433;DATABASE=Henkel_SC_UserModule;UID=sa;PWD=Welcome@123')
	cursor = connection.cursor() #using pyodbc cursor
	#query1 = "exec [USP_Admin_UserAppRoleGroup_EditBlockReset] @userID ='"+user_id+"',@firstname ='"+firstname+"',@lastname ='"+lastname+"',@user_alias ='"+user_alias+"',@updated_by ='"+updated_by+"',@created_by ='"+updated_by+"',@created_date ='"+created_date+"',@flag_active = '"+str(user_flag)+"',@password='"+str(password1)+"',@Edit_Block_Reset='EDIT'"
	#query = "exec [USP_Admin_User_ChangePassword] @userID ='"+user_id+"',@old_password='"+str(currentpassword)+"',@new_password='"+str(password)+"'"
	query = "select * from USP_Admin_User_ChangePassword("+user_id+",'"+currentpassword+"','"+password+"')"
	cursor.execute(query)
	finalresult=cursor.fetchall()
	#cursor.commit()
	for i in finalresult:
		d=OrderedDict()
		d['flag']=i[0]
		data.append(d)
	return HttpResponse(json.dumps(data),content_type="application/json")