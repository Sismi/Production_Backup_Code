from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
import psycopg2
import json
import requests, time
import pymongo 
from pymongo import MongoClient
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext, loader
from collections import OrderedDict
import datetime
#from django.core.urlresolvers import reverse
from threading import Timer
#import qualitypulse.pages_display
from qualitypulse.views import pages_display
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from django.contrib import messages
import cgi, os, urllib
import hashlib,ast
from Crypto.Cipher import AES
from datetime import timedelta
import cgitb; cgitb.enable()

def connection_licensing():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.rule_data_access	
	return table_cursor
	
def connection_src_processorders():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.src_processorders	
	return table_cursor

def connection_reporting_table():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_fng_qualscore_qualitysegment	
	return table_cursor
	
def uploadExclusionFile(request):
    filename=''
    if request.method == 'POST':
        form = cgi.FieldStorage()
        fileitem = request.FILES['uploadexcfile']
        filename = fileitem.name
        path = default_storage.save('fuploads/'+filename, ContentFile(fileitem.read()))
    return HttpResponse(filename, content_type="application/json")
	
# Create your views here.
@csrf_exempt
def login_user(request):
	username = request.POST.get("username")
	password = request.POST.get("password")
	data = request.POST.copy
	parameterdict ={}
	for item in request.POST.iterlists():
		parameterdict[item[0]]=item[1]
	#Non-MES
	#parameterdict['mes_username']= '3o6MnQGHusT7M+4s5KZLMg=='
	#parameterdict = {u'mes_username': [u'1eIGhsrKQfjStxPNmIqX3g==']}
	if 'password' in parameterdict.keys() and 'username' in parameterdict.keys():
		print 'into login authentication'
		if request.method == 'POST' and (username != '' or password != ''):
			data = request.POST.copy
			cur = connection.cursor()
			query1 = "select * from public.usp_userauthentication('"+username+"','"+password+"')"
			cur.execute(query1)
			result = cur.fetchone()
			cur.close()
			#request.session.set_expiry(3600)
			try:
				if(result[0] == 1):
				   # s = request.session()
					request.session['username'] = username
					request.session['user_id'] = result[1]
					user_alias = result[3]
					# request.session['user_alias'] = result[3]
					if (user_alias == ''):
						request.session['user_alias'] = username
					else:
						request.session['user_alias'] = user_alias
					display_name = username
					user_id = result[1]
					query2 = "select * from user_getassociatedapppages("+str(user_id)+")"
					cur1 = connection.cursor()
					cur1.execute(query2)
					result1 = cur1.fetchall()
					if len(result1) == 0:
						print 'len pages check'
						#if 'user_id' in request.session:
						#	del request.session['user_id']
						return render(request, "login_page.html",{'groups_addition': True })
					
					finished_goods_list = []
					raw_materials_list = []
					administrator_list = []
					for i in result1:
						d = OrderedDict()
						if i[4] == 'Finished Goods':
							finished_goods_list.append(i)
						if i[4] == 'Raw Materials':
							raw_materials_list.append(i)
						if i[4] == 'Administrator':
							administrator_list.append(i)
					pagedatarray=OrderedDict()
					firstpagetoshow=''
					if(len(finished_goods_list)>0):
						pagedatarray['Finished Good']=finished_goods_list
						firstpagetoshow=finished_goods_list[0][3]
					if(len(raw_materials_list)>0):
						pagedatarray['Raw Materials']=raw_materials_list
						if(firstpagetoshow==''):
							firstpagetoshow=raw_materials_list[0][3]      
					if(len(administrator_list)>0):
						pagedatarray['Administrator']=administrator_list 
					  
					return HttpResponseRedirect(firstpagetoshow)
				else:
					#print '111'
					#if 'user_id' in request.session:
					#	del request.session['user_id']
					return render(request, "login_page.html",{'invalid': True })				
			except:
				print '222'
				#if 'user_id' in request.session:
				#	del request.session['user_id']
				return render(request, "login_page.html",{'invalid': True })
		else:
			print '333'
			#if 'user_id' in request.session:
			#	del request.session['user_id']
			return render(request, "login_page.html",{'invalid': True })
				
	if 'mes_username' in parameterdict.keys():
		mes_username = parameterdict['mes_username']
		if mes_username != '':
			username_url = request.POST.get('mes_username')
			decryption_key = '67468738'
			print 'below is username_url'
			print username_url
			if '%' in username_url:
				username=urllib.unquote(username_url).decode('utf8')
			else:
				username = username_url
			
			print username
			original_username = AESdecrypt(decryption_key, username)
			encrpytion_key = "71079162"
			username_session = original_username
			encrpyted_username = AESencrypt(encrpytion_key, username_session)
			print encrpyted_username
			encoded_username_url = urllib.quote(encrpyted_username, safe='')
			print encoded_username_url
			app = '20263146'
			url = "http://ap-hipas.henkelgroup.net/iPAS_Service/3AppService.svc/GetMESUserBasicProfile?3app="+app+"&mes_username="+encoded_username_url
			res=requests.get(url)
			user_basic_info = res.content
			'''user_basic_info = {
			  "GetMESUserBasicProfileResult": {
				"FirstName": "Test",
				"LastName": "Admin",
				"UserName": "45lim",
				"UserPLineInfo": {
				  "FullAccess": "Y",
				  "PlineList": []
				},
				"UserSiteList": [
				  {
					"ID": 1,
					"Name": "Human Plant",
				  },
				  {
				  "ID": 2,
					"Name": "Dragon Plant",
				}
				]
			  }
			}'''
			
			x = json.loads(user_basic_info)
			#request.session.set_expiry(3600)
			print 'below is x'
			print x
			final_data = ast.literal_eval(x['GetMESUserBasicProfileResult'])
			final_data = json.loads(x['GetMESUserBasicProfileResult'])
			if x != '':
				if 'UserName' in final_data:
					actual_username = final_data['UserName']
				else:
					return render(request, "login_page.html", {'invalid': False })
					
				if 'EmailID' in final_data:
					email_address = final_data['EmailID']
				else:
					email_address = final_data['FirstName']+'.'+final_data['LastName']+'@henkel.com'
					#return render(request, "login_page.html", {'invalid': False })
				
				if 'UserPLineInfo' in final_data:
					actual_line_list = final_data['UserPLineInfo']
				else:
					return render(request, "login_page.html", {'invalid': False })
				
				if 'UserSiteList' in final_data:
					actual_plant_list = final_data['UserSiteList']
				else:
					return render(request, "login_page.html", {'invalid': False })
					
				plant_id = ''
				line_id = ''
				if 'FullAccess' in actual_plant_list:
					plant_id = 'Y'
				else:
					for item in actual_plant_list:
						plant_id += str(item['ID']) + ','
					plant_id = plant_id[:-1]
				
				if 'FullAccess' in actual_line_list:
					line_id = 'Y'
				else:
					for item in actual_line_list:
						line_id += str(item['ID']) + ','
					line_id = line_id[:-1]
				
				cur = connection.cursor()
				query1="Select * from mes_user_data_role_sync('"+actual_username+"','"+plant_id+"','"+line_id+"','"+email_address+"')"
				cur.execute(query1)
				result= cur.fetchone()
				if result[0] == 1:
					request.session['username'] = original_username
					request.session['user_id'] = result[1]
					user_alias = result[3]
					if (user_alias == ''):
						request.session['user_alias'] = username
					else:
						request.session['user_alias'] = user_alias
					display_name = username
					user_id = result[1]
					query2 = "select * from user_getassociatedapppages("+str(user_id)+")"
					cur1 = connection.cursor()
					cur1.execute(query2)
					result1 = cur1.fetchall()
					finished_goods_list = []
					raw_materials_list = []
					administrator_list = []
					for i in result1:
						d = OrderedDict()
						if i[4] == 'Finished Goods':
							finished_goods_list.append(i)
						if i[4] == 'Raw Materials':
							raw_materials_list.append(i)
						if i[4] == 'Administrator':
							administrator_list.append(i)
					pagedatarray=OrderedDict()
					firstpagetoshow=''
					if(len(finished_goods_list)>0):
						pagedatarray['Finished Good']=finished_goods_list
						firstpagetoshow=finished_goods_list[0][3]
					if(len(raw_materials_list)>0):
						pagedatarray['Raw Materials']=raw_materials_list
						if(firstpagetoshow==''):
							firstpagetoshow=raw_materials_list[0][3]      
					if(len(administrator_list)>0):
						pagedatarray['Administrator']=administrator_list 
					  
					return HttpResponseRedirect(firstpagetoshow)
			else:
				print '666'
				return render(request, "login_page.html", {'invalid': False })
		else:
			print '555'
			return render(request, "login_page.html", {'invalid': False })
	else:
		print 'inside else'
		if 'user_id' in request.session:
			return HttpResponseRedirect(reverse('index'))
		else:
			return render(request, "login_page.html",{'invalid': False })
		
def AESdecrypt(password, ciphertext, base64=True):
    DERIVATION_ROUNDS=1337
    BLOCK_SIZE = 16
    KEY_SIZE = 32
    MODE = AES.MODE_CBC
     
    if base64:
        import base64
        decodedCiphertext = base64.b64decode(ciphertext)
    else:
        decodedCiphertext = ciphertext.decode("hex")
    derivedKey = password
    derivedKey = hashlib.sha256(derivedKey).digest()
    derivedKey = derivedKey[:KEY_SIZE]
    startIv = derivedKey[:BLOCK_SIZE]
    cipherSpec = AES.new(derivedKey, MODE, startIv)
    plaintextWithPadding = cipherSpec.decrypt(decodedCiphertext)
    paddingLength = ord(plaintextWithPadding[-1])
    plaintext = plaintextWithPadding[:-paddingLength]
    return plaintext
	
# for encrpytion
def AESencrypt(password, plaintext, base64=True):
    SALT_LENGTH = 32
    DERIVATION_ROUNDS=1337
    BLOCK_SIZE = 16
    KEY_SIZE = 32
    MODE = AES.MODE_CBC
     
    paddingLength = 16 - (len(plaintext) % 16)
    paddedPlaintext = plaintext+chr(paddingLength)*paddingLength
    derivedKey = password
    derivedKey = hashlib.sha256(derivedKey).digest()
    derivedKey = derivedKey[:KEY_SIZE]
    iv = derivedKey[:BLOCK_SIZE]
    cipherSpec = AES.new(derivedKey, MODE, iv)
    ciphertext = cipherSpec.encrypt(paddedPlaintext)
    if base64:
        import base64
        return base64.b64encode(ciphertext)
    else:
        return ciphertext.encode("hex")

		
		
def logout(request):
	del request.session['user_id']
	return HttpResponseRedirect("/user_mod/login/")

#---------------------------------connection function--------------------------------------#
def connection_mongo():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_fng_qualscore_qualitysegment
	return table_cursor

def admin(request):
    if 'user_id'  in request.session:
        pagedatarray = pages_display(request)
        template = loader.get_template('admin.html')
        result_cursor=connection_mongo().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
        for r in result_cursor:
            syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
        context = RequestContext(request, {'appuser':request.session['user_alias'],'appuserid':request.session['user_id'], "pages_dict": pagedatarray,'syncdate':'' })
        return HttpResponse(template.render(context), content_type="text/html")
    else:
        return HttpResponseRedirect("/user_mod/login/")

def fetch_select2_values(request):
    list_sitename=[]
    list_sbu=[]
    list_siteid=[]
    list_technology=[]
    list_linename=[]
    list_fgproducts=[]
    cur1 = connection.cursor()
    cur2 = connection.cursor()
    cur3 = connection.cursor()
    cur4 = connection.cursor()
    cur5 = connection.cursor()
    cur6 = connection.cursor()
    query1 = "select * from public.param_name_value('SiteName')"
    query2 = "select * from public.param_name_value('SBU')"
    query3 = "select * from public.param_name_value('Resources')"
    query4 = "select * from public.param_name_value('Technology')"
    query5 = "select * from public.param_name_value('LineName')"
    query6 = "select * from public.param_name_value('FGProducts')"
    cur1.execute(query1)
    cur2.execute(query2)
    cur3.execute(query3)
    cur4.execute(query4)
    cur5.execute(query5)
    cur6.execute(query6)
    sitename_list = cur1.fetchall()
    sbu_list = cur2.fetchall()
    siteid_list = cur3.fetchall()
    technology_list = cur4.fetchall()
    linename_list = cur5.fetchall()
    fgproducts_list = cur6.fetchall()
    list_sitename.append(sitename_list)
    list_sbu.append(sbu_list)
    list_siteid.append(siteid_list)
    list_technology.append(technology_list)
    list_linename.append(linename_list)
    list_fgproducts.append(fgproducts_list)
    cur1.close()
    cur2.close()
    cur3.close()
    cur4.close()
    cur5.close()
    cur6.close()
    pagedata={'SiteName':sitename_list,'SBU':sbu_list,'Resources':siteid_list,'Technology':technology_list,'LineName':linename_list,'FGProducts':fgproducts_list}
    return HttpResponse(json.dumps(pagedata), content_type="application/json")
	
def licenses_select2_values(request):
	site_list = connection_src_processorders().aggregate([{'$group':{'_id':{'SiteName':'$SiteName','SiteID':'$SiteID'}}},{'$project':{'_id':0,'SiteName':'$_id.SiteName','SiteID':'$_id.SiteID'}}])
	tech_list = connection_src_processorders().aggregate([{'$group':{'_id':{'Technology':'$Technology','TechnologyID':'$Technology'}}},{'$project':{'_id':0,'Technology':'$_id.Technology','TechnologyID':'$_id.TechnologyID'}}])
	sitename_list=[]
	technology_list=[]
	for site in site_list:
		sitename_list.append(site)
	
	for tech in tech_list:
		technology_list.append(tech)
		
	pagedata={'SiteName':sitename_list,'Technology':technology_list}
	return HttpResponse(json.dumps(pagedata), content_type="application/json")
	
def check_procurement_licenses(request):		
	app_url = request.GET.get('page_url')	
	cur = connection.cursor()		
	query1 = "SELECT * FROM return_license_count('"+app_url+"');"    		
	cur.execute(query1)		
	max_license_limit = cur.fetchone()
	#max_license_limit = cur.execute(query1)
	#query2 = connection1().distinct('SiteName',{'POCount':{'$gte':30}})		
	license_limit_reached = connection_licensing().count({'Action':'Granted'})	
	#license_limit_reached = 1
	if license_limit_reached >= max_license_limit[0]:		
		result = 'Error'		
	else:		
		result = 'Allow'	

	return HttpResponse(json.dumps(result),content_type="application/json")

@csrf_exempt	
def get_syncdate(request):
	syncdate=''
	result_cursor_datedata=connection_reporting_table().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])

	for r in result_cursor_datedata:
		max_date = r['maxdate']
		min_date = r['mindate']
		sync_date = r['syncdate']
	d = max_date - timedelta(days=365)
	x = d.replace(day=1)
	end_date = x.date()
	min_date = min_date.date()
	result_cursor=connection_reporting_table().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
	for r in result_cursor:
		syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
	pagedata={'syncdate':syncdate,'min_date':min_date,'end_date':end_date,'sync_date':sync_date}
	return HttpResponse(syncdate,content_type="application/json")

def user_group(request):
	companyvalue=[]
	cur = connection.cursor()
	query1='Select * from UserAppRoleGroup_ListUserGroups()'
	cur.execute(query1)
	result= cur.fetchall()
	for i in result:
		d=OrderedDict()
		d['label']=i[0]
		d['value']=i[1]
		companyvalue.append(d)
	cur.close()
	pagedata={'collect':companyvalue}
	return HttpResponse(json.dumps(pagedata),content_type="application/json")

def data_user_group(request):
    companyvalue=[]
    cur = connection.cursor()
    query1='Select * from user_data_rolegroup_listusergroups()'
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def user_list(request):
    companyvalue=[]
    cur = connection.cursor()
    query1="select * from public.users_listusers()"
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def absent_user_list(request):
    companyvalue=[]
    groupId=request.GET.get('group_id')
    label_text=request.GET.get('label_text')
    cur = connection.cursor()
    query1="Select * from public.user_data_APP_rolegroup_listusernotgroup("+groupId+",'"+label_text+"')"
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['username']=i[1]
        d['firstname']=i[2]
        d['email_address']=i[5]
        d['user_alias']=i[4]
        d['created_date']=i[7].isoformat()
        d['user_id']=i[0]
        companyvalue.append(d)
    cur.close()
    #pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(companyvalue),content_type="application/json")

def pages_list(request):
    companyvalue=[]
    cur = connection.cursor()
    query1='select pk_page_id, page_name from "AppPages"'
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def GetUsersList_SameGroup(request):
    GroupId=request.POST.get('group_id')
    Data=[]
    cursor = Connection.cursor()
    query="Select pk_user_id ,username from Users where flag_active=1 and fk_userrolegroup_id='"+GroupId+"'"
    cursor.execute(query)
    result=cursor.fetchall()
    for row in result:
        DataDict={}
        DataDict['GroupId']=row[0]
        DataDict['GroupName']=row[1]
        Data.append(DataDict)
    return HttpResponse(json.dumps(Data), content_type="application/json")  

def user_list_selected(request):
    companyvalue=[]
    userrolegroup_id=request.GET.get("group_id")
    cur = connection.cursor()
    query1= "select * from public.userapprolegroup_listusers('"+userrolegroup_id+"')"
    cur.execute(query1)
    result= cur.fetchall()
    for i in result:
        d=OrderedDict()
        d['label']=i[0]
        d['value']=i[1]
        d['password']=i[2]
        d['firstname']=i[3]
        d['email_address']=i[4]
        d['created_date']=i[5]
        d['flag_active']=i[6]
        d['fk_userrolegroup_id']=i[7]
        d['check']='<input type="checkbox" />'
        companyvalue.append(d)
    cur.close()
    pagedata={'collect':companyvalue}
    return HttpResponse(json.dumps(pagedata),content_type="application/json")

def list_related_users(request):
	companyvalue = []
	userrolegroup_id = request.GET.get("group_id")
	cur = connection.cursor()
	query1= "select * from public.userapprolegroup_listusers('"+userrolegroup_id+"')"
	cur.execute(query1)

	result = cur.fetchall()
	for i in result:
		d = OrderedDict()
		d['username']=i[1]
		if i[2]:
			d['firstname'] = i[2]
		else:
			d['firstname']= ''
		d['email_address']=i[6]
		d['created_date']=i[8].isoformat()
		d['user_alias']=i[5]
		d['user_id']=i[0]
		companyvalue.append(d)
	cur.close()
	return HttpResponse(json.dumps(companyvalue), content_type='application/json')

def list_all_licenses(request):
	param = request.GET.get('param')
	companyvalue = []
	url = "mongodb://henkelUser_Prod:QpQc#123@localhost:27017/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
	#table_cursor=conn.henkelProduction.rule_data_access
	db = conn.henkelProduction
	#result_cursor = table_cursor.find({'ActiveFlag':1},{'_id':0})
	#result_cursor = table_cursor.find({'Upload File':{'$exists':True,'$ne':''}})		
	#result_cursor = table_cursor.find({'Upload File':{'$exists':True,'$ne':''},'SiteName':{'$exists':True,'$ne':''}})		
	#result_cursor = connection_licensing().find({'Upload File':{'$exists':True,'$ne':''},'SiteID':{'$exists':True,'$ne':''},'TechnologyID':{'$exists':True,'$ne':''}})
	
	result_cursor = connection_licensing().find({'ActiveFlag':1,'Action':'Granted'})
		
	for row in result_cursor:
		d = OrderedDict()
		d['SiteName']=row['SiteName']
		d['Technology']=row['Technology']
		d['Approved By']=row['ApprovedBy']		
		d['UploadFilePath']=row['UploadFilePath']		
		d['Approved Date']=row['ApprovedTimeStamp']		
		#d['Approved Date']=row['Approved Date']		
		d['Created By']=row['CreatedBy']		
		d['CreatedTimeStamp']=row['CreatedTimeStamp'].isoformat()		
		d['SiteID']=row['SiteID']	
		d['Updated Date']=row['UpdatedDateTime'].isoformat()		
		d['Updated By']=row['UpdatedBy']		
		d['TechnologyID']=row['TechnologyID']		
		d['Description']=row['Description']		
		#d['SiteName']=row['SiteName']	
		companyvalue.append(d)		
	# cur.close()	
	return HttpResponse(json.dumps(companyvalue), content_type='application/json')

	
def revoke_license(request):
    license_val = request.GET.get('license_str')
    each_val = license_val.split('&')
    each_val_individual = []
    for i in each_val:
        each_val_temp = i.split('=')
        each_val_individual.append(each_val_temp)
    for item in each_val_individual:
        name = item[0]
        value = item[1]

        if name == 'SiteID':
            site_id = value
        elif name == 'TechnologyID':
            tech_id = value
        elif name == 'SiteName':
            site_name = value
        elif name == 'TechnologyName':
            tech_name = value
        elif name == 'CreatedBy':
            created_by = value
        elif name == 'UpdatedBy':
            updated_by = value
        elif name == 'UploadFilePath':
            file_path = value
        elif name == 'ApprovedBy':
            approvedby = value
        elif name == 'Description':
            descriptionlicense = value
        elif name == 'CreatedDate':
            createddate = value
        elif name == 'UpdatedDate':
            updateddate = value
        elif name == 'ApprovedDate':
            approveddate = value

    # created_by = request.session['username']
    created_by = request.session['username']
    Action_new = 'Revoked'
    url = "mongodb://henkelUser:QpQc#123@localhost:27017/admin?authMechanism=SCRAM-SHA-1"
    conn = MongoClient(url)
    table_cursor=conn.henkelProduction.rule_data_access
    db = conn.henkelProduction
    x = db.system_js.license_insert_update_action(site_id,site_name,tech_id,tech_name,descriptionlicense,created_by,approvedby,approveddate,created_by,file_path,Action_new)
    msg = 'The license was uploaded successfully.'
    return HttpResponse(license_val,content_type="application/json")
	
	
def list_related_datausers(request):
    companyvalue = []
    userrolegroup_id = request.GET.get("group_id")
    cur = connection.cursor()
    query1= "select * from user_data_rolegroup_listusers('"+userrolegroup_id+"')"
    cur.execute(query1)
    #cur.execute(query1)
    result = cur.fetchall()
    for i in result:
        d = OrderedDict()
        d['username']=i[1]
        if i[2]:
            d['firstname'] = i[2]
        else:
            d['firstname']= ''
        d['email_address']=i[6]
        d['created_date']=i[8].isoformat()
        d['user_alias']=i[5]
        d['user_id']=i[0]
        companyvalue.append(d)
    cur.close()
    return HttpResponse(json.dumps(companyvalue), content_type='application/json')

def user_description(request):
	user = request.user
	username = user.username
	userdetail=[]
	user_id=request.GET.get("user_id")
	cur = connection.cursor()
	query1 = "select * from public.users_users_details('"+user_id+"');"
	cur.execute(query1)
	result= cur.fetchall()
	for i in result:
		d=OrderedDict()
		d['username']=i[1]
		d['firstname']=i[3]
		d['email']=i[6]
		d['datepicker']=i[8].isoformat()
		d['user_alias']=i[5]
		d['lastname']=i[4]        
		d['flag_active']=i[11]
		d['contact']=i[5]
		d['address']=i[7]
		d['created_by']=i[7]
		d['status']=i[12]
		d['password']=i[2]
		
		userdetail.append(d)
	cur.close()
	return HttpResponse(json.dumps(userdetail),content_type="application/json")

def new_user_role_group(request):
    #groupname1=request.GET.get("idkey")
    name=request.GET.get("name")
    description=request.GET.get("description")
    comments=request.GET.get("comments")
    pages_list=request.GET.get("pages_list")
    name = name.title()
    cur = connection.cursor()
    query1 = "select * from public.insert_usergroup_apppages('"+pages_list+"','"+name+"','"+description+"','"+comments+"')"
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def new_data_user_group(request):
    name=request.GET.get("name")
    description=request.GET.get("description")
    comments=request.GET.get("comments")
    values_list=request.GET.get("values_list")
    name = name.title()
    cur = connection.cursor()
    query1 = "select * from insert_user_data_group_dataparam('"+values_list+"','"+name+"','"+description+"','"+comments+"')"
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

#mongodb
@csrf_exempt
def new_license_addition1(request):
    try:
        approvedby=request.POST.get("approved-by")
        approveddate=request.POST.get("approved-date")
        descriptionlicense=request.POST.get("description-license")
        plant_list=request.POST.getlist("plant-list")
        technology_list=request.POST.getlist("technology-list")
        url = "mongodb://henkelUser2:QpQc#123@localhost/admin?authMechanism=SCRAM-SHA-1"
	conn = MongoClient(url)
        table_cursor=conn.henkel.test_licensing
        one_dict = {}
        one_dict['Approved Date']=approveddate
        one_dict['ActiveFlag']=1
        one_dict['Approved By']=approvedby
        one_dict['Description']=descriptionlicense
        one_dict['SiteID']=plant_list
        one_dict['Technology']=technology_list
        one_dict['Upload File']=technology_list

        table_cursor.insert(one_dict)
        return HttpResponse(1,content_type="application/json")
    except Exception as e:
        print e
		
@csrf_exempt
def new_license_addition(request):
    try:
        approvedby=request.POST.get("approved-by")
        approveddate=request.POST.get("approved-date")
        descriptionlicense=request.POST.get("description-license")
        plant_list=request.POST.getlist("plant-list")
        technology_list=request.POST.getlist("technology-list")

        if request.FILES.get('file-upload'):
            file_name = request.FILES['file-upload'].name
            timestamp = int(time.time())
            folder_path_with_slash = '%s/%s/' % (
                settings.UPLOAD_DIR, timestamp
            )
            # create the folder to save the attachment file if it doesn't
            # exists then change its permission to 777
            if not os.path.exists(folder_path_with_slash):
                os.mkdir(folder_path_with_slash, 0777)
                os.chmod(folder_path_with_slash, 0777)

            # opening the file to write the attachment
            file_path = '%s%s' % (folder_path_with_slash, file_name)
            destination = open(file_path, 'wb+')
            # writing the file to specified path
            for chunk in request.FILES['file-upload'].chunks():
                destination.write(chunk)

            # closing the file
            destination.close()

        url = "mongodb://henkelUser:QpQc#123@localhost:27017/admin?authMechanism=SCRAM-SHA-1"
        conn = MongoClient(url)
        # table_cursor=conn.henkel_ver2.report_fng_qualscore_qualitysegment
        # cur = connection.cursor()
        # query1='Select * from user_data_rolegroup_listusergroups()'
        table_cursor=conn.henkelProduction.rule_data_access
        # result_cursor = table_cursor.find({'ActiveFlag':1})

        db = conn.henkelProduction
        # table_cursor=conn.henkel.test_licensing
        # table_cursor=conn.henkel.rule_data_access
        # add created_by, Action(Granted for insert/Revoked for remove), created timestamp, 
        # remove Active flag
        one_dict = {}
        site_id = ''
        site_name = ''
        tech_id = ''
        tech_name = ''
        plant_item = ''
        for item in plant_list:
            site_id = item.split('_')[0]
            site_name = item.split('_')[1]

        for item in technology_list:
            tech_id = item.split('_')[0]
            tech_name = item.split('_')[1]

        one_dict['Approved Date']=approveddate
        # one_dict['ActiveFlag']=1
        one_dict['Approved By']=approvedby
        one_dict['Description']=descriptionlicense
        one_dict['SiteID']=site_id
        one_dict['SiteName']=site_name
        one_dict['TechnologyID']=tech_id
        one_dict['TechnologyName']= tech_name
        one_dict['Upload File']=file_path
        one_dict['Created By'] = request.session['username']
        created_by = request.session['username']
        Action_new = 'Granted'
        # table_cursor.insert_one(one_dict)
        # db.system_js.license_insert_update_action(Site_ID,Site_Name,Technology_ID,Technology_Name,Desc,Created_By,Approved_By,Approved_Time,Updated_By,UploadFilePath,Action_new)
        x = db.system_js.license_insert_update_action(site_id,site_name,tech_id,tech_name,descriptionlicense,created_by,approvedby,approveddate,created_by,file_path,Action_new)
        msg = 'The license was uploaded successfully.'
        messages.add_message(request, messages.INFO, msg)
        return HttpResponse(1,content_type="application/json")
    except Exception as e:
        print e
        return HttpResponseRedirect(e,content_type="application/json")

		
def __unicode__(self):
    return self.user.username

def new_user_addition(request):
	name=request.GET.get("editusername")
	firstname=request.GET.get("editfirstname")
	lastname=request.GET.get("editlastname")
	email=request.GET.get("editUseremail")
	user_alias=request.GET.get("edituseralias")
	#address=request.GET.get("useraddress")
	password1=request.GET.get("edituserpassword")
	created_date=request.GET.get("editdatepicker")
	user_flag=request.GET.get("edituserstatus")
	mes_flag=request.GET.get("editmesstatus")
	mes_status = 'false'
	
	if mes_flag == 'on':
		mes_flag = 'true'
		
	elif mes_flag == 'off':
		mes_flag = 'false'
		
	created_by = request.session['username']
	# created_by = 'System'
	cur = connection.cursor()
	query1 = "select * from public.insert_user('"+name+"','"+password1+"','"+firstname+"','"+lastname+"','"+user_alias+"','"+email+"','"+created_by+"','"+created_date+"','"+mes_flag+"');"
	cur.execute(query1)
	cur.close()
	return HttpResponse(1,content_type="application/json")
	
def mes_pulse_user_creation(request):
    name=request.GET.get("username")
    firstname=request.GET.get("firstname")
    lastname=request.GET.get("lastname")
    email=request.GET.get("email")
    created_date=request.GET.get("created_date")
    plant_list=request.GET.get("site_access")
    line_list=request.GET.get("line_access")
    cur = connection.cursor()
    query1 = "Select * from insert_mes_user('"+name+"','"+firstname+"','"+lastname+"','"+email+"','"+created_date+"','"+plant_list+"','"+line_list+"');"
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def new_user_absent_list(request):
    #form = cgi.FieldStorage()
    group_id = request.GET.get("group_id")
    user_id_list = request.GET.get("user_str")
    action = request.GET.get("action")
    label_text=request.GET.get("label_text")
    catid=request.GET.get("catid")
    name=request.GET.get("username")
    firstname=request.GET.get("firstname")
    lastname=request.GET.get("lastname")
    email=request.GET.get("Useremail")
    contact=request.GET.get("usercontact")
    address=request.GET.get("useraddress")
    password1=request.GET.get("userpassword")
    datepicker=request.GET.get("datepicker")
    '''name = name.title()
    firstname = firstname.title()
    lastname = lastname.title()'''
    cur = connection.cursor()
    #query1= "insert into users (fk_group_id,user_name,first_name,last_name,email,phone_num,address,password,join_date) values ('"+str(catid)+"','"+str(name)+"','"+str(firstname)+"','"+str(lastname)+"' ,'"+str(email)+"','"+str(contact)+"','"+str(address)+"','"+str(password1)+"','"+str(datepicker)+"' )"
    query = "select * from public.insert_usergroup_datagroup_users("+group_id+",'"+str(user_id_list)+"','"+str(action)+"','"+label_text+"')"
    cur.execute(query)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def delete_data_user_group(request):
    #custid = request.GET.get("id")
    group_id= request.GET.get("group_id")
    selection_text = request.GET.get("selection_text")
    cur = connection.cursor()
    #query = "delete from usergroups_detail where category_id = '"+custid+"' and catname='"+usergroup+"'" 
    query1 = "Select * from user_data_app_rolegroup_deletefromusergroup('"+group_id+"','"+selection_text+"')"
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def delete_from_user(request):
    users_list = request.GET.get("user_str")
    cur = connection.cursor()
    query1 = "select * from public.users_deletefromuser('"+users_list+"')"
    cur.execute(query1)
    cur.close()
    return HttpResponse(1,content_type="application/json")

def validate_user_details(request):
    user_text=request.GET.get("user_text")
    label_text = request.GET.get("label_text")
    cur = connection.cursor()
    query1 = "select * from validate_user_details('"+user_text+"','"+label_text+"')"
    cur.execute(query1)
    row=cur.fetchone()
    cur.close()
    return HttpResponse(row,content_type="text/html")

def validate_group_details(request):
    user_text=request.GET.get("user_text")
    label_text = request.GET.get("label_text")
    group_initials = request.GET.get("group_initials")
    cur = connection.cursor()
    query1 = "select * from public.validate_UserGroup_AppData('"+user_text+"','"+label_text+"','"+group_initials+"')"
    cur.execute(query1)
    row=cur.fetchone()
    cur.close()
    return HttpResponse(row,content_type="text/html")

def count_users(request):
    group_id=request.GET.get("group_id")
    selection_text = request.GET.get("selection_text")
    cur = connection.cursor()
    query2 = "select * from public.user_data_App_rolegroup_count_users('"+group_id+"', '"+selection_text+"')"
    #query1 = "select * from public.userapprolegroup_count_users('"+group_id+"')"
    cur.execute(query2)
    row=cur.fetchone()
    cur.close()
    return HttpResponse(row,content_type="text/html")

def count_data_users(request):
    group_id=request.GET.get("group_id")
    cur = connection.cursor()
    query1 = "select * from public.user_data_rolegroup_count_users('"+group_id+"')"
    cur.execute(query1)
    row=cur.fetchone()
    cur.close()
    return HttpResponse(row,content_type="text/html")

def edit_user_role_group(request):
    groupdetail=[]
    groupdetail1=[]
    groupname=request.GET.get("groupname")
    group_custid=request.GET.get("group_id")
    cur1 = connection.cursor()
    cur2 = connection.cursor()
    #query1= "select * from 'UserAppRoleGroup' where userrolegroup_name='"+groupname+"' and pk_userrolegroup_id='"+group_id+"' "
    query1 = "Select * from UserAppRoleGroup_UserGroupDetails('"+group_custid+"')"
    query2 = "select * from UserAppRoleGroup_GetAssociatedPage('"+group_custid+"')"
    cur1.execute(query1)
    cur2.execute(query2)
    x = cur2.fetchall()
    result= cur1.fetchall()
    for row in x:
        groupdetail1.append(row[0])

    for i in result:
        d=OrderedDict()
        d['userrolegroup_id']=i[0]
        d['userrolegroup_name']=i[1]
        d['description']=i[3]
        d['comments']=i[4]
        d['pages_list']=groupdetail1
        groupdetail.append(d)
    cur1.close()
    cur2.close()
    return HttpResponse(json.dumps(groupdetail),content_type="application/json")

def edit_data_user_group(request):
    groupdetail=[]
    groupdetail1=[]
    # groupdetail2=[]
    group_custid=request.GET.get("group_id")
    cur1 = connection.cursor()
    cur2 = connection.cursor()
    #query1= "select * from 'UserAppRoleGroup' where userrolegroup_name='"+groupname+"' and pk_userrolegroup_id='"+group_id+"' "
    query1 = "Select * from user_data_rolegroup_usergroupdetails('"+group_custid+"')"
    query2 = "select * from user_data_rolegroup_getassociateddataparamvalue('"+group_custid+"')"
    cur1.execute(query1)
    cur2.execute(query2)
    x = cur2.fetchall()
    result= cur1.fetchall()
    requireddata={}
    for row in x:
		key=row[2]
		if (row[2] in requireddata.keys()):
			requireddata[key].append(row[0])
		else:
			requireddata[key]=[]
			requireddata[key].append(row[0])


    for i in result:
        d=OrderedDict()
        d['userrolegroup_id']=i[0]
        d['userrolegroup_name']=i[1]
        d['description']=i[3]
        d['comments']=i[4]
        d['values_list']=requireddata
        groupdetail.append(d)
    cur1.close()
    cur2.close()
    return HttpResponse(json.dumps(groupdetail),content_type="application/json")

def edit_single_user(request):
	groupdetail=[]
	groupdetail1=[]
	name=request.GET.get("editusername")
	firstname=request.GET.get("editfirstname")
	lastname=request.GET.get("editlastname")
	email=request.GET.get("editUseremail")
	user_alias=request.GET.get("edituseralias")
	#address=request.GET.get("useraddress")
	password1=request.GET.get("edituserpassword")
	created_date=request.GET.get("editdatepicker")
	user_flag=request.GET.get("edituserstatus")
	label_text=request.GET.get("label_text")
	user_id=request.GET.get("user_id")
	updated_by = request.session['username']
	mes_flag=request.GET.get("editmesstatus")
	
	if mes_flag == 'on':		
		mes_flag = 'true'		
	elif mes_flag == 'off':		
		mes_flag = 'false'

	if user_flag == 'on':
		user_flag = True
	elif user_flag == 'off':
		user_flag = False


	if not lastname:
		lastname = ''
	elif lastname:
		lastname= lastname

	cur1 = connection.cursor()

	cur2 = connection.cursor()
	query1 = "Select * from userapprolegroup_editblockreset("+user_id+",'"+firstname+"','"+lastname+"','"+user_alias+"','"+updated_by+"',"+str(user_flag)+",'"+password1+"','"+label_text+"','"+mes_flag+"')"
	cur1.execute(query1)
	cur1.close()
	return HttpResponse(1,content_type="application/json")

def getRegionHierarchyData(request):
    query='SELECT * FROM "Hierarchy"'
    cur = connection.cursor()
    cur.execute(query)
    result = cur.fetchall()
    hierarchy_array=[]
    for row in result:
        d = OrderedDict()
        d['clientId'] = row[0]
        d['id'] = row[0]
        d['value'] = row[4]
        d['parentClientId'] = row[1]
        d['clientName'] = row[2]
        d['level'] = row[3]
        d['title'] = row[2]
        hierarchy_array.append(d)
    cur.close()
    return HttpResponse(json.dumps(hierarchy_array), content_type="application/json")

def list_all_users(request):
    companyvalue = []
    cur = connection.cursor()
    query1= "select * from public.users_listusers()"
    cur.execute(query1)
    result = cur.fetchall()
    for i in result:
        d = OrderedDict()
        d['username']=i[1]
        if i[2]:
            d['firstname'] = i[3]
        else:
            d['firstname']= ''
        d['email_address']=i[6]
        d['created_date']=i[8].isoformat()
        d['user_alias']=i[5]
        d['user_id']=i[0]
        d['flag_active']=i[11]
        companyvalue.append(d)
    cur.close()
    return HttpResponse(json.dumps(companyvalue), content_type='application/json')

	
def resetUserPassword(request):
	data=[]
	currentpassword=request.GET.get("currentpassword")
	password=request.GET.get("newuserpassword")
	user_id=request.GET.get("user_id")
	updated_by='System'
	#connection = pyodbc.connect('DRIVER={SQL Server};SERVER=13.94.252.185;PORT=1433;DATABASE=Henkel_SC_UserModule;UID=sa;PWD=Welcome@123')
	cursor = connection.cursor() #using pyodbc cursor
	#query1 = "exec [USP_Admin_UserAppRoleGroup_EditBlockReset] @userID ='"+user_id+"',@firstname ='"+firstname+"',@lastname ='"+lastname+"',@user_alias ='"+user_alias+"',@updated_by ='"+updated_by+"',@created_by ='"+updated_by+"',@created_date ='"+created_date+"',@flag_active = '"+str(user_flag)+"',@password='"+str(password1)+"',@Edit_Block_Reset='EDIT'"
	#query = "exec [USP_Admin_User_ChangePassword] @userID ='"+user_id+"',@old_password='"+str(currentpassword)+"',@new_password='"+str(password)+"'"
	query = "select * from USP_Admin_User_ChangePassword("+user_id+",'"+currentpassword+"','"+password+"')"
	cursor.execute(query)
	finalresult=cursor.fetchall()
	#cursor.commit()
	for i in finalresult:
		d=OrderedDict()
		d['flag']=i[0]
		data.append(d)
	return HttpResponse(json.dumps(data),content_type="application/json")
