from django.shortcuts import render
from django.db.backends.utils import CursorWrapper
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.db import connection as default_connection
#from mongoengine import *
#from datetime import datetime
import datetime
from datetime import timedelta
from django.contrib.auth.decorators import login_required
import pymongo
import logging
import json
#from sets import Set
import math
#from user_mod.views import login_user
from decimal import *
import ast
import csv
import requests
import statistics
import hashlib, os, urllib
from Crypto.Cipher import AES
from operator import itemgetter
from collections import OrderedDict
from pymongo import MongoClient
from dateutil.parser import parse
download_pipeline={}

#---------------------------------connection_reportingtable function--------------------------------------#
def connection_reportingtable():	
	# url = "mongodb://henkelUser2:QpQc#123@localhost:27017/admin?authMechanism=SCRAM-SHA-1"
	# conn = MongoClient(url)
	# table_cursor=conn.henkel_ver2.report_fng_qualscore_qualitysegment
	# return table_cursor
	
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_fng_qualscore_qualitysegment
	return table_cursor

def mongoConnection_to_ver2():
	client = MongoClient('193.96.100.95',27017,connect=False)
	client.admin.authenticate('henkelUser', 'QpQc#123', mechanism='SCRAM-SHA-1')
	db=client.henkelProduction
	return db	
	
	
def connection_report_CPK():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_fng_opquality_CPK	
	return table_cursor
	
def connection_audit_log():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.audit_log	
	return table_cursor
	
def connection_src_processorders():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.src_processorders
	return table_cursor

def connection_process_fng_opqualityscore():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.process_fng_opqualityscore
	return table_cursor
	
def connection_history_fng_processorders():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.history_fng_processorders
	return table_cursor

def connection_benchmark():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.config_fng_opquality_benchmark
	return table_cursor
def connection_weights():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.config_fng_opquality_weights
	return table_cursor
def connection_benchmark_indicator():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.rule_fng_poqualityparam_benchmark
	return table_cursor

def connection_rejected_po():
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.rejected_fng_processorders
	return table_cursor
#---------------------------------LoginPage-------------------------------------------------------#
def login(request):
	template = loader.get_template('qualitypulse/henkel.html')
	context = RequestContext(request, {'appuser': "Admin"})
	return HttpResponse(template.render(context), content_type="text/html")
	
	#---------------------------------LoginPage-------------------------------------------------------#
	
	
def get_filter_data_config(request):
	plant=request.GET.get('plant').encode('utf-8')
	product_list=[]
	product_dict={}
	filterdata_dict=filterdata_display(request)
	pipeline={}
	pipeline['SiteID']={'$in':[plant]} 
	product_data = filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
	return HttpResponse(json.dumps(product_data), content_type="application/json")

def filter_dict(param_name,param_display,pipeline,param_dict,param_list,filterdata_dict):
	db=mongoConnection_to_ver2()
	filters_dict={}	
	dict={}
	list=[]
	result_cursor=db.dimension_filter_table.aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	if param_name=='ResourceID':
		param_name_temp='Resources'
	elif param_name=='Site_Display':
		param_name_temp='SiteName'
	else:
		param_name_temp=param_name
	
	for r in result_cursor:
		if(r[param_name].encode('utf-8') in [x[0] for x in filterdata_dict[param_name_temp]]):
			list.append(r[param_name].encode('utf-8'))
			dict[r[param_name]]=r[param_display].encode('utf-8')
			#dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list


	return filters_dict	
	
def rejected_filter_dict(param_name,param_display,pipeline,param_dict,param_list,filterdata_dict):
	filters_dict={}	
	dict={}
	list=[]
	
	if param_display=='Material_Display':
		param_display='MaterialDescription'
	elif param_display=='Site_Display':
		param_display='SiteName'
	#{'$match':pipeline},
	result_cursor=connection_rejected_po().aggregate([{'$match':pipeline},{'$group':{'_id':{param_name:'$'+param_name,param_display:'$'+param_display}}},{'$project':{param_name:'$_id.'+param_name,param_display:'$_id.'+param_display}}])
	

	if param_name=='Site_Display':
		param_name_temp='SiteName'
	else:
		param_name_temp=param_name

	for r in result_cursor:
		#if(r[param_name].encode('utf-8') in [x[0] for x in filterdata_dict[param_name_temp]]):
		list.append(r[param_name].encode('utf-8'))
		dict[r[param_name]]=r[param_display].encode('utf-8')
		#dict[r[param_name]]=r[param_display].encode('utf-8')
	filters_dict[param_dict]=dict
	filters_dict[param_list]=list


	return filters_dict	
	
	#---------------------------------LoginPage-------------------------------------------------------#


def pages_display(request):
    query2 = "select * from user_getassociatedapppages("+str(request.session['user_id'])+")"
    cur1 = default_connection.cursor()
    cur1.execute(query2)
    result1 = cur1.fetchall()
    finished_goods_list = []
    raw_materials_list = []
    administrator_list = []
    for i in result1:
        d = OrderedDict()
        if i[4] == 'Finished Goods':
            finished_goods_list.append(i)
        if i[4] == 'Raw Materials':
            raw_materials_list.append(i)
        if i[4] == 'Administrator':
            administrator_list.append(i)
    pagedatarray=OrderedDict()
    if(len(finished_goods_list)>0):
        pagedatarray['Finished Good']=finished_goods_list
    if(len(raw_materials_list)>0):
        pagedatarray['Raw Materials']=raw_materials_list
    if(len(administrator_list)>0):
        pagedatarray['Administrator']=administrator_list 
    return pagedatarray
	
# for encrpytion		
def AESencrypt(password, plaintext, base64=True):		
    SALT_LENGTH = 32		
    DERIVATION_ROUNDS=1337		
    BLOCK_SIZE = 16		
    KEY_SIZE = 32		
    MODE = AES.MODE_CBC		
     		
    paddingLength = 16 - (len(plaintext) % 16)		
    paddedPlaintext = plaintext+chr(paddingLength)*paddingLength		
    derivedKey = password		
    derivedKey = hashlib.sha256(derivedKey).digest()		
    derivedKey = derivedKey[:KEY_SIZE]		
    iv = derivedKey[:BLOCK_SIZE]		
    cipherSpec = AES.new(derivedKey, MODE, iv)		
    ciphertext = cipherSpec.encrypt(paddedPlaintext)		
    if base64:		
        import base64		
        return base64.b64encode(ciphertext)		
    else:		
        return ciphertext.encode("hex")
	
def filterdata_display(request):
	query2 = "select * from user_getassociateddataparamvalue("+str(request.session['user_id'])+")"
	cur1 = default_connection.cursor()
	cur1.execute(query2)
	result1 = cur1.fetchall()
	len_result1 = len(result1)
#	if len_result1 == 0:
		#return render(request, "login_page.html",{'groups_addition': True })
#		return HttpResponseRedirect("/user_mod/login/")
	#else:	
	filterdata_dict={}
	mappingdict={'SiteName':'SiteID','SBU':'SBU','Technology':'Technology','FGProducts':'MaterialNumber','LineName':'LineID','Resources':'Resources'}
	for eachrow in result1:
			k=mappingdict[eachrow[1]]
			if(k in filterdata_dict.keys()):
				filterdata_dict[k].append([eachrow[2],eachrow[3]])
			else:
				filterdata_dict[k]=[]
				filterdata_dict[k].append([eachrow[2],eachrow[3]])
	return filterdata_dict

	
# @login_required
def configuration_benchmark(request):
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filters_dict={}
		filterdata_dict=filterdata_display(request)
		syncdate=''
		last_load_date = ''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		#result_cursor=connection_reportingtable().aggregate([{'$group':{'_id':{'SiteName':'$SiteName','Site_Display':'$Site_Display'}}},{'$project':{'_id':0,'SiteName':'$_id.SiteName','Site_Display':'$_id.Site_Display'}}])
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		filters_dict['Plant_list']=Plant_list
				
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_list
		pagedatarray =pages_display(request)  
		url_check='/qualitypulse/configuration_benchmark'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/configuration_benchmark.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_dict":Plant_dict,'product_dict':product_dict,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")	

	else:
		return HttpResponseRedirect("/user_mod/login/")

	
#--------------------------------- function for overview page--------------------------------------#
# @login_required
def grid_view(request):
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filterdata_dict=filterdata_display(request)
		filters_dict={}
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		Plant_list=[]
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		#Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		filters_dict['Plant_list']=Plant_list
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
			
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		#technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		filters_dict['technology_list']=technology_list		

		for key,value in technology_dict.iteritems():
			technology_list.append(key)
		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':[technology_list[0]]}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		
			
		
		line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':[technology_list[0]]}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']
		
		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':[technology_list[0]]}
		pipeline['MaterialNumber']={'$in':product_list}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		
		filters_dict['resource_list']=resource_list	
		pagedatarray =pages_display(request)  
		url_check='/qualitypulse/grid_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/grid.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'resource_dict':resource_dict,'technology_dict':technology_dict,'sbu_dict':sbu_dict,'plant_dict':Plant_dict,'line_dict':line_dict,'product_dict':product_dict,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date  })	
			return HttpResponse(template.render(context), content_type="text/html")	


	else:
		return HttpResponseRedirect("/user_mod/login/")

def rejected_view(request):
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filterdata_dict=filterdata_display(request)
		filters_dict={}
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		syncdate=''
		last_load_date=''
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
		
		Plant_list=[]
		pipeline={}
		Plant_data = rejected_filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		filters_dict['Plant_list']=Plant_list
		
		'''sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		
		filters_dict['technology_list']=technology_list'''		

		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		#pipeline['SBU']={'$in':sbu_list}
		#pipeline['Technology']={'$in':technology_list}
		product_dict=rejected_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_list
		

		'''line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']
		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('Resource_Display','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		
		filters_dict['resource_list']=resource_list'''
		pagedatarray =pages_display(request)  
		url_check='/qualitypulse/rejected_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/rejected_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'plant_dict':Plant_dict,'product_dict':product_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date  })	
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate, 'last_load_date':last_load_date  })	
			return HttpResponse(template.render(context), content_type="text/html")	


	else:
		return HttpResponseRedirect("/user_mod/login/")
	
# @login_required
def configuration_view(request):
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filters_dict={}
		filterdata_dict=filterdata_display(request)
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')	
		result_cursor=connection_reportingtable().distinct('SiteName')
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		
		Plant_list=[]
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		
		filters_dict['Plant_list']=Plant_list
		
		product_list=[]
		product_dict={}
		pipeline['SiteID']=Plant_list[0]
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		pagedatarray =pages_display(request)  
		url_check='/qualitypulse/configuration_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/config.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'max_date':max_date,'min_date':min_date,'sync_date':sync_date,'plant_dict':Plant_dict,'product_dict':product_dict, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")	

	else:
		return HttpResponseRedirect("/user_mod/login/")
@csrf_exempt
# @login_required
def index(request):	
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filterdata_dict=filterdata_display(request)
		filters_dict={}
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		sync_date = sync_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		Plant_list=[]
		Plant_dict={}
		pipeline={}
		
		'''if not filterdata_dict == True:
		#if "Content-Type: text/html;" in filterdata_dict:
			print 'filterdata_dict is zero'
			return HttpResponseRedirect("/user_mod/login/")
			#return render(request, "login_page.html",{'groups_addition': True })
		else:'''
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		
		filters_dict['Plant_list']=Plant_list
		
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		
		filters_dict['technology_list']=technology_list		

		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)



		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		
			
		
		result_cursor=connection_reportingtable().distinct('LineName',{'SiteName':{'$in':Plant_list},'SBU':{'$in':sbu_list},'Technology':{'$in':technology_list},'MaterialNumber':{'$in':product_list} })
		line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']

		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		
		
		filters_dict['resource_list']=resource_list	
		pagedatarray =pages_display(request) 
		url_check='/qualitypulse/'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/overview.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'resource_dict':resource_dict,'technology_dict':technology_dict,'sbu_dict':sbu_dict,'line_dict':line_dict,'plant_dict':Plant_dict,'product_dict':product_dict,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date,'end_date':end_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")		
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date,'end_date':end_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")	
		

	else:
		return HttpResponseRedirect("/user_mod/login/")
		
		
		
		
		
# @login_required
'''def configuration_benchmark(request):
	if 'user_id'  in request.session:
		user_id=request.GET.get('username')
		filterdata_dict=filterdata_display(request)
		filters_dict={}
		syncdate=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		sync_date = sync_date.date()
		
		result_cursor=connection_reportingtable().aggregate([{'$group':{'_id':{'SiteName':'$SiteName','Site_Display':'$Site_Display'}}},{'$project':{'_id':0,'SiteName':'$_id.SiteName','Site_Display':'$_id.Site_Display'}}])
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		filters_dict['Plant_list']=Plant_list
				
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_list
		
		pagedatarray =pages_display(request)  
		url_check='/qualitypulse/configuration_benchmark'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/configuration_benchmark.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_dict":Plant_dict,'product_dict':product_dict,'syncdate':syncdate,'min_date':min_date, 'end_date':end_date, 'max_date':max_date})
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'syncdate': syncdate,'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate,'min_date':min_date, 'end_date':end_date, 'max_date':max_date})
			return HttpResponse(template.render(context), content_type="text/html")	

	else:
		return HttpResponseRedirect("/user_mod/login/")	'''	
		
	

#--------------------------------- function for plant view page--------------------------------------#	
@csrf_exempt
# @login_required
def plant_view(request):
	if 'user_id'in request.session:
		user_id=request.session['user_id']
		filterdata_dict=filterdata_display(request)
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		filters_dict={}
		
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_dict = Plant_data['plant_dict']
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list
		
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		
		filters_dict['technology_list']=technology_list		

		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		
			
		line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']
		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		
		filters_dict['resource_list']=resource_list	
		
		pagedatarray =pages_display(request)  
		
		
		url_check='/qualitypulse/plant_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/plant_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'resource_dict':resource_dict,'technology_dict':technology_dict,'sbu_dict':sbu_dict,'line_dict':line_dict,'plant_dict':Plant_dict,'product_dict':product_dict,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'min_date':min_date, 'end_date':end_date, 'max_date':max_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")	

		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'min_date':min_date, 'end_date':end_date, 'max_date':max_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")			
	else:
		return HttpResponseRedirect("/user_mod/login/")

#--------------------------------- function for line view page--------------------------------------#
# @login_required
def line_view(request):
	if 'user_id'  in request.session:
		user_id=request.session['user_id']
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
		filters_dict={}
		filterdata_dict=filterdata_display(request)
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_dict = Plant_data['plant_dict']
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list
		
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		
		filters_dict['technology_list']=technology_list		

		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		filters_dict['product_list']=product_list
		
			
		
		line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_dict = line_dict['line_dict']

		
		for key,value in line_dict.iteritems():
			line_list.append(key)
		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		pipeline['LineID']={'$in':[line_list[0]]}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		
		filters_dict['resource_list']=resource_list	
		pagedatarray =pages_display(request)  

		url_check='/qualitypulse/line_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/line_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'resource_dict':resource_dict,'technology_dict':technology_dict,'sbu_dict':sbu_dict,'plant_dict':Plant_dict,'line_dict':line_dict,'product_dict':product_dict,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date  })
			return HttpResponse(template.render(context), content_type="text/html")	
	else:
		return HttpResponseRedirect("/user_mod/login/")

#--------------------------------- function for resource view page--------------------------------------#
# @login_required
def resource_view(request):	
	if 'user_id'  in request.session:
		filterdata_dict=filterdata_display(request)
		user_id=request.session['user_id']
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		filters_dict={}
		user_id=request.GET.get('username')
		syncdate=''
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
		filters_dict={}
		
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_dict = Plant_data['plant_dict']
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list
		
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		
		filters_dict['technology_list']=technology_list		

		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		
			
		
		line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_dict = line_dict['line_dict']
		
		for key,value in line_dict.iteritems():
			line_list.append(key)
		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':product_list}
		pipeline['LineID']={'$in':[line_list[0]]}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		filters_dict['resource_list']=resource_list	
			
		pagedatarray =pages_display(request) 

		url_check='/qualitypulse/resource_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/resource_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'resource_dict':resource_dict,'technology_dict':technology_dict,'sbu_dict':sbu_dict,'line_dict':line_dict,'product_dict':product_dict,'plant_dict':Plant_dict,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate, 'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date 
			})
			return HttpResponse(template.render(context), content_type="text/html")	
		

	else:
		return HttpResponseRedirect("/user_mod/login/")

# @login_required
def product_view(request):	
	if 'user_id'  in request.session:
		user_id=request.session['user_id']
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])

		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		filterdata_dict=filterdata_display(request)
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'

		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		filters_dict={}
		user_id=request.GET.get('username')
		syncdate=''
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')
		filters_dict={}
		
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		
		filters_dict['Plant_list']=Plant_list
		
		sbu_list=[]
		sbu_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		
		filters_dict['sbu_list']=sbu_list

		technology_list=[]
		technology_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']
		
		filters_dict['technology_list']=technology_list		

		
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)



		product_dict = product_dict['product_dict']
		
		for key,value in product_dict.iteritems():
			product_list.append(key)
		filters_dict['product_list']=product_list	
		
			
		
		line_list=[]
		line_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':[product_list[0]]}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']
		filters_dict['line_list']=line_list
		

		resource_list=[]
		resource_dict={}
		pipeline['SiteID']={'$in':Plant_list}
		pipeline['SBU']={'$in':sbu_list}
		pipeline['Technology']={'$in':technology_list}
		pipeline['MaterialNumber']={'$in':[product_list[0]]}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']
		filters_dict['resource_list']=resource_list	
		
		
		pagedatarray =pages_display(request) 

		url_check='/qualitypulse/product_view'
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/product_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_dict':line_dict,'resource_dict':resource_dict,'technology_dict':technology_dict,'sbu_dict':sbu_dict,'product_dict':product_dict,'plant_dict':Plant_dict,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'line_list':filters_dict['line_list'],"plant_list":filters_dict['Plant_list'],"resource_list":filters_dict['resource_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'syncdate':syncdate,'max_date':max_date,'min_date':min_date,'sync_date':sync_date, 'end_date':end_date, 'last_load_date':last_load_date })
			return HttpResponse(template.render(context), content_type="text/html")
		

	else:
		return HttpResponseRedirect("/user_mod/login/")
	
	
#----------------------------------------------view for cpk screen----------------------------------#	
# @login_required
def cpk_view(request):	
	if 'user_id'  in request.session:
		user_id=request.session['user_id']
		filterdata_dict=filterdata_display(request)
		syncdate=''
		last_load_date=''
		result_cursor_datedata=connection_reportingtable().aggregate([{'$group':{'_id':0,'syncdate':{'$max':'$ProductionStartDateTime'},'maxdate':{'$max':'$ProductionStartDateTime'},'mindate':{'$min':'$ProductionStartDateTime'}}}])
		for r in result_cursor_datedata:
			max_date = r['maxdate']
			min_date = r['mindate']
			sync_date = r['syncdate']
		d = max_date - timedelta(days=365)
		x = d.replace(day=1)
		end_date = x.date()
		min_date = min_date.date()
		result_last_load_cursor = connection_audit_log().aggregate([{'$match':{'JobName':'Wrapper','JobStatus':'Success'}},{'$sort':{'JobEndTime':-1}},{'$limit':1}])
		for r in result_last_load_cursor:
			last_load_date = r['JobEndTime'].strftime('%d %b %y')
		print '--------'
		print last_load_date
		print '++++++++'
		result_cursor=connection_reportingtable().aggregate( [ {'$group':{ '_id':{'ProductionStartDateTime':'$ProductionStartDateTime'}} },{'$sort':{'_id.ProductionStartDateTime':-1}},{ '$limit' : 1}])
		for r in result_cursor:
			syncdate=r['_id']['ProductionStartDateTime'].strftime('%d %b %y')

		filters_dict={}
		Plant_list=[]
		Plant_dict={}
		pipeline={}
		Plant_data = filter_dict('SiteID','Site_Display',pipeline,'plant_dict','plant_list',filterdata_dict)
		#Plant_list=Plant_data['plant_list']
		Plant_dict = Plant_data['plant_dict']
		
		for key,value in Plant_dict.iteritems():
			Plant_list.append(key)
		filters_dict['Plant_list']=Plant_list
		
		
		#result_cursor=connection_reportingtable().aggregate([{'$match':{'$and':[{'SiteName':Plant_list[0]},{'POCount':{'$gte':30}}]}},{'$group':{"_id":{'product':'$MaterialNumber','pc':'$POCount'}}},{'$sort':{"_id.pc":-1}},{'$project':{'_id':0,'product':'$_id.product'}}])
		#logging.info([{'$match':{'$and':[{'SiteName':Plant_list[0]},{'POCount':{'$gte':30}}]}},{'$group':{"_id":{'product':'$MaterialNumber','pc':'$POCount'}}},{'$sort':{"_id.pc":-1}},{'$project':{'_id':0,'product':'$_id.product'}}])
		product_list=[]
		product_dict={}
		pipeline['SiteID']={'$in':[Plant_list[0]]}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']
		
		filters_dict['product_list']=product_list
		date_list=[]
		try:
			result_cursor=connection_reportingtable().aggregate([{'$match':{'ProductionStartDateTime':{'$ne':''}}},{'$group':{'_id':{'year':{'$year':'$ProductionStartDateTime'}}}},{'$project':{'_id':0,'year':'$_id.year'}},{'$sort':{'year':1}}])
			for result in result_cursor:
				if(result['year'] not in date_list):
					date_list.append(result['year'])
		except:
			date_list=[]
		filters_dict['date_list']=date_list
		pagedatarray =pages_display(request)  
	
		url_check="/qualitypulse/cpk_view"
		url_status=0
		for row in pagedatarray['Finished Good']:
			if(url_check in row[3]):
				url_status=1
		if(url_status==1):
			template = loader.get_template('qualitypulse/cpk_view.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,'product_dict':product_dict,'plant_dict':Plant_dict,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate,'date_list':filters_dict['date_list'],'min_date':min_date, 'end_date':end_date, 'max_date':max_date, 'last_load_date':last_load_date})
			return HttpResponse(template.render(context), content_type="text/html")	
		else:
			template = loader.get_template('qualitypulse/forbidden.html')
			context = RequestContext(request, {'appuser':request.session['user_alias'], "pages_dict": pagedatarray,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'syncdate':syncdate,'date_list':filters_dict['date_list'],'min_date':min_date, 'end_date':end_date, 'max_date':max_date, 'last_load_date':last_load_date})
			return HttpResponse(template.render(context), content_type="text/html")

	else:
		return HttpResponseRedirect("/user_mod/login/")
	
def coming_soon(request):
	return HttpResponseRedirect("/qualitypulse/comingsoon.html")
	
def get_filter_pipeline(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_dict={}
	plant=''
	line=''
	resource=''
	product=''
	sbu=''
	technology=''

#---------------------getting data------------------------------------#	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data.encode('utf-8'))
	else:
		plant=data_list[0].encode('utf-8')
		
	
		
	data_list=[]
	data_list=request.POST.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data.encode('utf-8'))
	else:
		sbu=data_list[0].encode('utf-8')


	data_list=[]
	data_list=request.POST.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data.encode('utf-8'))
	else:
		technology=data_list[0].encode('utf-8')		
		
		
	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data.encode('utf-8'))
	else:
		product=data_list[0].encode('utf-8')	

		
		
	data_list=[]
	data_list=request.POST.getlist('line_list[]')
	if len(data_list)>1:
		for data in data_list:
			line_list.append(data.encode('utf-8'))
	else:
		line=data_list[0].encode('utf-8')
		
	data_list=[]
	data_list=request.POST.getlist('resource_list[]')
	if len(data_list)>1:
		for data in data_list:
			resource_list.append(data.encode('utf-8'))
	else:
		resource=data_list[0].encode('utf-8')
#----------------------making Pipeline-------------------------------
	if (len(plant_list)>=2 ):
		filter_dict['SiteID']={'$in':plant_list }  
	else:
		if(plant!='all'):	
			filter_dict['SiteID']=plant  
	
	if (len(sbu_list)>=2 ):
		filter_dict['SBU']={'$in':sbu_list }  
	else:
		if(sbu!='all'):	
			filter_dict['SBU']=sbu 

	if (len(technology_list)>=2 ):
		filter_dict['Technology']={'$in':technology_list }  
	else:
		if(technology!='all'):	
			filter_dict['Technology']=technology  

	if (len(product_list)>=2 ):
		filter_dict['MaterialNumber']={'$in':product_list }  
	else:
		if(product!='all'):	
			filter_dict['MaterialNumber']=product  			
			
	if (len(line_list)>=2 ):
		filter_dict['LineID']={'$in':line_list } 	
	else:
		if(line!='all'):
			filter_dict['LineID']=line
			
	if (len(resource_list)>=2 ):
		filter_dict['ResourceName']={'$in':resource_list }
	else:
		if(resource!='all'):
			filter_dict['ResourceName']=resource
	return filter_dict


@csrf_exempt			
def get_filter_data(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	pipeline=get_filter_pipeline(request)
	change_type=request.POST.get("change_type", "")
	change_type=change_type.encode('utf-8')
	filterdata_dict=filterdata_display(request)
	if (change_type=='plant'):
		sbu_list=[]
		sbu_dict={}
		#pipeline={}
		sbu_dict=filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		filters_dict['sbu_list']=sbu_dict
		
		technology_dict={}	
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']	
		
		filters_dict['technology_list']=technology_dict		

		product_list=[]
		product_dict={}

		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']	
		filters_dict['product_list']=product_dict
		
		line_list=[]
		line_dict={}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']				

		filters_dict['resource_list']=resource_dict


	elif (change_type=='sbu'):

		technology_list=[]
		technology_dict={}
		technology_dict=filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']			

		filters_dict['technology_list']=technology_dict			
		product_list=[]
		product_dict={}

		pipeline['Technology']={'$in':technology_list}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']				
		
		filters_dict['product_list']=product_dict
		
		
		line_list=[]
		line_dict={}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']				

		filters_dict['resource_list']=resource_dict	


	elif (change_type=='technology'):
		product_list=[]
		product_dict={}
		product_dict=filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']				

		filters_dict['product_list']=product_dict
		
		line_list=[]
		line_dict={}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']

		filters_dict['resource_list']=resource_dict	


	elif (change_type=='product'):
		line_list=[]
		line_dict={}
		line_dict=filter_dict('LineID','Line_Display',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']				
		
		filters_dict['resource_list']=resource_dict	


	else:		
	#	line_dict={}
		resource_list=[]
		resource_dict={}
		resource_data=filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']	

		filters_dict['resource_list']=resource_dict	
	
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")


@csrf_exempt			
def get_rejected_filter_data(request):
	filters_dict={}	
	sbu_list=[]
	line_list=[]
	technology_list=[]
	resource_list=[]
	product_list=[]
	pipeline=get_filter_pipeline(request)
	change_type=request.POST.get("change_type", "")
	change_type=change_type.encode('utf-8')
	filterdata_dict=filterdata_display(request)
	if (change_type=='plant'):
		sbu_list=[]
		sbu_dict={}
		#pipeline={}
		sbu_dict=rejected_filter_dict('SBU','SBU',pipeline,'sbu_dict','sbu_list',filterdata_dict)
		sbu_list=sbu_dict['sbu_list']
		sbu_dict = sbu_dict['sbu_dict']
		filters_dict['sbu_list']=sbu_dict
		
		technology_dict={}	
		pipeline['SBU']={'$in':sbu_list}
		technology_dict=rejected_filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']	
		
		filters_dict['technology_list']=technology_dict		

		product_list=[]
		product_dict={}

		pipeline['Technology']={'$in':technology_list}
		product_dict=rejected_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']	
		filters_dict['product_list']=product_dict
		
		line_list=[]
		line_dict={}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=rejected_filter_dict('PlineID','PlineName',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=rejected_filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']				

		filters_dict['resource_list']=resource_dict	
	elif (change_type=='sbu'):

		technology_list=[]
		technology_dict={}
		technology_dict=rejected_filter_dict('Technology','Technology',pipeline,'technology_dict','technology_list',filterdata_dict)
		technology_list=technology_dict['technology_list']
		technology_dict = technology_dict['technology_dict']			

		filters_dict['technology_list']=technology_dict			
		product_list=[]
		product_dict={}

		pipeline['Technology']={'$in':technology_list}
		product_dict=rejected_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']				
		
		filters_dict['product_list']=product_dict
		
		
		line_list=[]
		line_dict={}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=rejected_filter_dict('PlineID','PlineName',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=rejected_filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']				

		filters_dict['resource_list']=resource_dict	


	elif (change_type=='technology'):
		product_list=[]
		product_dict={}
		product_dict=rejected_filter_dict('MaterialNumber','Material_Display',pipeline,'product_dict','product_list',filterdata_dict)
		product_list=product_dict['product_list']
		product_dict = product_dict['product_dict']				

		filters_dict['product_list']=product_dict
		
		line_list=[]
		line_dict={}
		pipeline['MaterialNumber']={'$in':product_list}
		line_dict=rejected_filter_dict('PlineID','PlineName',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=rejected_filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']

		filters_dict['resource_list']=resource_dict	


	elif (change_type=='product'):		
		line_list=[]
		line_dict={}
		line_dict=rejected_filter_dict('PlineID','PlineName',pipeline,'line_dict','line_list',filterdata_dict)
		line_list=line_dict['line_list']
		line_dict = line_dict['line_dict']				

		filters_dict['line_list']=line_dict
		
		
		resource_list=[]
		resource_dict={}
		pipeline['LineID']={'$in':line_list}
		resource_data=rejected_filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']				
		
		filters_dict['resource_list']=resource_dict	


	else:		
	#	line_dict={}
		resource_list=[]
		resource_dict={}
		resource_data=rejected_filter_dict('ResourceID','Resource_Display',pipeline,'resource_dict','resource_list',filterdata_dict)
		resource_list=resource_data['resource_list']
		resource_dict = resource_data['resource_dict']	

		filters_dict['resource_list']=resource_dict
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")		


#--------------------------------- function for getting pipeline for graphs --------------------------------------#	
	
def get_pipeline(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	date_list=[]
	filter_list=[ {'QualitySegment':{'$ne':''}}, {'ProductionStartDateTime':{'$ne':''}} ]
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	line=''
	product=''
	resource=''
	date='1900-01-01'
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]

	data_list=[]
	data_list=request.POST.getlist('line_list[]')
	if len(data_list)>1:
		for data in data_list:
			line_list.append(data)
	else:
		line=data_list[0]

	data_list=[]
	data_list=request.POST.getlist('resource_list[]')
	if len(data_list)>1:
		for data in data_list:
			resource_list.append(data)
	else:
		resource=data_list[0]

	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data)
	else:	
		product=data_list[0].encode('utf-8')	
		
	data_list=[]
	data_list=request.POST.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]		
	
	data_list=[]
	data_list=request.POST.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data)
	else:
		sbu=data_list[0]	
		
	data_list=[]
	data_list=request.POST.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data)
	else:
		technology=data_list[0]			
		
	if (len(technology_list)>=2 ):
		filter_list.append({'Technology':{'$in':technology_list } } )
	else:
		if(technology!='all'):
			filter_list.append( {'Technology':technology } )
			
	if (len(sbu_list)>=2 ):
		filter_list.append({'SBU':{'$in':sbu_list } } )
	else:
		if(sbu!='all'):
			filter_list.append( {'SBU':sbu } )

	if (len(plant_list)>=2 ):
		filter_list.append({'SiteID':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteID':plant } )



	if (len(line_list)>=2 ):
		filter_list.append( {'LineID':{'$in':line_list } }	)	
	else:
		if(line!='all'):
			filter_list.append( {'LineID':line }	)
			
			
	if (len(resource_list)>=2 ):
		filter_list.append( {'ResourceID': {'$in':resource_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'ResourceID':resource }	)
			
	if (len(product_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':product_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'MaterialNumber':product }	)
			
		
	if (len(date_list)>=2 ):
		filter_list.append( {'ProductionStartDateTime': {'$lte':datetime.datetime.strptime(date_list[1],'%Y-%m-%d'),'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d') } } )
	else:
		#start=datetime(date.year(),date[1],date[2],0,0,0,0)
        #end=datetime(date[0],date[1],date[2],11,59,59,999999)
		filter_list.append ({'ProductionStartDateTime':{'$lt':datetime.datetime.strptime(date, '%Y-%m-%d')+timedelta(days=1),'$gte':datetime.datetime.strptime(date, '%Y-%m-%d') }})
		#filter_list.append ( {'ProductionStartDateTime':datetime.datetime.strptime(date, '%Y-%m-%d')} )
	
	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			logging.info('***********************pipeline**********************')
			logging.info(filter_pipeline)
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline

	
#--------------------------------- function for getting pipeline for graphs --------------------------------------#	
	
def get_pipeline_complaints(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	date_list=[]
	#filter_list=[ {'QualitySegment':{'$ne':''}}]
	filter_list=[]
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	line=''
	product=''
	resource=''
	date=''
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]

	data_list=[]
	data_list=request.POST.getlist('line_list[]')
	if len(data_list)>1:
		for data in data_list:
			line_list.append(data)
	else:
		line=data_list[0]
		
	data_list=[]
	data_list=request.POST.getlist('resource_list[]')
	if len(data_list)>1:
		for data in data_list:
			resource_list.append(data)
	else:
		resource=data_list[0]

	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data)
	else:	
		product=data_list[0].encode('utf-8')	
		
	data_list=[]
	data_list=request.POST.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]		
		
		
	data_list=[]
	data_list=request.POST.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data)
	else:
		sbu=data_list[0]	
		
	data_list=[]
	data_list=request.POST.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data)
	else:
		technology=data_list[0]			
		
	if (len(technology_list)>=2 ):
		filter_list.append({'Technology':{'$in':technology_list } } )
	else:
		if(technology!='all'):
			filter_list.append( {'Technology':technology } )
			
	if (len(sbu_list)>=2 ):
		filter_list.append({'SBU':{'$in':sbu_list } } )
	else:
		if(sbu!='all'):
			filter_list.append( {'SBU':sbu } )

	if (len(plant_list)>=2 ):
		filter_list.append({'SiteID':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteID':plant } )



	if (len(line_list)>=2 ):
		filter_list.append( {'LineID':{'$in':line_list } }	)	
	else:
		if(line!='all'):
			filter_list.append( {'LineID':line }	)
			
			
	if (len(resource_list)>=2 ):
		filter_list.append( {'ResourceID': {'$in':resource_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'ResourceID':resource }	)
			
	if (len(product_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':product_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'MaterialNumber':product }	)
			
		
	if (len(date_list)>=2 ):
		filter_list.append( {'ProductionStartDateTimeTime': {'$lte':datetime.datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d') } } )
	else:
		filter_list.append ( {'ProductionStartDateTimeTime':datetime.datetime.strptime(date, '%Y-%m-%d')} )
		#filter_list.append( {'ProductionStartDateTimeTime': {'$lte':datetime.strptime(date, '%Y-%m-%d'),'$gte':datetime.strptime(date, '%Y-%m-%d') } } )
		
	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			logging.info('***********************pipeline**********************')
			logging.info(filter_pipeline)
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline
	
	


	
@csrf_exempt
def Overall_Quality(request):
	result_list=[]
	quality_percentage=''
	query=[get_pipeline(request),{'$group':{'_id':{'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.Quality_Segment','q_count':1 }} ] 
	result_cursor=connection_reportingtable().aggregate([get_pipeline(request),{'$group':{'_id':{'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.Quality_Segment','q_count':1 }} ] )
	color_dict={'A':'#00B050','B':'#FFFF00','C':'#F8963F','D':'#FF0000'}
	for result in result_cursor:
		if result['q_segment'] in ['A','B','C','D']:
			result_dict={}
			result_dict['name']=result['q_segment']
			result_dict['y']=result['q_count']
			result_dict['color']=color_dict[result['q_segment']]
			result_list.append(result_dict)
	return HttpResponse(json.dumps( sorted(result_list, key = lambda result_list: (result_list['name']))  ), content_type="application/json")

	
	
@csrf_exempt
def customer_complaints(request):
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_customer_complaints
	pipeline=get_pipeline(request)
	data_list=[]	
	#color_list=['#327e96','#f2af37','#b22c20','#fdd201','#FF9800','#90CAF9','#CDDC39']
	pipeline['$match']['$and'].append({'Coding':'Product'})
	result_list=[]
	query=[pipeline,{'$group':{'_id':{'Defect Type':'$DefectType'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'Defect Type':'$_id.Defect Type','q_count':1 }} ] 

	result_cursor=table_cursor.aggregate(query)
	count=0
	for result in result_cursor:
		
		result_dict={}
		result_dict['name']=result['Defect Type']
		result_dict['y']=result['q_count']
		#result_dict['color']=color_list[count]
		count = count + 1
		result_list.append(result_dict)		
	
	return HttpResponse(json.dumps(result_list), content_type="application/json")
	
	
	
	
@csrf_exempt
def customer_complaints_modal_view(request):
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_customer_complaints
	pipeline=get_pipeline(request)
	date_list=[]
	date=''
	data_list=[]
	complaints_list=[]
	deatils_list=[]
	data_list=request.POST.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]		
	data_list=[]	
	data_list=request.POST.getlist('defect_list[]')
	pipeline['$match']['$and'][7]['Coding']= 'Product'
	pipeline['$match']['$and'].append({'DefectType':data_list[0].encode('utf-8')}) 
	result_list=[]
	query=[pipeline,{'$group':{'_id':{'QualitySegment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.QualitySegment','q_count':1 }} ] 
	query1=[pipeline,{'$project': {'_id': 0,'q_segment': '$QualitySegment','MaterialNumber':'$MaterialNumber','Material_Display':'$Material_Display','ExecutionInfoExistsInMES':'$ExecutionInfoExistsInMES','ProcessOrder':'$ProcessOrder','BatchNumber':'$BatchNumber','AvgQualityScore':'$AvgQualityScore','SiteID':'$SiteID','SiteName':'$SiteName','SBU':'$SBU','Technology':'$Technology','ProductionStartDateTime':'$ProductionStartDateTime','PreviousPO':'$PreviousPO','PreviousMaterial':'$PreviousMaterial','PrevAvgQualityScore':'$PrevAvgQualityScore','NextPO':'$NextPO','NextMaterial':'$NextMaterial','NextAvgQualityScore':'$NextAvgQualityScore'}}]
	result_cursor=table_cursor.aggregate(query)
	username = request.session['username']
	encrpytion_key = "71079162"
	encrpyted_username = AESencrypt(encrpytion_key, username)
	encoded_username_url = urllib.quote(encrpyted_username, safe='')
	app = '20263146'
	result_cursor1=table_cursor.aggregate(query1)
	color_dict={'A':'#00B050','B':'#FFFF00','C':'#F8963F','D':'#FF0000'}
	for result in result_cursor:
		result_dict={}
		result_dict['name']=result['q_segment']
		result_dict['y']=result['q_count']
		result_dict['color']=color_dict.get(result['q_segment'],'')   
		result_list.append(result_dict)	
	for result in result_cursor1:
		l=[]
		deatil_list=[]
		l.append(result['q_segment'])	
		l.append(result['MaterialNumber'])
		l.append(result['Material_Display'])
		l.append(result['BatchNumber'])
		l.append(result['AvgQualityScore'])
		l.append(result['SiteID'])
		l.append(result['SiteName'])
		l.append(result['SBU'])
		l.append(result['Technology'])
		if(result['ProductionStartDateTime']!=''):
			l.append(result['ProductionStartDateTime'].strftime('%y-%m-%d'))
		else:
			l.append(result['ProductionStartDateTime'])
		l.append(app)
		l.append(encoded_username_url)
		#l.append(result.get('ProcessOrder',''))
		l.append(result['ProcessOrder'])
		l.append(result['ExecutionInfoExistsInMES'])
		l.append(result['PreviousPO'])
		l.append(result['PreviousMaterial'])
		l.append(result['PrevAvgQualityScore'])
		l.append(result['NextPO'])
		l.append(result['NextMaterial'])
		l.append(result['NextAvgQualityScore'])
		complaints_list.append(l)
		deatil_list.append(result['q_segment'])
		deatil_list.append(result['MaterialNumber'])
		deatil_list.append(result['Material_Display'])
		deatil_list.append(result['BatchNumber'])
		deatil_list.append(result['AvgQualityScore'])
		deatil_list.append(result['SiteID'])
		deatil_list.append(result['SiteName'])
		deatil_list.append(result['SBU'])
		deatil_list.append(result['Technology'])
		deatil_list.append(result['ProcessOrder'])
		deatil_list.append(result['PreviousPO'])
		deatil_list.append(result['PreviousMaterial'])
		deatil_list.append(result['PrevAvgQualityScore'])
		deatil_list.append(result['NextPO'])
		deatil_list.append(result['NextMaterial'])
		deatil_list.append(result['NextAvgQualityScore'])
		deatils_list.append(deatil_list)
	result_list.append(complaints_list)
	result_list.append(deatils_list)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	
	
	
	
@csrf_exempt
def Trend_Of_Quality(request):
	result_list=[]
	result_list1=[]
	Excellent_list=[]
	Good_list=[]
	Bad_list=[]
	Poor_list=[]
	quality_list=[]
	x_categories=set()
	x_categories_list=[]
	data_list=[]
	month_list={'1':'JAN','2':'FEB','3':'MAR','4':'APR','5':'MAY','6':'JUN','7':'JUL','8':'AUG','9':'SEP','10':'OCT','11':'NOV','12':'DEC'}
	data_list=request.POST.getlist('graphtype_list[]') 
	graphtype=data_list[0].encode('utf-8')
	if(graphtype=='day'):
		sorted_dict=OrderedDict()
		sorted_dict['_id.year']=1
		sorted_dict['_id.month']=1
		sorted_dict['_id.day']=1
		pipe = get_pipeline(request)
		print "********************pipe"
		#result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$group':{'_id':{'Date':"$ProductionStartDateTime" ,'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1} } },{ '$sort' : {'_id.Date': 1} } ,{'$project':{'_id':0,'x_categories':'$_id.Date','name':'$_id.Quality_Segment','data':'$q_count'}},{'$sort':{'x_categories':1} } ])
		result_cursor=connection_reportingtable().aggregate([get_pipeline(request),{ "$group": {  "_id": {"day": { "$dayOfMonth" : "$ProductionStartDateTime" },"month": { "$month" : "$ProductionStartDateTime" }, "year": { "$year" : "$ProductionStartDateTime" },'Quality_Segment':'$QualitySegment'},"Count": { "$sum" : 1 }}      },{"$sort": sorted_dict },{ "$project": { "_id": 0,'name':'$_id.Quality_Segment',  'data': '$Count', 'x_categories': {"$concat": [  {"$substr" : [ "$_id.day", 0, 2]}, "-",{"$substr" : [ "$_id.month", 0, 2]}, "-",{"$substr" : [ "$_id.year", 0, 4]}]}}}])
		
		#query=[get_pipeline(request),{ "$group": {  "_id": {"day": { "$dayOfMonth" : "$ProductionStartDateTime" },"month": { "$month" : "$ProductionStartDateTime" }, "year": { "$year" : "$ProductionStartDateTime" },'Quality_Segment':'$QualitySegment'},"Count": { "$sum" : 1 }}      },{"$sort": { "_id.year": 1, "_id.month": 1,"_id.day": 1 } },{ "$project": { "_id": 0,'name':'$_id.Quality_Segment',  'data': '$Count' , 'x_categories': {"$concat": [  {"$substr" : [ "$_id.day", 0, 2]}, "-",{"$substr" : [ "$_id.month", 0, 2]}, "-",{"$substr" : [ "$_id.year", 0, 4]}]}}},{'$project':{'d':datetime.strptime('2014-2-3', "%Y-%m-%d") }   }    ]
		for r in result_cursor:
			#r['x_categories']=r['x_categories'].strftime('%y-%m-%d')
			r['x_categories']=r['x_categories']
			result_list.append(r)
			if r['x_categories'] not in x_categories_list:
				x_categories_list.append(r['x_categories'])

	if(graphtype=='week'):
		result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$group':{'_id':{'year': { '$year': '$ProductionStartDateTime'},'zweek':{'$week': '$ProductionStartDateTime' },'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1} } },{ '$sort' : {'_id.year': 1,'_id.zweek':1}} ,{'$project':{'_id':0,'year':'$_id.year','week':'$_id.zweek','name':'$_id.Quality_Segment','data':'$q_count'}} ])
		for r in result_cursor:
			#r['x_categories']='W '+str(r["week"]+1)+' '+' '+''+str(r["year"])[2:4]+''
			
			r['x_categories']="W"+str(r["week"]+1)+""+"'"+""+str(r["year"])[2:4]+""
			
			result_list.append(r)
			if r['x_categories'] not in x_categories_list:
				x_categories_list.append(r['x_categories'])		
	if(graphtype=='month'):

		result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$group':{'_id':{'year': { '$year': '$ProductionStartDateTime'},'month':{'$month': '$ProductionStartDateTime'},'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1} } },{ '$sort' : {'_id.year': 1,'_id.month':1} } ,{'$project':{'_id':0,'year':'$_id.year','month':'$_id.month','name':'$_id.Quality_Segment','data':'$q_count'}} ])
		
		for r in result_cursor:
			r['x_categories']=""+str(month_list[str(r["month"])])+""+"'"+""+str(r["year"])[2:4]+""
			result_list.append(r)
			if r['x_categories'] not in x_categories_list:
				x_categories_list.append(r['x_categories'])		
		
		
	for x_categorie in x_categories_list:
		Excellent_flag=0
		Good_flag=0
		Bad_flag=0
		Poor_flag=0
		for result in result_list:
			if(x_categorie ==result['x_categories']):
				if(result['name']=='A'):
					Excellent_list.append(result['data'])
					Excellent_flag=1
				elif(result['name']=='B'):
					Good_list.append(result['data'])
					Good_flag=1
				elif(result['name']=='C'):
					Bad_list.append(result['data'])
					Bad_flag=1
				elif(result['name']=='D'):
					Poor_list.append(result['data'])	
					Poor_flag=1
				else:
					print "else"
		if(Excellent_flag==0):
			Excellent_list.append(0)
		if(Good_flag==0):
			Good_list.append(0)
		if(Bad_flag==0):
			Bad_list.append(0)
		if(Poor_flag==0):
			Poor_list.append(0)
			
	for i in range(0,len(Excellent_list)):
		total=Excellent_list[i]+Good_list[i]+Bad_list[i]+Poor_list[i]
		if(total==0):
			total=1
		quality=Excellent_list[i]+Good_list[i]
		per=int((quality*100)/total)
		quality_list.append(per)
	series_list=[]
	series_list.append({'name':'D','data':Poor_list,'color':'#FF0000','type': 'column', 'yAxis': 1})
	series_list.append({'name':'C','data':Bad_list,'color':'#F8963F','type': 'column', 'yAxis': 1})
	series_list.append({'name':'B','data':Good_list,'color':'#FFFF00','type': 'column', 'yAxis': 1})
	series_list.append({'name':'A','data':Excellent_list,'color':'#00B050','type': 'column', 'yAxis': 1})
	series_list.append({'name':'QualityScore','data':quality_list,'type': 'spline','color':'#3A539B', 'tooltip': { 'valueSuffix': '%'},'marker':{'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#3A539B'} })
	
	result_list1.append({'series':series_list,'x_categories':x_categories_list})
	return HttpResponse(json.dumps(result_list1), content_type="application/json")
	#return HttpResponse(x_categories_list, content_type="application/json")


@csrf_exempt
def Trend_Of_Quality_Line(request):	
	v1=''
	radio_select=request.POST.getlist('radio_select[]')
	v1=radio_select[0]
	if v1=='$Operators':
		result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$unwind':v1},{'$group':{'_id':{'Quality_Segment':'$QualitySegment','Line':v1},'q_count':{'$sum':1} } } ,{'$project':{'_id':0,'x_categorie':'$_id.Line','name':'$_id.Quality_Segment','data':'$q_count'}},{ '$sort' : {'x_categorie': 1} } ])
	else:
		if v1=='$Resources':
			display_column='$Resource_Display'
		elif v1=='$SiteName':
			display_column='$Site_Display'
		elif v1=='$MaterialNumber':
			display_column='$Material_Display'
		elif v1=='$SBU':
			display_column='$SBU'
		elif v1=='$Technology':
			display_column='$Technology'
		elif v1=='$LineName':
			display_column='$Line_Display'
		
		#result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$group':{'_id':{'Quality_Segment':'$QualitySegment','Line':v1,'Description':display_column},'q_count':{'$sum':1} } } ,{'$project':{'_id':0,'x_categorie':'$_id.Line','Description':'$_id.Description','name':'$_id.Quality_Segment','data':'$q_count'}},{ '$sort' : {'x_categorie': 1} }])
		
		result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$group':{'_id':{'Quality_Segment':'$QualitySegment','Line':display_column},'q_count':{'$sum':1} } } ,{'$project':{'_id':0,'x_categorie':'$_id.Line','name':'$_id.Quality_Segment','data':'$q_count'}},{ '$sort' : {'x_categorie': 1} }])
	result_list=[]
	result_list1=[]
	Excellent_list=[]
	Good_list=[]
	Bad_list=[]
	Poor_list=[]
	x_categories=[]
	x_categories_list=[]
	for r in result_cursor:
		result_list.append(r)
		'''if r['x_categorie'] not in x_categories: 
			x_categories.append(r['x_categorie'])'''
		'''if 'Description' in r:
			if r['Description'] not in x_categories:
				x_categories.append(r['Description'])
		else:'''
		if r['x_categorie'] not in x_categories:
				x_categories.append(r['x_categorie'])
	for x_categorie in x_categories:
		x_categories_list.append(x_categorie)
		Excellent_flag=0
		Good_flag=0
		Bad_flag=0
		Poor_flag=0
		for result in result_list:
			if 'Description' in result:
				if(x_categorie ==result['Description']):
					if(result['name']=='A'):
						Excellent_list.append(result['data'])
						Excellent_flag=1
					elif(result['name']=='B'):
						Good_list.append(result['data'])
						Good_flag=1
					elif(result['name']=='C'):
						Bad_list.append(result['data'])
						Bad_flag=1
					elif(result['name']=='D'):
						Poor_list.append(result['data'])	
						Poor_flag=1
					else:
						print "else"
			else:
				if(x_categorie ==result['x_categorie']):
					if(result['name']=='A'):
						Excellent_list.append(result['data'])
						Excellent_flag=1
					elif(result['name']=='B'):
						Good_list.append(result['data'])
						Good_flag=1
					elif(result['name']=='C'):
						Bad_list.append(result['data'])
						Bad_flag=1
					elif(result['name']=='D'):
						Poor_list.append(result['data'])	
						Poor_flag=1
					else:
						print "else"
		if(Excellent_flag==0):
			Excellent_list.append(0)
		if(Good_flag==0):
			Good_list.append(0)
		if(Bad_flag==0):
			Bad_list.append(0)
		if(Poor_flag==0):
			Poor_list.append(0)
	series_list=[]
	series_list.append({'name':'D','data':Poor_list,'color':'#FF0000'})
	series_list.append({'name':'C','data':Bad_list,'color':'#F8963F'})
	series_list.append({'name':'B','data':Good_list,'color':'#FFFF00'})
	series_list.append({'name':'A','data':Excellent_list,'color':'#00B050'})	
	result_list1.append({'series':series_list,'x_categories':x_categories_list})
		
	return HttpResponse(json.dumps(result_list1), content_type="application/json")

@csrf_exempt
def Trend_Of_Quality_Line_benchmark(request):
	result_list=[]
	result_list1=[]
	result_list2=[]
	result_val1=[]
	result_val2=[]
	result_val3=[]
	result_val4=[]
	result_val5=[]
	result_median=[]
	date_list=[]
	Flag=''
	date_dict={}
	p=0
	q=0
	r=0
	cpk=0
	cp=0
	sd=0
	s=0
	t=0
	result_value1=[]
	result_value2=[]
	result_value3=[]
	#MaterialNumber=request.GET.getlist('product_list[]')[0]
	MaterialNumber=request.GET.get('product_list')
	qccharecter=request.GET.getlist('parameter_list[]')[0]
	plant=request.GET.get('plant_list')
	parameter='POSAPQCCharacterInfo.'+qccharecter+'.MeanValue'
	dateorderdict=OrderedDict()
	UsageDecision=['P']
	parameter1='ExOS.'+qccharecter
	Flag='ExOS'
	print "*****"
	print MaterialNumber
	print plant
	print "****"
	result_cursor=connection_history_fng_processorders().aggregate([{'$match':{'MaterialNumber':MaterialNumber,'SiteID':plant,'UsageDecision':{'$in':UsageDecision},'POSAPQCCharacterInfo':{'$ne':[]},'ProductionStartDateTime':{'$ne':''}}},{'$sort':{'ProductionStartDateTime':1}},{'$project':{'_id':0,'ProductionStartDateTime':1,'POSAPQCCharacterInfo':1}}])
	result_cursor1=connection_report_CPK().find({'MaterialNumber':MaterialNumber,'SiteID':plant},{parameter1:1,'_id':0})	
	#result_cursor1=connection_benchmark().find({'MaterialNumber':MaterialNumber,'SiteID':plant},{parameter1:1,'_id':0})	
	for result in result_cursor:
		if qccharecter in result['POSAPQCCharacterInfo']:
			x=result['ProductionStartDateTime'].strftime("%d %b'%y")
			result_list.append(x)
			#for charecter in result['POSAPQCCharacterInfo']:
			#if charecter.has_key(qccharecter):
			y=float(result['POSAPQCCharacterInfo'][qccharecter]['MeanValue'])
			result_list1.append(float(y))
	total_data={'result_dict':result_list,'result_list1':result_list1}	
	
	a=len(result_list1)			
	for result in result_cursor1:
		if(result.has_key(Flag)):
			if(result[Flag].has_key(qccharecter)):
				print 'USL'
				USL=float("{0:.4f}".format(float (result[Flag][qccharecter]["UpperLimit"])))
				Mean=float("{0:.4f}".format(result[Flag][qccharecter]["ProcessMean"]))
				LSL=float("{0:.4f}".format(float(result[Flag][qccharecter]["LowerLimit"])))
				Benchmark=(USL+LSL)/2
				median=float("{0:.4f}".format(statistics.median(result_list1)))
				if(result[Flag][qccharecter]['CpK']!=''):
					cpk=float("{0:.2f}".format(result[Flag][qccharecter]['CpK']))
				if(result[Flag][qccharecter]['Cp']!=''):
					cp=float("{0:.2f}".format(result[Flag][qccharecter]['Cp']))
				if(result[Flag][qccharecter]['ProcessSD']!=''):
					sd=float("{0:.2f}".format(result[Flag][qccharecter]['ProcessSD']))
	for i in range(0,a):
		result_val1.append(USL)
		result_val3.append(Mean)
		result_val4.append(Benchmark)
		result_val5.append(LSL)
		result_median.append(median)
	total_data={'result_dict':result_list,'result_list1':result_list1,'result_val1':result_val1,'result_val3':result_val3,'result_val4':result_val4,'result_val5':result_val5,'cpk_value':cpk,'cp_value':cp,'sd_value':sd,'Median':result_median}
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
@csrf_exempt
def quality_variation_by_line(request):
	bar_dict={}
	result_data_dict={}
	series=[]
	v1=''
	radio_select=request.POST.getlist('radio_select[]')	
	v1=radio_select[0]
	bar_data=[]
	spline_data=[]
	x_categories=[]
	hover_data=[]
	if v1=='$Operators':
		result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$unwind':v1},{'$group': {'_id': {'categories':v1},"EG_Count": { "$sum": { '$cond':[{'$or':[{'$eq':[ "$QualitySegment", "A" ]},{'$eq':[ "$QualitySegment", "B" ]}]},1,0]}},"TotCount": { "$sum": 1 }}}, {'$project':{"_id":0,"Material_Display":"$Material_Display","categories":"$_id.categories","TotalCount":"$TotCount","Quality_Score": { '$cond': [{ '$eq': [ "$EG_Count", 0 ] },0,{ '$multiply':[{'$divide': [ "$EG_Count", "$TotCount" ]} ,100]}]}}},{ '$sort':{'categories': 1}}])
	else:
		#result_cursor=connection_reportingtable().aggregate([ get_pipeline(request),{'$group': {'_id': {'categories':v1},"EG_Count": { "$sum": { '$cond':[{'$or':[{'$eq':[ "$QualitySegment", "A" ]},{'$eq':[ "$QualitySegment", "B" ]}]},1,0]}},"TotCount": { "$sum": 1 }}}, {'$project':{"_id":0,"categories":"$_id.categories","TotalCount":"$TotCount","Quality_Score": { '$cond': [{ '$eq': [ "$EG_Count", 0 ] },0,{ '$multiply':[{'$divide': [ "$EG_Count", "$TotCount" ]} ,100]}]}}},{ '$sort':{'categories': 1}} ])
		if v1=='$Resources':
			display_column='$Resource_Display'
		elif v1=='$SiteName':
			display_column='$Site_Display'
		elif v1=='$MaterialNumber':
			display_column='$Material_Display'
		elif v1=='$SBU':
			display_column='$SBU'
		elif v1=='$Technology':
			display_column='$Technology'
		elif v1=='$LineName':
			display_column='$Line_Display'
		result_cursor=connection_reportingtable().aggregate([get_pipeline(request),{'$unwind':display_column},{'$group':{'_id': {'categories':display_column,'description':display_column},"EG_Count": { "$sum": { '$cond':[{'$or':[{'$eq':[ "$QualitySegment", "A" ]},{'$eq':[ "$QualitySegment", "B" ]}]},1,0]}},"TotCount": { "$sum": 1 }}},{'$project':{"_id":0,"description":"$_id.description","categories":"$_id.categories","TotalCount":"$TotCount","Quality_Score": { '$cond': [{ '$eq': [ "$EG_Count", 0 ] },0,{ '$multiply':[{'$divide': [ "$EG_Count", "$TotCount" ]} ,100]}]}}},{ '$sort':{'description': 1}}])
	for row in result_cursor:
		if 'description' in row:
			hover_data.append(row['description'])
		if 'description' in row:
			x_categories.append(row['description'])
		else:
			x_categories.append(row['categories'])
		spline_data.append(int(row['Quality_Score']))
		bar_data.append(row['TotalCount'])	
		
		
	if v1=='$Operators':
		series=[{'name':'ProcessOrders','type':'column','color':'#6bafbd','data':bar_data,'tooltip':{'valueSuffix': ' '}},{'name':'QualityScore','type':'spline','color':'#3A539B','data':spline_data,'tooltip':{'valueSuffix': '%'},'yAxis':1,'marker':{'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#3A539B'} }  ]
	else:
		series=[{'name':'ProcessOrders','type':'column','color':'#6bafbd','display1':'jhkgfkh','data':bar_data,'tooltip':{ 'headerFormat': '<small>{point.key}</small><br>','valueSuffix': ' '}},{'name':'QualityScore','type':'spline','display1':'jhkgfkh','color':'#3A539B','data':spline_data,'tooltip':{'valueSuffix': '%'},'yAxis':1,'marker':{'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#3A539B'} }  ]
	result_data_dict['categories']=x_categories
	result_data_dict['series']=series
	return HttpResponse(json.dumps(result_data_dict), content_type="application/json")

	
	
	
@csrf_exempt
def get_process_order(request):
	pipeline=get_filter_pipeline(request)
	product_list=[]
	date=''
	date_list=[]
	data_list=request.POST.getlist('date_list[]')
	for date in data_list:
			date_list.append(date)
	if(len(date_list)>=2):
		pipeline['ProductionStartDateTime']={'$lt':datetime.datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d')}
	else:
		pipeline['ProductionStartDateTime']=datetime.datetime.strptime(date_list[0], '%Y-%m-%d')
		
	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	for data in data_list:
			product_list.append(data)
	if len(data_list)>1:
		pipeline['MaterialNumber']={'$in':product_list } 
	else:	
		pipeline['MaterialNumber']=product_list[0]
	pipeline['QualitySegment']={'$ne':''}
	r=connection_reportingtable().distinct('ProcessOrder',pipeline)
	po_count=len(r)
	return HttpResponse(json.dumps(po_count), content_type="application/json")

	
@csrf_exempt
def get_rejected_orders(request):
	page_view=request.POST.get('page_view')
	if (page_view=='cpk_view'):
		pipeline={}
		data_list=[]
		plant_list=[]
		product_list=[]
		data_list=request.POST.getlist('product_list[]')
		for data in data_list:
			product_list.append(data)
		if len(data_list)>1:
			pipeline['MaterialNumber']={'$in':product_list } 
		else:	
			pipeline['MaterialNumber']=product_list[0]
		data_list=[]
		data_list=request.POST.getlist('plant_list[]')
		for data in data_list:
				plant_list.append(data)
		if len(data_list)>1:
			pipeline['SiteID']={'$in':plant_list } 
		else:	
			pipeline['SiteID']=plant_list[0]
		pipeline['UsageDecision']='R'
	else:
		pipeline=get_filter_pipeline(request)
		#pipeline['ResourceID']=pipeline['ResourceName']
		del pipeline['ResourceName']
	
	if (page_view!='cpk_view'):
		
		product_list=[]
		date=''
		date_list=[]
		data_list=request.POST.getlist('date_list[]')
		for date in data_list:
				date_list.append(date)
		if(len(date_list)>=2):
			pipeline['ProductionStartDateTime']={'$lt':datetime.datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d')}
		else:
			#pipeline['ProductionStartDateTime']=datetime.datetime.strptime(date_list[0], '%Y-%m-%d')
			date_to=datetime.datetime.strptime(date_list[0], '%Y-%m-%d')+timedelta(days=1)
			pipeline['ProductionStartDateTime']={'$lt':date_to,'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d')}
			
		'''
		data_list=[]
		data_list=request.POST.getlist('product_list[]')
		for data in data_list:
				product_list.append(data)
		if len(data_list)>1:
			pipeline['MaterialNumber']={'$in':product_list } 
		else:	
			pipeline['MaterialNumber']=product_list[0]
		pipeline['UsageDecision']='R'
		pipeline['LineID']=pipeline['LineID']
		#del pipeline['LineID']
		pipeline['ResourceName']=pipeline['Resources']
		#del pipeline['Resources']
		pipeline['SiteID']=pipeline['SiteID']
		'''
		#del pipeline['SiteID']
		pipeline['UsageDecision']='R'
		logging.info([pipeline,{'SiteName':1,'SBU':1,'Technology':1, 'BatchNumber':1 ,'MaterialNumber':1,'ResourceName':1,'PlineName':1,'ProductionStartDateTime':1}])
		#r=connection_src_processorders().find(pipeline).count()
		#result_cursor=connection_src_processorders().find(pipeline,{'SiteName':1,'SBU':1,'Technology':1, 'BatchNumber':1 ,'MaterialNumber':1,'ResourceName':1,'PlineName':1,'ProductionStartDateTime':1})
		result_cursor=connection_history_fng_processorders().find(pipeline,{'SiteName':1,'SiteID':1,'SBU':1,'Technology':1,'MaterialNumber':1,'ExistsInCorrelator':1,'BatchNumber':1 ,'Material_Display':1,'ResourceName':1,'LineName':1,'ProductionStartDateTime':1})
		rejected_list=[]
		for result in result_cursor:
			l=[]
			l.append(result['SiteName'])
			l.append(result['SBU'])
			l.append(result['Technology'])
			l.append(result['Material_Display'])
			l.append(result['LineName'])
			l.append(result['ResourceName'])
			l.append(result['BatchNumber'])
			l.append(result['ProductionStartDateTime'].strftime('%y-%m-%d'))
			l.append(result['MaterialNumber'])
			l.append(result['SiteID'])
			l.append(result['ExistsInCorrelator'])
			rejected_list.append(l)
		return HttpResponse(json.dumps(rejected_list), content_type="application/json")	
		
	else:
#		result_cursor=connection_src_processorders().find(pipeline,{'SiteName':1,'SBU':1,'Technology':1, 'BatchNumber':1 ,'MaterialNumber':1,'ResourceName':1,'PlineName':1,'ProductionStartDateTime':1})
		result_cursor=connection_history_fng_processorders().find(pipeline,{'SiteName':1,'SiteID':1,'SBU':1,'Technology':1, 'BatchNumber':1,'MaterialNumber':1,'ExistsInCorrelator':1,'Material_Display':1,'ResourceName':1,'PlineName':1,'ProductionStartDateTime':1})
		rejected_list=[]
		for result in result_cursor:
			l=[]
			l.append(result['SiteName'])
			l.append(result['SBU'])
			l.append(result['Technology'])
			l.append(result['Material_Display'])
			l.append(result['PlineName'])
			l.append(result['ResourceName'])
			l.append(result['BatchNumber'])
			l.append(result['ProductionStartDateTime'].strftime('%y-%m-%d'))
			l.append(result['MaterialNumber'])
			l.append(result['SiteID'])
			l.append(result['ExistsInCorrelator'])
			rejected_list.append(l)
		return HttpResponse(json.dumps(rejected_list), content_type="application/json")	

@csrf_exempt
def get_process_order_poor(request):
	pipeline=get_filter_pipeline(request)
	product_list=[]
	date=''
	date_list=[]
	data_list=request.POST.getlist('date_list[]')
	for date in data_list:
			date_list.append(date)
	if(len(date_list)>=2):
		pipeline['ProductionStartDateTime']={'$lt':datetime.datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d')}
	else:
		pipeline['ProductionStartDateTime']=datetime.datetime.strptime(date_list[0], '%Y-%m-%d')
		
	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	for data in data_list:
			product_list.append(data)
	if len(data_list)>1:
		pipeline['MaterialNumber']={'$in':product_list } 
	else:	
		pipeline['MaterialNumber']=product_list[0]
	pipeline['QualitySegment']={'$ne':''}
	pipeline['QualitySegment']='P'	
	r=connection().distinct('ProcessOrder',pipeline)
	po_count=len(r)
	return HttpResponse(json.dumps(po_count), content_type="application/json")
	
@csrf_exempt
def quality_trend_onclick(request):
	date=''
	date_list=[]
	q_total_line={}
	q_total_plant={}
	q_total_date={}
	by_line_data=[]
	by_plant_data=[]
	by_date_data=[]
	total_data=[]
	filter_pipeline={}
	data_list=[]
	pipeline=get_pipeline(request)
	data_list=[]
	data_list=request.POST.getlist('segment_list[]')
	segment=data_list[0].encode('utf-8')
	pipeline['$match']['$and'].append({'QualitySegment':segment})
	
	linechart_color=''		
	if(segment=='A'):		
		linechart_color='#00B050'		
	elif(segment=='B'):		
		linechart_color='#FFFF00'		
	elif(segment=='C'):		
		linechart_color='#F8963F'		
	elif(segment=='D'):		
		linechart_color='#FF0000'		



#------------------Query for Line--------------------#	
	
	
	result_cursor2=connection_reportingtable().aggregate([ pipeline, {'$group':{'_id':{'LineName':'$Line_Display','Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1} } },{'$project':{'_id':0,'name':'$_id.Quality_Segment','data':'$q_count','LineName':'$_id.LineName'}} ])
#------------------Query for Plant--------------------#	
	
	
	result_cursor4=connection_reportingtable().aggregate([ pipeline,  {'$group':{'_id':{'SiteName':'$SiteName','Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1} } },  {'$group':{'_id':{'SiteName':'$_id.SiteName'},'q_count':{'$sum':'$q_count'} } }, {'$project':{'_id':0,'name':'$_id.Quality_Segment','data':'$q_count','SiteName':'$_id.SiteName'}} ])
	
#------------------Query for Date--------------------#
	try:
		result_cursor6=connection_reportingtable().aggregate([ pipeline,{ "$group": {  "_id": { "day": { "$dayOfMonth" : "$ProductionStartDateTime" },"month": { "$month" : "$ProductionStartDateTime" }, "year": { "$year" : "$ProductionStartDateTime" }},"Count": { "$sum" : 1 } } }, {  "$sort": { "_id.year": 1, "_id.month": 1,"_id.day": 1} },{ "$project": {  "_id": 1,  "dateDay": { "$concat": [ {"$substr" : [ "$_id.day", 0, 2]}, "-",{"$substr" : [ "$_id.month", 0, 2]}, "-", {"$substr" : [ "$_id.year", 0, 4]} ] }, "Count": 1}}])	
	
	
		add_list = []
		for re in result_cursor6:
			processedDict = re
			processedDict['Date']=datetime.datetime(re['_id']['year'],re['_id']['month'],re['_id']['day'],0,0)
			add_list.append(processedDict)
		
		add_list1 = sorted(add_list, key=itemgetter('Date'))
				
		print '******'
		print len(add_list1)
		#add_list1 = add_list.sort(key=itemgetter('dateDay'))	
		#add_list1 = sorted(add_list, key=itemgetter('dateDay')) 	
		#print len(add_list1)
	#-----------for trend chart------------#			
		categories=[]
		data=[]
		temp_dict={}
		series=[]
		for r in add_list1:
			data.append(r['Count'])
			categories.append(r['dateDay'])
		temp_dict['name']='Quality Trend'
		temp_dict['data']=data
		temp_dict['color']=linechart_color
		temp_dict['marker']={'fillColor': '#FFFFFF','lineWidth':1,'lineColor':linechart_color} 
		series.append(temp_dict)
		temp_dict={}
		temp_dict['series']=series
		temp_dict['categories']=categories
		by_date_data.append(temp_dict)
	except:
		categories=[]
		data=[]
		temp_dict={}
		series=[]		
		temp_dict['name']='Quality Trend'
		temp_dict['data']=data
		temp_dict['color']='#DA484B'
		temp_dict['marker']={'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#DA484B'} 
		series.append(temp_dict)
		temp_dict={}
		temp_dict['series']=series
		temp_dict['categories']=categories
		by_date_data.append(temp_dict)
#-----------for Donut Chart By Line------------#	
	color_list=['#327e96','#f2af37','#b22c20','#fdd201','#FF9800','#90CAF9','#CDDC39','#327e96','#f2af37','#b22c20','#fdd201','#FF9800','#90CAF9','#CDDC39','#327e96','#f2af37','#b22c20','#fdd201','#FF9800','#90CAF9','#CDDC39','#327e96','#f2af37','#b22c20','#fdd201','#FF9800','#90CAF9','#CDDC39']
	counter = 0	
	for r in result_cursor2:
		temp_dict={}
		temp_dict['name']=r['LineName']
		temp_dict['y']=r['data']
		#temp_dict['color']=color_list[counter]		
		counter = counter+1
		by_line_data.append(temp_dict)
		
#-----------for Donut Chart By Plant------------#		
	counter1 = 0
	for r in result_cursor4:
		temp_dict={}
		temp_dict['name']=r['SiteName']
		temp_dict['y']=r['data']
		#temp_dict['color']=color_list[counter1]		
		counter1 = counter1+1
		by_plant_data.append(temp_dict)

	total_data=[{'By_Line':by_line_data,'By_Plant':by_plant_data,'By_date':by_date_data}]
	return HttpResponse(json.dumps(total_data), content_type="application/json")
@csrf_exempt	
def get_top_pos(request):
	pipeline=get_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	#encrpyting username		
	encrpytion_key = "71079162"		
	username = request.session['username']		
	encrypted_username = AESencrypt(encrpytion_key, username)
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')	
	data_list=[]
	data_list=request.POST.getlist('segment_list[]')
	segment_list=data_list
	if(len(data_list)>=2):
		pipeline['$match']['$and'].append({'QualitySegment':{'$in':segment_list} })
	else:
		pipeline['$match']['$and'].append({'QualitySegment':segment_list[0].encode('utf-8')})	
	download_pipeline=pipeline
	if type=='total':
		if page_view=='grid_view':
				batchnumber=request.POST.get('batchnumber')
				logging.info(batchnumber)
				if(batchnumber!='all'):					
					pipeline['$match']['$and'].append({'BatchNumber':batchnumber.encode('utf-8')})
					logging.info(pipeline)
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','po':'$BatchNumber','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
				result_cursor1=connection_reportingtable().aggregate([ pipeline,{'$unwind': { 'path': "$Operators", 'preserveNullAndEmptyArrays':True }},{'$project':{'_id':0,'QualitySegment':1,'SiteName':1,'ProcessOrder':1, 'SBU':1, 'Technology':1, 'MaterialNumber':1, 'LineName':1, 'Resource_Display':1, 'Operators':1, 'BatchNumber':1, 'AvgQualityScore':1,'Material_Display':1}}])
	
		elif page_view!='product_view':
			if (segment_list[0]=='A'or segment_list[0]=='B'):
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
				result_cursor1=connection_reportingtable().aggregate([ pipeline,{'$unwind': { 'path': "$Operators", 'preserveNullAndEmptyArrays':True }},{'$project':{'_id':0,'QualitySegment':1,'SiteName':1,'ProcessOrder':1, 'SBU':1, 'Technology':1, 'MaterialNumber':1, 'LineName':1, 'Resource_Display':1, 'Operators':1, 'BatchNumber':1, 'AvgQualityScore':1,'Material_Display':1}}  ,{'$sort':{'AvgQualityScore':1}}])
			else:
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
				result_cursor1=connection_reportingtable().aggregate([ pipeline,{'$unwind': { 'path': "$Operators", 'preserveNullAndEmptyArrays':True }},{'$project':{'_id':0,'QualitySegment':1,'SiteName':1, 'SBU':1,'ProcessOrder':1,'Technology':1, 'MaterialNumber':1, 'LineName':1, 'Resource_Display':1, 'Operators':1, 'BatchNumber':1, 'AvgQualityScore':1,'Material_Display':1}}])
		
		
		else:
			if (segment_list[0]=='A'or segment_list[0]=='B'):
				pipeline['$match']['$and'].append({'OutputScore':{'$ne':''}})
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
				#result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}},{'$sort':{'avg':1}}])
				result_cursor1=connection_reportingtable().aggregate([pipeline,{'$unwind': { 'path': "$Operators", 'preserveNullAndEmptyArrays':True }},{'$project':{'_id':0,'QualitySegment':1,'SiteName':1,'LineName':1, 'Resource_Display':1, 'ProcessOrder':1, 'SBU':1, 'Technology':1, 'MaterialNumber':1, 'BatchNumber':1, 'OutputScore':1,'AvgQualityScore':1,'ProductionStartDateTime':1,'Material_Display':1}}])
			else:
				pipeline['$match']['$and'].append({'OutputScore':{'$ne':''}})
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
				#result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}},{'$sort':{'avg':-1}} ])
				result_cursor1=connection_reportingtable().aggregate([ pipeline,{'$unwind': { 'path': "$Operators", 'preserveNullAndEmptyArrays':True }},{'$project':{'_id':0,'QualitySegment':1,'SiteName':1,'LineName':1, 'Resource_Display':1, 'ProcessOrder':1, 'SBU':1, 'Technology':1, 'MaterialNumber':1, 'BatchNumber':1, 'OutputScore':1,'AvgQualityScore':1,'ProductionStartDateTime':1,'Material_Display':1}}])
	else:
		if (segment_list[0]=='A'or segment_list[0]=='B'):
			result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}},{ '$limit' : 5 } ])
		else:
			result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}  ,{ '$limit' : 5 } ])
	po_list=[]
	product_list=[]
	productname_list=[]
	po_avgscr=[]
	po_segment=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_productiondate_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	if (type=='total' ):
		if(page_view!='product_view'):
			for r in result_cursor1:
				deatil_list=[]
				deatil_list.append(r['QualitySegment'])
				deatil_list.append(r['SiteName'])
				deatil_list.append(r['SBU'])
				deatil_list.append(r['Technology'])
				deatil_list.append(r['MaterialNumber'])
				deatil_list.append(r['Material_Display'])
				deatil_list.append(r['LineName'])
				if(r.has_key('Resource_Display')):			
					deatil_list.append(r['Resource_Display'])
				else:
					deatil_list.append(0)
				#deatil_list.append(r['Resources'][0])
				if(r.has_key('Operators')):			
					deatil_list.append(r['Operators'])
				else:
					deatil_list.append(0)
				deatil_list.append(r['BatchNumber'])
				deatil_list.append(r['ProcessOrder'])
				deatil_list.append(float("{0:.4f}".format(r['AvgQualityScore'])))
				deatils_list.append(deatil_list)
		else:
			data_list=[]
			data_list=request.POST.getlist('product_list[]')
			if len(data_list)>1:
				for data in data_list:
					product_list.append(data)
			else:	
				product=data_list[0].encode('utf-8')
			plant_list=[]	
			data_list=[]
			data_list=request.POST.getlist('plant_list[]')
			for data in data_list:
					plant_list.append(data)
			if len(data_list)>1:
				parameter={'$in':plant_list } 
			else:	
				parameter=plant_list[0]				
					
			conn=pymongo.MongoClient('localhost', 27017)
			cursor=conn.henkelProduction
			cursor.authenticate('henkelUser_Prod','QpQc#123')
			header_cursor =conn.henkelProduction.config_fng_opquality_weights.find_one({'SiteID':parameter,'MaterialNumber':product}, {'OPWeights':1, '_id':0})
			for k,v in header_cursor['OPWeights'].iteritems():
				if v != 0:
					excel_header.append(k)		
			for r in result_cursor1:
				deatil_list=[]
				deatil_list.append(r['ProductionStartDateTime'].strftime('%d-%m-%Y'))
				deatil_list.append(r['QualitySegment'])
				deatil_list.append(r['SiteName'])
				deatil_list.append(r['SBU'])
				deatil_list.append(r['Technology'])
				deatil_list.append(r['MaterialNumber'])
				deatil_list.append(r['Material_Display'])
				deatil_list.append(r['LineName'])
				#deatil_list.append(r['Resources'][0])
				if(r.has_key('Resource_Display')):			
					deatil_list.append(r['Resource_Display'])
				else:
					deatil_list.append(0)
				deatil_list.append(r['BatchNumber'])
				deatil_list.append(r['ProcessOrder'])
				for header in excel_header:
					if(r['OutputScore'].has_key(header)):
						if r['OutputScore'][header]['ActualValue']!='':	
							deatil_list.append(r['OutputScore'][header]['ActualValue'])
						else:
							deatil_list.append('-NA-')
				deatil_list.append(float("{0:.4f}".format(r['AvgQualityScore'])))
				deatils_list.append(deatil_list)
				deatils_list.sort(key=lambda x: x[-1])
			excel_header.insert(0,'ProductionStartDateTime')
			excel_header.insert(1,'Segment')
			excel_header.insert(2,'SiteName')
			excel_header.insert(3,'SBU')
			excel_header.insert(4,'Technology')
			excel_header.insert(5,'MaterialNumber')
			excel_header.insert(6,'MaterialName')
			excel_header.insert(7,'LineName')
			excel_header.insert(8,'Resources')
			excel_header.insert(9,'BatchNumber')
			excel_header.insert(10,'ProcessOrder')
			excel_header.append('AvgQualityScore')
			
	for r in result_cursor:
		po_list.append(r['po'])
		product_list.append(r['product'])
		po_avgscr.append(float("{0:.4f}".format(r['avg'])))
		processorder_list.append(r['ProcessOrder'])
		plantid_list.append(r['SiteName'])
		po_siteid_list.append(r['SiteID'])
		po_technology_list.append(r['Technology'])
		po_sbu_list.append(r['SBU'])	
		po_productiondate_list.append(r['ProductionStartDateTime'].strftime('%Y-%m-%d'))
		po_segment.append(r['QualitySegment'])	
	temp_dict['po_numbers']=po_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_productiondate']=po_productiondate_list
	temp_dict['product_numbers']=product_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_score']=po_avgscr
	temp_dict['po_segment']=po_segment
	temp_dict['app'] = '20263146'		
	temp_dict['username'] = encrypted_username
	temp_dict['download_list']=deatils_list
	temp_dict['excel_header']=excel_header
	temp_dict['color']='#F3778B'
	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po), content_type="application/json")



@csrf_exempt	
def get_top_five_pos(request):
	pipeline=get_pipeline(request)
	processorder_list=[]
	encrpytion_key = "71079162"		
	username = request.session['username']		
	encrypted_username = AESencrypt(encrpytion_key, username)
	plantid_list=[]
	segment=request.POST.getlist('segment_list[]')[0]
	pipeline['$match']['$and'].append({'QualitySegment':segment.encode('utf-8')})	
	if (segment=='A'or segment=='B'):
		result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}  ,{'$sort':{'avg':1}},{ '$limit' : 5 } ])
	else:
		result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}  ,{'$sort':{'avg':-1}},{ '$limit' : 5 } ])
	batchNumber_list=[]
	product_list=[]
	po_avgscr=[]
	po_segment=[]
	po_siteid_list=[]
	po_sitename_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_productiondate_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	mes_exist=[]
	OrderType=[]
	cor_exist=[]
	for r in result_cursor:
		batchNumber_list.append(r['BatchNumber'])
		product_list.append(r['product'])
		#po_avgscr.append(r['avg'])
		po_avgscr.append(float("{0:.4f}".format(r['avg'])))
		processorder_list.append(r['ProcessOrder'])
		po_sitename_list.append(r['SiteName'])
		po_siteid_list.append(r['SiteID'])
		po_technology_list.append(r['Technology'])
		po_sbu_list.append(r['SBU'])	
		mes_exist.append(r['ExecutionInfoExistsInMES'])	
		if(r['ProductionStartDateTime']!=''):
			po_productiondate_list.append(r['ProductionStartDateTime'].strftime('%Y-%m-%d'))
		else:
			po_productiondate_list.append(r['ProductionStartDateTime'])
		po_segment.append(r['QualitySegment'])	
		OrderType.append(r['OrderType'])
		cor_exist.append(r['ExistsInCorrelator'])
	temp_dict['batchnumbers']=batchNumber_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['mes_exist']=mes_exist
	temp_dict['cor_exist']=cor_exist
	temp_dict['po_productiondate']=po_productiondate_list
	temp_dict['product_numbers']=product_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['po_sitename']=po_sitename_list
	temp_dict['po_score']=po_avgscr
	temp_dict['app'] = '20263146'		
	temp_dict['username'] = encrypted_username
	temp_dict['po_segment']=po_segment
	temp_dict['color']='#F3778B'
	top_po.append(temp_dict)
	temp_dict['OrderType']=OrderType
	return HttpResponse(json.dumps(top_po), content_type="application/json")



	
@csrf_exempt
def get_pos_download(request):
	return HttpResponse(json.dumps(download_pipeline), content_type="application/json")
	#return download_pipeline


@csrf_exempt
def cpk_graph(request):
	result_list=[]
	result_list1=[]
	result_list2=[]
	result_val1=[]
	result_val2=[]
	result_val3=[]
	result_val4=[]
	result_val5=[]
	result_median=[]
	date_list=[]
	Flag=''
	date_dict={}
	p=0
	q=0
	r=0
	cpk=0
	cp=0
	sd=0
	s=0
	t=0
	result_value1=[]
	result_value2=[]
	result_value3=[]
	MaterialNumber=request.GET.getlist('product_list[]')[0]
	qccharecter=request.GET.getlist('parameter_list[]')[0]
	plant=request.GET.getlist('plant_list[]')[0]
	cpk_start_date=request.GET.get('cpk_start_date')
	cpk_end_data=request.GET.get('cpk_end_data')
	parameter='POSAPQCCharacterInfo.'+qccharecter+'.MeanValue'
	radio_option=request.GET.get('radio_option')
	dateorderdict=OrderedDict()
	dateorderdict['$lte']=datetime.datetime.strptime(cpk_end_data, '%Y-%m-%d')
	dateorderdict['$gte']=datetime.datetime.strptime(cpk_start_date, '%Y-%m-%d')
	if radio_option=='on':
		parameter1='InOS.'+qccharecter
		UsageDecision=['P','R']
		Flag='InOS'
	else:
		UsageDecision=['P']
		parameter1='ExOS.'+qccharecter
		Flag='ExOS'
	result_cursor=connection_history_fng_processorders().aggregate([{'$match':{'MaterialNumber':MaterialNumber,'SiteID':plant,'UsageDecision':{'$in':UsageDecision},'POSAPQCCharacterInfo':{'$ne':[]},'ProductionStartDateTime':{'$ne':''}}},{'$sort':{'ProductionStartDateTime':1}},{'$project':{'_id':0,'ProductionStartDateTime':1,'POSAPQCCharacterInfo':1}}])
	result_cursor1=connection_report_CPK().find({'MaterialNumber':MaterialNumber,'SiteID':plant},{parameter1:1,'_id':0})
	for result in result_cursor:
		#for charecter in result['POSAPQCCharacterInfo']:
		if result['POSAPQCCharacterInfo'].has_key(qccharecter):
			y=float(result['POSAPQCCharacterInfo'][qccharecter]['MeanValue'])
			x=result['ProductionStartDateTime'].strftime("%d %b'%y")
			result_list.append(x)
			result_list1.append(float(y))
		total_data={'result_dict':result_list,'result_list1':result_list1}	
	a=len(result_list1)			
	
	for result in result_cursor1:		
		if(result.has_key(Flag)):
			
			if(result[Flag].has_key(qccharecter)):
				USL=float("{0:.4f}".format(float (result[Flag][qccharecter]["UpperLimit"])))
				Mean=float("{0:.4f}".format(result[Flag][qccharecter]["ProcessMean"]))
				#Benchmark=float("{0:.2f}".format(result[Flag][qccharecter]["Benchmark"]))
				LSL=float("{0:.4f}".format(float(result[Flag][qccharecter]["LowerLimit"])))
				if result[Flag][qccharecter]['ProcessSD']<>'null':
					if(result[Flag][qccharecter]['CpK']!=''):
						cpk=float("{0:.2f}".format(result[Flag][qccharecter]['CpK']))
					if(result[Flag][qccharecter]['Cp']!=''):
						cp=float("{0:.2f}".format(result[Flag][qccharecter]['Cp']))
					if(result[Flag][qccharecter]['ProcessSD']!=''):
						sd=float("{0:.2f}".format(result[Flag][qccharecter]['ProcessSD']))

	for i in range(0,a):
		result_val1.append(USL)
		result_val3.append(Mean)
		#result_val4.append(Benchmark)
		result_val5.append(LSL)
	total_data={'result_dict':result_list,'result_list1':result_list1,'result_val1':result_val1,'result_val3':result_val3,'result_val4':result_val4,'result_val5':result_val5,'cpk_value':cpk,'cp_value':cp,'sd_value':sd}
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
	
	

@csrf_exempt
def cpk_graph1(request):
	data_list=[]
	data_list1=[]
	date_list=[]
	date=''
	date_dict={}
	para_list=[]
	year_list=[]
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	MaterialNumber=request.GET.getlist('product_list[]')[0]
	qccharecter=request.GET.getlist('parameter_list[]')[0]
	plant=request.GET.getlist('plant_list[]')[0]
	data_list=request.GET.getlist('year_list[]')
	radio_option=request.GET.get('radio_option')
	for data in data_list:
		year_list.append(int(data))
	
	if(len(year_list)>1):
		year={'$in':year_list}
	else:
		year=year_list[0]
	
	time_scale=request.GET.getlist('time_scale[]')
	if(time_scale[0]=='Month'):
			table_cursor=conn.henkelProduction.report_fng_opquality_monthlyqcvalues	
			column_name='Month'
	elif(time_scale[0]=='Quarter'):
			table_cursor=conn.henkelProduction.report_fng_opquality_quarterlyqcvalues
			column_name='Quarter'
	elif(time_scale[0]=='Half'):
			table_cursor=conn.henkelProduction.report_fng_opquality_halfyearlyqcvalues
			column_name='Half'
	else:
		table_cursor=conn.henkelProduction.report_fng_opquality_yearlyqcvalues
		column_name='Year'
	if radio_option=='on':
		Flag='InOS'
	else:
		Flag='ExOS'	
	parameter='Output.'+qccharecter+'.CpK'
	parameter1='Output.'+qccharecter+'.ProcessSD'
	sorted_dict=OrderedDict()
	sorted_dict['Year']=1
	sorted_dict[column_name]=1
	result_cpk=table_cursor.aggregate([{'$match':{'MaterialNumber':MaterialNumber,'SiteID':plant,'Flag':Flag,'Output':{'$ne':{} },parameter:{'$ne':{}},parameter:{'$exists':'true'},'Year':year}},{'$sort':sorted_dict},{'$project':{'MaterialNumber':1,'Year':1,column_name:1,parameter:1,parameter1:1,'_id':0}}])
	result_list1=[]
	result_list2=[]
	result_list3=[]
	arr=['a','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	for result in result_cpk:
		if(column_name=='Year'):
			x=str(int(result[column_name]))
		else:
			if(column_name=='Month'):
				x=str(arr[int(result[column_name])])+"'"+str(int(result['Year']))[2:]
			else:
				x=str(result[column_name])+"'"+str(int(result['Year']))[2:]
		z=result['Output'][qccharecter]['CpK']
		u=result['Output'][qccharecter]['ProcessSD']
		'''if z == '':
			z = None
		else:
			#z=float("{0:.2f}".format(z))
			z=float("%.2f" % z)
		if u == '':
			u = None
		else:
			#u=float("{0:.2f}".format(u))
			u=float("%.2f" % u)'''
		if z!= None:
			#z=float("{0:.2f}".format(z))
			z=float("%.2f" % z)
		if u!= None:
			#u=float("{0:.2f}".format(u))
			u=float("%.4f" % u)
		result_list1.append(x)
		result_list2.append(z)
		result_list3.append(u)	
	total_data ={'result_xaxis':result_list1,'result_cpk':result_list2,'result_sd':result_list3}
	
	return HttpResponse(json.dumps(total_data), content_type="application/json")
		
@csrf_exempt
def cpk_parameter_data(request):	
	parameter_list={}
	MaterialNumber=request.GET.get('product_list')
	plant=request.GET.getlist('plant_list[]')[0]
	para_result=connection_weights().find_one({'MaterialNumber':MaterialNumber,'SiteID':plant,'ActiveFlag':1},{'OPWeights':1, '_id':0})	
	for k,v in para_result['OPWeights'].iteritems():
		if v['Weight'] != 0:
			parameter_list[k]=k	
	total_data={'parameter_list':parameter_list}
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
	
@csrf_exempt	
def cpk_onclick_parameter(request):
	parameter_list=[]
	parameter_value=[]
	segment=request.GET.getlist('segment_list[]')[0]
	MaterialNumber=request.GET.getlist('product_list[]')[0]
	plant=request.GET.getlist('plant_list[]')[0]
	result_cursor=connection_report_CPK().find({'MaterialNumber':MaterialNumber,'SiteID':plant},{'Overall':1})
	resultdict={}
	for r in result_cursor:
		if(r.has_key('Overall')):
			if(r['Overall'].has_key(segment)):
				for k,v in r['Overall'][segment].iteritems():
					for kk,vv in v.iteritems():
						if(kk=='CpK'):
							x=float("{0:.2f}".format(vv))
							parameter_value.append(x)
							parameter_list.append(k)
	total_data={'parameter_list':parameter_list,'parameter_value':parameter_value}	
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
	
	
@csrf_exempt	
def cpk_download(request):
	plant=request.GET.getlist('plant_list[]')[0]
	MaterialNumber=request.GET.getlist('product_list[]')[0]
	result_cursor=connection_report_CPK().find({'MaterialNumber':MaterialNumber,'SiteID':plant},{'Overall':1})
	data=[]
	for r in result_cursor:
		if(r.has_key('Overall')):
			for k,v in r['Overall'].items():
				if(k in ['A','B','C','D']):
					for kk,vv in v.items():
						if vv !=None:
							for kkk,vvv in vv.items():
								if(kkk=='CpK' and vvv!=None):
									new_dict={}
									new_dict['SiteID'] = plant
									new_dict['MaterialNumber']=MaterialNumber
									new_dict['QSeg'] = k
									new_dict['Par'] = kk
									new_dict['Cpk'] = float("{0:.2f}".format(vvv))
									new_dict['Count'] = vv['Count']
									data.append(new_dict)
	return HttpResponse(json.dumps(data), content_type="application/json")
		
@csrf_exempt	
def Overall_Quality_cpk(request):
	result_list=[]
	data_list=request.GET.getlist('plant_list[]')
	data_list1=request.GET.getlist('product_list[]')
	result_cursor=connection_reportingtable().aggregate([{'$match':{'SiteID':data_list[0], 'MaterialNumber':data_list1[0]}},{'$group':{'_id':{'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.Quality_Segment','q_count':1 }}])
	for result in result_cursor:
		result_dict={}
		if result['q_segment']=='A':
			result_dict['name']='A'
			result_dict['y']=result['q_count']
			result_dict['color']='#00B050'
			result_list.append(result_dict)		
		if result['q_segment']=='B':
			result_dict['name']='B'
			result_dict['y']=result['q_count']
			result_dict['color']='#FFFF00'
			result_list.append(result_dict)		
		if result['q_segment']=='C':
			result_dict['name']='C'
			result_dict['y']=result['q_count']
			result_dict['color']='#F8963F'
			result_list.append(result_dict)		
		if result['q_segment']=='D':
			result_dict['name']='D'
			result_dict['y']=result['q_count']
			result_dict['color']='#FF0000'
			result_list.append(result_dict)	
	return HttpResponse(json.dumps(sorted(result_list,key = lambda result_list:(result_list['name']))),content_type="application/json")		
	
	
	
	
@csrf_exempt
def customer_complaints_cpk(request):
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.report_customer_complaints	
	cnt=0
	plant=request.GET.get('plant_list').encode('utf-8')
	product=request.GET.get('product_list').encode('utf-8')
	result_list=[]
	query=[{'$match':{'$and':[{'MaterialNumber':product },{'SiteID':plant }]}},{'$group':{'_id':0,'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_count':1 }} ] 
	logging.info("******************************query for customer complaint***************************************")
	logging.info(query)
	result_cursor=table_cursor.aggregate(query)
	for result in result_cursor:
		cnt=result['q_count']
	return HttpResponse(json.dumps(cnt), content_type="application/json")
	

def get_threshold_data(request):
	result_list=[]
	plant=str(request.GET.get('plant').encode('utf-8'))
	product_list=request.GET.get('product_list')
	#if(len(product_list)>1):
		#product_cond={'$in':product_list}
	#else:
	product_cond=product_list
	result_cursor=connection_benchmark().find({'SiteID': plant,'MaterialNumber':product_cond,'ActiveFlag':1,'QualityParameterType':'Scale'},{'_id':0,'Material_Display':1,'MaterialNumber':1,'SiteName':1,'ParameterName':1,'UpperLimit':1,'LowerLimit':1,'Benchmark':1,'CreatedTimeStamp':1,'Owner':1,'BenchmarkIndicator':1,'SiteID':1})
	for r in result_cursor:
		if(r['CreatedTimeStamp']!=''):
			r['CreatedTimeStamp']=r['CreatedTimeStamp'].strftime('%Y-%m-%d')
		result_list.append(r)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	
	
def get_weight_data(request):
	result_list=[]
	plant=str(request.GET.get('plant').encode('utf-8'))
	product_list=request.GET.get('product_list')
	product_cond=product_list
	result_cursor=connection_weights().aggregate([{'$match':{'SiteID': plant,'MaterialNumber':product_cond,'ActiveFlag':1}},{'$project':{'_id':0,'OPWeights':1,'MaterialNumber':1,'SiteName':1,'CreatedTimeStamp':1,'Owner':1,'LoadID':1,'EndDateTime':1,'SiteID':1}},{'$sort':{'LoadID':-1,'EndDateTime':-1}},{'$limit':1}])
	for r in result_cursor:
		del r['EndDateTime']
		changedOPWeights={}
		if(r['CreatedTimeStamp']!=''):
			r['CreatedTimeStamp']=r['CreatedTimeStamp'].strftime('%Y-%m-%d')
		for key,value in r['OPWeights'].iteritems():
			if value['QualityParameterType']!='Categorical':
				changedOPWeights[key]=value
		r['OPWeights']=changedOPWeights

		result_list.append(r)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	

def get_segment_data(request):
	result_list=[]
	plant=str(request.GET.get('plant').encode('utf-8'))
	product_list=request.GET.get('product_list')
	#if(len(product_list)>1):
		#product_cond={'$in':product_list}
	#else:
	product_cond=product_list
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.config_fng_opqualitysegments
	segmentarray=OrderedDict()
	result_cursor=table_cursor.find({'$and':[{'SiteID': plant,'MaterialNumber':product_cond,'ActiveFlag':1}]},{'UpdatedTimeStamp':0,'_id':0,'ActiveFlag':0})
	for r in result_cursor:
		if(r['CreatedTimeStamp']!=''):
			r['CreatedTimeStamp']=r['CreatedTimeStamp'].strftime('%Y-%m-%d')
		if(r['Segments']!=''):
			if r['Segments'].has_key('A'):
				segmentarray['A']=r['Segments']['A']
			if r['Segments'].has_key('B'):
				segmentarray['B']=r['Segments']['B']
			if r['Segments'].has_key('C'):
				segmentarray['C']=r['Segments']['C']
			if r['Segments'].has_key('D'):
				segmentarray['D']=r['Segments']['D']	        		        	        	
			r['Segment']=segmentarray
		result_list.append(r)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	
	
def get_revision_count_data1(request):
	result_list=[]
	plant=str(request.GET.get('plant').encode('utf-8'))
	product_list=request.GET.get('product_list')
	#if(len(product_list)>1):
		#product_cond={'$in':product_list}
	#else:
	product_cond=product_list
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.rule_fng_poqualityparam_revisioncount
	segmentarray=OrderedDict()
	result_cursor=table_cursor.find({'$and':[{'SiteID': plant,'MaterialNumber':product_cond,'ActiveFlag':1,'QualityParameterType':{'$ne':'Categorical'}}]},{'UpdatedTimeStamp':0,'_id':0,'ActiveFlag':0})
	for r in result_cursor:
		del r['EndDateTime']
		del r['StartDateTime']
		if(r['CreatedTimeStamp']!=''):
			r['CreatedTimeStamp']=r['CreatedTimeStamp'].strftime('%Y-%m-%d')
		result_list.append(r)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	
	
def threshold_update(request):
	check_val=request.GET.get('chk')
	data_dict=ast.literal_eval(check_val)
	Owner=request.session['username']	
	if(data_dict!=''):
		SiteID=str(data_dict.keys()[0].split('_')[0])
		MaterialNumber=str(data_dict.keys()[0].split('_')[1])
		SearchKey=SiteID+'_'+MaterialNumber
		output=data_dict[SearchKey]['Output']
		for param in output:
			old_benchmark={}
			new_benchmark={}
			result_cursor=connection_benchmark().find({'SiteID': SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1,'QualityParameterType':'Scale','ParameterName':param},{'_id':0})
			for rec in result_cursor:
				old_benchmark=rec
			new_benchmark=old_benchmark.copy()
			old_benchmark_val=old_benchmark['Benchmark']
			old_benchmark_ind=old_benchmark['BenchmarkIndicator']
			new_benchmark_val=output[param]['Benchmark']
			new_benchmark_ind=output[param]['BenchmarkIndicator']
			
			if (new_benchmark_ind!='As Previous'):
				new_benchmark['Benchmark']=new_benchmark_val
				new_benchmark['BenchmarkIndicator']=new_benchmark_ind
				new_benchmark['ActiveFlag']=1
				new_benchmark['StartDateTime']=datetime.datetime.now()
				new_benchmark['Owner']=Owner
				new_benchmark['CreatedTimeStamp']=datetime.datetime.now()
				new_benchmark['UpdatedTimeStamp']=''
				new_benchmark['EndDateTime']=datetime.datetime(2200, 12, 31, 1, 1, 1, 1)
				
				old_benchmark['EndDateTime']=datetime.datetime.now()-datetime.timedelta(days=1)
				old_benchmark['ActiveFlag']=0
				
				connection_benchmark().update({'SiteID': SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1,'QualityParameterType':'Scale','ParameterName':param},{ '$set':{'ActiveFlag':0,'UpdatedTimeStamp':datetime.datetime.now(),'EndDateTime':datetime.datetime.now()-datetime.timedelta(days=1)}})
				connection_benchmark().insert_one(new_benchmark)
				'''
				if (new_benchmark_ind!='UserDefined'):
					connection_benchmark_indicator().update({'SiteID': SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1,'ParameterName':param},{'$set':{'ActiveFlag':0}})
					rule_benchmark={}
					rule_benchmark['SiteName']=old_benchmark['SiteName']
					rule_benchmark['SiteID']=old_benchmark['SiteID']
					rule_benchmark['MaterialNumber']=old_benchmark['MaterialNumber']
					#rule_benchmark['Technology']=old_benchmark['Technology']
					rule_benchmark['ActiveFlag']=1
					rule_benchmark['ParameterName']=param
					rule_benchmark['BenchmarkIndicator']=new_benchmark_ind
					rule_benchmark['CreatedTimeStamp']=datetime.datetime.now()
					rule_benchmark['UpdatedTimeStamp']=''
					rule_benchmark['Owner']=Owner
					
					connection_benchmark_indicator().insert_one(rule_benchmark)
				'''
	return HttpResponse('success', content_type="application/json")
	
	
def weight_update(request):
	check_val=request.GET.get('chk')
	data_dict=ast.literal_eval(check_val)
	Owner=request.session['username']
	data_list=[]
	k=''
	for key in data_dict:
		one_dict=data_dict[key]
		k=key
		SiteID=str(k.split('_')[0])
		MaterialNumber=str(k.split('_')[1])
		r=connection_weights().find_one({'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{'_id':0})
		old_config=connection_weights().find_one({'MaterialNumber':MaterialNumber,'SiteID':SiteID,'ActiveFlag':1})
		one_dict['CreatedTimeStamp']=datetime.datetime.now()
		one_dict['UpdatedTimeStamp']=''
		one_dict['ActiveFlag']=1
		one_dict['EndDateTime']=datetime.datetime(2200, 12, 31, 1, 1, 1, 1)
		one_dict['MaterialNumber']=MaterialNumber
		one_dict['Site_Display']=old_config['Site_Display']
		one_dict['SBU']=old_config['SBU']
		one_dict['Material_Display']=old_config['Material_Display']
		one_dict['MaterialDescription']=old_config['MaterialDescription']
		one_dict['Technology']=old_config['Technology']
		one_dict['StartDateTime']=datetime.datetime.now()
		one_dict['SiteID']=SiteID
		one_dict['Owner']=Owner
		for key,value in r['OPWeights'].iteritems():
			if(one_dict['OPWeights'].has_key(key)):
			 r['OPWeights'][key]['Weight']=one_dict['OPWeights'][key]
		one_dict['OPWeights']=r['OPWeights']	
		data_list.append(one_dict)
		connection_weights().update( { 'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{ '$set':{'ActiveFlag':0,'UpdatedTimeStamp':datetime.datetime.now(),'EndDateTime':datetime.datetime.now()-timedelta(days=1)}})
		connection_weights().insert_one(one_dict)
	return HttpResponse(data_list, content_type="application/json")

def segment_update(request):
	check_val=request.GET.get('chk')
	data_dict=ast.literal_eval(check_val)
	Owner=request.session['username']
	data_list=[]
	k=''
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.config_fng_opqualitysegments
	
	for key in data_dict:
		one_dict=data_dict[key]
		k=key
		SiteID=str(k.split('_')[0])
		MaterialNumber=str(k.split('_')[1])
		old_config=table_cursor.find_one({'MaterialNumber':MaterialNumber,'SiteID':SiteID,'ActiveFlag':1})
		one_dict['CreatedTimeStamp']=datetime.datetime.now()
		one_dict['UpdatedTimeStamp']=''
		one_dict['ActiveFlag']=1
		one_dict['MaterialNumber']=MaterialNumber
		one_dict['Material_Display']=old_config['Material_Display']
		one_dict['MaterialDescription']=old_config['MaterialDescription']
		one_dict['Site_Display']=old_config['Site_Display']
		one_dict['SiteID']=SiteID
		one_dict['Owner']=Owner
		segmentlist=['A','B','C','D']
		one_dict['Segments']={}
		for k in one_dict['Segment'].keys():
			if k in segmentlist:
				one_dict['Segments'][k]=one_dict['Segment'][k]
		del one_dict['Segment']
		table_cursor.update( { 'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{ '$set':{'ActiveFlag':0,'UpdatedTimeStamp':datetime.datetime.now()}})
		table_cursor.insert_one(one_dict)
	return HttpResponse(one_dict, content_type="application/json")
	
def revision_count_update1(request):
	check_val=request.GET.get('chk')
	data_dict=ast.literal_eval(check_val)
	Owner=request.session['username']
	data_list=[]
	k=''
	#conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	#cursor=conn.henkel
	#cursor.authenticate('henkelUser','QpQc#123')
	conn=pymongo.MongoClient('localhost', 27017)
	cursor=conn.henkelProduction
	cursor.authenticate('henkelUser_Prod','QpQc#123')
	table_cursor=conn.henkelProduction.rule_fng_poqualityparam_revisioncount
	#table_cursor=conn.henkel.rule_fng_poqualityparam_revisioncount
	for key in data_dict:
		##for every quality parameter
		one_dict=data_dict[key]
		k=key
		SiteID=str(one_dict['SiteID'])
		# MaterialNumber=str(data_dict[k])
		Indicator = one_dict['Indicator']
		old_config=table_cursor.find_one({'MaterialNumber':str(one_dict['MaterialNumber']),'SiteID':SiteID,'ActiveFlag':1,'ParameterName':k})
		if Indicator!='As Previous':
			MaterialNumber = str(one_dict['MaterialNumber'])
			one_dict['CreatedTimeStamp']=datetime.datetime.now()
			one_dict['UpdatedTimeStamp']=''
			one_dict['ActiveFlag']=int(1)
			one_dict['EndDateTime']=datetime.datetime(2200, 12, 31, 1, 1, 1, 1)
			one_dict['StartDateTime']=datetime.datetime.now()

			one_dict['MaterialNumber']=MaterialNumber
			one_dict['SiteID']=SiteID
			one_dict['SiteName']=old_config['SiteName']
			one_dict['SBU']=old_config['SBU']
			one_dict['MaterialDescription']=old_config['MaterialDescription']
			one_dict['Site_Display']=old_config['Site_Display']
			one_dict['Material_Display']=old_config['Material_Display']
			one_dict['Technology']=old_config['Technology']
			one_dict['ParameterName'] = key
			one_dict['QualityParameterType'] = 'Scale'
			one_dict['Owner']=Owner
			one_dict['RevisionCount']= data_dict[key]['RevisionCount']
			data_list.append(one_dict)
			table_cursor.update({ 'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1,'ParameterName':k},{ '$set':{'ActiveFlag':0,'EndDateTime':datetime.datetime.now()-datetime.timedelta(days=1),'UpdatedTimeStamp':datetime.datetime.now()}})
			del one_dict['Indicator']
			table_cursor.insert(one_dict)
	return HttpResponse(data_list, content_type="application/json")

@csrf_exempt	
def lazyload_datatable(request):
	#startpoint = request.POST.get("start")
	pagelength = request.POST.get("length")
	draw = request.POST.get("draw")
	#endcounter = int(startpoint) + int(pagelength)
	#endcounter = pagelength
	startpoint=int(request.POST.get("start"))
	endcounter=int(startpoint)+int(pagelength)
	pipeline=get_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	
	data_list=[]
	data_list=request.POST.getlist('segment_list[]')
	segment_list=data_list
	if(len(data_list)>=2):
		pipeline['$match']['$and'].append({'QualitySegment':{'$in':segment_list} })
	else:
		pipeline['$match']['$and'].append({'QualitySegment':segment_list[0].encode('utf-8')})	
	download_pipeline=pipeline
	if type=='total':	
		if page_view=='grid_view':
				batchnumber=request.POST.get('batchnumber')
				logging.info(batchnumber)
				if(batchnumber!='all'):	
					pipeline['$match']['$and'].append({'BatchNumber':batchnumber.encode('utf-8')})
					logging.info(pipeline)
				#result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','po':'$BatchNumber','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}  ,{'$sort':{'avg':1}}])
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'Technology':'$Technology','Resources':'$Resource_Display','LineName':'$LineName','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','po':'$BatchNumber','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
		elif page_view!='product_view':
			if (segment_list[0]=='A'or segment_list[0]=='B'):
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
			else:
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])			
		else:
			if (segment_list[0]=='A'or segment_list[0]=='B'):
				pipeline['$match']['$and'].append({'OutputScore':{'$ne':''}})
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])

			else:
				pipeline['$match']['$and'].append({'OutputScore':{'$ne':''}})
				result_cursor=connection_reportingtable().aggregate([ pipeline,{'$project':{'_id':0,'OrderType':1,'ExecutionInfoExistsInMES':1,'ExistsInCorrelator':1,'po':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrder':'$ProcessOrder','productName':'$Material_Display','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','ProductionStartDateTime':'$ProductionStartDateTime'}}])
				
	po_list=[]
	product_list=[]
	po_avgscr=[]
	po_segment=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_productiondate_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	for r in result_cursor:	
		if(counter>=startpoint and counter<endcounter):
			templist=[]
			segmenttemp=''
			link=''
			link1=''
			linking_div=''
			po_list.append(r['po'])
			product_list.append(r['product'])
			po_avgscr.append(r['avg'])
			processorder_list.append(r['ProcessOrder'])
			plantid_list.append(r['SiteName'])
			po_siteid_list.append(r['SiteID'])
			username = request.session['username']
			encrpytion_key = "71079162"
			encrpyted_username = AESencrypt(encrpytion_key, username)
			encoded_username_url = urllib.quote(encrpyted_username, safe='')
			app = '20263146'
			po_technology_list.append(r['Technology'])
			po_sbu_list.append(r['SBU'])	
			po_productiondate_list.append(r['ProductionStartDateTime'].strftime('%Y-%m-%d'))
			po_segment.append(r['QualitySegment'])
			if r['ExecutionInfoExistsInMES']!='N':
				if r['OrderType'] == 'B':
					link='<a class="linking_mes_correlator" title="MES Link" href="http://ap-hipas.henkelgroup.net/ipas/ipas_Preweigh/Manufacture/ViewManufacturePO.aspx?id='+ r['SiteID']+'&processorder='+r['ProcessOrder']+'&archive=true&3app='+app+'&username='+encoded_username_url+'" target="blank" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'
				else:
					link='<a class="linking_mes_correlator" title="MES Link" href="http://ap-hipas.henkelgroup.net/ipas/ipas_Preweigh/Manufacture/ViewPackOffPO.aspx?id='+ r['SiteID']+'&processorder='+r['ProcessOrder']+'&archive=true&hideMenu=false" target="blank" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'
			else:
				link='<a class="linking_mes_correlator no_data_available_mes" title="MES Link" href="#" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'		
#			link='<a class="linking_mes_correlator" title="MES Link" href="http://ap-hipas.henkelgroup.net/ipas/ipas_Preweigh/Manufacture/PhaseData.aspx?id='+ r['SiteID']+'&processorder='+r['ProcessOrder']+'&archive=true" target="blank" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'
			#link='<a class="linking_mes_correlator" title="MES Link" href="http://ap-hipas.henkelgroup.net/ipas/ipas_Preweigh/Manufacture/PhaseData.aspx?id='+ r['SiteID']+'&processorder='+r['ProcessOrder']+'&archive=true&3app='+app+'&username='+encoded_username_url+'" target="blank" style="text-decoration:none;"><i class="fa fa-external-link" aria-hidden="true"></i></a>'
			#link1='"http://193.96.100.95/qualitycorrelator/rootcauseanalysis/?siteid='+ r['SiteID']+'&sitename='+r['SiteName']+'&sbu='+r['SBU']+'&technology='+r['Technology']+'&product='+r['product']+'&processorder='+r['ProcessOrder']+'&productiondate='+r['ProductionStartDateTime'].strftime('%Y-%m-%d')+'&from_link=rawmaterial_view"'link1='"http://henkelpilot.westeurope.cloudapp.azure.com/qualitycorrelator1/rootcauseanalysis/?siteid='+ r['SiteID']+'&sitename='+r['SiteName']+'&sbu='+r['SBU']+'&technology='+r['Technology']+'&product='+r['product']+'&processorder='+r['ProcessOrder']+'&productiondate='+r['ProductionStartDateTime'].strftime('%Y-%m-%d')+'&from_link=rawmaterial_view"'
			if r['ExistsInCorrelator']!='N':
				linking_div='<a class="linking_mes_correlator"  href="http://193.96.100.95/qualitycorrelator/rootcauseanalysis/?siteid='+r['SiteID']+'&sitename='+r['SiteName']+'&product='+r['product']+'&processorder='+r['ProcessOrder']+'&sbu='+r['SBU']+'&technology='+r['Technology']+'&productiondate='+r['ProductionStartDateTime'].strftime('%Y-%m-%d')+'" target="blank" style="text-decoration:none;"><span style="color: rgba(127, 119, 119, 0.87);font-size:12px;"><u>'+r['po']+'</u></span></a>'
			else:
				linking_div='<a class="linking_mes_correlator no_data_available_cor" title="" href="#" style="text-decoration:none;"><span style="color: rgba(127, 119, 119, 0.87);font-size:12px;"><u>'+r['po']+'</u></span></a>'
			#templist=[segmenttemp,r['product'],linking_div,r['avg'],link,r['ProcessOrder'],r['SiteName'],r['SiteID'],r['Technology'],r['ProductionStartDateTime'].strftime('%Y-%m-%d')]
			if page_view=='grid_view':		
				templist=[r['QualitySegment'],r['SiteName'],r['SBU'],r['Technology'],r['product'],r['productName'],r['LineName'],r['Resources'],r['po'],r['ProcessOrder'],float("{0:.4f}".format(r['avg']))]		
			else:		
				templist=[r['QualitySegment'],r['product'],r['productName'],linking_div,float("{0:.4f}".format(r['avg'])),link,r['ProcessOrder'],r['SiteName'],r['SiteID'],r['Technology'],r['ProductionStartDateTime'].strftime('%Y-%m-%d')]
			data.append(templist)
		counter=counter+1
	data.sort(key=lambda x: x[-1])
	temp_dict['po_numbers']=po_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_productiondate']=po_productiondate_list
	temp_dict['product_numbers']=product_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_score']=po_avgscr
	temp_dict['po_segment']=po_segment
	temp_dict['color']='#F3778B'
	temp_dict['recordsFiltered']=counter
	temp_dict['recordsTotal']=counter
	temp_dict['data']=data
	temp_dict['draw']=int(draw)
	temp_dict['start']=startpoint
	temp_dict['length']=endcounter

	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po[0]), content_type="application/json")

	
@csrf_exempt
def cpk_values(request):
	MaterialNumber=request.GET.getlist('product_list[]')[0]
	qccharecter=request.GET.getlist('parameter_list[]')[0]
	plant=request.GET.getlist('plant_list[]')[0]
	result_cursor1=connection_report_CPK().find({'MaterialNumber':MaterialNumber,'SiteID':plant},{'InOS':1,'ExOS':1,'_id':0})	
	total_data={'CpK':{},'Cp':{},'ProcessSD':{}}
	for result in result_cursor1:
		if(result.has_key('InOS')):
			if(result['InOS'].has_key(qccharecter)):		
				if(result['InOS'][qccharecter]['CpK']!=''):
					total_data['CpK']['InOS']=float("{0:.2f}".format(result['InOS'][qccharecter]['CpK']))
				if(result['InOS'][qccharecter]['Cp']!=''):
					total_data['Cp']['InOS']=float("{0:.2f}".format(result['InOS'][qccharecter]['Cp']))
				if(result['InOS'][qccharecter]['ProcessSD']!=''):
					total_data['ProcessSD']['InOS']=float("{0:.2f}".format(result['InOS'][qccharecter]['ProcessSD']))
		if(result.has_key('ExOS')):
			if(result['ExOS'].has_key(qccharecter)):		
				if(result['ExOS'][qccharecter]['CpK']!=''):
					total_data['CpK']['ExOS']=float("{0:.2f}".format(result['ExOS'][qccharecter]['CpK']))
				if(result['ExOS'][qccharecter]['Cp']!=''):
					total_data['Cp']['ExOS']=float("{0:.2f}".format(result['ExOS'][qccharecter]['Cp']))
				if(result['ExOS'][qccharecter]['ProcessSD']!=''):
					total_data['ProcessSD']['ExOS']=float("{0:.2f}".format(result['ExOS'][qccharecter]['ProcessSD']))
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
@csrf_exempt	
def rejected_datatable(request):
	#startpoint = request.POST.get("start")
	pagelength = request.POST.get("length")
	draw = request.POST.get("draw")
	#endcounter = int(startpoint) + int(pagelength)
	#endcounter = pagelength
	startpoint=int(request.POST.get("start"))
	endcounter=int(startpoint)+int(pagelength)
	pipeline=get_rejected_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	
	product_date=request.POST.get('productstart_date')
	
	'''plant_list=request.POST.getlist('plant_list[]')
	product_list=request.POST.getlist('product_list[]')'''
	
	#data_list=[]
	if type=='total':	
		if page_view=='rejected_view':
				batchnumber=request.POST.get('batchnumber')
				if product_date=='is_null':
					pipeline['$match']['$and'][1]['ProductionStartDateTime']={'$eq':''}
					del pipeline['$match']['$and'][4]
				if(batchnumber!='all'):			
					pipeline['$match']['$and'].append({'BatchNumber':batchnumber})
				del pipeline['$match']['$and'][0]
				result_cursor=connection_rejected_po().aggregate([ pipeline, {'$project':{'_id':0,'Technology':'$Technology','ResourceName':'$ResourceName','PlineName':'$PlineName','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrderNumber':'$ProcessOrderNumber','po':'$BatchNumber','product':'$MaterialNumber','Pulse_PO_Rejection_Reason_Code':'$Pulse_PO_Rejection_Reason_Code','ProductionStartDateTime':'$ProductionStartDateTime'}} ,{'$sort':{'avg':1}}])
				
	po_list=[]
	product_list=[]
	po_avgscr=[]
	processorder_list=[]
	line_list=[]
	resource_list=[]
	plantid_list=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_rejection_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	for r in result_cursor:	
		if(counter>=startpoint and counter<endcounter):
			templist=[]
			segmenttemp=''
			link=''
			link1=''
			linking_div=''
			plantid_list.append(r['SiteName'])
			po_sbu_list.append(r['SBU'])
			po_technology_list.append(r['Technology'])
			product_list.append(r['product'])
			line_list.append(r['PlineName'])
			resource_list.append(r['ResourceName'])
			po_list.append(r['po'])
			processorder_list.append(r['ProcessOrderNumber'])
			po_rejection_list.append(r['Pulse_PO_Rejection_Reason_Code'])
			po_siteid_list.append(r['SiteID'])	
			templist=[r['SiteName'],r['SBU'],r['Technology'],r['product'],r['PlineName'],r['ResourceName'],r['po'],r['ProcessOrderNumber'],r['Pulse_PO_Rejection_Reason_Code']]
			data.append(templist)
		counter=counter+1
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['product_numbers']=product_list
	temp_dict['line']=line_list
	temp_dict['resource']=resource_list
	temp_dict['po_numbers']=po_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['rejection_reason']=po_rejection_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['color']='#F3778B'
	temp_dict['recordsFiltered']=counter
	temp_dict['recordsTotal']=counter
	temp_dict['data']=data
	temp_dict['draw']=int(draw)
	temp_dict['start']=startpoint
	temp_dict['length']=endcounter

	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po[0]), content_type="application/json")

@csrf_exempt	
def get_rejected_data(request):
	pipeline=get_rejected_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	product_date=request.POST.get('productstart_date')
	#download_pipeline=pipeline
	if type=='total':
		if page_view=='rejected_view':
			batchnumber=request.POST.get('batchnumber')
			if product_date=='is_null':
				pipeline['$match']['$and'][1]['ProductionStartDateTime']={'$eq':''}
				del pipeline['$match']['$and'][4]
			if(batchnumber!='all'):			
				pipeline['$match']['$and'].append({'BatchNumber':batchnumber})
			del pipeline['$match']['$and'][0]
			result_cursor=connection_rejected_po().aggregate([ pipeline, {'$project':{'_id':0,'Technology':'$Technology','ResourceName':'$ResourceName','PlineName':'$PlineName','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','ProcessOrderNumber':'$ProcessOrderNumber','po':'$BatchNumber','product':'$MaterialNumber','Pulse_PO_Rejection_Reason_Code':'$Pulse_PO_Rejection_Reason_Code','ProductionStartDateTime':'$ProductionStartDateTime'}} ,{'$sort':{'avg':1}}])
				
	po_list=[]
	product_list=[]
	po_avgscr=[]
	processorder_list=[]
	line_list=[]
	resource_list=[]
	plantid_list=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_rejection_list=[]
	temp_dict={}
	top_po=[]
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	if (type=='total' ):
		if(page_view=='rejected_view'):		
			for r in result_cursor:
				plantid_list.append(r['SiteName'])
				po_sbu_list.append(r['SBU'])
				po_technology_list.append(r['Technology'])
				product_list.append(r['product'])
				line_list.append(r['PlineName'])
				resource_list.append(r['ResourceName'])
				po_list.append(r['po'])
				processorder_list.append(r['ProcessOrderNumber'])
				po_rejection_list.append(r['Pulse_PO_Rejection_Reason_Code'])
				po_siteid_list.append(r['SiteID'])
				deatil_list=[]
				deatil_list.append(r['SiteName'])
				deatil_list.append(r['SBU'])
				deatil_list.append(r['Technology'])
				deatil_list.append(r['product'])
				deatil_list.append(r['PlineName'])
				deatil_list.append(r['ResourceName'])
				deatil_list.append(r['po'])
				deatil_list.append(r['ProcessOrderNumber'])
				deatil_list.append(r['Pulse_PO_Rejection_Reason_Code'])
				deatils_list.append(deatil_list)
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['product_numbers']=product_list
	temp_dict['line']=line_list
	temp_dict['resource']=resource_list
	temp_dict['po_numbers']=po_list
	temp_dict['processorder_list']=processorder_list
	temp_dict['rejection_reason']=po_rejection_list
	temp_dict['download_list']=deatils_list
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['color']='#F3778B'
	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po), content_type="application/json")
	
	
def get_rejected_pipeline(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	date_list=[]
	filter_list=[ {'QualitySegment':{'$ne':''}}, {'ProductionStartDateTime':{'$ne':''}} ]
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	line=''
	product=''
	resource=''
	date='1900-01-01'
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]


	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data)
	else:	
		product=data_list[0].encode('utf-8')	
		
	data_list=[]
	data_list=request.POST.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]		
	
	if (len(plant_list)>=2 ):
		filter_list.append({'SiteID':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteID':plant } )
				
	if (len(product_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':product_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'MaterialNumber':product }	)
			
		
	if (len(date_list)>=2 ):
		filter_list.append( {'ProductionStartDateTime': {'$lte':datetime.datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.datetime.strptime(date_list[0], '%Y-%m-%d') } } )
	else:
		#start=datetime(date.year(),date[1],date[2],0,0,0,0)
        #end=datetime(date[0],date[1],date[2],11,59,59,999999)
		filter_list.append ( {'ProductionStartDateTime':{'$lt':datetime.datetime.strptime(date, '%Y-%m-%d')+timedelta(days=1),'$gte':datetime.datetime.strptime(date, '%Y-%m-%d') }})
		#filter_list.append ( {'ProductionStartDateTime':datetime.datetime.strptime(date, '%Y-%m-%d')} )
		
	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			logging.info('***********************pipeline**********************')
			logging.info(filter_pipeline)
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline

@csrf_exempt	
def Config_datatable(request):
	#startpoint = request.POST.get("start")
	pagelength = request.POST.get("length")
	draw = request.POST.get("draw")
	#endcounter = int(startpoint) + int(pagelength)
	#endcounter = pagelength
	startpoint=int(request.POST.get("start"))
	endcounter=int(startpoint)+int(pagelength)
	pipeline=get_benchmark_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	
	history=request.POST.get('historical_benchmark')
	config_flag=request.POST.get('display')
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	top_po=[]
	if config_flag=='Benchmark':
		benchmark_source_list=[]
		
		del pipeline['$match']['$and'][0]
		del pipeline['$match']['$and'][0]
		if history=='Y':
			pipeline['$match']['$and'][1]['ActiveFlag']={'$in':[0,1]}
		else:
			pipeline['$match']['$and'][1]['ActiveFlag']=1
		pipeline['$match']['$and'][0]['QualityParameterType']='Scale'
		benchmark_cursor=connection_benchmark().aggregate([pipeline])

		benchmark_target_list=[]
		benchmark_target_dict=[]

		for record in benchmark_cursor:
			benchmark_source_list.append(record)
				
		for record in benchmark_source_list:
			data_dict={}
			data_dict['Product']=record['MaterialNumber']
			data_dict['SiteName']=record['SiteName']
			data_dict['Parameter']=record['ParameterName']
			data_dict['Benchmark_Indicator']=record['BenchmarkIndicator']
			#data_dict['Benchmark']=record['Benchmark']
			data_dict['Benchmark']='{0:.4f}'.format(float(record['Benchmark']))
			data_dict['Benchmark_Updated_By']=record['Owner']
			if record['UpdatedTimeStamp']=='':
				data_dict['Benchmark_Last_Updated_Date']=record['CreatedTimeStamp'].strftime('%Y-%m-%d')
			else:
				data_dict['Benchmark_Last_Updated_Date']=record['UpdatedTimeStamp'].strftime('%Y-%m-%d')
			data_dict['Benchmark_ActiveFlag']=record['ActiveFlag']
			data_dict['Benchmark_StartDateTime']=record['StartDateTime'].strftime('%Y-%m-%d')
			data_dict['Benchmark_EndDateTime']=record['EndDateTime'].strftime('%Y-%m-%d')
			benchmark_target_dict.append(data_dict)
			
		sorted_benchmark_dict=sorted(benchmark_target_dict, key=itemgetter('Product','Benchmark_ActiveFlag','Parameter','Benchmark_StartDateTime'))
		
		for data_dict in sorted_benchmark_dict:
			if(counter>=startpoint and counter<endcounter):
				new_row=[]
				new_row=[data_dict['Product'],data_dict['SiteName'],data_dict['Parameter'].encode('utf-8'),data_dict['Benchmark'],data_dict['Benchmark_StartDateTime'],data_dict['Benchmark_EndDateTime'],data_dict['Benchmark_ActiveFlag'],data_dict['Benchmark_Updated_By'],data_dict['Benchmark_Last_Updated_Date']]
				'''new_row.append(data_dict['Product'])
				new_row.append(data_dict['SiteName'])
				new_row.append(data_dict['Parameter'].encode('utf-8'))
				new_row.append(data_dict['Benchmark'])
				new_row.append(data_dict['Benchmark_StartDateTime'])
				new_row.append(data_dict['Benchmark_EndDateTime'])
				new_row.append(data_dict['Benchmark_ActiveFlag'])
				new_row.append(data_dict['Benchmark_Updated_By'])
				new_row.append(data_dict['Benchmark_Last_Updated_Date'])'''
				data.append(new_row)
			counter=counter+1
		data_dict['color']='#F3778B'
		data_dict['recordsFiltered']=counter
		data_dict['recordsTotal']=counter
		data_dict['data']=data
		data_dict['draw']=int(draw)
		data_dict['start']=startpoint
		data_dict['length']=endcounter

		top_po.append(data_dict)
		return HttpResponse(json.dumps(top_po[0]), content_type="application/json")
	else:
		del pipeline['$match']['$and'][0]
		del pipeline['$match']['$and'][0]
		if history=='Y':
			pipeline['$match']['$and'][1]['ActiveFlag']={'$in':[0,1]}
		else:
			pipeline['$match']['$and'][1]['ActiveFlag']=1
		weights_source_list=[]
		weights_target_dict=[]
		weights_target_list=[]
		weights_cursor=connection_weights().aggregate([pipeline])
		for record in weights_cursor:
			weights_source_list.append(record)
			
		for record in weights_source_list:
			weights_data=record
			data_dict={}
			data_dict['Product']=record['MaterialNumber']
			data_dict['SiteName']=record['SiteName']
			data_dict['Weight_Updated_By']=weights_data['Owner']
			if record['UpdatedTimeStamp']=='':
				data_dict['Weight_Last_Updated_Date']=record['CreatedTimeStamp'].strftime('%Y-%m-%d')
			else:
				data_dict['Weight_Last_Updated_Date']=record['UpdatedTimeStamp'].strftime('%Y-%m-%d')
			data_dict['Weight_ActiveFlag']=weights_data['ActiveFlag']
			data_dict['Weight_StartDateTime']=weights_data['StartDateTime'].strftime('%Y-%m-%d')
			data_dict['Weight_EndDateTime']=weights_data['EndDateTime'].strftime('%Y-%m-%d')
			for parameter in weights_data['OPWeights']:
				if weights_data['OPWeights'][parameter]['QualityParameterType']=='Scale':
					data_dict['Parameter']=parameter
					data_dict['Weight']=weights_data['OPWeights'][parameter]['Weight']
					weights_target_dict.append(data_dict.copy())	
		
		sorted_weights_dict=sorted(weights_target_dict, key=itemgetter('Product','Weight_ActiveFlag','Parameter','Weight_StartDateTime'))
		
		for data_dict in sorted_weights_dict:
			if(counter>=startpoint and counter<endcounter):
				new_row=[]
				#new_row=[data_dict['Product'],data_dict['SiteName'],data_dict['Parameter'].encode('utf-8'),data_dict['Weight'],data_dict['Weight_StartDateTime'],data_dict['Weight_EndDateTime'],data_dict['Weight_ActiveFlag'],data_dict['Weight_Updated_By'],data_dict['Weight_Last_Updated_Date']]
				new_row.append(data_dict['Product'])
				new_row.append(data_dict['SiteName'])
				new_row.append(data_dict['Parameter'].encode('utf-8'))
				new_row.append(data_dict['Weight'])
				new_row.append(data_dict['Weight_StartDateTime'])
				new_row.append(data_dict['Weight_EndDateTime'])
				new_row.append(data_dict['Weight_ActiveFlag'])
				new_row.append(data_dict['Weight_Updated_By'])
				new_row.append(data_dict['Weight_Last_Updated_Date'])
				data.append(new_row)
			counter=counter+1
		data_dict['color']='#F3778B'
		data_dict['recordsFiltered']=counter
		data_dict['recordsTotal']=counter
		data_dict['data']=data
		data_dict['draw']=int(draw)
		data_dict['start']=startpoint
		data_dict['length']=endcounter

		top_po.append(data_dict)
		return HttpResponse(json.dumps(top_po[0]), content_type="application/json")
def get_benchmark_pipeline(request):
	plant_list=[]
	resource_list=[]
	line_list=[]	
	date_list=[]
	filter_list=[ {'QualitySegment':{'$ne':''}}, {'ProductionStartDateTime':{'$ne':''}} ]
	product_list=[]
	sbu_list=[]
	technology_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	line=''
	product=''
	resource=''
	date='1900-01-01'
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]


	data_list=[]
	data_list=request.POST.getlist('product_list[]')
	if len(data_list)>1:
		for data in data_list:
			product_list.append(data)
	else:	
		product=data_list[0].encode('utf-8')	
		
	
	if (len(plant_list)>=2 ):
		filter_list.append({'SiteID':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteID':plant } )
				
	if (len(product_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':product_list } }	)	
	else:
		if(resource!='all'):
			filter_list.append( {'MaterialNumber':product }	)
			
	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			logging.info('***********************pipeline**********************')
			logging.info(filter_pipeline)
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline
			
@csrf_exempt	
def get_benchmark_data(request):
	pipeline=get_benchmark_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	history=request.POST.get('historical_benchmark')
	config_flag=request.POST.get('display')
	color='#F3778B'
	deatils_list=[]
	counter=0
	data=[]
	top_po=[]
	if config_flag=='Benchmark':
		benchmark_source_list=[]
		
		del pipeline['$match']['$and'][0]
		del pipeline['$match']['$and'][0]
		if history=='Y':
			pipeline['$match']['$and'][1]['ActiveFlag']={'$in':[0,1]}
		else:
			pipeline['$match']['$and'][1]['ActiveFlag']=1
		pipeline['$match']['$and'][0]['QualityParameterType']='Scale'
		benchmark_cursor=connection_benchmark().aggregate([pipeline])

		benchmark_target_list=[]
		benchmark_target_dict=[]

		for record in benchmark_cursor:
			benchmark_source_list.append(record)
				
		for record in benchmark_source_list:
			data_dict={}
			data_dict['Product']=record['MaterialNumber']
			data_dict['SiteName']=record['SiteName']
			data_dict['Parameter']=record['ParameterName']
			data_dict['Benchmark_Indicator']=record['BenchmarkIndicator']
			data_dict['Benchmark']='{0:.4f}'.format(float(record['Benchmark']))
			data_dict['Benchmark_Updated_By']=record['Owner']
			if record['UpdatedTimeStamp']=='':
				data_dict['Benchmark_Last_Updated_Date']=record['CreatedTimeStamp'].strftime('%Y-%m-%d')
			else:
				data_dict['Benchmark_Last_Updated_Date']=record['UpdatedTimeStamp'].strftime('%Y-%m-%d')
			data_dict['Benchmark_ActiveFlag']=record['ActiveFlag']
			data_dict['Benchmark_StartDateTime']=record['StartDateTime'].strftime('%Y-%m-%d')
			data_dict['Benchmark_EndDateTime']=record['EndDateTime'].strftime('%Y-%m-%d')
			benchmark_target_dict.append(data_dict)
			
		sorted_benchmark_dict=sorted(benchmark_target_dict, key=itemgetter('Product','Benchmark_ActiveFlag','Parameter','Benchmark_StartDateTime'))
		
		for data_dict in sorted_benchmark_dict:
			deatil_list=[]
			deatil_list.append(data_dict['Product'])
			deatil_list.append(data_dict['SiteName'])
			deatil_list.append(data_dict['Parameter'].encode('utf-8'))
			deatil_list.append(data_dict['Benchmark'])
			deatil_list.append(data_dict['Benchmark_StartDateTime'])
			deatil_list.append(data_dict['Benchmark_EndDateTime'])
			deatil_list.append(data_dict['Benchmark_ActiveFlag'])
			deatil_list.append(data_dict['Benchmark_Updated_By'])
			deatil_list.append(data_dict['Benchmark_Last_Updated_Date'])
			deatils_list.append(deatil_list)
		data_dict['color']='#F3778B'
		data_dict['download_list']=deatils_list
		top_po.append(data_dict)
		return HttpResponse(json.dumps(top_po), content_type="application/json")
	elif config_flag == 'Weight':
		del pipeline['$match']['$and'][0]
		del pipeline['$match']['$and'][0]
		if history=='Y':
			pipeline['$match']['$and'][1]['ActiveFlag']={'$in':[0,1]}
		else:
			pipeline['$match']['$and'][1]['ActiveFlag']=1
		weights_source_list=[]
		weights_target_dict=[]
		weights_target_list=[]
		weights_cursor=connection_weights().aggregate([pipeline])
		for record in weights_cursor:
			weights_source_list.append(record)
			
		for record in weights_source_list:
			weights_data=record
			data_dict={}
			data_dict['Product']=record['MaterialNumber']
			data_dict['SiteName']=record['SiteName']
			data_dict['Weight_Updated_By']=weights_data['Owner']
			if record['UpdatedTimeStamp']=='':
				data_dict['Weight_Last_Updated_Date']=record['CreatedTimeStamp'].strftime('%Y-%m-%d')
			else:
				data_dict['Weight_Last_Updated_Date']=record['UpdatedTimeStamp'].strftime('%Y-%m-%d')
			data_dict['Weight_ActiveFlag']=weights_data['ActiveFlag']
			data_dict['Weight_StartDateTime']=weights_data['StartDateTime'].strftime('%Y-%m-%d')
			data_dict['Weight_EndDateTime']=weights_data['EndDateTime'].strftime('%Y-%m-%d')
			for parameter in weights_data['OPWeights']:
				if weights_data['OPWeights'][parameter]['QualityParameterType']=='Scale':
					data_dict['Parameter']=parameter
					data_dict['Weight']=weights_data['OPWeights'][parameter]['Weight']
					weights_target_dict.append(data_dict.copy())
		sorted_weights_dict=sorted(weights_target_dict, key=itemgetter('Product','Weight_ActiveFlag','Parameter','Weight_StartDateTime'))
		
		
		for data_dict in weights_target_dict:
			deatil_list=[]
			deatil_list.append(data_dict['Product'])
			deatil_list.append(data_dict['SiteName'])
			deatil_list.append(data_dict['Parameter'].encode('utf-8'))
			deatil_list.append(data_dict['Weight'])
			deatil_list.append(data_dict['Weight_StartDateTime'])
			deatil_list.append(data_dict['Weight_EndDateTime'])
			deatil_list.append(data_dict['Weight_ActiveFlag'])
			deatil_list.append(data_dict['Weight_Updated_By'])
			deatil_list.append(data_dict['Weight_Last_Updated_Date'])
			deatils_list.append(deatil_list)
		data_dict['color']='#F3778B'
		data_dict['download_list']=deatils_list
		top_po.append(data_dict)
		return HttpResponse(json.dumps(top_po), content_type="application/json")

	