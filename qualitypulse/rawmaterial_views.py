from django.shortcuts import render
from django.db.backends.util import CursorWrapper
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
#from mongoengine import *
from datetime import datetime
import pymongo
import logging
import json
#from sets import Set
import math
from decimal import *
import ast
import csv
from operator import itemgetter
from collections import OrderedDict



#---------------------------------connection function--------------------------------------#
def connection():
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_QualScore_QualSegment_RMInspLot_1	
	return table_cursor


#---------------------------------connection function--------------------------------------#
def connection1():
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Rule_POCount	
	return table_cursor
@csrf_exempt
def RM_overview(request):	
	syncdate=''
	result_cursor=connection().aggregate( [ {'$group':{ '_id':{'InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}} },{'$sort':{'_id.InspectionLotCreationDateTime':-1}},{ '$limit' : 1}])
	for r in result_cursor:
		syncdate=r['_id']['InspectionLotCreationDateTime'].strftime('%d %b %y')	
	user_id=request.POST.get('username')
	filters_dict={}
	syncdate=''
	Plant_list=[]
	result_cursor=connection().distinct('SiteName')
	for r in result_cursor:
		Plant_list.append(r.encode('utf-8'))
	filters_dict['Plant_list']=Plant_list
	sbu_list=[]
	result_cursor=connection().distinct('SBU')
	for r in result_cursor:
		sbu_list.append(r.encode('utf-8'))
	filters_dict['sbu_list']=sbu_list
	technology_list=[]
	result_cursor=connection().distinct('Technology')
	for r in result_cursor:
		technology_list.append(r.encode('utf-8'))
	filters_dict['technology_list']=technology_list			
	product_list=[]
	result_cursor=connection().distinct('MaterialNumber')
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	filters_dict['product_list']=product_list
	
	vendor_list=[]
	result_cursor=connection().distinct('VendorName',{'VendorName':{'$ne':''}})
	for r in result_cursor:
		vendor_list.append(r.encode('utf-8'))
	template = loader.get_template('qualitypulse/rm_overview.html')
	context = RequestContext(request, {'appuser':user_id,'page':"","plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'vendor_list':vendor_list,'syncdate':syncdate })
	return HttpResponse(template.render(context), content_type="text/html")
	
@csrf_exempt
def rawmaterial_view(request):	
	syncdate=''
	result_cursor=connection().aggregate( [ {'$group':{ '_id':{'InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}} },{'$sort':{'_id.InspectionLotCreationDateTime':-1}},{ '$limit' : 1}])
	for r in result_cursor:
		syncdate=r['_id']['InspectionLotCreationDateTime'].strftime('%d %b %y')	
	user_id=request.POST.get('username')
	filters_dict={}
	syncdate=''
	Plant_list=[]
	result_cursor=connection().distinct('SiteName')
	for r in result_cursor:
		Plant_list.append(r.encode('utf-8'))
	filters_dict['Plant_list']=Plant_list
	sbu_list=[]
	result_cursor=connection().distinct('SBU')
	for r in result_cursor:
		sbu_list.append(r.encode('utf-8'))
	filters_dict['sbu_list']=sbu_list
	technology_list=[]
	result_cursor=connection().distinct('Technology')
	for r in result_cursor:
		technology_list.append(r.encode('utf-8'))
	filters_dict['technology_list']=technology_list			
	product_list=[]
	result_cursor=connection().distinct('MaterialNumber')
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	filters_dict['product_list']=product_list
	
	vendor_list=[]
	result_cursor=connection().distinct('VendorName',{'VendorName':{'$ne':''}})
	for r in result_cursor:
		vendor_list.append(r.encode('utf-8'))
	template = loader.get_template('qualitypulse/rawmaterial_view.html')
	context = RequestContext(request, {'appuser':user_id,'page':"","plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'vendor_list':vendor_list,'syncdate':syncdate })
	return HttpResponse(template.render(context), content_type="text/html")
	
	
	
@csrf_exempt
def vendor_view(request):
	syncdate=''
	result_cursor=connection().aggregate( [ {'$group':{ '_id':{'InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}} },{'$sort':{'_id.InspectionLotCreationDateTime':-1}},{ '$limit' : 1}])
	for r in result_cursor:
		syncdate=r['_id']['InspectionLotCreationDateTime'].strftime('%d %b %y')		
	user_id=request.POST.get('username')
	filters_dict={}
	syncdate=''
	Plant_list=[]
	result_cursor=connection().distinct('SiteName')
	for r in result_cursor:
		Plant_list.append(r.encode('utf-8'))
	filters_dict['Plant_list']=Plant_list
	sbu_list=[]
	result_cursor=connection().distinct('SBU')
	for r in result_cursor:
		sbu_list.append(r.encode('utf-8'))
	filters_dict['sbu_list']=sbu_list
	technology_list=[]
	result_cursor=connection().distinct('Technology')
	for r in result_cursor:
		technology_list.append(r.encode('utf-8'))
	filters_dict['technology_list']=technology_list			
	product_list=[]
	result_cursor=connection().distinct('MaterialNumber')
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	filters_dict['product_list']=product_list
	
	vendor_list=[]
	result_cursor=connection().distinct('VendorName',{'VendorName':{'$ne':''}})
	for r in result_cursor:
		vendor_list.append(r.encode('utf-8'))
	template = loader.get_template('qualitypulse/vendor_view.html')
	context = RequestContext(request, {'appuser':user_id,'page':"","plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'vendor_list':vendor_list,'syncdate':syncdate })
	return HttpResponse(template.render(context), content_type="text/html")
		
	
	
	
	
	
	
def RM_get_filter_pipeline(request):
	plant_list=[]
	vendor_list=[]	
	rawmaterial_list=[]
	sbu_list=[]
	technology_list=[]
	filter_dict={}
	plant=''
	vendor=''
	rawmaterial=''
	sbu=''
	technology=''
#---------------------getting data------------------------------------#	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data.encode('utf-8'))
	else:
		plant=data_list[0].encode('utf-8')
		
		
	data_list=[]
	data_list=request.POST.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data.encode('utf-8'))
	else:
		sbu=data_list[0].encode('utf-8')


	data_list=[]
	data_list=request.POST.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data.encode('utf-8'))
	else:
		technology=data_list[0].encode('utf-8')		
		
		
	data_list=[]
	data_list=request.POST.getlist('rawmaterial_list[]')
	if len(data_list)>1:
		for data in data_list:
			rawmaterial_list.append(data.encode('utf-8'))
	else:
		rawmaterial=data_list[0].encode('utf-8')	

		
		
	data_list=[]
	data_list=request.POST.getlist('vendor_list[]')
	if len(data_list)>1:
		for data in data_list:
			vendor_list.append(data.encode('utf-8'))
	else:
		vendor=data_list[0].encode('utf-8')
		
#----------------------making Pipeline-------------------------------
	if (len(plant_list)>=2 ):
		filter_dict['SiteName']={'$in':plant_list }  
	else:
		if(plant!='all'):	
			filter_dict['SiteName']=plant  
	
	if (len(sbu_list)>=2 ):
		filter_dict['SBU']={'$in':sbu_list }  
	else:
		if(sbu!='all'):	
			filter_dict['SBU']=sbu 

	if (len(technology_list)>=2 ):
		filter_dict['Technology']={'$in':technology_list }  
	else:
		if(technology!='all'):	
			filter_dict['Technology']=technology  

	if (len(rawmaterial_list)>=2 ):
		filter_dict['MaterialNumber']={'$in':rawmaterial_list }  
	else:
		if(rawmaterial!='all'):	
			filter_dict['MaterialNumber']=rawmaterial  			
			
	if (len(vendor_list)>=2 ):
		filter_dict['VendorName']={'$in':vendor_list } 	
	else:
		if(vendor!='all'):
			filter_dict['VendorName']=vendor
	return filter_dict


	
	
def RM_grid_view(request):
	user_id=request.POST.get('username')
	filters_dict={}
	syncdate=''
	Plant_list=[]
	result_cursor=connection().distinct('SiteName')
	for r in result_cursor:
		Plant_list.append(r.encode('utf-8'))
	filters_dict['Plant_list']=Plant_list
	sbu_list=[]
	result_cursor=connection().distinct('SBU')
	for r in result_cursor:
		sbu_list.append(r.encode('utf-8'))
	filters_dict['sbu_list']=sbu_list
	technology_list=[]
	result_cursor=connection().distinct('Technology')
	for r in result_cursor:
		technology_list.append(r.encode('utf-8'))
	filters_dict['technology_list']=technology_list			
	product_list=[]
	result_cursor=connection().distinct('MaterialNumber')
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	filters_dict['product_list']=product_list
	vendor_list=[]
	result_cursor=connection().distinct('VendorName')
	for r in result_cursor:
		vendor_list.append(r.encode('utf-8'))
	template = loader.get_template('qualitypulse/rawmaterial_grid_view.html')
	context = RequestContext(request, {'appuser':user_id,'page':"","plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'sbu_list':filters_dict['sbu_list'],'technology_list':filters_dict['technology_list'],'vendor_list':vendor_list,'syncdate':syncdate })
	return HttpResponse(template.render(context), content_type="text/html")
	
	
@csrf_exempt			
def RM_get_filter_data(request):
	filters_dict={}	
	sbu_list=[]
	technology_list=[]
	vendor_list=[]
	rawmaterial_list=[]
	pipeline=RM_get_filter_pipeline(request)
	change_type=request.POST.get("change_type", "")
	change_type=change_type.encode('utf-8')
	
	if (change_type=='plant'):
		sbu_list=[]
		result_cursor=connection().distinct('SBU',pipeline)
		for r in result_cursor:
			sbu_list.append(r.encode('utf-8'))
		filters_dict['sbu_list']=sbu_list
		
		pipeline['SBU']={'$in':sbu_list}
		technology_list=[]
		result_cursor=connection().distinct('Technology',pipeline)
		for r in result_cursor:
			technology_list.append(r.encode('utf-8'))
		filters_dict['technology_list']=technology_list		

		pipeline['Technology']={'$in':technology_list}
		rawmaterial_list=[]
		result_cursor=connection().distinct('MaterialNumber',pipeline)
		for r in result_cursor:
			rawmaterial_list.append(r.encode('utf-8'))
		filters_dict['rawmaterial_list']=rawmaterial_list
		
		pipeline['MaterialNumber']={'$in':rawmaterial_list}
		pipeline['VendorName']={'$ne':''}
		result_cursor=connection().distinct('VendorName',pipeline)
		vendor_list=[]
		for r in result_cursor:
			vendor_list.append(r.encode('utf-8'))
		filters_dict['vendor_list']=vendor_list
		



	elif (change_type=='sbu'):
		technology_list=[]
		result_cursor=connection().distinct('Technology',pipeline)
		for r in result_cursor:
			technology_list.append(r.encode('utf-8'))
		filters_dict['technology_list']=technology_list		

		pipeline['Technology']={'$in':technology_list}
		rawmaterial_list=[]
		result_cursor=connection().distinct('MaterialNumber',pipeline)
		for r in result_cursor:
			rawmaterial_list.append(r.encode('utf-8'))
		filters_dict['rawmaterial_list']=rawmaterial_list
		
		pipeline['MaterialNumber']={'$in':rawmaterial_list}
		pipeline['VendorName']={'$ne':''}
		result_cursor=connection().distinct('VendorName',pipeline)
		vendor_list=[]
		for r in result_cursor:
			vendor_list.append(r.encode('utf-8'))
		filters_dict['vendor_list']=vendor_list


	elif (change_type=='technology'):
		rawmaterial_list=[]
		result_cursor=connection().distinct('MaterialNumber',pipeline)
		for r in result_cursor:
			rawmaterial_list.append(r.encode('utf-8'))
		filters_dict['rawmaterial_list']=rawmaterial_list
		
		pipeline['MaterialNumber']={'$in':rawmaterial_list}
		pipeline['VendorName']={'$ne':''}
		result_cursor=connection().distinct('VendorName',pipeline)
		vendor_list=[]
		for r in result_cursor:
			vendor_list.append(r.encode('utf-8'))
		filters_dict['vendor_list']=vendor_list


	else:		
		pipeline['VendorName']={'$ne':''}
		result_cursor=connection().distinct('VendorName',pipeline)
		vendor_list=[]
		for r in result_cursor:
			vendor_list.append(r.encode('utf-8'))
		filters_dict['vendor_list']=vendor_list
		
	return HttpResponse(json.dumps(filters_dict), content_type="application/json")
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
#--------------------------------- function for getting pipeline for graphs --------------------------------------#	
	
def RM_get_pipeline(request):
	plant_list=[]	
	date_list=[]
	filter_list=[ {'QualitySegment':{'$ne':''}}]
	rawmaterial_list=[]
	sbu_list=[]
	technology_list=[]
	vendor_list=[]
	filter_pipeline={}
	pipeline=[]
	plant=''
	vendor=''
	rawmaterial=''
	date=''
	sbu=''
	technology=''
	
	data_list=[]
	data_list=request.POST.getlist('plant_list[]') 
	if len(data_list)>1:
		for data in data_list:
			plant_list.append(data)
	else:
		plant=data_list[0]
				
	data_list=[]
	data_list=request.POST.getlist('sbu_list[]')
	if len(data_list)>1:
		for data in data_list:
			sbu_list.append(data)
	else:
		sbu=data_list[0]	
		
	data_list=[]
	data_list=request.POST.getlist('technology_list[]')
	if len(data_list)>1:
		for data in data_list:
			technology_list.append(data)
	else:
		technology=data_list[0]	

	data_list=[]
	data_list=request.POST.getlist('rawmaterial_list[]')
	if len(data_list)>1:
		for data in data_list:
			rawmaterial_list.append(data)
	else:	
		rawmaterial=data_list[0].encode('utf-8')	
		
		

	data_list=[]
	data_list=request.POST.getlist('vendor_list[]')
	if len(data_list)>1:
		for data in data_list:
			vendor_list.append(data)
	else:	
		vedor=data_list[0].encode('utf-8')		
		
		
	data_list=[]
	data_list=request.POST.getlist('date_list[]')
	if len(data_list)>1:
		for data in data_list:
			date_list.append(data)
	else:
		date=data_list[0]	


		
		
	if (len(technology_list)>=2 ):
		filter_list.append({'Technology':{'$in':technology_list } } )
	else:
		if(technology!='all'):
			filter_list.append( {'Technology':technology } )
			
	if (len(sbu_list)>=2 ):
		filter_list.append({'SBU':{'$in':sbu_list } } )
	else:
		if(sbu!='all'):
			filter_list.append( {'SBU':sbu } )

	if (len(plant_list)>=2 ):
		filter_list.append({'SiteName':{'$in':plant_list } } )
	else:
		if(plant!='all'):
			filter_list.append( {'SiteName':plant } )

	if (len(vendor_list)>=2 ):
		filter_list.append( {'VendorName':{'$in':vendor_list } }	)	
	else:
		if(vedor!='all'):
			filter_list.append( {'VendorName':vendor }	)
			
			
	if (len(rawmaterial_list)>=2 ):
		filter_list.append( {'MaterialNumber': {'$in':rawmaterial_list } }	)	
	else:
		if(rawmaterial!='all'):
			filter_list.append( {'MaterialNumber':rawmaterial }	)
			
	
	if (len(date_list)>=2 ):
		filter_list.append( {'InspectionLotCreationDateTime': {'$lt':datetime.strptime(date_list[1], '%Y-%m-%d'),'$gte':datetime.strptime(date_list[0], '%Y-%m-%d') } } )
	else:
		filter_list.append ( {'InspectionLotCreationDateTime':datetime.strptime(date, '%Y-%m-%d')} )

	if(len(filter_list)==1):
			filter_pipeline={'$match':filter_list[0]}
			logging.info('***********************pipeline**********************')
			logging.info(filter_pipeline)
			return filter_pipeline
	if(len(filter_list)>=2):
			filter_pipeline={'$match':{'$and':filter_list}}
			return filter_pipeline

	

	
@csrf_exempt
def RM_Overall_Quality(request):
	result_list=[]
	quality_percentage=''
	logging.info("******************************query for donut***************************************")
	query=[RM_get_pipeline(request),{'$group':{'_id':{'BatchNumber':'$BatchNumber','QualitySegment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$group':{'_id':{'QualitySegment':'$_id.QualitySegment'},'Count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.QualitySegment','q_count':1 }} ] 
	logging.info(query)
	result_cursor=connection().aggregate([RM_get_pipeline(request),{'$group':{'_id':{'BatchNumber':'$BatchNumber','QualitySegment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$group':{'_id':{'QualitySegment':'$_id.QualitySegment'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.QualitySegment','q_count':1 }} ] )
	for result in result_cursor:
		result_dict={}
		if result['q_segment']=='E':
			result_dict['name']='A'
			result_dict['y']=result['q_count']
			result_dict['color']='#00B050'
			result_list.append(result_dict)	
		if result['q_segment']=='G':
			result_dict['name']='B'
			result_dict['y']=result['q_count']
			result_dict['color']='#FFFF00'
			result_list.append(result_dict)	
		if result['q_segment']=='B':
			result_dict['name']='C'
			result_dict['y']=result['q_count']
			result_dict['color']='#F8963F'
			result_list.append(result_dict)	
		if result['q_segment']=='P':
			result_dict['name']='D'
			result_dict['y']=result['q_count']
			result_dict['color']='#FF0000'
			result_list.append(result_dict)
	return HttpResponse(json.dumps( sorted(result_list, key = lambda result_list: (result_list['name']))  ), content_type="application/json")
	
	
	
@csrf_exempt
def RM_Trend_Of_Quality(request):
	result_list=[]
	result_list1=[]
	Excellent_list=[]
	Good_list=[]
	Bad_list=[]
	Poor_list=[]
	quality_list=[]
	x_categories=set()
	x_categories_list=[]
	data_list=[]
	month_list={'1':'JAN','2':'FEB','3':'MAR','4':'APR','5':'MAY','6':'JUN','7':'JUL','8':'AUG','9':'SEP','10':'OCT','11':'NOV','12':'DEC'}
	data_list=request.POST.getlist('graphtype_list[]') 
	graphtype=data_list[0].encode('utf-8')
	if(graphtype=='day'):
		sorted_dict=OrderedDict()
		sorted_dict['_id.year']=1
		sorted_dict['_id.month']=1
		sorted_dict['_id.day']=1
		result_cursor=connection().aggregate([RM_get_pipeline(request),{'$group':{'_id':{'BatchNumber':'$BatchNumber', 'QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}},{ "$group": {  "_id": {"day": { "$dayOfMonth" : "$_id.InspectionLotCreationDateTime" },"month": { "$month" : "$_id.InspectionLotCreationDateTime" }, "year": { "$year" : "$_id.InspectionLotCreationDateTime" },'Quality_Segment':'$_id.QualitySegment'},"Count": { "$sum" : 1 }}      },{"$sort": sorted_dict },{ "$project": { "_id": 0,'name':'$_id.Quality_Segment',  'data': '$Count', 'x_categories': {"$concat": [  {"$substr" : [ "$_id.day", 0, 2]}, "-",{"$substr" : [ "$_id.month", 0, 2]}, "-",{"$substr" : [ "$_id.year", 0, 4]}]}}}])
		for r in result_cursor:
			r['x_categories']=r['x_categories']
			result_list.append(r)
			if r['x_categories'] not in x_categories_list:
				x_categories_list.append(r['x_categories'])


	if(graphtype=='week'):
		result_cursor=connection().aggregate([ RM_get_pipeline(request),{'$group':{'_id':{'BatchNumber':'$BatchNumber', 'QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}},{'$group':{'_id':{'year': { '$year': '$_id.InspectionLotCreationDateTime'},'zweek':{'$week': '$_id.InspectionLotCreationDateTime' },'Quality_Segment':'$_id.QualitySegment'},'q_count':{'$sum':1} } },{ '$sort' : {'_id.year': 1,'_id.zweek':1}} ,{'$project':{'_id':0,'year':'$_id.year','week':'$_id.zweek','name':'$_id.Quality_Segment','data':'$q_count'}} ])
		for r in result_cursor:			
			r['x_categories']="W"+str(r["week"]+1)+""+"'"+""+str(r["year"])[2:4]+""
			result_list.append(r)
			if r['x_categories'] not in x_categories_list:
				x_categories_list.append(r['x_categories'])		
	if(graphtype=='month'):	
		result_cursor=connection().aggregate([ RM_get_pipeline(request),{'$group':{'_id':{'BatchNumber':'$BatchNumber', 'QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}},{'$group':{'_id':{'year': { '$year': '$_id.InspectionLotCreationDateTime'},'month':{'$month': '$_id.InspectionLotCreationDateTime'},'Quality_Segment':'$_id.QualitySegment'},'q_count':{'$sum':1} } },{ '$sort' : {'_id.year': 1,'_id.month':1} } ,{'$project':{'_id':0,'year':'$_id.year','month':'$_id.month','name':'$_id.Quality_Segment','data':'$q_count'}} ])
		for r in result_cursor:
			r['x_categories']=""+str(month_list[str(r["month"])])+""+"'"+""+str(r["year"])[2:4]+""
			result_list.append(r)
			if r['x_categories'] not in x_categories_list:
				x_categories_list.append(r['x_categories'])		


	for x_categorie in x_categories_list:
		Excellent_flag=0
		Good_flag=0
		Bad_flag=0
		Poor_flag=0
		for result in result_list:
			if(x_categorie ==result['x_categories']):
				if(result['name']=='E'):
					Excellent_list.append(result['data'])
					Excellent_flag=1
				elif(result['name']=='G'):
					Good_list.append(result['data'])
					Good_flag=1
				elif(result['name']=='B'):
					Bad_list.append(result['data'])
					Bad_flag=1
				elif(result['name']=='P'):
					Poor_list.append(result['data'])	
					Poor_flag=1
				else:
					print " "
		if(Excellent_flag==0):
			Excellent_list.append(0)
		if(Good_flag==0):
			Good_list.append(0)
		if(Bad_flag==0):
			Bad_list.append(0)
		if(Poor_flag==0):
			Poor_list.append(0)
			
	for i in range(0,len(Excellent_list)):
		total=Excellent_list[i]+Good_list[i]+Bad_list[i]+Poor_list[i]
		if(total==0):
			total=1
		quality=Excellent_list[i]+Good_list[i]
		per=int((quality*100)/total)
		quality_list.append(per)
	series_list=[]
	series_list.append({'name':'D','data':Poor_list,'color':'#FF0000','type': 'column', 'yAxis': 1})
	series_list.append({'name':'C','data':Bad_list,'color':'#F8963F','type': 'column', 'yAxis': 1})
	series_list.append({'name':'B','data':Good_list,'color':'#FFFF00','type': 'column', 'yAxis': 1})
	series_list.append({'name':'A','data':Excellent_list,'color':'#00B050','type': 'column', 'yAxis': 1})
	series_list.append({'name':'QualityScore','data':quality_list,'type': 'spline','color':'#3A539B', 'tooltip': { 'valueSuffix': '%'},'marker':{'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#3A539B'} })
	result_list1.append({'series':series_list,'x_categories':x_categories_list})
	return HttpResponse(json.dumps(result_list1), content_type="application/json")

	
	

@csrf_exempt
def RM_Trend_Of_Quality_Line(request):
	v1=''
	radio_select=request.POST.getlist('radio_select[]')
	v1=radio_select[0]	
	result_cursor=connection().aggregate([ RM_get_pipeline(request), {'$group':{'_id':{'BatchNumber':'$BatchNumber', 'QualitySegment':'$QualitySegment','Line':v1}}}, {'$group':{'_id':{'QualitySegment':'$_id.QualitySegment','Line':'$_id.Line'},'q_count':{'$sum':1} } } ,{'$project':{'_id':0,'x_categorie':'$_id.Line','name':'$_id.QualitySegment','data':'$q_count'}},{ '$sort' : {'x_categorie': 1} } ])
	result_list=[]
	result_list1=[]
	Excellent_list=[]
	Good_list=[]
	Bad_list=[]
	Poor_list=[]
	x_categories=[]
	x_categories_list=[]
	for r in result_cursor:
		result_list.append(r)
		if r['x_categorie'] not in x_categories: 
			x_categories.append(r['x_categorie'])	
	for x_categorie in x_categories:
		x_categories_list.append(x_categorie)
		Excellent_flag=0
		Good_flag=0
		Bad_flag=0
		Poor_flag=0
		for result in result_list:
			if(x_categorie ==result['x_categorie']):
				if(result['name']=='E'):
					Excellent_list.append(result['data'])
					Excellent_flag=1
				elif(result['name']=='G'):
					Good_list.append(result['data'])
					Good_flag=1
				elif(result['name']=='B'):
					Bad_list.append(result['data'])
					Bad_flag=1
				elif(result['name']=='P'):
					Poor_list.append(result['data'])	
					Poor_flag=1
				else:
					print " "
		if(Excellent_flag==0):
			Excellent_list.append(0)
		if(Good_flag==0):
			Good_list.append(0)
		if(Bad_flag==0):
			Bad_list.append(0)
		if(Poor_flag==0):
			Poor_list.append(0)
	series_list=[]
	series_list.append({'name':'D','data':Poor_list,'color':'#FF0000'})
	series_list.append({'name':'C','data':Bad_list,'color':'#F8963F'})
	series_list.append({'name':'B','data':Good_list,'color':'#FFFF00'})
	series_list.append({'name':'A','data':Excellent_list,'color':'#00B050'})	
	result_list1.append({'series':series_list,'x_categories':x_categories_list})
		
	return HttpResponse(json.dumps(result_list1), content_type="application/json")

	
@csrf_exempt
def RM_quality_variation_by_line(request):
	v1=''
	radio_select=request.POST.getlist('radio_select[]')
	v1=radio_select[0]
	bar_dict={}
	result_data_dict={}
	series=[]
	bar_data=[]
	spline_data=[]
	x_categories=[]
	result_cursor=connection().aggregate([ RM_get_pipeline(request),{'$group':{'_id':{'BatchNumber':'$BatchNumber', 'QualitySegment':'$QualitySegment','Line':v1}}},{'$group': {'_id': {'categories':'$_id.Line'},"EG_Count": { "$sum": { '$cond':[{'$or':[{'$eq':[ "$_id.QualitySegment", "E" ]},{'$eq':[ "$_id.QualitySegment", "G" ]}]},1,0]}},"TotCount": { "$sum": 1 }}}, {'$project':{"_id":0,"categories":"$_id.categories","TotalCount":"$TotCount","Quality_Score": { '$cond': [{ '$eq': [ "$EG_Count", 0 ] },0,{ '$multiply':[{'$divide': [ "$EG_Count", "$TotCount" ]} ,100]}]}}},{ '$sort':{'Quality_Score': 1}} ])
	for row in result_cursor:
		x_categories.append(row['categories'])
		spline_data.append(int(row['Quality_Score']))
		bar_data.append(row['TotalCount'])		
	series=[{'name':'ProcessOrders','type':'column','color':'#6bafbd','data':bar_data,'tooltip':{'valueSuffix': ' '}},{'name':'QualityScore','type':'spline','color':'#3A539B','data':spline_data,'tooltip':{'valueSuffix': '%'},'yAxis':1,'marker':{'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#3A539B'} }  ]
	result_data_dict['categories']=x_categories
	result_data_dict['series']=series
	return HttpResponse(json.dumps(result_data_dict), content_type="application/json")
	
	
	
@csrf_exempt
def RM_get_rejected_orders(request):
	page_view=request.POST.get('page_view').encode('utf-8')
	if (page_view=='RM_cpk_view'):
		pipeline={'$match':{'$and':[]}}
		data_list=[]
		plant_list=[]
		product_list=[]
		data_list=request.POST.getlist('product_list[]')
		for data in data_list:
				product_list.append(data)
		if len(data_list)>1:
			pipeline['$match']['$and'].append({'MaterialNumber':{'$in':product_list } })
		else:
			pipeline['$match']['$and'].append({'MaterialNumber':product_list[0]  })
		data_list=[]
		data_list=request.POST.getlist('plant_list[]')
		for data in data_list:
				plant_list.append(data)
		if len(data_list)>1:
			pipeline['$match']['$and'].append({'MaterialNumber':{'$in':plant_list } })
		else:	
			pipeline['$match']['$and'].append({'MaterialNumber':plant_list[0] })
	else:
		pipeline=RM_get_pipeline(request)
		
	pipeline['$match']['$and'].append({'UsageDecision':{'$eq':'R'}})
	result_cursor=connection().aggregate([pipeline,{'$project':{'SiteName':1,'SBU':1,'Technology':1, 'BatchNumber':1 ,'MaterialNumber':1,'VendorName':1,'ExpiryDate':1,'InspectionLotCreationDateTime':1}}])
	rejected_list=[]
	for result in result_cursor:
		l=[]
		l.append(result['SiteName'])
		l.append(result['SBU'])
		l.append(result['Technology'])
		l.append(result['MaterialNumber'])
		l.append(result['BatchNumber'])
		l.append(result['VendorName'])
		l.append(result['InspectionLotCreationDateTime'].strftime('%y-%m-%d'))
		rejected_list.append(l)
	return HttpResponse(json.dumps(rejected_list), content_type="application/json")	
		

@csrf_exempt	
def RM_get_top_pos(request):
	pipeline=RM_get_pipeline(request)
	excel_header=[]
	data_list=[]
	processorder_list=[]
	plantid_list=[]
	data_list=request.POST.getlist('page_view[]')
	page_view=data_list[0].encode('utf-8')	
	type_list=request.POST.getlist('type_list[]')
	type=type_list[0].encode('utf-8')
	data_list=[]
	data_list=request.POST.getlist('segment_list[]')
	segment_list=data_list
	if(len(data_list)>=2):
		pipeline['$match']['$and'].append({'QualitySegment':{'$in':segment_list} })
	else:	
		pipeline['$match']['$and'].append({'QualitySegment':segment_list[0].encode('utf-8')})	
	download_pipeline=pipeline
	if type=='total':
		if page_view=='RM_grid_view':
				batchnumber=request.POST.get('batchnumber')
				logging.info(batchnumber)
				if(batchnumber!='all'):					
					pipeline['$match']['$and'].append({'BatchNumber':batchnumber.encode('utf-8')})
					logging.info(pipeline)
				result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'ProcessOrderList':'$ProcessOrderList','BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}  ,{'$sort':{'avg':1}}])
				result_cursor1=connection().aggregate([pipeline,{'$unwind':'$ProcessOrderList'},{'$project':{'_id':0,'QualitySegment':1,'ProcessOrderList':1,'VendorName':1,'PurchaseOrderNumber':1,'SiteName':1, 'SBU':1, 'Technology':1, 'MaterialNumber':1, 'BatchNumber':1, 'AvgQualityScore':1,'InspectionLotCreationDateTime':1}}  ,{'$sort':{'AvgQualityScore':1}}])
	
		else:
			if (segment_list[0]=='E'or segment_list[0]=='G'):
				result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'ProcessOrderList':'$ProcessOrderList','BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}  ,{'$sort':{'avg':1}}])
				result_cursor1=connection().aggregate([pipeline,{'$unwind':'$ProcessOrderList'},{'$project':{'_id':0,'QualitySegment':1,'ProcessOrderList':1,'VendorName':1,'PurchaseOrderNumber':1,'SiteName':1,  'SBU':1, 'Technology':1, 'MaterialNumber':1, 'BatchNumber':1, 'AvgQualityScore':1,'InspectionLotCreationDateTime':1}}  ,{'$sort':{'AvgQualityScore':1}}])
			else:
				result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'ProcessOrderList':'$ProcessOrderList','BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}  ,{'$sort':{'avg':-1}}])
				result_cursor1=connection().aggregate([ pipeline,{'$unwind':'$ProcessOrderList'},{'$project':{'_id':0,'QualitySegment':1,'ProcessOrderList':1,'VendorName':1,'PurchaseOrderNumber':1,'SiteName':1, 'SBU':1, 'Technology':1, 'MaterialNumber':1, 'BatchNumber':1, 'AvgQualityScore':1,'InspectionLotCreationDateTime':1}}  ,{'$sort':{'AvgQualityScore':-1}}])
	else:
		if (segment_list[0]=='E'or segment_list[0]=='G'):
			result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'ProcessOrderList':'$ProcessOrderList','BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}  ,{'$sort':{'avg':1}},{ '$limit' : 5 } ])
		else:
			result_cursor=connection().aggregate([ pipeline,{'$project':{'_id':0,'ProcessOrderList':'$ProcessOrderList','BatchNumber':'$BatchNumber','Technology':'$Technology','SBU':'$SBU','SiteID':'$SiteID','SiteName':'$SiteName','product':'$MaterialNumber','avg':'$AvgQualityScore','QualitySegment':'$QualitySegment','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}}  ,{'$sort':{'avg':-1}},{ '$limit' : 5 } ])
	po_list=[]
	product_list=[]
	po_avgscr=[]
	po_segment=[]
	po_siteid_list=[]
	po_technology_list=[]
	po_sbu_list=[]
	po_productiondate_list=[]
	temp_dict={}
	top_po=[]
	po_list_dict={}
	color='#F3778B'
	deatils_list=[]
	if (type=='total' ):
		for r in result_cursor1:
			deatil_list=[]
			if(r['QualitySegment']=='E'):
				deatil_list.append('A')
			elif(r['QualitySegment']=='G'):
				deatil_list.append('B')
			elif(r['QualitySegment']=='B'):
				deatil_list.append('C')
			else:
				deatil_list.append('D')
			deatil_list.append(r['SiteName'])
			deatil_list.append(r['SBU'])
			deatil_list.append(r['Technology'])
			deatil_list.append(r['MaterialNumber'])
			deatil_list.append(r['PurchaseOrderNumber'])
			deatil_list.append(r['BatchNumber'])
			deatil_list.append(r['VendorName'])
			deatil_list.append(r['AvgQualityScore'])
			#deatil_list.append(r[''])
			deatil_list.append(r['InspectionLotCreationDateTime'].strftime('%Y-%m-%d'))
			deatils_list.append(deatil_list)		
	for r in result_cursor:
		product_list.append(r['product'])
		po_avgscr.append(r['avg'])
		processorder_list.append(r['BatchNumber'])
		po_list_dict[r['BatchNumber']]=r['ProcessOrderList']
		plantid_list.append(r['SiteName'])
		po_siteid_list.append(r['SiteID'])
		po_technology_list.append(r['Technology'])
		po_sbu_list.append(r['SBU'])	
		po_productiondate_list.append(r['InspectionLotCreationDateTime'].strftime('%Y-%m-%d'))
		if(r['QualitySegment']=='E'):
			po_segment.append('A')
		elif(r['QualitySegment']=='G'):
			po_segment.append('B')
		elif(r['QualitySegment']=='B'):
			po_segment.append('C')
		else:
			po_segment.append('D')	
	temp_dict['po_siteid']=po_siteid_list
	temp_dict['po_technology']=po_technology_list
	temp_dict['po_sbu']=po_sbu_list
	temp_dict['po_productiondate']=po_productiondate_list
	temp_dict['po_list_dict']=po_list_dict
	temp_dict['product_numbers']=product_list
	temp_dict['po_numbers']=processorder_list
	temp_dict['plantid_list']=plantid_list
	temp_dict['po_score']=po_avgscr
	temp_dict['po_segment']=po_segment
	temp_dict['download_list']=deatils_list
	temp_dict['excel_header']=excel_header
	temp_dict['color']='#F3778B'
	top_po.append(temp_dict)
	return HttpResponse(json.dumps(top_po), content_type="application/json")
		

@csrf_exempt
def RM_quality_trend_onclick(request):
	date=''
	date_list=[]
	filter_pipeline={}
	data_list=[]
	pipeline=RM_get_pipeline(request)
	pipeline1=RM_get_pipeline(request)
	data_list=[]
	data_list=request.POST.getlist('segment_list[]')
	segment=data_list[0].encode('utf-8')
	pipeline['$match']['$and'].append({'QualitySegment':segment})
	

#------------------Query for Vendor--------------------#	
	
	
	#result_cursor2=connection().aggregate([ pipeline, {'$group':{'_id':{'LineName':'$VendorName','Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1} } },{'$project':{'_id':0,'name':'$_id.Quality_Segment','data':'$q_count','LineName':'$_id.LineName'}} ])
	result_cursor2=connection().aggregate([ pipeline,{'$group':{'_id':{'LineName':'$VendorName','BatchNumber':'$BatchNumber'} } }, {'$group':{'_id':{'LineName':'$_id.LineName'},'q_count':{'$sum':1} } },{'$project':{'_id':0,'data':'$q_count','LineName':'$_id.LineName'}} ])

#------------------Query for Plant--------------------#	
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_CustComp_QSegment_1
	pipeline1['$match']['$and'].append({'RawMaterialNumber':pipeline1['$match']['$and'][5]['MaterialNumber']})
	pipeline1['$match']['$and'].append({'POCount':{'$gte':30}})
	del pipeline1['$match']['$and'][5]
	logging.info('#------------------Query for finished--------------------#')
	pipeline1['$match']['$and'].append({'RMBatchQualitySegment':segment})
	logging.info(pipeline1)
	result_cursor4=table_cursor.aggregate([  pipeline1, {'$group':{'_id':{'QualitySegment':'$QualitySegment'},'q_count':{'$sum':1} } }, {'$project':{'_id':0,'name':'$_id.QualitySegment','data':'$q_count'}} ])
	
#------------------Query for Date--------------------#	
	data_sort=OrderedDict()
	data_sort["_id.year"]=1
	data_sort["_id.month"]=1
	data_sort["_id.day"]=1	
	logging.info('#------------------Query for Date--------------------#')
	logging.info([ pipeline, {'$group':{'_id':{'Date':'$InspectionLotCreationDateTime'},'q_count':{'$sum':1} } },  {'$project':{'_id':0,'name':'$_id.Date','data':'$q_count'}},{ '$sort':{'name': 1}} ])
	#result_cursor6=table_cursor.aggregate([ pipeline, {'$group':{'_id':{'Date':'$InspectionLotCreationDateTime'},'q_count':{'$sum':1} } },  {'$project':{'_id':0,'name':'$_id.Date','data':'$q_count'}},{ '$sort':{'name': 1}} ])
	result_cursor6=connection().aggregate([pipeline,{'$group':{'_id':{'BatchNumber':'$BatchNumber','InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'} } }, { "$group": {  "_id": { "day": { "$dayOfMonth" : "$_id.InspectionLotCreationDateTime" },"month": { "$month" : "$_id.InspectionLotCreationDateTime" }, "year": { "$year" : "$_id.InspectionLotCreationDateTime" }},"Count": { "$sum" : 1 } } }, {  "$sort": data_sort},{ "$project": {  "_id": 0,  "dateDay": { "$concat": [ {"$substr" : [ "$_id.day", 0, 2]}, "-",{"$substr" : [ "$_id.month", 0, 2]}, "-", {"$substr" : [ "$_id.year", 0, 4]} ] }, "Count": 1}}])	
	q_total_line={}
	q_total_plant={}
	q_total_date={}
	by_line_data=[]
	by_plant_data=[]
	by_date_data=[]
	total_data=[]
#-----------for Donut Chart By Vendor--------------------------------#	
	for r in result_cursor2:
		temp_dict={}
		temp_dict['name']=r['LineName']
		temp_dict['y']=r['data']
		by_line_data.append(temp_dict)
		
#-----------for Donut Chart By Plant------------#		
	segment_mapping={'E':'A(Excellent)','G':'B(Good)','B':'C(Bad)','P':'D(Poor)'}
	color_mapping={'E':'#00B050','G':'#FFFF00','B':'#F8963F','P':'#FF0000'}
	for r in result_cursor4:
		temp_dict={}
		temp_dict['name']=segment_mapping[r['name']]
		temp_dict['color']=color_mapping[r['name']]
		temp_dict['y']=r['data']
		by_plant_data.append(temp_dict)
#-----------for trend chart------------#			
	categories=[]
	data=[]
	temp_dict={}
	series=[]
	for r in result_cursor6:
		data.append(r['Count'])
		categories.append(r['dateDay'])
	temp_dict['name']='Quality Trend'
	temp_dict['data']=data
	temp_dict['color']='#DA484B'
	temp_dict['marker']={'fillColor': '#FFFFFF','lineWidth':1,'lineColor':'#DA484B'} 
	series.append(temp_dict)
	temp_dict={}
	temp_dict['series']=series
	temp_dict['categories']=categories
	by_date_data.append(temp_dict)
	total_data=[{'By_Line':by_line_data,'By_Plant':by_plant_data,'By_date':by_date_data}]
	return HttpResponse(json.dumps(total_data), content_type="application/json")		



@csrf_exempt
def RM_customer_complaints(request):	
	by_plant_data=[]
	pipeline1=RM_get_pipeline(request)
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_CustComp_QSegment_1
	pipeline1['$match']['$and'].append({'RawMaterialNumber':pipeline1['$match']['$and'][5]['MaterialNumber']})
	del pipeline1['$match']['$and'][5]
	pipeline1['$match']['$and'].append({'ProductionDate':pipeline1['$match']['$and'][5]['InspectionLotCreationDateTime']})
	del pipeline1['$match']['$and'][5]
	pipeline1['$match']['$and'].append({'POCount':{'$gte':30}})
	pipeline1['$match']['$and'].append({'RMBatchQualitySegment':{'$ne':''}})
	logging.info('#------------------Query for finished--------------------#')
	logging.info(pipeline1)
	#result_cursor4=table_cursor.aggregate([  pipeline1, {'$group':{'_id':{'QualitySegment':'$QualitySegment'},'q_count':{'$sum':1} } }, {'$project':{'_id':0,'name':'$_id.QualitySegment','data':'$q_count'}} ])
	
	result_cursor4=table_cursor.aggregate([  pipeline1,{'$group':{'_id':{'ProcessOrder':'$ProcessOrder','QualitySegment':'$QualitySegment'},'Count':{'$sum':1}}},{'$group':{'_id':{'QualitySegment':'$_id.QualitySegment'},'Count':{'$sum':1}}},{'$project':{'_id':0,'name':'$_id.QualitySegment','data':'$Count'}} ])
	segment_mapping={'E':'A(Excellent)','G':'B(Good)','B':'C(Bad)','P':'D(Poor)'}
	color_mapping={'E':'#00B050','G':'#FFFF00','B':'#F8963F','P':'#FF0000'}
	for r in result_cursor4:
		temp_dict={}
		temp_dict['name']=segment_mapping[r['name']]
		temp_dict['color']=color_mapping[r['name']]
		temp_dict['y']=r['data']
		by_plant_data.append(temp_dict)
	return HttpResponse(json.dumps(by_plant_data), content_type="application/json")


@csrf_exempt

def RM_customer_complaints_modal_view(request):
	by_plant_data=[]
	pipeline1=RM_get_pipeline(request)
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_CustComp_QSegment_1
	pipeline1['$match']['$and'].append({'RawMaterialNumber':pipeline1['$match']['$and'][5]['MaterialNumber']})
	pipeline1['$match']['$and'].append({'POCount':{'$gte':30}})
	segment_mapping={'A':'E','B':'G','C':'B','D':'P'}
	segment=segment_mapping[request.POST.getlist('defect_list[]')[0].encode()[0]]
	
	del pipeline1['$match']['$and'][5]
	logging.info(segment)
	pipeline1['$match']['$and'].append({'QualitySegment':segment})
	pipeline1['$match']['$and'].append({'RMBatchQualitySegment': {'$ne': ''}})
	logging.info('#------------------Query for finished--------------------#')
	logging.info(pipeline1)
	result_cursor4=table_cursor.aggregate([pipeline1,{'$group':{'_id':{'ProcessOrder':'$ProcessOrder'},'QualityScore' : { '$first': '$QualityScore'},'A_count':{'$sum': { '$cond':[{'$eq':[ "$RMBatchQualitySegment", "E" ]},1,0]}},'B_count':{'$sum': { '$cond':[{'$eq':[ "$RMBatchQualitySegment", "G" ]},1,0]}},'C_count':{'$sum': { '$cond':[{'$eq':[ "$RMBatchQualitySegment", "B" ]},1,0]}},'D_count':{'$sum': { '$cond':[{'$eq':[ "$RMBatchQualitySegment", "P" ]},1,0]}}}},     {'$project':{'_id':0,'ProcessOrder':'$_id.ProcessOrder','QualityScore':'$QualityScore','A_Count':'$A_count','B_Count':'$B_count','C_Count':'$C_count','D_Count':'$D_count'}},{'$sort':{'QualityScore':1}} ])
	for r in result_cursor4:
		by_plant_data.append(r)
	return HttpResponse(json.dumps(by_plant_data), content_type="application/json")


@csrf_exempt
def RM_rawmaterialinfo_view(request):
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_CustComp_QSegment1
	processorder=request.POST.get('processorder').encode('utf-8')
	result_cursor=table_cursor.find({'ProcessOrder':processorder,'RMBatchQualitySegment':{'$ne':''}},{'RawMaterialNumber':1,'RMBatchQualitySegment':1,'SiteID':1})
	segment_maapping={'E':'A(Excellent)','G':'B(Good)','B':'C(Bad)','P':'D(Poor)'}
	rawmaterial_data=[]
	for res in result_cursor:
		rawmaterial_data.append({'RawMaterialNumber':res['RawMaterialNumber'].encode('utf-8'),'Segment':segment_maapping[res['RMBatchQualitySegment']],'SiteID':res['SiteID']})
	return HttpResponse(json.dumps(rawmaterial_data), content_type="application/json")	


	
	
	
def RM_get_threshold_data(request):
	result_list=[]
	plant=str(request.GET.get('plant').encode('utf-8'))
	product_list=request.GET.getlist('product_list[]')
	if(len(product_list)>1):
		product_cond={'$in':product_list}
	else:
		product_cond=product_list[0]
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Config_RMQualityScore_Benchmark_copy
	print ([{'$and':[{'SiteName': plant,'MaterialNumber':product_cond,'ActiveFlag':1}]},{'CreatedDate':0,'_id':0,'ActiveFlag':0}])
	result_cursor=table_cursor.find({'$and':[{'SiteName': plant,'MaterialNumber':product_cond,'ActiveFlag':1}]},{'CreatedDate':0,'_id':0,'ActiveFlag':0})
	for r in result_cursor:
		if(r['UpdatedDate']!=''):
			r['UpdatedDate']=r['UpdatedDate'].strftime('%Y-%m-%d')
		result_list.append(r)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	
	
def RM_get_weight_data(request):
	result_list=[]
	plant=str(request.GET.get('plant').encode('utf-8'))
	product_list=request.GET.getlist('product_list[]')
	if(len(product_list)>1):
		product_cond={'$in':product_list}
	else:
		product_cond=product_list[0]
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Config_RMQualityScore_Weights_copy
	result_cursor=table_cursor.find({'$and':[{'SiteName': plant,'MaterialNumber':product_cond,'ActiveFlag':1}]},{'CreatedDate':0,'_id':0,'ActiveFlag':0})
	for r in result_cursor:
		r['UpdatedDate']=r['UpdatedDate'].strftime('%Y-%m-%d')
		result_list.append(r)
	return HttpResponse(json.dumps(result_list), content_type="application/json")	
	
	
def RM_threshold_update(request):
	check_val=request.GET.get('chk')
	data_dict=ast.literal_eval(check_val)
	
	data_list=[]
	k=''
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Config_RMQualityScore_Benchmark_copy
	for key in data_dict:
		print key
		one_dict=data_dict[key]
		k=key
		SiteID=str(k.split('_')[0])
		print SiteID
		
		MaterialNumber=str(k.split('_')[1])
		print MaterialNumber
		print ([{'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{'CreatedDate':1,'_id':0}])
		r=table_cursor.find_one({'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{'CreatedDate':1,'_id':0})
		one_dict['CreatedDate']=r['CreatedDate']
		one_dict['UpdatedDate']=datetime.now()
		one_dict['ActiveFlag']=1
		one_dict['MaterialNumber']=MaterialNumber
		one_dict['SiteID']=SiteID
		data_list.append(one_dict)
		table_cursor.update( { 'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{ '$set':{'ActiveFlag':0}})
	table_cursor.insert(data_list)
	return HttpResponse(data_list, content_type="application/json")
	
	
def RM_weight_update(request):
	check_val=request.GET.get('chk')
	data_dict=ast.literal_eval(check_val)
	
	data_list=[]
	k=''
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Config_RMQualityScore_Weights_copy
	for key in data_dict:
		print key
		one_dict=data_dict[key]
		k=key
		SiteID=str(k.split('_')[0])
		MaterialNumber=str(k.split('_')[1])
		r=table_cursor.find_one({'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{'CreatedDate':1,'_id':0})
		one_dict['CreatedDate']=r['CreatedDate']
		one_dict['UpdatedDate']=datetime.now()
		one_dict['ActiveFlag']=1
		one_dict['MaterialNumber']=MaterialNumber
		one_dict['SiteID']=SiteID
		#data_list.append(one_dict)
		table_cursor.update( { 'SiteID':SiteID,'MaterialNumber':MaterialNumber,'ActiveFlag':1},{ '$set':{'ActiveFlag':0}})
		table_cursor.insert(one_dict)
	return HttpResponse(data_list, content_type="application/json")

	
def RM_configuration_view(request):
	user_id=request.GET.get('username')
	filters_dict={}
	if(user_id=='SREEKUMA'or user_id=='Merget'):
		template = loader.get_template('qualitypulse/rawmaterial_config.html')
	else:
		template = loader.get_template('qualitypulse/forbidden.html')
	syncdate=''
	result_cursor=connection().aggregate( [ {'$group':{ '_id':{'InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}} },{'$sort':{'_id.InspectionLotCreationDateTime':-1}},{ '$limit' : 1}])
	for r in result_cursor:
		syncdate=r['_id']['InspectionLotCreationDateTime'].strftime('%d %b %y')	
	result_cursor=connection().distinct('SiteName')
	Plant_list=[]
	for r in result_cursor:
		Plant_list.append(r.encode('utf-8'))
	filters_dict['Plant_list']=Plant_list

	product_list=[]
	result_cursor=connection().distinct('MaterialNumber',{'SiteName':Plant_list[0]})
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	filters_dict['product_list']=product_list
	
	context = RequestContext(request, {'syncdate': syncdate,'appuser': user_id,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list']})
	return HttpResponse(template.render(context), content_type="text/html")

def RM_get_filter_data_config(request):
	plant=request.GET.get('plant').encode('utf-8')
	product_list=[]
	result_cursor=connection().distinct('MaterialNumber',{'SiteName':plant})
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	return HttpResponse(json.dumps(product_list), content_type="application/json")	
	
	
	
#----------------------------------------------view for cpk screen----------------------------------#	
def RM_cpk_view(request):	
	user_id=request.GET.get('username')
	filters_dict={}

	template = loader.get_template('qualitypulse/RM_cpk_view.html')

	syncdate=''
	result_cursor=connection().aggregate( [ {'$group':{ '_id':{'InspectionLotCreationDateTime':'$InspectionLotCreationDateTime'}} },{'$sort':{'_id.InspectionLotCreationDateTime':-1}},{ '$limit' : 1}])
	for r in result_cursor:
		syncdate=r['_id']['InspectionLotCreationDateTime'].strftime('%d %b %y')	
	result_cursor=connection().distinct('SiteName')
	Plant_list=[]
	for r in result_cursor:
		Plant_list.append(r.encode('utf-8'))
	filters_dict['Plant_list']=Plant_list

	product_list=[]
	result_cursor=connection().distinct('MaterialNumber',{'SiteName':Plant_list[0]})
	for r in result_cursor:
		product_list.append(r.encode('utf-8'))
	filters_dict['product_list']=product_list

	
	date_list=[]
	result_cursor=connection().aggregate([{'$match':{'InspectionLotCreationDateTime':{'$ne':'NA'}}},{'$project':{ 'year': { '$year': "$InspectionLotCreationDateTime" },'_id':0 }}] )
	print result_cursor
	date_list=[]
	for result in result_cursor:
		if(result['year'] not in date_list):
			date_list.append(result['year'])
	filters_dict['date_list']=date_list	

	context = RequestContext(request, {'syncdate': syncdate,'appuser': user_id,"plant_list":filters_dict['Plant_list'],'product_list':filters_dict['product_list'],'date_list':filters_dict['date_list']})
	return HttpResponse(template.render(context), content_type="text/html")
@csrf_exempt	


def RM_Overall_Quality_cpk(request):
	result_list=[]
	data_list=request.GET.getlist('plant_list[]')
	data_list1=request.GET.getlist('product_list[]')
	result_cursor=connection().aggregate([{'$match':{'$and':[{'MaterialNumber':data_list1[0] },{'SiteName':data_list[0] },{'QualitySegment':{'$ne':''}}]}},{'$group':{'_id':{'Quality_Segment':'$QualitySegment'},'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_segment':'$_id.Quality_Segment','q_count':1 }}])
	segment_mapping={'E':'A','G':'B','B':'C','P':'D'}
	color_mapping={'E':'#00B050','G':'#FFFF00','B':'#F8963F','P':'#FF0000'}
	for r in result_cursor:
		temp_dict={}
		temp_dict['name']=segment_mapping[r['q_segment']]
		temp_dict['color']=color_mapping[r['q_segment']]
		temp_dict['y']=r['q_count']
		result_list.append(temp_dict)
	return HttpResponse(json.dumps(result_list), content_type="application/json")		

	
@csrf_exempt
def RM_cpk_parameter_data(request):	
	parameter_list=[]
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=cursor.Config_RMQualityScore_Weights
	data_list1=request.POST.getlist('product_list[]')
	plant=request.POST.getlist('plant_list[]')
	print data_list1
	print plant
	para_result=table_cursor.find_one({'MaterialNumber':data_list1[0],'SiteName':plant[0]},{'RMWeights':1, '_id':0})			
	for k,v in para_result['RMWeights'].iteritems():
		if v != 0:
			parameter_list.append(k)
			
	total_data={'parameter_list':parameter_list}
	
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
@csrf_exempt
def RM_customer_complaints_cpk(request):
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=conn.henkel.Report_CustComp_QSegment
	cnt=0
	plant=request.GET.get('plant_list').encode('utf-8')
	product=request.GET.get('product_list').encode('utf-8')
	result_list=[]
	query=[{'$match':{'$and':[{'MaterialNumber':product },{'SiteName':plant }]}},{'$group':{'_id':0,'q_count':{'$sum':1}}},{'$project':{'_id':0,'q_count':1 }} ] 
	result_cursor=table_cursor.aggregate(query)
	for result in result_cursor:
		cnt=result['q_count']
	return HttpResponse(json.dumps(cnt), content_type="application/json")
	
	
	
	
@csrf_exempt	
def RM_cpk_onclick_parameter(request):
	data_list=[]
	data_list1=[]
	parameter_list=[]
	parameter_value=[]
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor1=cursor.Report_RMQuality_SegmentwiseCpk
	table_cursor=cursor.Config_RMQualityScore_Weights
	
	data_list=request.GET.getlist('segment_list[]')
	data_list1=request.GET.getlist('product_list[]')
	plant=request.GET.getlist('plant_list[]')
	para_result=table_cursor.find({'MaterialNumber':data_list1[0].encode('utf-8'),'SiteName':plant[0].encode('utf-8')},{'RMWeights':1, '_id':0})
	print {'MaterialNumber':data_list1[0].encode('utf-8'),'SiteName':plant[0].encode('utf-8')}
	for result in para_result:
		for k,v in result['RMWeights'].iteritems():
			if v != 0:
				parameter_list.append(k)
	a=len(parameter_list)
	p=0
	for i in range(0,a):
		string_val='QualitySegment.'+data_list[0]+'.'+parameter_list[i]+'.QCValues.Cpk'
		para_value_result=table_cursor1.find({'MaterialNumber':data_list1[0],'SiteName':plant[0]},{string_val:1})
		for r in para_value_result:
			logging.info(r['QualitySegment'][data_list[0]][parameter_list[i]])
			if r['QualitySegment'][data_list[0]][parameter_list[i]]['QCValues'].has_key('Cpk'):
				x=r['QualitySegment'][data_list[0]][parameter_list[i]]['QCValues']['Cpk']
				logging.info(type(x))
				x=float("{0:.2f}".format(x))
				parameter_value.append(x)
	logging.info(parameter_list)
	logging.info(parameter_value)
	total_data={'parameter_list':parameter_list,'parameter_value':parameter_value}	
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
	
	
	
@csrf_exempt
def RM_cpk_graph(request):
	result_list=[]
	result_list1=[]
	result_list2=[]
	result_val1=[]
	result_val2=[]
	result_val3=[]
	result_val4=[]
	result_val5=[]
	data_list=[]
	data_list1=[]
	date=''
	date_list=[]
	date_dict={}
	para_list=[]
	p=0
	q=0
	r=0
	cpk=0
	cp=0
	sd=0
	s=0
	t=0
	result_value1=[]
	result_value2=[]
	result_value3=[]
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	table_cursor=cursor.src_RawMaterials
	data_list1=request.GET.getlist('product_list[]')
	para_list=request.GET.getlist('parameter_list[]')
	plant=request.GET.getlist('plant_list[]')
	cpk_start_date=request.GET.get('cpk_start_date')
	cpk_end_data=request.GET.get('cpk_end_data')
	
	parameter1='QualityParameter.'+para_list[0]
	parameter='SAPQCCharacterInfo.'+para_list[0]+'.MeanValue'
	
	
	radio_option=request.GET.get('radio_option')
	if radio_option=='on':
		table_cursor1=cursor.Report_RMQuality_QCValues_InOOS
		
		print [{'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'SAPQCCharacterInfo':{'$ne':[]},parameter:{'$ne':''},'UsageDecision':{'$in':['P','R']},'InspectionLotCreationDateTime': { '$lte': datetime.strptime(cpk_start_date, '%Y-%m-%d') ,'$gte': datetime.strptime(cpk_end_data, '%Y-%m-%d') }  },'$orderby':{'InspectionLotCreationDateTime':1}},{'InspectionLotCreationDateTime':1, parameter:1,'_id':0}]
		
		result_cursor=table_cursor.find({'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'SAPQCCharacterInfo':{'$ne':[]},parameter:{'$ne':''},'UsageDecision':{'$in':['P','R']},'InspectionLotCreationDateTime': { '$lte': datetime.strptime(cpk_start_date, '%Y-%m-%d') ,'$gte': datetime.strptime(cpk_end_data, '%Y-%m-%d') }  },'$orderby':{'InspectionLotCreationDateTime':1}},{'InspectionLotCreationDateTime':1, parameter:1,'_id':0})		
	else:
		table_cursor1=cursor.Report_RMQuality_QCValues_ExOOS
		
		print [{'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'SAPQCCharacterInfo':{'$ne':[]},parameter:{'$ne':''},'UsageDecision':{'$in':['P']}, 'InspectionLotCreationDateTime': { '$lte': datetime.strptime(cpk_start_date, '%Y-%m-%d'),'$gte': datetime.strptime(cpk_end_data, '%Y-%m-%d') }   },'$orderby':{'InspectionLotCreationDateTime':1}},{'InspectionLotCreationDateTime':1, parameter:1,'_id':0}]
		result_cursor=table_cursor.find({'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'SAPQCCharacterInfo':{'$ne':[]},parameter:{'$ne':''},'UsageDecision':{'$in':['P']}, 'InspectionLotCreationDateTime': { '$lte': datetime.strptime(cpk_start_date, '%Y-%m-%d'),'$gte': datetime.strptime(cpk_end_data, '%Y-%m-%d') }   },'$orderby':{'InspectionLotCreationDateTime':1}},{'InspectionLotCreationDateTime':1, parameter:1,'_id':0})		
	result_cursor1=table_cursor1.find({'MaterialNumber':data_list1[0],'SiteName':plant[0]},{parameter1:1,'_id':0})	
	for result in result_cursor:
		x=result['InspectionLotCreationDateTime'].strftime("%d %b'%y")
		result_list.append(x)
		print result['SAPQCCharacterInfo']
		for key,value in result['SAPQCCharacterInfo'].iteritems():
			if value.has_key('MeanValue'):
				y=float(value['MeanValue'])
				result_list1.append(y)
		total_data={'result_dict':result_list,'result_list1':result_list1}	
	a=len(result_list1)	
	print result_list1
	for result1 in result_cursor1:
		o=result1['QualityParameter'][para_list[0]]	
		print o
		if(len(result1['QualityParameter'])>0 ):
			result_list2.append(result1['QualityParameter'][para_list[0]])
			p=float("{0:.2f}".format(float (result_list2[0]["USL"])))
			r=float("{0:.2f}".format(result_list2[0]["Mean"]))
			benchmark=float("{0:.2f}".format(result_list2[0]["Benchmark"]))
			t=float("{0:.2f}".format(float(result_list2[0]["LSL"])))
			if(result_list2[0]['Cpk']!=''):
				cpk=float("{0:.2f}".format(result_list2[0]['Cpk']))
			if(result_list2[0]['Cp']!=''):
				cp=float("{0:.2f}".format(result_list2[0]['Cp']))
			if(result_list2[0]['SD']!=''):
				sd=float("{0:.2f}".format(result_list2[0]['SD']))
	for i in range(0,a):
		result_val1.append(p)
		result_val3.append(r)
		result_val4.append(benchmark)
		result_val5.append(t)
	total_data={'result_dict':result_list,'result_list1':result_list1,'result_val1':result_val1,'result_val3':result_val3,'result_val4':result_val4,'result_val5':result_val5,'cpk_value':cpk,'cp_value':cp,'sd_value':sd}
	return HttpResponse(json.dumps(total_data), content_type="application/json")
	
	

@csrf_exempt
def RM_cpk_graph1(request):
	data_list=[]
	data_list1=[]
	date_list=[]
	date=''
	date_dict={}
	para_list=[]
	year_list=[]
	conn=pymongo.MongoClient('henkelpilot.westeurope.cloudapp.azure.com', 27017)
	cursor=conn.henkel
	cursor.authenticate('henkelUser','QpQc#123')
	
	data_list1=request.GET.getlist('product_list[]')
	para_list=request.GET.getlist('parameter_list[]')
	plant=request.GET.getlist('plant_list[]')
	data_list=request.GET.getlist('year_list[]')
	radio_option=request.GET.get('radio_option')
	for data in data_list:
		year_list.append(int(data))
	
	if(len(year_list)>1):
		year={'$in':year_list}
	else:
		year=year_list[0]
	
	time_scale=request.GET.getlist('time_scale[]')
	if(time_scale[0]=='Month'):
		if radio_option=='on':
			table_cursor=cursor.Report_RMQuality_MonthlyQCValues_InOOS
		else:
			table_cursor=cursor.Report_RMQuality_MonthlyQCValues_ExOOS		
		column_name='Month'
	elif(time_scale[0]=='Quarter'):
		if radio_option=='on':
			table_cursor=cursor.Report_RMQuality_QuarterlyQCValues_InOOS
		else:
			table_cursor=cursor.Report_RMQuality_QuarterlyQCValues_ExOOS
		column_name='Quarter'
	elif(time_scale[0]=='Half'):
		if radio_option=='on':
			table_cursor=cursor.Report_RMQuality_HalfYearlyQCValues_InOOS
		else:
			table_cursor=cursor.Report_RMQuality_HalfYearlyQCValues_ExOOS
		column_name='Half'
	else:
		if radio_option=='on':
			table_cursor=cursor.Report_RMQuality_YearlyQCValues_InOOS
		else:
			table_cursor=cursor.Report_RMQuality_YearlyQCValues_ExOOS
		column_name='Year'
	
	parameter='Output.'+para_list[0]+'.Cpk'
	parameter1='Output.'+para_list[0]+'.ProcessSD'
	if (column_name=='Quarter'):
		sorted_dict=OrderedDict()
		sorted_dict['Year']=1
		sorted_dict['Quarter']=1
		result_cpk=table_cursor.find({'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'Output':{'$ne':{} },parameter:{'$ne':{}},'Year':year},'$orderby':sorted_dict},{'MaterialNumber':1,'Year':1,column_name:1,parameter:1,parameter1:1,'_id':0})
		logging.info( "*********************** Query for cpk graph1        ***********************************")
		logging.info({'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'Output':{'$ne':{} },parameter:{'$ne':{}},'Year':year},'$orderby':sorted_dict},{'MaterialNumber':1,'Year':1,column_name:1,parameter:1,parameter1:1,'_id':0})

	else:
		sorted_dict=OrderedDict()
		sorted_dict['Year']=1
		sorted_dict[column_name]=1
		logging.info( "*********************** Query for cpk graph1        ***********************************")
		result_cpk=table_cursor.find({'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'Output':{'$ne':{} },parameter:{'$ne':{}},'Year':year},'$orderby':sorted_dict},{'MaterialNumber':1,'Year':1,column_name:1,parameter:1,parameter1:1,'_id':0})
		logging.info({'$query':{'MaterialNumber':data_list1[0],'SiteName':plant[0],'Output':{'$ne':{} },parameter:{'$ne':{}},'Year':year},'$orderby':sorted_dict},{'MaterialNumber':1,'Year':1,column_name:1,parameter:1,parameter1:1,'_id':0})
	result_list1=[]
	result_list2=[]
	result_list3=[]
	arr=['a','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	for result in result_cpk:
		if(column_name=='Year'):
			x=str(result[column_name])
		else:
			if(column_name=='Month'):
				x=str(arr[result[column_name]])+"'"+str(result['Year'])[2:]
			else:
				x=str(result[column_name])+"'"+str(result['Year'])[2:]
		z=result['Output'][para_list[0]]['Cpk']
		u=result['Output'][para_list[0]]['ProcessSD']
		if z == '':
			z = None
		else:
			z=float("{0:.2f}".format(z))
			
		if u == '':
			u = None
		else:
			u=float("{0:.2f}".format(u))
			

		result_list1.append(x)
		result_list2.append(z)
		result_list3.append(u)
		
	total_data ={'result_xaxis':result_list1,'result_cpk':result_list2,'result_sd':result_list3}
	
	return HttpResponse(json.dumps(total_data), content_type="application/json")